/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.utils;

/**
 * Represents an Interval. Makes use of the Interval Tree implementation by Kevin Dolan.
 *
 * @author josef
 */
public class Interval<Type> implements Comparable<Interval<Type>> {

  private double start;
  private double end;
  private Type data;

  public Interval(double start, double end, Type data) {
    this.start = start;
    this.end = end;
    this.data = data;
  }

  public double getStart() {
    return start;
  }

  public void setStart(double start) {
    this.start = start;
  }

  public double getEnd() {
    return end;
  }

  public void setEnd(double end) {
    this.end = end;
  }

  public Type getData() {
    return data;
  }

  public void setData(Type data) {
    this.data = data;
  }

  /**
   * @param time
   * @return	true if this interval contains time (inclusive)
   */
  public boolean contains(double time) {
    return time <= end && time >= start;
  }

  /**
   * @param other
   * @return	return true if this interval intersects other
   */
  public boolean intersects(Interval<?> other) {
    return other.getEnd() > start && other.getStart() < end;
  }

  /**
   * Return -1 if this interval's start time is less than the other, 1 if greater
   * In the event of a tie, -1 if this interval's end time is less than the other, 1 if greater, 0 if same
   * @param other
   * @return 1 or -1
   */
  public int compareTo(Interval<Type> other) {
    if(start < other.getStart())
      return -1;
    else if(start > other.getStart())
      return 1;
    else if(end < other.getEnd())
      return -1;
    else if(end > other.getEnd())
      return 1;
    else
      return 0;
  }

}