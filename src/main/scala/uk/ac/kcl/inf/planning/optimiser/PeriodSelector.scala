/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.optimiser

import uk.ac.kcl.inf.planning.solver.Problem
import uk.ac.kcl.inf.planning.validator.{PlanTimeline, PlanTimepoint, GroundedPlan}

import scala.util.Random

/**
 * Created by Josef on 06/01/2015.
 */
trait PeriodSelector {
  def selectPeriod(problem : Problem, plan : GroundedPlan, excludedPeriods : Set[Int]) : Option[Int]
}

object EarliestViolationPeriodSelector extends PeriodSelector
{
  private def findNonExcludedPeriod(timeline : PlanTimeline, from : Int = 0, excludedPeriods : Set[Int] = Set[Int]()) : Option[Int] = {
    val timepoints = timeline.futureTimeline.toList
    val period = timepoints.indexWhere(_.cumulativeViolations.nonEmpty, from)
    if (period == -1)
    {
      if (timeline.violationCount > 0)
        Some(timepoints.size - 1) //the goals are the only violations, return the period after the last timepoint
      else
        None //no violations
    }
    else if (excludedPeriods.contains(period))
    {
       findNonExcludedPeriod(timeline, period + 1, excludedPeriods)
    }
    else
    {
      Some(period)
    }
  }

  override def selectPeriod(problem : Problem, plan: GroundedPlan, excludedPeriods : Set[Int] = Set[Int]()): Option[Int] = {
    findNonExcludedPeriod(plan.timeline)
  }
}

object LatestViolationPeriodSelector extends PeriodSelector
{
  private def findNonExcludedPeriod(timeline : PlanTimeline, from : Int = 0, excludedPeriods : Set[Int] = Set[Int]()) : Option[Int] = {
    val timepoints = timeline.futureTimeline.toList
    val period = timepoints.lastIndexWhere(_.cumulativeViolations.nonEmpty, from)
    if (period == -1)
    {
      if (timeline.violationCount > 0)
        Some(timepoints.size - 1) //the goals are the only violations, return the period after the last timepoint
      else
        None //no violations
    }
    else if (excludedPeriods.contains(period))
    {
      findNonExcludedPeriod(timeline, period - 1, excludedPeriods)
    }
    else
    {
      Some(period)
    }
  }

  override def selectPeriod(problem : Problem, plan: GroundedPlan, excludedPeriods : Set[Int] = Set[Int]()): Option[Int] = {
    findNonExcludedPeriod(plan.timeline)
  }
}

object RandomPeriodSelector extends PeriodSelector
{
  val random = new Random()

  private def findNonExcludedPeriod(timeline : PlanTimeline, excludedPeriods : Set[Int] = Set[Int]()) : Option[Int] = {

    val period = random.nextInt(timeline.futureTimeline.size)

    if (excludedPeriods.contains(period))
    {
      findNonExcludedPeriod(timeline, excludedPeriods)
    }
    else
    {
      Some(period)
    }
  }

  override def selectPeriod(problem : Problem, plan: GroundedPlan, excludedPeriods : Set[Int] = Set[Int]()): Option[Int] = {
    findNonExcludedPeriod(plan.timeline)
  }
}
