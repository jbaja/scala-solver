/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.temporal

import uk.ac.kcl.inf.planning.solver.expressions.{Expression, Condition}
import uk.ac.kcl.inf.planning.solver.{SolverSettings, Problem, State, AtomicFormula}
import uk.ac.kcl.inf.planning.solver.expressions.numeric.{NumericConstraint, LinearEffect, LinearCondition}

/**
 *
 * Date: 21/01/15
 * Time: 19:02 
 * @author josef
 */
trait TemporalAction {

  def applyToTemporalState(temporalState : TemporalState) : Option[TemporalState]

  def applyToRelaxedState(relaxedState : RelaxedState, cumulativeRelaxedState : RelaxedState, elapsedTime : Double) : RelaxedState

  def isApplicableRelaxed(relaxedState : RelaxedState) : Boolean

  def getLinearPreconditions(timeDependentVariables : Set[AtomicFormula], state : State) : Set[LinearCondition]

  def getLinearEffects(timeDependentVariables : Set[AtomicFormula], state : State) : List[LinearEffect]

  def positiveEffects : Set[AtomicFormula]
  def negativeEffects : Set[AtomicFormula]

  def positivePropositionalPreconditions : Set[AtomicFormula]
  def negativePropositionalPreconditions: Set[AtomicFormula]

  def numericEffects : Set[AtomicFormula]

  def precondition : Condition

  def invariant : Condition

  def getTemporalConstraints(index : Int, temporalState : TemporalState)   = {
    //todo: do not add this if index-1 is the start of this action, use lower bound instead (using EPSILON as the minimum)
    val newActionTemporalConstraints = if (index > 0)
      temporalState.temporalConstraints + IntervalTemporalConstraint(index - 1, index, NumericConstraint.GEQ, SolverSettings.settings.EPSILON) //at least EPSILON apart
    else
      temporalState.temporalConstraints + AbsoluteTemporalConstraint(index, NumericConstraint.GEQ, 0.0)

    temporalState.timedActions.headOption match {
      case Some(timedAction) => newActionTemporalConstraints + AbsoluteTemporalConstraint(index, NumericConstraint.LES, timedAction.at)

      case None => newActionTemporalConstraints
    }
  }
}
