/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.validator

import uk.ac.kcl.inf.planning.solver.expressions._
import uk.ac.kcl.inf.planning.solver.expressions.temporal.TimeSpecifier
import TimeSpecifier.TimeSpecifier
import uk.ac.kcl.inf.planning.solver.State
import uk.ac.kcl.inf.planning.solver.DurativeAction
import uk.ac.kcl.inf.planning.solver.AtomicFormula
import uk.ac.kcl.inf.planning.solver.Action

/**
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 */
trait TimedApplicable {
  def at : Double
  def duration : Double

  def precondition: Set[Condition]

  def violations(state: State) : Set[Condition] = precondition.foldLeft(Set[Condition]())(
    (violations, condition) => violations ++ condition.violations(state))

  def applyToState(state: State) : State
}

case class TimedApplicableAction(at : Double, action : Action) extends TimedApplicable {
  override def toString: String = at + ": " + action

  override def applyToState(state: State): State = action.applyToState(state)

  override def duration: Double = 0.0

  override lazy val precondition: Set[Condition] = Set(action.precondition)
}

case class TimedApplicableDurativeAction(at : Double,
                                         durativeAction : DurativeAction,
                                         when : TimeSpecifier,
                                         parent : Option[TimedApplicableDurativeAction] = None)
                                         extends TimedApplicable {

  override def toString: String = at + ": " + durativeAction + " " + when + (when match {
    case TimeSpecifier.AT_START => " [" + durativeAction.duration.get + "]"
    case _ => ""
  })

  def toPlanString : String = at + ": " + durativeAction + " " + (when match {
    case TimeSpecifier.AT_START => " [" + durativeAction.duration.get + "]"
    case _ => ""
  })

  override def applyToState(state: State): State = durativeAction.applyToState(state, when)

  lazy val duration: Double = durativeAction.duration.getOrElse(0.0)

  override lazy val precondition: Set[Condition] = durativeAction.conditionsAt(when)

  override def violations(state: State) : Set[Condition] = {
    val violations = super.violations(state)

    //todo: cater for when the duration expression should be calculated AT END
    if (when.equals(TimeSpecifier.AT_START) && (!durativeAction.durationValidAtState(state))) {
      violations + durativeAction.durationCondition
    }
    else {
      violations
    }
  }
}

case class TimedInitialLiteral(at : Double, expression : Expression) extends TimedApplicable {
  lazy val effects : (Set[AtomicFormula], Set[AtomicFormula]) = {
    expression match {
      case modalEffect : Modal => modalEffect.atoms
      case _ => (Set[AtomicFormula](), Set[AtomicFormula]())
    }
  }

  lazy val positiveEffects  = effects._1
  lazy val negativeEffects = effects._2

  override def toString: String = at + ": " + expression + " [TIL]"

  override def applyToState(state: State): State = State(state.modalFacts -- negativeEffects ++ positiveEffects, state.numericFluents)

  override def duration: Double = 0.0

  override lazy val precondition: Set[Condition] = Set[Condition]()
}

case class TimedInitialFluent(at : Double, atomicFormula : AtomicFormula, value : Double) extends TimedApplicable {
  override def toString: String = at + ": (= " + atomicFormula + " " + value + ") [TIF]"

  override def applyToState(state: State): State = State(state.modalFacts, state.numericFluents.updated(atomicFormula, value) )

  override def duration: Double = 0.0

  override lazy val precondition: Set[Condition] = Set[Condition]()
}

object TimedInvariants {
  def violationCount(state: State, invariants : Set[Condition]) : Int  = {
    invariants.foldLeft(0)((count, condition) => count + condition.violationCount(state))
  }

  def violations(state : State, invariants : Set[Condition]) : Set[Condition] = {
    invariants.foldLeft(Set[Condition]())((violations, condition) => violations ++ condition.violations(state))
  }
}
