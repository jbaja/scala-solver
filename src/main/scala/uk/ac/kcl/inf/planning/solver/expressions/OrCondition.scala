/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.State
import uk.ac.kcl.inf.planning.solver.GroundedTerm
import uk.ac.kcl.inf.planning.solver.LiftedTerm
import uk.ac.kcl.inf.planning.solver.expressions.numeric.ExternalEvaluator
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState

/**
 *
 * Date: 10/02/14
 * Time: 18:13 
 * @author josef
 */
case class OrCondition(expressions : List[Expression]) extends Expression.Aux[OrCondition] with Condition
{
  override lazy val isGrounded = expressions.forall(exp => exp.isGrounded)

  /**
   * An OR expression is satisfied if at least one of the sub expressions are satisfied
   * @param state The list of known true facts
   * @return true if the facts satisfy the expression, false otherwise.
   */
  override def satisfiedBy(state : State, context: Map[Expression, Double] = Map[Expression, Double]()) = (expressions collect {
    case condition : Condition => condition
  }).exists(condition => condition.satisfiedBy(state, context))

  override def relaxedSatisfiedBy(relaxedState: RelaxedState): Boolean = (expressions collect {
    case condition : Condition => condition
  }).exists(p => p.relaxedSatisfiedBy(relaxedState))


  override def violationCount(state: State): Int = subConditions.foldLeft(0)((count, cond) => {
    val violCount = cond.violationCount(state)
    if (count <= violCount) count else violCount
  })

  lazy val subConditions = expressions collect {
    case condition : Condition => condition
  }
  
  override def violations(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): Set[Condition] =
    if (satisfiedBy(state, context))
      Set[Condition]()
    else
      subConditions.foldLeft(Set[Condition]())((violatedConditions, condition) => violatedConditions ++ condition.violations(state))

  override def substitute(parameter: LiftedTerm, argument: GroundedTerm) : OrCondition =
    OrCondition(expressions.map(exp => exp.substitute(parameter, argument)))

  override def typify(typedParameter: Term) : OrCondition =
    OrCondition(expressions.map(exp => exp.typify(typedParameter)))

  def updateTypeHierarchy(termType: TermType): OrCondition =
    OrCondition(expressions.map(exp => exp.updateTypeHierarchy(termType)))

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): OrCondition =
    copy(expressions = expressions.map(exp => exp.hookExternalEvaluator(evaluators)))


  override def toString =
    expressions.foldLeft("OR (")((str, exp) => str + " " + exp) + ")"

  override def invert: Expression with Condition = AndCondition(subConditions.map(_.invert))

  override def equals(obj: scala.Any): Boolean = obj.getClass().equals(this.getClass()) &&
    (obj.asInstanceOf[this.type].subConditions.forall(this.subConditions.contains(_))) &&
    (this.subConditions.forall(obj.asInstanceOf[this.type].subConditions.contains(_)))


  override def isClause: Boolean = {
    expressions.forall {
      case af: AtomicFormula => true
      case nc: NumericCondition => true

      case NotExpression(af) => af match {
        case af: AtomicFormula => true
        case nc: NumericCondition => true
        case _ => false
      }

      case _ => false
    }
  }

  override val atomicFormulas : Set[AtomicFormula] = {
    subConditions.foldLeft(Set[AtomicFormula]())((accSet, condition) => {
      accSet ++ condition.atomicFormulas
    })
  }
}
