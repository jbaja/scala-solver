/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.temporal

import java.io.FileWriter

import uk.ac.kcl.inf.planning.solver.{SolverSettings, GoalUnreachableException}
import uk.ac.kcl.inf.planning.solver.expressions.temporal.{ContinuousNumericEffect, TimeSpecifier}
import uk.ac.kcl.inf.planning.solver.expressions.{NotExpression, ComparisonCondition, ModalExpression, Condition}

import scala.annotation.tailrec
import scala.collection.immutable.TreeMap

/**
 * Implementation of the Temporal Relaxed Planning Graph (based on COLIN).
 * Created by Josef on 24/01/2015.
 * @author Josef Bajada
 */
class TemporalRPG(temporalState : TemporalState) {

  val initialRelaxedState = RelaxedState.fromTemporalState(temporalState)
  val runningEndSnapActionTimes: Map[TemporalAction, Double] = TemporalRPG.getScheduledActions(temporalState)

  val outstandingGoals: Set[Condition] = temporalState.problem.goalCondition.atomicConditions.filterNot(_.relaxedSatisfiedBy(initialRelaxedState))

  /**
   * The first layer, representing the current state (together with the applicable actions)
   */
  val initialLayer = TrpgLayer(temporalState.problem, initialRelaxedState, outstandingGoals, runningEndSnapActionTimes.keySet, runningEndSnapActionTimes, temporalState.problem.groundedActions, temporalState.problem.groundedDurativeStartActions)

  /**
   * The TRPG layers. Note that these are in reverse order, last layer is the head of the list (for efficiency reasons).
   */
  val layers: List[TrpgLayer] = buildRPGLayers()

  /**
   * The analysis information of the constructed TRPG.
   */
  lazy val info : TrpgInfo = analyse(layers.head)

  /**
   * The helpful actions, corresponding to all the achiever actions in the first action layer.
   */
  lazy val helpfulActions = info.layerActions(0) ++ temporalState.endSnapActions
  lazy val relaxedPlan = info.relaxedPlan.distinct
  lazy val heuristic : Int = relaxedPlan.size

  @tailrec
  private def buildRPGLayers(accLayers: List[TrpgLayer] = List[TrpgLayer](initialLayer)): List[TrpgLayer] = {
    accLayers.head.traverse match {
      case Some(newLayer) =>
        buildRPGLayers(newLayer :: accLayers)

      case None => accLayers
    }
  }

  @tailrec
  private def analyse(layer: TrpgLayer,
                      trpgInfo : TrpgInfo = TrpgInfo(outstandingGoals.map((_, Integer.MAX_VALUE)).toMap)) : TrpgInfo = {

    val satisfiedGoals = trpgInfo.goalLayers.keys.filter(_.relaxedSatisfiedBy(layer.newRelaxedState))
    val (oldSatisfied, nowSatisfied) = satisfiedGoals.partition(_.relaxedSatisfiedBy(layer.relaxedState))

    val achieverGoalActions : Map[Condition, Set[TemporalAction]] = nowSatisfied.map(goal =>
      (goal, getAchieverActions(layer, goal))).toMap

    //////////////////////////////////////////////
    //check the achiever actions
    val achieverActions = achieverGoalActions.values.flatten.toSet
    val achieverPreconditions = achieverActions.flatMap(_.precondition.atomicConditions) //these need to go as goals to the preceeding layer

    /////////////////////////////////////////////////////////////////////////////////
    //check the invariants of the achiever actions - need to go in current layer
    val achieverInvariants = achieverActions.flatMap(_.invariant.atomicConditions)
    val (prevInv, nowInv) = achieverInvariants.partition(_.relaxedSatisfiedBy(layer.relaxedState))
    val achieverInvGoalActions : Map[Condition, Set[TemporalAction]] = nowInv.map(goal =>
      (goal, getAchieverActions(layer, goal))).toMap

    val achieverInvActions = achieverInvGoalActions.values.flatten.toSet
    val achieverInvPreconditions = achieverInvActions.flatMap(_.precondition.atomicConditions) //these need to go as goals to the preceeding layer

    /////////////////////////////////////////////////////////////////////////////////////////////////
    //check the continuous effects (for implicit intermediate goals) - need to go in current layer
    val achieverGoalContinuousEffects : Map[Condition, Set[ContinuousNumericEffect]] = nowSatisfied.map(goal =>
      (goal, getAchieverContEffects(layer, goal))).toMap

    val achieverContEffects = achieverGoalContinuousEffects.values.flatten.toSet

    //todo: rvalue of discrete effects need to feature here too!  revisit this and make sure it makes sense

    val implicitNumericGoals = achieverContEffects.flatMap(continuousEffect => continuousEffect.getRelaxedEffectPreconditions(layer.discreteNewRelaxedState, layer.elapsed))
    val implicitInitiators = achieverContEffects.flatMap(continuousEffect => getContEffectInitiators(layer, continuousEffect))
    val implicitIntermediateGoals = implicitNumericGoals ++ implicitInitiators

    val (prevImplicit, nowImplicit) = implicitIntermediateGoals.partition(_.relaxedSatisfiedBy(layer.relaxedState))
    val achieverImplicitGoalActions : Map[Condition, Set[TemporalAction]] = nowImplicit.map(goal =>
      (goal, getAchieverActions(layer, goal))).toMap

    val achieverImplicitActions = achieverImplicitGoalActions.values.flatten.toSet
    val achieverImplicitPreconditions: Set[Condition] = achieverImplicitActions.flatMap(_.precondition.atomicConditions) //these need to go as goals to the preceeding layer

    //combine all achievers of this layer (which need to be in the relaxed plan)
    val allAchieverActions = achieverImplicitActions.toList ++ achieverInvActions ++ achieverActions

    //update the goal layers map, with the layer number (which will be smaller) if they are achieved in this layer
    val satisfiedGoalLayers: Map[Condition, Int] = trpgInfo.goalLayers ++ satisfiedGoals.map(_ -> layer.index)

    //add the new discovered goals, with the current layer index (they will be updated to smaller ones in the next iteration)
    val newGoalLayers = satisfiedGoalLayers ++  //the previous goals
                            achieverPreconditions.map(_ -> layer.index) ++ //preconditions of the new achievers
                            achieverInvariants.map(_ -> layer.index) ++  //invariant achievers
                            achieverInvPreconditions.map(_ -> layer.index) ++ //preconditions of the invariant achievers
                            implicitIntermediateGoals.map(_ -> layer.index) ++ //implicit intermediate goals for numeric effects
                            achieverImplicitPreconditions.map(_ -> layer.index) //preconditions of the implicit intermediate goal achievers

    val newLayerActions = trpgInfo.layerActions + (layer.index -> allAchieverActions)

    val newRelaxedPlan = allAchieverActions ++ trpgInfo.relaxedPlan

    layer.preceedingLayer match {
      case Some(preceedingLayer) => analyse(preceedingLayer, TrpgInfo(newGoalLayers, newLayerActions, newRelaxedPlan))

      case None => TrpgInfo(newGoalLayers, newLayerActions, newRelaxedPlan)
    }
  }


  @tailrec
  private def constructRelaxedPlan(relaxedPlan : List[TemporalAction],
                                   goalActions : List[(Condition, Set[TemporalAction])])
                                    : List[TemporalAction] = {
    //todo: only include start actions whose end actions were included already
    goalActions match {
      case (goal, actions) :: tail =>
        if (actions.nonEmpty) {
          constructRelaxedPlan(actions.head :: relaxedPlan, tail)
        }
        else
        {
          println("***** Empty actions for goal " + goal)
          constructRelaxedPlan(relaxedPlan, tail)
        }

      case Nil => relaxedPlan
    }
  }

  private def getAchieverActions(layer : TrpgLayer, goal : Condition): Set[TemporalAction] = {
    goal match {
      case modal: ModalExpression =>
        layer.actions.find(action => action.positiveEffects.contains (modal.predicate)).toSet

      case not: NotExpression =>
        layer.actions.find(action => action.negativeEffects.contains(not.modalExpression.predicate)).toSet

      case numeric: ComparisonCondition =>
         layer.actions.find (action => {
          val newRelaxedState = action.applyToRelaxedState(layer.relaxedState, layer.relaxedState, layer.elapsed)
          goal.relaxedSatisfiedBy(newRelaxedState)
        }).toSet
    }
  }

  private def getContEffectInitiators(layer : TrpgLayer, continuousEffect : ContinuousNumericEffect) : Set[Condition] = {
     (layer.actions collect {
        case snapAction : SnapAction => snapAction
      }).filter(_.when == TimeSpecifier.AT_START)
        .find(_.durativeAction.continuousEffects.contains(continuousEffect))
        .map(sa => ModalExpression(sa.durativeAction.dummyProposition))
        .toSet
  }


  private def getAchieverContEffects(layer: TrpgLayer, goal : Condition) : Set[ContinuousNumericEffect] = {
    //todo: should this separate the newRelaxedState after the discrete effect from the newRelaxedState after the continuous effect?
    goal match {
      case numeric : ComparisonCondition =>
        layer.newRelaxedState.continuousEffects.find(effect => {
          val newRelaxedState = effect.relaxedEffect(layer.discreteNewRelaxedState, layer.discreteNewRelaxedState, layer.elapsed)
          goal.relaxedSatisfiedBy(newRelaxedState)
        }).toSet

      case _ => Set[ContinuousNumericEffect]()
    }
  }


  lazy val debugString : String = {
    val s = new StringBuilder("\n\n**** TRPG H Value: %d\n".format(heuristic))
    layers.reverse.foreach(layer => s ++= layer.debugString)
    s ++= "\nHelpful Actions:\n"
    helpfulActions.foreach(helpfulAction => s ++= "%s\n".format(helpfulAction))
    s ++= "\nRelaxed Plan:\n"
    relaxedPlan.foreach(action => s ++= "%s\n".format(action))
    s ++= "\n**********************\n\n"

    s.toString()
  }
}

object TemporalRPG {
  var index = 0

  def applyActions(actions : Set[TemporalAction], relaxedState: RelaxedState, elapsed : Double) : RelaxedState =  {
    actions.foldLeft(relaxedState)((accRelaxedState, action) => {
      action.applyToRelaxedState(relaxedState, accRelaxedState, elapsed)
    })
  }

  /**
   * Constructs a relaxed planning graph to test the reachability of this state to the goal.
   * @param temporalState - the temporal state being analysed
   * @return an option with a temporal state (including the TRPG for it), or None if cannot reach goal.
   */
  def reachabilityAnalysis(temporalState : TemporalState) : Option[TemporalRPG] = {
    try {
      Some(new TemporalRPG(temporalState))
    }
    catch {
      case ex : GoalUnreachableException => None
    }
  }

  def getTimedActionTimes(temporalState : TemporalState) : Map[TemporalAction, Double] = {
    if (temporalState.timedActions.isEmpty) {
      Map[TemporalAction, Double]()
    }
    else {
      val initTime = temporalState.timedActions.head.at
      temporalState.timedActions.map(timedAction => (timedAction, timedAction.at - initTime)).toMap
    }
  }

  def getEndSnapActionTimes(temporalState : TemporalState) : Map[TemporalAction, Double] = {
    if (temporalState.runningActions.isEmpty) {
      Map[TemporalAction, Double]()
    }
    else {
      val absoluteEndActionTimes  = temporalState.runningActions
                                                 .map(runningAction =>
                                                        (runningAction.endAction, runningAction.lbDuration.getOrElse(SolverSettings.settings.EPSILON)))
                                                 .sortBy(_._2)
      val initTime = absoluteEndActionTimes.head._2
      absoluteEndActionTimes.map(endSnapActionTime => (endSnapActionTime._1, endSnapActionTime._2 - initTime)).toMap
    }
  }

  def getScheduledActions(temporalState : TemporalState) : Map[TemporalAction, Double] = {
    if (temporalState.timedActions.isEmpty) {
      getEndSnapActionTimes(temporalState)
    }
    else if (temporalState.runningActions.isEmpty) {
      getTimedActionTimes(temporalState)
    }
    else {
      //todo: check that this logic makes sense
      val absoluteEndActionTimes  = temporalState.runningActions
                                                  .map(runningAction =>
                                                    (runningAction.endAction, runningAction.lbDuration.getOrElse(SolverSettings.settings.EPSILON)))
                                                  .sortBy(_._2)

      val timedActionInitTime = temporalState.schedule.map(planTimestamps =>  temporalState.timedActions.head.at - planTimestamps.last)
                                                      .getOrElse(temporalState.timedActions.head.at)

      val initTime = Math.min(absoluteEndActionTimes.head._2, timedActionInitTime)
      (temporalState.timedActions.map(timedAction => (timedAction, timedAction.at - initTime)) ++
        absoluteEndActionTimes.map(endSnapActionTime => (endSnapActionTime._1, endSnapActionTime._2 - initTime))).toMap
    }
  }

  def saveDebugInfo(action: TemporalAction, trpgOpt: Option[TemporalRPG]): Unit =
  {
    trpgOpt match {
      case Some(trpg) =>

        val fw = new FileWriter("debug/trpg." + index + ".txt", false)
        try {
          fw.write("Applying: " + action)
          fw.write(trpg.debugString)
        }
        finally fw.close()
        index += 1

      case None =>
    }
  }

  /**
    * Sorts by heuristic, and implements basic tie breaking
    * @param t1 - first temporal state to be compared
    * @param t2 - second temporal state to be compared
    */
  def sortByHeuristicWithTieBreaks(t1 : TemporalState, t2 : TemporalState) : Boolean = {
    if (t1.heuristic == t2.heuristic) {
      //tie breaking, put TILs and TIFs last  //todo: is there a better tie breaking method we could use?
      t1.plan.lastOption.forall(h => h.temporalAction match {
        case t : TimedAction => false

        case _ => true
      })
    }
    else {
      t1.heuristic <= t2.heuristic
    }
  }
}

case class TrpgInfo( goalLayers : Map[Condition, Int],
                     layerActions : TreeMap[Int, List[TemporalAction]] = TreeMap[Int, List[TemporalAction]](),
                     relaxedPlan : List[TemporalAction] = List[TemporalAction]())