/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.numeric

import uk.ac.kcl.inf.planning.solver.State
import uk.ac.kcl.inf.planning.solver.expressions.Expression
import uk.ac.kcl.inf.planning.solver.ff.ValueBounds
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState


/**
 *
 * Date: 10/02/14
 * Time: 16:57 
 * @author josef
 */
trait Numeric
{
  /**
   * Provides the numeric value of this entity for this state
   * @param state the current state
   * @param context any additional contextual information, mainly local variables such as ?duration
   * @return An option with a double value Some(Double), or None if it is undefined in the current state.
   */
  def value(state : State, context: Map[Expression, Double] = Map[Expression, Double]()) : Option[Double]

  def relaxedBounds(relaxedState: RelaxedState) : Option[ValueBounds]

  def maxValue(relaxedState : RelaxedState) = math.max(value(relaxedState.maxState).getOrElse(0.0), value(relaxedState.minState).getOrElse(0.0))

  def minValue(relaxedState : RelaxedState) = math.min(value(relaxedState.maxState).getOrElse(0.0), value(relaxedState.minState).getOrElse(0.0))

  /**
   * @return all the fluents in this numeric expression
   */
  def fluents : Set[NumericFluent]

  def constant : Double


  //todo: add isDurationDependent flag to determine the presence of ?duration
}
