/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.GroundedTerm
import uk.ac.kcl.inf.planning.solver.TermType
import uk.ac.kcl.inf.planning.solver.LiftedTerm
import uk.ac.kcl.inf.planning.solver.expressions.numeric.{ExternalEvaluator, LinearCondition}
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState

/**
 *
 * Date: 10/02/14
 * Time: 18:10 
 * @author josef
 */
case class AndCondition(expressions : List[Expression]) extends Expression.Aux[AndCondition] with NumericCondition with Modal  {

  override lazy val isGrounded = expressions.forall(exp => exp.isGrounded)

  override def substitute(parameter: LiftedTerm, argument: GroundedTerm)  =
    AndCondition(expressions = expressions.map(exp => exp.substitute(parameter, argument)))

  override def typify(typedParameter: Term): AndCondition =
    AndCondition(expressions.map(exp => exp.typify(typedParameter)))

  def updateTypeHierarchy(termType: TermType): AndCondition =
    AndCondition(expressions.map(exp => exp.updateTypeHierarchy(termType)))

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): AndCondition =
    AndCondition(expressions.map(exp => exp.hookExternalEvaluator(evaluators)))

  override def toString =
    expressions.foldLeft("AND (")((str, exp) => str + " " + exp) + ")"

  /**
   * Determines whether this condition is satisfied in the provided state.
   * @param state The state to be checked for this condition.
   * @return true if the state satisfies the condition, false otherwise
   */
  override def satisfiedBy(state : State, context: Map[Expression, Double] = Map[Expression, Double]()) = (expressions collect {
      case condition : Condition => condition
    }).forall(p => p.satisfiedBy(state, context))

  override def relaxedSatisfiedBy(relaxedState: RelaxedState): Boolean = (expressions collect {
    case condition : Condition => condition
  }).forall(p => p.relaxedSatisfiedBy(relaxedState))

//  override def relaxedSatisfiedBy(relaxedState: State): Boolean = ???

  val modalExpressions = expressions collect {
    case modalExpression : Modal => modalExpression
  }

  override lazy val positiveAtoms =
    modalExpressions.foldLeft(Set[AtomicFormula]())((s, a) => s union a.positiveAtoms)

  override lazy val negativeAtoms =
    modalExpressions.foldLeft(Set[AtomicFormula]())((s, a) => s union a.negativeAtoms)

  lazy val subConditions = expressions collect {
    case condition : Condition => condition
  }

  lazy val numericSubConditions = expressions collect {
    case numericCondition : NumericCondition => numericCondition
  }

  override def violationCount(state: State): Int = subConditions.foldLeft(0)((count, cond) => count + cond.violationCount(state))

  override def violations(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): Set[Condition] = subConditions.foldLeft(Set[Condition]())(
    (violatedConditions, condition) => violatedConditions ++ condition.violations(state))

  override def invert = OrCondition(subConditions.map(_.invert))

  override def toLinearCondition(timeDependentVariables : Set[AtomicFormula], state : State, t : Double = 0.0) : Set[LinearCondition] = {
    numericSubConditions.flatMap(_.toLinearCondition(timeDependentVariables, state, t)).toSet
  }

  override def isTemporallyDependent(timeDependentFluents: Set[AtomicFormula]): Boolean = {
    numericSubConditions.find(_.isTemporallyDependent(timeDependentFluents))  match {
      case Some(condition) => true
      case None => false
    }
  }

  override def getNonTemporallyDependentConditions(timeDependentFluents : Set[AtomicFormula]) : Set[Condition] = {
    subConditions.foldLeft(Set[Condition]())((accSet, condition) => {
      accSet ++ condition.getNonTemporallyDependentConditions(timeDependentFluents)
    })
  }

  override val atomicConditions : Set[Condition] = {
    subConditions.foldLeft(Set[Condition]())((accSet, condition) => {
      accSet ++ condition.atomicConditions
    })
  }

  override val atomicFormulas : Set[AtomicFormula] = {
    subConditions.foldLeft(Set[AtomicFormula]())((accSet, condition) => {
      accSet ++ condition.atomicFormulas
    })
  }

  val fluents : Set[AtomicFormula] = numericSubConditions.flatMap(_.fluents).toSet

  override def equals(obj: scala.Any): Boolean = obj.getClass.equals(this.getClass()) &&
    obj.asInstanceOf[this.type].subConditions.forall(this.subConditions.contains(_)) &&
    this.subConditions.forall(obj.asInstanceOf[this.type].subConditions.contains(_))

  override def isClause: Boolean = {
    expressions.forall {
      case af: AtomicFormula => true
      case nc: NumericCondition => true

      case NotExpression(af) => af match {
        case af: AtomicFormula => true
        case nc: NumericCondition => true
        case _ => false
      }

      case _ => false
    }
  }

}
