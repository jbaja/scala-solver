/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver

import uk.ac.kcl.inf.planning.solver.expressions._
import uk.ac.kcl.inf.planning.solver.expressions.temporal._
import TimeSpecifier.TimeSpecifier
import uk.ac.kcl.inf.planning.solver.expressions.numeric._
import uk.ac.kcl.inf.planning.solver.temporal._

import scala.annotation.tailrec

/**
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 */
case class DurativeAction(name: String,
                          parameters: List[Term],
                          durationConstraint: Expression,
                          condition: Expression,
                          effect: Expression,
                          duration: Option[Double]) {

  val durationCondition: Condition = durationConstraint.asInstanceOf[Condition]

  /**
   * @return true if all the parameters, preconditions and effects are grounded
   */
  lazy val isGrounded: Boolean = {
    parameters.forall(term => term.isGrounded) &&
      durationConstraint.isGrounded &&
      condition.isGrounded &&
      effect.isGrounded &&
      (duration match {
        case Some(number) => true
        case None => false
      })
  }

  /**
   * Helper value for the parameters that have not yet been bound to a value
   */
  lazy val liftedParameters: List[LiftedTerm] =
    parameters collect {
      case pt: LiftedTerm => pt
    }

  /**
   * Substitutes a lifted parameter with a grounded argument.
   * @param parameter The lifted parameter
   * @param argument The grounded argument
   * @return A new action instance with the parameter substituted with the argument if the lifted parameter was found,
   *         otherwise it returns itself.
   */
  def substitute(parameter: LiftedTerm, argument: GroundedTerm): DurativeAction = {
    //here we assume that a parameter only occurs once in the Action parameter list
    val i = parameters.indexOf(parameter)
    if (i >= 0) {
      DurativeAction(name, parameters.updated(i, argument),
        durationConstraint.substitute(parameter, argument),
        condition.substitute(parameter, argument),
        effect.substitute(parameter, argument), duration)
    }
    else
      this
  }

  /**
   * Substitutes a list of parameters with their respective grounded terms
   * @param pairList a list of pairs, the parameter and respective argument to be substituted with
   * @return the action with the specified parameters replaced by their respective arguments
   */
  def substituteList(pairList: List[(LiftedTerm, GroundedTerm)]): DurativeAction = {
    pairList match {
      case (parameter, argument) :: tail =>
        substitute(parameter, argument).substituteList(tail)

      case Nil => this //all substitutions done
    }
  }

  private def updateTypeHierarchy(newTermType: TermType): DurativeAction = {
    @tailrec def updateTypeHierarchyParameters(from: Int, actionAcc: DurativeAction): DurativeAction = {
      val i = actionAcc.parameters.indexWhere(parameter => parameter.termType.name == newTermType.name, from)
      if (i >= 0) {
        updateTypeHierarchyParameters(i + 1, DurativeAction(actionAcc.name,
          actionAcc.parameters.updated(i, actionAcc.parameters(i).updateTypeHierarchy(newTermType)),
          actionAcc.durationConstraint, actionAcc.condition, actionAcc.effect, duration))
      }
      else
        actionAcc
    }

    updateTypeHierarchyParameters(0,
      DurativeAction(name, parameters, durationConstraint.updateTypeHierarchy(newTermType), condition.updateTypeHierarchy(newTermType), effect.updateTypeHierarchy(newTermType), duration))
  }

  @tailrec final def updateTypeHierarchy(typeList: List[TermType]): DurativeAction = {
    typeList match {
      case termType :: tail =>
        updateTypeHierarchy(termType).updateTypeHierarchy(tail)

      case Nil => this
    }
  }

  def hookExternalEvaluator(evaluators : Map[String, ExternalEvaluator]) : DurativeAction = {
    DurativeAction(name, parameters, durationConstraint.hookExternalEvaluator(evaluators), condition.hookExternalEvaluator(evaluators), effect.hookExternalEvaluator(evaluators), duration)
  }

  def positiveEffects(when: TimeSpecifier): Set[AtomicFormula] = propositionalEffects.getOrElse(when, (Set[AtomicFormula](), Set[AtomicFormula]()))._1

  def negativeEffects(when: TimeSpecifier): Set[AtomicFormula] = propositionalEffects.getOrElse(when, (Set[AtomicFormula](), Set[AtomicFormula]()))._2

  lazy val propositionalEffects: Map[TimeSpecifier, (Set[AtomicFormula], Set[AtomicFormula])] = {
    effect match {
      case timedModal: TimedModal => {

        @tailrec def getTimedEffects(ts: TimeSpecifier, mapEffectsAcc: Map[TimeSpecifier, (Set[AtomicFormula], Set[AtomicFormula])]):
        Map[TimeSpecifier, (Set[AtomicFormula], Set[AtomicFormula])] = {
          val atoms: (Set[AtomicFormula], Set[AtomicFormula]) = timedModal.atoms(ts)
          val newMap: Map[TimeSpecifier, (Set[AtomicFormula], Set[AtomicFormula])] = mapEffectsAcc.updated(ts, atoms)

          if (ts.equals(TimeSpecifier.AT_START))
            getTimedEffects(TimeSpecifier.AT_END, newMap)
          else
            newMap
        }

        getTimedEffects(TimeSpecifier.AT_START, Map[TimeSpecifier, (Set[AtomicFormula], Set[AtomicFormula])]())
      }

      case _ => Map[TimeSpecifier, (Set[AtomicFormula], Set[AtomicFormula])]()
    }
  }

  lazy val numericEffects: Map[TimeSpecifier, Set[AtomicFormula]] = {
    effect match {
      case timedNumeric: TimedNumericEffect => {

        @tailrec def getTimedNumericEffects(ts: TimeSpecifier, mapEffectsAcc: Map[TimeSpecifier, Set[AtomicFormula]]):
        Map[TimeSpecifier, (Set[AtomicFormula])] = {
          val fluents: Set[AtomicFormula] = timedNumeric.numericFluents(ts).map(_.functionPredicate)
          val newMap: Map[TimeSpecifier, Set[AtomicFormula]] = mapEffectsAcc.updated(ts, fluents)

          if (ts.equals(TimeSpecifier.AT_START))
            getTimedNumericEffects(TimeSpecifier.AT_END, newMap)
          else
            newMap
        }

        getTimedNumericEffects(TimeSpecifier.AT_START, Map[TimeSpecifier, Set[AtomicFormula]]())
      }

      case _ => Map[TimeSpecifier, Set[AtomicFormula]]()
    }
  }


  lazy val continuousEffects: Set[ContinuousNumericEffect] = {
    effect match {
      case andEffect: TemporalAndEffect => andEffect.continuousEffects.toSet

      case continuousEffect: ContinuousNumericEffect => Set[ContinuousNumericEffect](continuousEffect)

      case _ => Set[ContinuousNumericEffect]()
    }
  }

  lazy val conditions: Map[TimeSpecifier, (Set[AtomicFormula], Set[AtomicFormula])] = {
    condition match {
      case timedModal: TimedModal => {

        @tailrec def getTimedPreconditions(ts: TimeSpecifier, mapEffectsAcc: Map[TimeSpecifier, (Set[AtomicFormula], Set[AtomicFormula])]):
        Map[TimeSpecifier, (Set[AtomicFormula], Set[AtomicFormula])] = {
          val atoms: (Set[AtomicFormula], Set[AtomicFormula]) = timedModal.atoms(ts)
          val newMap: Map[TimeSpecifier, (Set[AtomicFormula], Set[AtomicFormula])] = mapEffectsAcc.updated(ts, atoms)

          //populate all of them recursively, one after the other
          if (ts.equals(TimeSpecifier.AT_START))
            getTimedPreconditions(TimeSpecifier.AT_END, newMap)
          else if (ts.equals(TimeSpecifier.AT_END))
            getTimedPreconditions(TimeSpecifier.OVER_ALL, newMap)
          else
            newMap
        }

        getTimedPreconditions(TimeSpecifier.AT_START, Map[TimeSpecifier, (Set[AtomicFormula], Set[AtomicFormula])]())
      }

      case _ => Map[TimeSpecifier, (Set[AtomicFormula], Set[AtomicFormula])]()
    }
  }

  def positiveConditions(when: TimeSpecifier): Set[AtomicFormula] = conditions.getOrElse(when, (Set[AtomicFormula](), Set[AtomicFormula]()))._1

  def negativeConditions(when: TimeSpecifier): Set[AtomicFormula] = conditions.getOrElse(when, (Set[AtomicFormula](), Set[AtomicFormula]()))._2

  /**
   * Checks if this action is applicable against a set of facts
   * @param state the facts that are holding true
   * @return true if the action is applicable, false otherwise
   */
  def isApplicable(state: State, when: TimeSpecifier): Boolean = {
    condition match {
      case timedCondition: TimedCondition => timedCondition.satisfiedBy(state, when)
      case _ => true
    }
  }

  def startSnapAction = new SnapAction(this, TimeSpecifier.AT_START)
  def endSnapAction(startAction : SnapAction) = new SnapAction(this, TimeSpecifier.AT_END, Some(startAction))

  /**
    * All conditions at a specified time, combined together with an AndCondition
    * @param when - the time (at start, at end or over all)
    */
  def conditionsAt(when: TimeSpecifier): Set[Condition] = {
    condition match {
      case timedCondition: TimedCondition => timedCondition.conditions(when)

      case _ => Set[Condition]()
    }
  }


  /**
   * Generates the grounded actions for this action
   * @param possibleValues The possible values for each type
   * @return
   */
  def generateGroundedActions(possibleValues: Map[TermType, Set[GroundedTerm]]): Set[DurativeAction] = {
    def generateGroundedActionsAcc(accSubstitutions: List[(LiftedTerm, GroundedTerm)], remainder: List[LiftedTerm]): Set[DurativeAction] = {
      remainder match {
        case Nil => Set(substituteList(accSubstitutions))

        case parameter :: tail =>
          if (possibleValues.contains(parameter.termType)) {
            for {
            //todo: support duration?
              argument <- possibleValues(parameter.termType)
              rest <- generateGroundedActionsAcc((parameter, argument) :: accSubstitutions, tail)
            } yield rest
          }
          else {
            //this action is not applicable for the problem because there is no object with the required parameter term type
            Set[DurativeAction]()
          }
      }
    }

    val groundedActions: Set[DurativeAction] = generateGroundedActionsAcc(Nil, liftedParameters)
    groundedActions.foreach(_.propositionalEffects)

    groundedActions
  }

  def durationAtState(state: State): Option[Double] = {
    durationCondition match {
      case EqualsCondition(NumericVariable("?duration"), exp2: Numeric) => exp2.value(state)
      //todo: handle duration inequalities  and AndCondition ?
      case _ => None
    }
  }

  def maximumDurationAtState(state: State): Option[Double] = {
    resolveMaximumBounds(state, durationCondition)
  }

  def minimumDurationAtState(state: State): Option[Double] = {
    resolveMinimumBounds(state, durationCondition)
  }

  def minimumDurationAtRelaxedState(relaxedState : RelaxedState) : Double = {
    val durations = List(resolveMinimumBounds(relaxedState.minState, durationCondition),
                          resolveMinimumBounds(relaxedState.maxState, durationCondition)).flatten

    if (durations.nonEmpty)
      durations.min
    else
      SolverSettings.settings.EPSILON
  }

  private def resolveMaximumBounds(state: State, condition: Condition): Option[Double] = {
    condition match {
      case EqualsCondition(NumericVariable("?duration"), exp2: Numeric) => exp2.value(state)

      case GreaterThanEqualCondition(exp1: Numeric, NumericVariable("?duration")) => exp1.value(state)

      case GreaterThanCondition(exp1: Numeric, NumericVariable("?duration")) => SubtractNumeric(exp1, NumericConstant(SolverSettings.settings.EPSILON)).value(state)

      case AndCondition(expressions) => {
        val valueBounds = for {
          exp <- expressions
          if exp.isInstanceOf[Condition] //just to be paranoid, all should be conditions
          bound <- resolveMaximumBounds(state, exp.asInstanceOf[Condition])
        } yield bound

        if (valueBounds.isEmpty) None
        else Some(valueBounds.min)
      }

      case _ => None
    }
  }

  def resolveMinimumBounds(state: State, condition: Condition): Option[Double] = {
    condition match {
      case EqualsCondition(NumericVariable("?duration"), exp2: Numeric) => exp2.value(state)

      case GreaterThanEqualCondition(NumericVariable("?duration"), exp2: Numeric) => exp2.value(state)

      case GreaterThanCondition(NumericVariable("?duration"), exp2: Numeric) => AddNumeric(List(exp2, NumericConstant(SolverSettings.settings.EPSILON))).value(state)

      case AndCondition(expressions) => {
        val valueBounds = for {
          exp <- expressions
          if exp.isInstanceOf[Condition] //just to be paranoid, all should be conditions
          bound <- resolveMinimumBounds(state, exp.asInstanceOf[Condition])
        } yield bound

        if (valueBounds.isEmpty) None
        else Some(valueBounds.max)
      }

      case _ => None
    }
  }

  def durationValidAtState(state: State): Boolean = {
    val context: Map[Expression, Double] = Map[Expression, Double]((NumericVariable("?duration"), duration.get))
    durationCondition.satisfiedBy(state, context)
  }

  def computeDuration(state: State): DurativeAction = {
    val dur = durationAtState(state)

    if (dur.getOrElse(0.0) < 0) {
      println("**!*!*!*!*! Duration for " + this + " is negative: " + dur)
      println("**!*!*!*!*! Expression : " + durationConstraint)
      println("**!*!*!*!*! State : " + state)
    }
    this.copy(duration = dur)
  }

  /**
   * Apply an action to a state, which will give us another state.
   * This follows the FF rule that first negative effects are removed, than positive effects are added
   * @param state The state to which this action will be applied
   * @return The new state after applying this action
   */
  def applyToState(state: State, when: TimeSpecifier): State = {
    val context: Map[Expression, Double] = Map[Expression, Double]((NumericVariable("?duration"), duration.get))
    val numericUpdates = effect match {
      case numericEffect: TimedNumericEffect => numericEffect.evaluateNumeric(state, when, context)

      case _ => Map[AtomicFormula, Double]()
    }

    State(state.modalFacts -- negativeEffects(when) ++ positiveEffects(when), state.numericFluents ++ numericUpdates)
  }

  val dummyProposition = AtomicFormula("$" + name, parameters)

  def addStartInformation(relaxedState: RelaxedState) : RelaxedState = {
    relaxedState.copy(modalFacts = relaxedState.modalFacts + dummyProposition, continuousEffects = relaxedState.continuousEffects ++ continuousEffects)
  }

  def applyToRelaxedState(oldRelaxedState : RelaxedState, cumulativeRelaxedState : RelaxedState, when : TimeSpecifier, elapsedTime: Double) : RelaxedState = {
    //compute the discrete updates (at start or at end)
    val discreteRelaxedState = effect match {
      case effect: TimedEffect => effect.evaluateRelaxed(oldRelaxedState, cumulativeRelaxedState, when)
    }
    //compute any continuous effects
    if (when.equals(TimeSpecifier.AT_START)) {
      if (elapsedTime > 0) {
        val contRelaxedState = continuousEffects.foldLeft(discreteRelaxedState)(
          (accRelaxedState, ceff) => ceff.relaxedEffect(discreteRelaxedState, accRelaxedState, elapsedTime)
        )
        //we add a dummy proposition for this action, so that the end snap action becomes applicable
        addStartInformation(contRelaxedState)
      }
      else {       //we add a dummy proposition for this action, so that the end snap action becomes applicable
        addStartInformation(discreteRelaxedState)
      }
    }
    else {
      discreteRelaxedState
    }
  }

  def getContinuousEffects(state: State, t: Double): Map[AtomicFormula, Double] = {
    val context: Map[Expression, Double] = Map[Expression, Double]((NumericVariable("?duration"), duration.get))

    continuousEffects.foldLeft(Map[AtomicFormula, Double]())((accMap: Map[AtomicFormula, Double], effect) => {
      accMap ++ effect.evaluateNumericDelta(state, t, context).map {
        case (key, value) => key -> (value + accMap.getOrElse(key, 0.0))
      }
    })
  }

  override val toString = parameters.foldLeft("(" + name)((str, term) => str + " " + term) + ")"
}
