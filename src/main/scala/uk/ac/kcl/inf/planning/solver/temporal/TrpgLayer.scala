/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.temporal

import uk.ac.kcl.inf.planning.solver.{SolverSettings, GoalUnreachableException, Action, Problem}
import uk.ac.kcl.inf.planning.solver.expressions.Condition

/**
  * @author Josef Bajada
  */
case class TrpgLayer(problem : Problem,
                     relaxedState : RelaxedState,
                     outstandingGoals : Set[Condition],
                     scheduledActions : Set[TemporalAction],
                     pendingScheduledActions : Map[TemporalAction, Double],
                     remainingInstantActions : Set[Action],
                     remainingStartActions : Set[SnapAction],
                     timestamp: Double = 0,
                     index : Int = 0,
                     preceedingLayer : Option[TrpgLayer] = None,
                     alreadyApplied : Set[TemporalAction] = Set[TemporalAction]())
{
  //check which instant actions we can apply
  private val applicableInstantActions: Set[Action] = remainingInstantActions.filter(_.isApplicableRelaxed(relaxedState))

  //check which end actions we can apply
  private val (pastEndActions, futureEndActions) = pendingScheduledActions.partition(_._2 <= timestamp)
  private val applicableEndActions = pastEndActions.keys.filter(_.isApplicableRelaxed(relaxedState))

  //check which start actions we can apply
  private val applicableStartActions: Set[SnapAction] = remainingStartActions.filter(_.isApplicableRelaxed(relaxedState))

  //the new actions we have to apply
  val newActions : Set[TemporalAction] = applicableInstantActions ++ applicableEndActions ++ applicableStartActions

  //all the actions that should be considered applicable in this layer
  val actions : Set[TemporalAction] = alreadyApplied ++ newActions

  //relaxed state after applying the actions
  lazy val discreteNewRelaxedState = TemporalRPG.applyActions(actions, relaxedState, 0)
  lazy val newRelaxedState = RelaxedState.applyContinuousEffects(TemporalRPG.applyActions(actions, relaxedState, elapsed))

  //the remaining goals we have not satisfied yet
  private lazy val newOutstandingGoals = outstandingGoals.filterNot(_.relaxedSatisfiedBy(newRelaxedState))

  //the new pending end snap actions map
  private lazy val startedActionEnds = applicableStartActions.map(startAction =>
    (startAction.durativeAction.endSnapAction(startAction), timestamp + startAction.durativeAction.minimumDurationAtRelaxedState(relaxedState)))
          .filterNot(endSnapAction => alreadyApplied.contains(endSnapAction._1))
          .filterNot(endSnapAction => pendingScheduledActions.contains(endSnapAction._1))

  private lazy val newPendingEndSnapActionTimes = (pendingScheduledActions -- pastEndActions.keySet) ++ startedActionEnds

  //the new remaining instant actions
  private lazy val newRemainingInstantActions = remainingInstantActions -- applicableInstantActions
  private lazy val newRemainingStartActions = remainingStartActions -- applicableStartActions

  private lazy val appliedAllScheduledActions : Boolean = {
    scheduledActions.subsetOf(alreadyApplied)
  }

  lazy val elapsed = {
    if (outstandingGoals.isEmpty && appliedAllScheduledActions) {
      Double.PositiveInfinity
    }
    else {
      if (newActions.isEmpty) {
        if (futureEndActions.nonEmpty)
          futureEndActions.values.min - timestamp
        else
          Double.PositiveInfinity
      }
      else  {
        SolverSettings.settings.EPSILON
      }
    }
  }

  lazy val traverse : Option[TrpgLayer] = {
    if (outstandingGoals.isEmpty && appliedAllScheduledActions) {
      None
    }
    else {
      //todo: review the fixed point check, its not clear if the distinction of when the goal is reachable or not, versus we're stuck, is completely correct
      if (newActions.isEmpty && newRelaxedState.reachedFixedPoint(relaxedState, problem)) {
        if (futureEndActions.isEmpty) {
          throw new GoalUnreachableException("Unable to reach goal from this state! (Layer: " + index + " state: " + relaxedState)
        }
        else {
          Some(TrpgLayer(problem, newRelaxedState, newOutstandingGoals, scheduledActions, newPendingEndSnapActionTimes, newRemainingInstantActions, newRemainingStartActions, futureEndActions.values.min, index + 1, Some(this), actions))
        }
      }
      else  {
        Some(TrpgLayer(problem, newRelaxedState, newOutstandingGoals, scheduledActions, newPendingEndSnapActionTimes, newRemainingInstantActions, newRemainingStartActions, timestamp + SolverSettings.settings.EPSILON, index + 1, Some(this), actions))
      }
    }
  }


  override val toString: String = "Layer " + index + " [" + timestamp + "]" + " applicable actions: " + actions.size + " (" + newActions.size + ") Future: " + futureEndActions.size

  lazy val debugString : String = {
    val s = new StringBuilder("Fact Layer %d Timestamp %.4f\n".format(index, timestamp))
    s ++= "Modal Facts:\n"
    relaxedState.modalFacts.foreach(fact => s ++= "%s\n".format(fact))
    s ++= "Numeric Facts:\n"
    relaxedState.numericFluentBounds.foreach(kv => s ++= "%s = (%.3f, %.3f)\n".format(kv._1, kv._2.min, kv._2.max))

    s ++= "\nApplicable Actions\n"
    actions.foreach(action => s ++= "%s\n".format(action))
    s ++= "\n"

    s.toString()
  }
}
