/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.utils

/**
 *
 * Date: 06/05/14
 * Time: 11:54 
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 */
object NumericUtils
{
  def round(value: Double, places: Int) : Double = {
      val factor = Math.pow(10, places)
      Math.round(value * factor) / factor
  }

  def floor(value: Double, places: Int) : Double = {
    val factor = Math.pow(10, places)
    Math.floor(value * factor) / factor
  }

}