/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.external

import uk.ac.kcl.inf.planning.solver.{AtomicFormula, State}
import uk.ac.kcl.inf.planning.solver.expressions.numeric.{NumericFluent, ExternalEvaluator}

/**
 * External module implementing Torricelli's law
 * @param aliasPrefix - the alias prefix in the PDDL file identifying this module.
 */
case class Torricelli(aliasPrefix : String) extends ExternalEvaluator
{
  val fullName = "Kcl.Planning.Torricelli"

  val GRAVITY = 9.81

  override def value(fluent : NumericFluent, state: State, t: Double): Option[Double] = {
    //todo: rather than do this all the time, return a function on hook attachment that calls directly the respective function (as defined in PDDLx)

    val method = fluent.functionPredicate.predicate.drop(aliasPrefix.length() + 1) //we add one to remove the .

    method match {
      case "drain-rate" => drainRate(fluent, state, t)

      case "height-change" => heightChange(fluent, state, t)

      case _ => handleStaticFluent(fluent, state)
    }
  }

  //this is expecting fluent to be (Torricelli.drainVelocity ?tank - Torricelli.Tank)
  def drainRate(fluent : NumericFluent, state : State, t : Double) : Option[Double] = {
    if (fluent.functionPredicate.parameters.size != 1)
    {
      throw new Exception("Invalid parameters for module method: " + fluent.functionPredicate + " Expecting 1 parameter of type Torricelli.Tank")
    }

    val height = state.numericFluents(AtomicFormula(aliasedName("height"), fluent.functionPredicate.parameters))
    val tankRadius = state.numericFluents(AtomicFormula(aliasedName("radius"), fluent.functionPredicate.parameters))
    val holeRadius = state.numericFluents(AtomicFormula(aliasedName("hole-radius"), fluent.functionPredicate.parameters))

    val timeLimit = Math.sqrt(2 * height / GRAVITY) * Math.pow(tankRadius / holeRadius, 2)

    val drainRateAt0 = rateAtHeight(holeRadius, height)
    val drainRateAtT = if (t < timeLimit) rateAtHeight(holeRadius, heightAt(height, tankRadius, holeRadius, t)) else 0.0

    val gradient = (drainRateAt0 + drainRateAtT) / 2
    Some(gradient)
  }



  def heightChange(fluent : NumericFluent, state : State, t : Double) : Option[Double] = {
    if (fluent.functionPredicate.parameters.size != 1)
    {
      throw new Exception("Invalid parameters for module method: " + fluent.functionPredicate + " Expecting 1 parameter of type Torricelli.Tank")
    }

    val height = state.numericFluents(AtomicFormula(aliasedName("height"), fluent.functionPredicate.parameters))
    val tankRadius = state.numericFluents(AtomicFormula(aliasedName("radius"), fluent.functionPredicate.parameters))
    val holeRadius = state.numericFluents(AtomicFormula(aliasedName("hole-radius"), fluent.functionPredicate.parameters))

    val timeLimit = Math.sqrt(2 * height / GRAVITY) * Math.pow(tankRadius / holeRadius, 2)

    val heightAtT = if (t < timeLimit) heightAt(height, tankRadius, holeRadius, t) else 0

    Some((height - heightAtT) / t)
  }

  private def rateAtHeight(holeRadius : Double, height : Double) = {
    Math.PI * holeRadius * holeRadius * Math.sqrt(2 * GRAVITY * height)
  }

  private def heightAt(height: Double, tankRadius : Double, holeRadius : Double, time : Double) = {
    val holeTankRatio = (holeRadius * holeRadius) / (tankRadius * tankRadius)
    height * Math.pow(1 -  holeTankRatio * Math.sqrt(GRAVITY / (2 * height)) * time , 2)
  }

  override def isConstant(fluent: NumericFluent, state : State, t : Double): Boolean = false  //non-linear in both cases
}
