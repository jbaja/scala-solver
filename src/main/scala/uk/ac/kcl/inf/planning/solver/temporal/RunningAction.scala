/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.temporal

import uk.ac.kcl.inf.planning.solver.{State, AtomicFormula}
import uk.ac.kcl.inf.planning.solver.expressions.numeric.{LinearCondition, NumericConstraint}

/**
 * Represents a running action of a temporal state
 * Created by Josef on 24/01/2015.
 */
case class RunningAction(startAction : SnapAction, startIndex : Integer, lbDuration : Option[Double], ubDuration : Option[Double])  {
  val endAction : SnapAction = startAction.durativeAction.endSnapAction(startAction)

  private def upperBound(endIndex : Integer, initialConstraints : Set[TemporalConstraint]) : Set[TemporalConstraint] =
    ubDuration.map(value => initialConstraints + IntervalTemporalConstraint(startIndex, endIndex, NumericConstraint.LEQ, value))
              .getOrElse(initialConstraints)

  private def lowerBoundAndUpperbound(endIndex : Integer, initialConstraints : Set[TemporalConstraint] = Set[TemporalConstraint]()) : Set[TemporalConstraint] =
    lbDuration.map(value =>
      upperBound(endIndex, initialConstraints + IntervalTemporalConstraint(startIndex, endIndex, NumericConstraint.GEQ, value))
    ).getOrElse(upperBound(endIndex, initialConstraints))

  def temporalConstraints(endIndex : Integer) : Set[TemporalConstraint] = lowerBoundAndUpperbound(endIndex)

  def getLinearOverallConditions(timeDependentVariables : Set[AtomicFormula], state : State) : Set[LinearCondition] = {
    startAction.getLinearOverallConditions(timeDependentVariables, state)
  }
}
