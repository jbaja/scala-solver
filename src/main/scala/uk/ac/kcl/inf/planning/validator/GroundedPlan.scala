/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.validator

import uk.ac.kcl.inf.planning.solver.expressions.{Condition, NumericEffect}
import uk.ac.kcl.inf.planning.utils.IntervalTree

import uk.ac.kcl.inf.planning.solver.{AtomicFormula, State, Problem}
import uk.ac.kcl.inf.planning.solver.expressions.temporal.{TimedNumericEffect, ContinuousNumericEffect, TimeSpecifier}

import scala.collection.JavaConverters._
import scala.collection.SortedSet


/**
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 */
case class GroundedPlan(problem: Problem, timedApplicables : List[TimedApplicable])
{
  val durativeActionsTree : IntervalTree[TimedApplicable] = {

    val tree = new IntervalTree[TimedApplicable]()

    timedApplicables foreach {
      case step: TimedApplicableDurativeAction =>
        if (step.when == TimeSpecifier.AT_START)
          tree.addInterval(step.at, step.at + step.duration, step)

      case step : TimedApplicableAction => tree.addInterval(step.at, step.at, step)

      case step : TimedInitialFluent => tree.addInterval(step.at, step.at, step)

      case step : TimedInitialLiteral => tree.addInterval(step.at, step.at, step)
    }

    tree
  }

  val intervals = {

    val timestamps = (0.0 :: timedApplicables.map(ta => ta.at)).distinct.sorted //get the timestamps


    if (timestamps.length > 1) {
      val internalPeriods: List[(Double, Double)] = timestamps.sliding(2, 1).map {
        case Seq(first, second) => (first, second)
      }.toList

      internalPeriods
    }
    else
    {
      List[(Double, Double)]()
    }
  }

  val futureIntervals = {
    intervals.filter(_._1 >= 0.0)
  }


  def invariantConditionsAt(at : Double) = {
    val concurrentActions = durativeActionsTree.get(at).asScala
//    println("Concurrent actions at " + at + " " + concurrentActions)
    (concurrentActions collect {
      case action: TimedApplicableDurativeAction => action
    }).withFilter(action => (action.at <= at) && (action.at + action.duration) > at)
      .flatMap(_.durativeAction.conditionsAt(TimeSpecifier.OVER_ALL)).toSet
  }


  def continuousEffectActionsAt(at : Double) : List[TimedApplicableDurativeAction] = {
    val concurrentActions = durativeActionsTree.get(at).asScala
    //todo: handle PDDL+ processes

    (concurrentActions collect {
      case action: TimedApplicableDurativeAction => action
    }).filter(action => (action.at < at) && (action.at + action.duration) >= at && action.durativeAction.continuousEffects.nonEmpty).toList
  }

  override def toString : String = timedApplicables.foldLeft("")((str, term) => str + "\n" + term)

  def toPlanString : String = (timedApplicables collect {
    case durativeAction : TimedApplicableDurativeAction => durativeAction
  }).filter(durativeAction => durativeAction.when.equals(TimeSpecifier.AT_START)).foldLeft("")((str, term) => str + "\n" + term.toPlanString)


  def applyContinuousEffects(state : State, continuousActions : List[TimedApplicableDurativeAction], t : Double) : State = {

    //accumulate all the changes
    val numericDeltas = continuousActions.foldLeft(Map[AtomicFormula, Double]())((accMap : Map[AtomicFormula, Double], action) => {
          accMap ++ action.durativeAction.getContinuousEffects(state, t).map {
            case (key, value) => key -> (value + accMap.getOrElse(key, 0.0))
          }
        })

    //translate them into the actual absolute values
    val numericUpdates = numericDeltas.map{
      case (af : AtomicFormula, delta : Double) => af -> (state.numericFluents.getOrElse(af, 0.0) + delta)
    }

    //update the state
    State(state.modalFacts, state.numericFluents ++ numericUpdates )
  }

  lazy val timeline : PlanTimeline =
  {
    val initialTimeline = PlanTimeline(problem, this, problem.initialState, SortedSet(PlanTimepoint(0.0, problem.initialState)))

    val planTimeline = timedApplicables.foldLeft(initialTimeline)(
      (currentTimeline : PlanTimeline, timedApplicable : TimedApplicable) => {
        if (timedApplicable.at >= 0.0)
        { //we only do this if it is a future timed activity

          //get the continuous effects
          val continuousEffectActions = continuousEffectActionsAt(timedApplicable.at)
          val continuouslyUpdatedState = applyContinuousEffects(currentTimeline.endState, continuousEffectActions, timedApplicable.at - currentTimeline.duration)

          val newState = timedApplicable.applyToState(continuouslyUpdatedState)

          //get all the OVER ALL conditions from the other durative actions running concurrently
          val invariantConditions = invariantConditionsAt(timedApplicable.at)

  //        println("Invariants at " + timedApplicable.at + " " + invariantConditions)
          val stateViolations = timedApplicable.violations(currentTimeline.endState) ++ TimedInvariants.violations(newState, invariantConditions)
          val newViolations = stateViolations -- currentTimeline.stateTimeline.last.cumulativeViolations
          //we do the following so that we don't count the same precondition or invariant more than once
          val cumulativeViolations = stateViolations ++ currentTimeline.stateTimeline.last.cumulativeViolations.filterNot(_.satisfiedBy(currentTimeline.endState))
          //todo: check duration condition

          //todo: same condition but different action needs to be counted double?
          //todo: same action spanning multiple timepoints need to be counted once

          val violationCount = newViolations.size
          PlanTimeline(problem, this, newState, currentTimeline.stateTimeline + PlanTimepoint(timedApplicable.at, newState, Some(timedApplicable), invariantConditions, cumulativeViolations, newViolations),  currentTimeline.violationCount + violationCount)
        }
        else {
          //we're in the past
          PlanTimeline(problem, this, State(), currentTimeline.stateTimeline + PlanTimepoint(timedApplicable.at, State(), Some(timedApplicable), Set[Condition](), Set[Condition](), Set[Condition]()),  0)
        }

        //    val violationCount = timedApplicable.violationCount(currentTimeline.endState) +
      //    TimedInvariants.violationCount(currentTimeline.endState, invariantConditions)
      }

    )

    val goalViolations = problem.goalCondition.violationCount(planTimeline.endState)

//    println("===== violations : " + (planTimeline.violationCount + goalViolations))
    planTimeline.copy( violationCount = planTimeline.violationCount + goalViolations)
  }

  def getPeriodStartTime(period : Int): Double = {
    if (period < this.futureIntervals.size)   //we picked a normal period
      this.futureIntervals(period)._1
    else if (this.futureIntervals.nonEmpty) //we picked the last period (which ends at infinity)
      this.futureIntervals.last._2
    else 0  //the plan is empty, we can only start at 0.
  }
}
