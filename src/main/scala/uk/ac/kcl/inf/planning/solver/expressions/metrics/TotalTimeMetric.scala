/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.metrics

import uk.ac.kcl.inf.planning.solver.expressions.numeric.ExternalEvaluator
import uk.ac.kcl.inf.planning.solver.{TermType, Term, LiftedTerm, GroundedTerm}
import uk.ac.kcl.inf.planning.solver.expressions.Expression
import uk.ac.kcl.inf.planning.validator.PlanTimeline

/**
 *
 * Date: 05/06/14
 * Time: 17:52 
 * @author josef
 */
case class TotalTimeMetric()  extends Expression.Aux[TotalTimeMetric] with MetricNumeric {

  override def updateTypeHierarchy(termType: TermType): TotalTimeMetric = this

  override def typify(typedParameter: Term): TotalTimeMetric = this

  override def substitute(parameter: LiftedTerm, argument: GroundedTerm): TotalTimeMetric = this
  /**
   * @return true if this expression is fully grounded, false otherwise
   */
  override def isGrounded: Boolean = true

  /**
   * Provides the numeric value of this entity for a plan
   * @param planTimeline the whole plan timeline to be evaluated
   * @return An double value
   */
  override def value(planTimeline: PlanTimeline): Double = {
    planTimeline.stateTimeline.last.at
  }

  override def toString: String = "total-time"

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): TotalTimeMetric = this
}
