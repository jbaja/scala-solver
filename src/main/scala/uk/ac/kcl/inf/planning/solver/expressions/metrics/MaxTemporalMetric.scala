/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.metrics

import uk.ac.kcl.inf.planning.solver.expressions.Expression
import uk.ac.kcl.inf.planning.solver.{GroundedTerm, LiftedTerm, Term, TermType}
import uk.ac.kcl.inf.planning.validator.{PlanTimepoint, PlanTimeline}
import uk.ac.kcl.inf.planning.solver.expressions.numeric.{ExternalEvaluator, Numeric}

/**
 *
 * Date: 05/06/14
 * Time: 17:56 
 * @author josef
 */
case class MaxTemporalMetric(exp: Expression) extends Expression.Aux[MaxTemporalMetric] with MetricNumeric {

  val numericExp : Numeric = exp.asInstanceOf[Numeric]

  override def updateTypeHierarchy(termType: TermType): MaxTemporalMetric = MaxTemporalMetric(exp.updateTypeHierarchy(termType))

  override def typify(typedParameter: Term): MaxTemporalMetric = MaxTemporalMetric(exp.typify(typedParameter))

  override def substitute(parameter: LiftedTerm, argument: GroundedTerm): MaxTemporalMetric = MaxTemporalMetric(exp.substitute(parameter, argument))

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): MaxTemporalMetric =
    copy(exp = exp.hookExternalEvaluator(evaluators))

  /**
   * @return true if this expression is fully grounded, false otherwise
   */
  override def isGrounded: Boolean = exp.isGrounded

  /**
   * Provides the numeric value of this entity for a plan
   * @param planTimeline the whole plan timeline to be evaluated
   * @return An double value
   */
  override def value(planTimeline: PlanTimeline): Double = {
    planTimeline.stateTimeline.map(timepoint => numericExp.value(timepoint.state)).max.getOrElse(0.0)
  }
}
