/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.numeric

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.expressions.Expression
import uk.ac.kcl.inf.planning.solver.ff.ValueBounds
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState

/**
 *
 * Date: 10/02/14
 * Time: 18:43 
 * @author josef
 */
case class NumericVariable(name : String) extends Expression.Aux[NumericVariable] with Numeric
{
  override def updateTypeHierarchy(termType: TermType): NumericVariable = this

  override def typify(typedParameter: Term): NumericVariable = this

  override def substitute(parameter: LiftedTerm, argument: GroundedTerm): NumericVariable = this

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): NumericVariable = this

  /**
   * @return true if this expression is fully grounded, false otherwise
   */
  override def isGrounded: Boolean = true

  /**
   * Provides the numeric value of this entity for this state
   * @param state the current state
   * @return An option with a double value Some(Double), or None if it is undefined in the current state.
   */
  override def value(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): Option[Double] =
  {
    context.get(this)
  }

  override def fluents: Set[NumericFluent] = Set[NumericFluent]()

  /**
   * @return The constant part of the expression, computed by adding the constant parts of the sub expressions.
   */
  override def constant : Double = 0.0

  override def toString = "(?" + name + ")"

  //todo: to handle duration variables ?duration this must work in some way
  override def relaxedBounds(relaxedState: RelaxedState): Option[ValueBounds] = None
}