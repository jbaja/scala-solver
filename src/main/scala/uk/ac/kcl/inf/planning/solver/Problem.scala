/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver

import uk.ac.kcl.inf.planning.solver.expressions.temporal.TimeSpecifier
import uk.ac.kcl.inf.planning.solver.temporal.{TimedAction, SnapAction}

import scala.annotation.tailrec
import uk.ac.kcl.inf.planning.solver.expressions.{Modal, Condition, Expression}
import uk.ac.kcl.inf.planning.solver.expressions.metrics.MetricSpec

import scala.util.parsing.input.Positional

class Problem(val domain : Domain, val name: String, entities: Set[GroundedTerm], goal: Expression,
              tils : Map[Double, List[Expression]] = Map[Double, List[Expression]](),
              tifs : Map[Double, List[(AtomicFormula, Double)]] = Map[Double, List[(AtomicFormula, Double)]](),
              val metric : Option[MetricSpec]) extends Positional
{
  val objects : Set[GroundedTerm] = domain.constants ++ entities.map(entity => entity.updateTypeHierarchy(domain.types)) collect {
    case gt : GroundedTerm => gt
  }

  /**
   * All the objects we can use, grouped by their type for easy grounding.
   */
  lazy val entityMap: Map[TermType, Set[GroundedTerm]] = objects.foldLeft(Map[TermType, Set[GroundedTerm]]())((acc, term) =>
    updateTermTypes(acc, term, term.termType)
  )

  @tailrec
  private def updateTermTypes(current : Map[TermType, Set[GroundedTerm]], term : GroundedTerm, termType : TermType) : Map[TermType, Set[GroundedTerm]] = {
    val updatedAcc = current.get(termType) match {
      case Some(set: Set[GroundedTerm]) => current + (termType -> (set + term))
      case None => current + (termType -> Set[GroundedTerm](term))
    }

    termType.superType match {
      case Some(superType) => updateTermTypes(updatedAcc, term, superType)

      case None => updatedAcc
    }
  }

  lazy val objectList = objects.toList

  val timedInitialLiterals  = {
    tils mapValues (_ map (_.typifyAll(objectList).updateTypeHierarchy(domain.types)))
  }

  val timedInitialFluents = {
    tifs mapValues (_ map ( tif => (tif._1.typifyAll(objectList).updateTypeHierarchy(domain.types), tif._2)))
  }

  val timedHappenings : List[TimedAction] = {
    val sortedTils: List[(Double, List[Expression])] = timedInitialLiterals.toSeq.filter(_._1 > 0.0).sortBy(_._1).toList
    val sortedTifs: List[(Double, List[(AtomicFormula, Double)])] = timedInitialFluents.toSeq.filter(_._1 > 0.0).sortBy(_._1).toList

    @tailrec
    def accTimedHappenings(tilseq : List[(Double, List[Expression])],
                           tifseq: List[(Double, List[(AtomicFormula, Double)])],
                           accList : List[TimedAction] = List[TimedAction]()): List[TimedAction] = {
      (tilseq, tifseq) match {
        case (Nil, Nil) => accList

        case (til :: tiltail, Nil) => accTimedHappenings(tiltail, tifseq, TimedAction(til._1, til._2, Map[AtomicFormula, Double]()) :: accList)

        case (Nil, tif :: tiftail) => accTimedHappenings(tilseq, tiftail, TimedAction(tif._1, List[Expression](), tif._2.toMap) :: accList)

        case (til :: tiltail, tif :: tiftail) => if (til._1 < tif._1)   {
          accTimedHappenings(tiltail, tifseq, TimedAction(til._1, til._2, Map[AtomicFormula, Double]()) :: accList)
        }
        else if (tif._1 < til._1) {
          accTimedHappenings(tilseq, tiftail, TimedAction(tif._1, List[Expression](), tif._2.toMap) :: accList)
        }
        else { //they're equal, combine them
          accTimedHappenings(tiltail, tiftail, TimedAction(til._1, til._2, tif._2.toMap) :: accList)
        }
      }
    }

    accTimedHappenings(sortedTils, sortedTifs)
  }.sortBy(th => th.at)

  val initialState = {

   val initialFacts: Set[AtomicFormula] = (timedInitialLiterals.getOrElse(0.0, List[Expression with Modal]()) collect {
      case af: Expression with Modal => af.positiveAtoms  //negative atoms are just missing
   }).flatten.toSet

   val initialFluents: Map[AtomicFormula, Double] =
      timedInitialFluents.getOrElse(0.0, List[(AtomicFormula, Double)]()).toMap

   State(initialFacts, initialFluents)
  }


  val goalState : Expression = { goal.typifyAll(objects.toList).updateTypeHierarchy(domain.types) }
  val goalCondition = goalState.asInstanceOf[Condition]

  lazy val groundedActions : Set[Action] = domain.actions.flatMap(action => action.generateGroundedActions(entityMap))
  lazy val groundedActionsList = groundedActions.toList

  lazy val groundedDurativeActions : Set[DurativeAction] = domain.durativeActions.flatMap(durativeAction => durativeAction.generateGroundedActions(entityMap))

  lazy val groundedDurativeStartActions : Set[SnapAction] = groundedDurativeActions.map(_.startSnapAction)

  lazy val groundedPredicates : Set[AtomicFormula] = domain.predicates.flatMap(pred => pred.generateGroundedAtomicFormulas(entityMap))

  lazy val propositionalAchieverActions : Map[AtomicFormula, Set[Action]] = {
    (for {
      action <- groundedActions
      effect <- action.propositionalEffects._1
    } yield (effect, action)).groupBy(_._1).mapValues(_.map(_._2))
  }

  lazy val propositionalAchieverDurativeActions : Map[AtomicFormula, Set[DurativeAction]] = {
    (for {
      durativeAction <- groundedDurativeActions
      allEffects = durativeAction.propositionalEffects(TimeSpecifier.AT_START)._1 ++ durativeAction.propositionalEffects(TimeSpecifier.AT_END)._1
      effect <- allEffects
    } yield (effect, durativeAction)).groupBy(_._1).mapValues(_.map(_._2))
  }

  lazy val numericAchieverActions : Map[AtomicFormula, Set[Action]] = {
    (for {
      action <- groundedActions
      effect <- action.numericEffects
    } yield (effect, action)).groupBy(_._1).mapValues(_.map(_._2))
  }

  lazy val numericAchieverDurativeActions : Map[AtomicFormula, Set[DurativeAction]] = {
    (for {
      durativeAction <- groundedDurativeActions
      allEffects : Set[AtomicFormula] = durativeAction.numericEffects(TimeSpecifier.AT_START) ++
                                          durativeAction.numericEffects(TimeSpecifier.AT_END) ++
                                            durativeAction.continuousEffects.flatMap(_.numericFluents).map(_.functionPredicate)

      effect : AtomicFormula <- allEffects
    } yield (effect, durativeAction)).groupBy(_._1).mapValues(_.map(_._2))
  }

  override def toString : String = "(problem " + name + " " + domain + "\n" +
    "objects : "  + objects + "\n" +
    "initialState: " + initialState + "\n" +
    "goalState: " + goalState  + "\n)"
}
