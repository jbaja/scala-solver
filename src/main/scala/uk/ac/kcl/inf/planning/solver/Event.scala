/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver

import uk.ac.kcl.inf.planning.solver.expressions._

/**
 * The events that are possible to trigger in a plan.
 * Each Event has a name, a sequence of parameters (terms), a precondition expression and an effect expression.
 *
 * Code here is experimental and does not work yet!!!
 *
 * Date: 14/11/2013 Time: 15:43
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 */
case class Event(name: String, parameters: List[Term], preconditionExpression: Expression, effectExpression: Expression)
{
  val precondition : Condition = preconditionExpression.asInstanceOf[Condition]
  val effect : Effect = effectExpression.asInstanceOf[Effect]

  /**
   * @return true if all the parameters, preconditions and effects are grounded
   */
  lazy val isGrounded : Boolean =
    parameters.forall(term => term.isGrounded) &&
      preconditionExpression.isGrounded &&
      effectExpression.isGrounded

  /**
   * Helper value for the parameters that have not yet been bound to a value
   */
  lazy val liftedParameters: List[LiftedTerm] =
    parameters collect {
      case pt: LiftedTerm => pt
    }

  /**
   * Substitutes a lifted parameter with a grounded argument.
   * @param parameter The lifted parameter
   * @param argument The grounded argument
   * @return A new event instance with the parameter substituted with the argument if the lifted parameter was found,
   *         otherwise it returns itself.
   */
  def substitute(parameter: LiftedTerm, argument: GroundedTerm): Event =
  {
    //here we assume that a parameter only occurs once in the Event parameter list
    val i = parameters.indexOf(parameter)
    if (i >= 0)
    {
      Event(name, parameters.updated(i, argument),
        preconditionExpression.substitute(parameter, argument),
        effectExpression.substitute(parameter, argument))
    }
    else
      this
  }

  /**
   * Substitutes a list of parameters with their respective grounded terms
   * @param pairList a list of pairs, the parameter and respective argument to be substituted with
   * @return the event with the specified parameters replaced by their respective arguments
   */
  def substituteList(pairList : List[(LiftedTerm, GroundedTerm)]) : Event =
  {
    pairList match
    {
      case (parameter, argument) :: tail =>
        substitute(parameter, argument).substituteList(tail)

      case Nil => this //all substitutions done
    }
  }

  def updateTypeHierarchy(newTermType : TermType) : Event =
  {
    def updateTypeHierarchyParameters(from : Int, eventAcc : Event) : Event =
    {
      var i = eventAcc.parameters.indexWhere(parameter => parameter.termType.name == newTermType.name, from)
      if (i >= 0)
      {
        updateTypeHierarchyParameters(i+1, Event(eventAcc.name,
          eventAcc.parameters.updated(i, eventAcc.parameters(i).updateTypeHierarchy(newTermType)),
          eventAcc.preconditionExpression, eventAcc.effectExpression))
      }
      else
        eventAcc
    }

    updateTypeHierarchyParameters(0,
      Event(name, parameters, preconditionExpression.updateTypeHierarchy(newTermType), effectExpression.updateTypeHierarchy(newTermType)))
  }

  def updateTypeHierarchy(typeList : List[TermType]) : Event =
  {
    typeList match
    {
      case termType :: tail =>
        updateTypeHierarchy(termType).updateTypeHierarchy(tail)

      case Nil => this
    }
  }

  /**
   * @return a pair with the positive effects followed with the negative effects
   */
  val effects : (Set[AtomicFormula], Set[AtomicFormula]) = {
    effectExpression match {
      case modalEffect : Modal => modalEffect.atoms
      case _ => (Set[AtomicFormula](), Set[AtomicFormula]())
    }
  }

  lazy val positiveEffects  = effects._1
  lazy val negativeEffects = effects._2

  /**
   * @return a pair with the positive effects followed with the negative effects
   */
  lazy val preconditions : (Set[AtomicFormula], Set[AtomicFormula]) =
  {
    preconditionExpression match {
      case modalCondition : Modal => modalCondition.atoms
      case _ => (Set[AtomicFormula](), Set[AtomicFormula]())
    }
  }

  lazy val positivePreconditions : Set[AtomicFormula] = preconditions._1
  lazy val negativePreconditions : Set[AtomicFormula] = preconditions._2

  /**
   * Checks if this event is applicable against a set of facts
   * @param state the facts that are holding true
   * @return true if the event is applicable, false otherwise
   */
  def isApplicable(state : State) : Boolean = { precondition.satisfiedBy(state) }

  /**
   * Generates the grounded events for this event
   * @param possibleValues The possible values for each type
   * @return
   */
  def generateGroundedEvents(possibleValues : Map[TermType, Set[GroundedTerm]]) : Set[Event] =  {
    def generateGroundedEventsAcc(accSubstitutions: List[(LiftedTerm, GroundedTerm)], remainder: List[LiftedTerm]): Set[Event] =  {
      remainder match {
        case Nil => Set(substituteList(accSubstitutions))

        case parameter :: tail => for
        {
          argument <- possibleValues(parameter.termType) if possibleValues.contains(parameter.termType)
          rest <- generateGroundedEventsAcc((parameter, argument) :: accSubstitutions, tail)
        } yield rest
      }
    }

    generateGroundedEventsAcc(Nil, liftedParameters)
  }


  /**
   * Apply an event to a state, which will give us another state.
   * This follows the FF rule that first negative effects are removed, than positive effects are added
   * @param state The state to which this event will be applied
   * @return The new state after applying this event
   */
  def applyToState(state: State) : State =  {
    val numericUpdates = effectExpression match {
      case numericEffect : NumericEffect => numericEffect.evaluateNumeric(state)
      case _ => Map[AtomicFormula, Double]()
    }

    State(state.modalFacts -- negativeEffects ++ positiveEffects, state.numericFluents ++ numericUpdates )
  }

  override val toString = parameters.foldLeft("(" + name)((str, term) => str + " " + term) + ")"
}
