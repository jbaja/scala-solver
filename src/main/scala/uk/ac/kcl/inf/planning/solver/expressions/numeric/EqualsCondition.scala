/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.numeric

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.GroundedTerm
import uk.ac.kcl.inf.planning.solver.TermType
import uk.ac.kcl.inf.planning.solver.LiftedTerm
import uk.ac.kcl.inf.planning.solver.expressions.{ComparisonCondition, NumericCondition, Condition, Expression}
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState

/**
 *
 * Date: 10/02/14
 * Time: 17:26 
 * @author josef
 */
case class EqualsCondition(expression1 : Expression, expression2 : Expression) extends Expression.Aux[EqualsCondition] with ComparisonCondition
{
  val numeric1: Numeric = expression1.asInstanceOf[Numeric]
  val numeric2: Numeric = expression2.asInstanceOf[Numeric]

  val normalisedConstant = numeric2.constant - numeric1.constant
  val normalisedExpression = SubtractNumeric(
    SubtractNumeric(expression1, NumericConstant(numeric1.constant)),
    SubtractNumeric(expression2, NumericConstant(numeric2.constant))
  )

  /**
   * Updates the type hierarchy of the specified type. While parsing the full type hierarchy might not be immediately
   * known, and determined from a separate type list. This method helps populate the type hierarchy properly.
   * @param termType - the full type to be populated
   * @return The expression updated with the right type information
   */
  override def updateTypeHierarchy(termType: TermType): EqualsCondition = copy(expression1 = expression1.updateTypeHierarchy(termType),
                                                                               expression2 = expression2.updateTypeHierarchy(termType))

  /**
   * If one of the lifted terms is untyped and has the same name, it is replaced by the specified typed parameter.
   * This is used mainly in places where the expression is being parsed without type information (such as PDDL) and type
   * information is provided afterwards.
   * @param typedParameter - The typed parameter to replace the untyped one (which has the same parameter name)
   * @return an expression with the typed parameter
   */
  override def typify(typedParameter: Term): EqualsCondition = copy(expression1 = expression1.typify(typedParameter), expression2 = expression2.typify(typedParameter))

  /**
   * Substitutes the lifted parameter with the grounded argument term.
   * @param parameter The lifted parameter to be substituted
   * @param argument The grounded argument to substitute the parameter
   * @return A new instance of the expression with the parameter substituted by the argument
   */
  override def substitute(parameter: LiftedTerm, argument: GroundedTerm): EqualsCondition = copy(expression1 = expression1.substitute(parameter, argument),
                                                                                                 expression2 = expression2.substitute(parameter, argument))

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): EqualsCondition =
    copy(expression1 = expression1.hookExternalEvaluator(evaluators), expression2 = expression2.hookExternalEvaluator(evaluators))


  /**
   * @return true if this expression is fully grounded, false otherwise
   */
  override def isGrounded: Boolean = expression1.isGrounded && expression2.isGrounded

  /**
   * Determines whether this condition is satisfied in the provided state.
   * @param state The state to be checked for this condition.
   * @return true if the state satisfies the condition, false otherwise
   */
  def satisfiedBy(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): Boolean =
    (numeric1.value(state, context), numeric2.value(state, context)) match {
      case (Some(number1), Some(number2)) => number1 == number2
      case _ => false
    }

  override def relaxedSatisfiedBy(relaxedState: RelaxedState): Boolean = {
    val smallestMax = Math.min(numeric1.maxValue(relaxedState), numeric2.maxValue(relaxedState))
    val largestMin = Math.max(numeric1.minValue(relaxedState), numeric2.minValue(relaxedState))

    largestMin <= smallestMax
  }


  override def violations(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): Set[Condition] =
    if (satisfiedBy(state, context)) Set[Condition]() else Set[Condition](this)

  override def invert: Expression with Condition = NotEqualsCondition(expression1, expression2)

  override def equals(obj: scala.Any): Boolean = obj.getClass().equals(this.getClass()) &&
    (obj.asInstanceOf[this.type].expression1.equals(this.expression1) && obj.asInstanceOf[this.type].expression2.equals(this.expression2))

  override def toLinearCondition(timeDependentVariables : Set[AtomicFormula], state : State, t : Double = 0.0) : Set[LinearCondition] = {
    (expression1, expression2) match {
      case (linear1 : Linear, linear2 : Linear) =>
        Set[LinearCondition](LinearCondition(linear1.linearFormula(timeDependentVariables, state, t),
          linear2.linearFormula(timeDependentVariables, state, t),
          NumericConstraint.EQ))
    }
  }

  override def isTemporallyDependent(timeDependentFluents: Set[AtomicFormula]): Boolean = {
    ((numeric1.fluents.map(_.functionPredicate) union numeric2.fluents.map(_.functionPredicate))
      intersect timeDependentFluents).nonEmpty
  }

  override def getNonTemporallyDependentConditions(timeDependentFluents: Set[AtomicFormula]): Set[Condition] = {
    if (isTemporallyDependent(timeDependentFluents)) Set[Condition]()
    else Set[Condition](this)
  }

  val fluents : Set[AtomicFormula] = numeric1.fluents.map(_.functionPredicate) union numeric2.fluents.map(_.functionPredicate)
}
