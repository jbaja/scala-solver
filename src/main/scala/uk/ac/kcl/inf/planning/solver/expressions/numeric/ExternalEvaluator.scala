/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.numeric

import uk.ac.kcl.inf.planning.solver.State
import uk.ac.kcl.inf.planning.solver.expressions.Expression

/**
 * Trait that must be implemented by an external evaluator of a numeric value
 * Created by Josef on 31/01/2015.
 */
trait ExternalEvaluator {
  def fullName : String

  def aliasPrefix : String

  def value(fluent : NumericFluent, state: State, t : Double = 0.0): Option[Double]

  def isConstant(fluent: NumericFluent, state : State, t : Double): Boolean

  def aliasedName(suffix : String) : String = String.format("%s.%s", aliasPrefix, suffix)

  def handleStaticFluent(fluent : NumericFluent, state : State) : Option[Double] = {
    state.numericFluents.get(fluent.functionPredicate) match {
      case Some(value) => Some(value)

      case None => throw new Exception("Unknown external module predicate/function/method: " + fluent.functionPredicate.predicate + " for " + aliasPrefix)
    }
  }

  override def toString = fullName + "(" + aliasPrefix + ")"
}
