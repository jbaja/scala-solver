/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.validator

/**
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 */
case class Plan(planSteps : List[PlanStep])
{
  override def toString : String = planSteps.foldLeft("")((str, term) => str + "\n" + term)
}

case class PlanStep(at : Double, actionSymbol : String, terms : List[String], duration: Double)
{
  val predicate = terms.foldLeft(actionSymbol)((str, term) => str + " " + term)

  override def toString: String = at + ": (" + predicate + ") [" + duration + "]"
}

