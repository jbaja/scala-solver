/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.temporal

import uk.ac.kcl.inf.planning.solver.expressions.numeric.LinearCondition
import uk.ac.kcl.inf.planning.solver.{AtomicFormula, State}
import uk.ac.kcl.inf.planning.solver.expressions.{NumericCondition, Condition}

/**
 * Represents a 'happening' occurring on the plan. This means an action was applied.
 * The oldState represents the state just before the action was applied, while the newState represents the state
 * just after the action was applied. The invariants represent the set of conditions that are required to hold
 * from this happening onwards (until the next happening).
 *
 * Created by Josef on 29/01/2015.
 * @author Josef Bajada
 */
case class Happening(temporalAction: TemporalAction, oldState : State, newState : State, invariants : Set[Condition]) {
  override def toString: String = {
    temporalAction.toString
  }

  def getLinearInvariantConditions(timeDependentVariables: Set[AtomicFormula]): Set[LinearCondition] = {
    (invariants collect {
      case numericCondition: NumericCondition => numericCondition
    }).flatMap(_.toLinearCondition(timeDependentVariables, newState))
  }
}