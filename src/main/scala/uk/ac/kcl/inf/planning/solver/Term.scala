/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver

/**
 * The term parameters. They can be either Lifted or Grounded
 * Date: 14/11/13 Time: 18:43
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 */
//parameter types
case class TermType(name: String, superType: Option[TermType] = Some(ObjectType)) {
  def isOfType(that: TermType) : Boolean = {
    if (this.name equals that.name) {
      true
    }
    else {
      superType match {
        case Some(superTypeValue) => superTypeValue.isOfType(that)
        case None => false
      }
    }
  }

  override def toString =
    name + (superType match {
      case Some(otherType) => " - " + otherType.toString
      case None => ""
    })
}

object ObjectType extends TermType("object", None)

//terms, which can be lifted or ground terms
sealed abstract class Term(val symbol: String, val termType: TermType) {
  def isGrounded : Boolean

  def updateTypeHierarchy(newTermType : TermType) : Term

  def updateTypeHierarchy(typeList : List[TermType]) : Term = {
    typeList.find(_.name equals termType.name) match {
      case Some(foundTermType) => updateTypeHierarchy(foundTermType)
      case _ => this
    }
  }
}

case class GroundedTerm(name: String, groundedTermType: TermType = ObjectType) extends Term(name, groundedTermType) {
  override def isGrounded = true

  override val toString = name /*+ " - " + groundedTermType*/

  /**
   * Replaces the term type with the new term type (if and only if the names of the term types match).
   * This is used to populate any term hierarchies not available initially, such as during PDDL parsing
   * @param newTermType the new term type
   * @return the new GroundedTerm with the new term type
   */
  def updateTypeHierarchy(newTermType : TermType) = {
    if (newTermType.name equals groundedTermType.name)
      GroundedTerm(name, newTermType)
    else
      this
  }
}

case class LiftedTerm(parameter: String, liftedTermType: TermType = ObjectType) extends Term(parameter, liftedTermType) {
  override def isGrounded = false

  override val toString = "?" + parameter + " - " + liftedTermType

  /**
   * Replaces the term type with the new term type (if and only if the names of the term types match).
   * This is used to populate any term hierarchies not available initially, such as during PDDL parsing
   * @param newTermType the new term type
   * @return the new GroundedTerm with the new term type
   */
  def updateTypeHierarchy(newTermType : TermType) = {
    if (newTermType.name equals liftedTermType.name)
      LiftedTerm(parameter, newTermType)
    else
      this
  }
}
