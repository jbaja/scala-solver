/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.numeric

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.State
import uk.ac.kcl.inf.planning.solver.GroundedTerm
import uk.ac.kcl.inf.planning.solver.LiftedTerm
import uk.ac.kcl.inf.planning.solver.AtomicFormula
import uk.ac.kcl.inf.planning.solver.expressions.Expression
import uk.ac.kcl.inf.planning.solver.ff.ValueBounds
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState

/**
 *
 * Date: 10/02/14
 * Time: 17:26 
 * @author josef
 */
case class NumericFluent(functionPredicate : AtomicFormula, externalEvaluator : Option[ExternalEvaluator] = None) extends Expression.Aux[NumericFluent] with Numeric with Linear
{
  /**
   * Updates the type hierarchy of the specified type. While parsing the full type hierarchy might not be immediately
   * known, and determined from a separate type list. This method helps populate the type hierarchy properly.
   * @param termType - the full type to be populated
   * @return The expression updated with the right type information
   */
  override def updateTypeHierarchy(termType: TermType): NumericFluent = copy(functionPredicate = functionPredicate.updateTypeHierarchy(termType))

  /**
   * If one of the lifted terms is untyped and has the same name, it is replaced by the specified typed parameter.
   * This is used mainly in places where the expression is being parsed without type information (such as PDDL) and type
   * information is provided afterwards.
   * @param typedParameter - The typed parameter to replace the untyped one (which has the same parameter name)
   * @return an expression with the typed parameter
   */
  override def typify(typedParameter: Term): NumericFluent = copy(functionPredicate = functionPredicate.typify(typedParameter))

  /**
   * Substitutes the lifted parameter with the grounded argument term.
   * @param parameter The lifted parameter to be substituted
   * @param argument The grounded argument to substitute the parameter
   * @return A new instance of the expression with the parameter substituted by the argument
   */
  override def substitute(parameter: LiftedTerm, argument: GroundedTerm): NumericFluent = copy(functionPredicate = functionPredicate.substitute(parameter, argument))

  def hookExternalEvaluator(evaluators : Map[String, ExternalEvaluator]) : NumericFluent = {
    val segments = this.functionPredicate.predicate.split("""\.""", 2)
    if ((segments.size == 2) &&
        (segments(0) != "") &&
        evaluators.get(segments(0)).isDefined) {
      println("Attaching " + this + " to " + evaluators(segments(0)))
      copy(externalEvaluator = evaluators.get(segments(0)))
    }
    else {
      this
    }
  }

  /**
   * @return true if this expression is fully grounded, false otherwise
   */
  override def isGrounded: Boolean = functionPredicate.isGrounded

  /**
   * Provides the numeric value of this entity for this state
   * @param state the current state
   * @return An option with a double value Some(Double), or None if it is undefined in the current state.
   */
  override def value(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): Option[Double] =
    if (!isGrounded)
      None
    else {
      externalEvaluator match {
        case Some(evaluator) => evaluator.value(this, state)

        case None => state.numericFluents.get(functionPredicate).orElse(context.get(this))
      }
    }



  override def relaxedBounds(relaxedState: RelaxedState): Option[ValueBounds] = {
    externalEvaluator match {
      case Some(evaluator) => {
        //todo: fix this properly for continuous effects, to use the actual time elapsed from the TRPGne
        val minStateVal = evaluator.value(this, relaxedState.minState)
        val maxStateVal = evaluator.value(this, relaxedState.maxState)
        (minStateVal, maxStateVal) match {
          case (Some(minVal), Some(maxVal)) => Some(ValueBounds(minVal, maxVal))

          case _ => None
        }
      }

      case None => relaxedState.numericFluentBounds.get(functionPredicate)
    }
  }

  override val fluents: Set[NumericFluent] = Set[NumericFluent](this)

  //the coeffecients of all variables and a constant
  override def linearFormula(timeDependentVariables : Set[AtomicFormula], state: State, t : Double = 1.0): LinearFormula = {
      if (timeDependentVariables.contains(functionPredicate)) {
        LinearFormula(Map[AtomicFormula, Double]((functionPredicate, 1.0)), 0.0)
      }
      else {
        //if it is not a continuous variable we can resolve it
        val constVal = externalEvaluator match {
          case Some(evaluator) => evaluator.value(this, state, t).get

          case None => value(state).getOrElse(0.0)
        }
        LinearFormula(Map[AtomicFormula, Double](), constVal)
      }
  }

  override def isConstant(timeDependentVariables : Set[AtomicFormula], state : State, t : Double): Boolean = {
    externalEvaluator match {
      case Some(evaluator) => evaluator.isConstant(this, state, t)

      case None => !timeDependentVariables.contains(this.functionPredicate)
    }
  }

  override def toString: String = functionPredicate.toString

  override def constant: Double = 0.0
}
