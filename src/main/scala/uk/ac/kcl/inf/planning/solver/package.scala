/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning

import uk.ac.kcl.inf.planning.solver.temporal.TemporalAction

/**
 * Convenience types to make the code more readable.
 * Date: 15/11/13
 * Time: 18:08 
 * @author josef
 */
package object solver
{
  /**
   * Alias for a Set of AtomicFormula where they are grouped together to represent a state.
   */
  case class State(modalFacts: Set[AtomicFormula] = Set[AtomicFormula](), numericFluents : Map[AtomicFormula, Double] = Map[AtomicFormula, Double]())

  /**
   * Alias for the LiftedTerm where Action is used, just for clarity.
   */
  type Parameter = LiftedTerm

  /**
   * Alias for a List of Actions in sequence, forming a plan
   */
  type Plan = List[Action]

  /**
    * Alias for a list of temporal actions in a sequence.
    */
  type ActionSequence = List[TemporalAction]

  /**
   * Helper Type of a pair of pairs, which associates a (grounded) action and the state it leads to, together with the plan that leads to it (in reverse)
   * with the heuristic value (Int) for the state and the set of helpful actions one should attempt after going to this state.
   */
  type ActionStateHeuristicInfo = ((Action, State, Plan), (Int, Set[Action]))
  //todo: Can we change ActionStateHeuristicInfo to a normal class? The tuple of tuples seems cryptic

  case class GoalUnreachableException(message : String) extends Exception
}
