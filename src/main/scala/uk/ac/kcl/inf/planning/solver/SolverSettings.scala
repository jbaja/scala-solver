/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver

/**
  * A set of mutable settings that are used around.
  * The reason that they are immutable is just so that they can be easily changed from the commandline and set.
  * (This will be changed to a separate immutable class initialised after reading the parameters, but for now this will do)
  * @author Josef Bajada
  */
case class SolverSettings (USE_Brfs : Boolean = false,
                           EHC_AB : Boolean = true,
                           EHC_H_ORDERING_THRESHOLD : Int = 25,
                           EPSILON : Double = 0.001,
                           MAX_ERROR : Double = 1.0,
                           MAX_IMPROVEMENT_ITERATIONS : Int = 10000,
                           USE_NUMERIC_HEURISTIC : Boolean = true,
                           USE_NUMERIC_HEURISTIC_LINEARONLY : Boolean = true,
                           PARALLEL : Boolean = false
                          )

object SolverSettings {
  var settings = SolverSettings()
}


