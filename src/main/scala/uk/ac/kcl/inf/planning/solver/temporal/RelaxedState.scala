/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.temporal

import uk.ac.kcl.inf.planning.solver.expressions.temporal.ContinuousNumericEffect
import uk.ac.kcl.inf.planning.solver.ff.ValueBounds
import uk.ac.kcl.inf.planning.solver.{SolverSettings, Problem, State, AtomicFormula}

/**
 *
 * Date: 10/03/15
 * Time: 15:39 
 * @author josef
 */
case class RelaxedState(modalFacts : Set[AtomicFormula] = Set[AtomicFormula](),
                        negatedFacts : Set[AtomicFormula] = Set[AtomicFormula](),
                        numericFluentBounds : Map[AtomicFormula, ValueBounds] = Map[AtomicFormula, ValueBounds](),
                        continuousEffects : List[ContinuousNumericEffect] = List[ContinuousNumericEffect]()
                        )
{
  val minState = State(modalFacts, numericFluentBounds.map(fluentBounds => (fluentBounds._1, fluentBounds._2.min)))

  val maxState = State(modalFacts, numericFluentBounds.map(fluentBounds => (fluentBounds._1, fluentBounds._2.max)))

  /**
   * Whether this state has reached a fixed point in relation to the previous state and the problem at hand.
   * @param previousRelaxedState - the previous relaxed state
   * @param problem - the problem being solved
   * @return true if we have reached a fixed point, i.e. the goal is not reachable, false otherwise
   */
  def reachedFixedPoint(previousRelaxedState : RelaxedState, problem : Problem) : Boolean = {
    if (this.modalFacts.equals(previousRelaxedState.modalFacts) && this.negatedFacts.equals(previousRelaxedState.negatedFacts)) {
      if (this.numericFluentBounds.equals(previousRelaxedState.numericFluentBounds)) {
        true
      }
      else {
        val extremeFluentBounds: Map[AtomicFormula, ValueBounds] = numericFluentBounds.map(fluentBound => {
          val oldValueBounds: Option[ValueBounds] = previousRelaxedState.numericFluentBounds.get(fluentBound._1)
          fluentBound._1 -> oldValueBounds.map(ValueBounds.infinitize(_, fluentBound._2)).getOrElse(fluentBound._2)
        })

        val extremeRelaxedState = RelaxedState(modalFacts, negatedFacts, extremeFluentBounds)

        //if even at the extremes the goal can't be satisfied, we have a dead end
        !problem.goalCondition.relaxedSatisfiedBy(extremeRelaxedState)
      }
    }
    else
    {
      false
    }
  }

  override def toString: String = "[Facts: " + modalFacts.toString() + " Numeric Bounds: " + numericFluentBounds + "]"
}

object RelaxedState
{
  def fromState(state : State) : RelaxedState = {
    RelaxedState(state.modalFacts, Set[AtomicFormula](),
      state.numericFluents.map(fluentValue =>
        (fluentValue._1, ValueBounds(fluentValue._2, fluentValue._2))))
  }

  def fromTemporalState(temporalState : TemporalState) : RelaxedState = {
    val runningActionDummyFacts = temporalState.runningActions.map(_.startAction.durativeAction.dummyProposition)

    val relaxedState = fromState(temporalState.state)
    applyContinuousEffects(relaxedState.copy(modalFacts = relaxedState.modalFacts ++ runningActionDummyFacts,
                                             numericFluentBounds = relaxedState.numericFluentBounds ++ temporalState.fluentBounds,
                                             continuousEffects = temporalState.continuousEffects))
  }

  def applyContinuousEffects(relaxedState : RelaxedState) : RelaxedState = {
    relaxedState.continuousEffects.foldLeft(relaxedState)((accState, effect) => {
      effect.relaxedEffect(relaxedState, accState, SolverSettings.settings.EPSILON)
    })
  }
}
