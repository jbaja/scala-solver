/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.temporal

import uk.ac.kcl.inf.planning.solver.expressions._
import uk.ac.kcl.inf.planning.solver.expressions.numeric._
import uk.ac.kcl.inf.planning.solver.{SolverSettings, Problem, State, AtomicFormula}

/**
  * Represents a timed initial fluent or timed initial literal.
  *
  * Date: 06/11/15
  * Time: 12:05 
  * @author Josef Bajada
  */
case class TimedAction(at : Double, tils : List[Expression], tifs : Map[AtomicFormula, Double]) extends TemporalAction {

  lazy val tifsEffects : Iterable[AssignEffect] = tifs.map{ case (af, v) => AssignEffect(NumericFluent(af), NumericConstant(v)) }

  lazy val effect : Effect = AndEffect(tils ++ tifsEffects)

  override def applyToTemporalState(temporalState: TemporalState): Option[TemporalState] = {
    val numericUpdates = tifs.toMap
    val newState = State(temporalState.state.modalFacts -- negativeEffects ++ positiveEffects,
      temporalState.state.numericFluents ++ numericUpdates)

    val index = temporalState.plan.size //we are adding it at the end

    val invariants = temporalState.runningActions.flatMap(_.startAction.invariants).toSet

    val newActionTemporalConstraints = if (index > 0)
      temporalState.temporalConstraints +
          (AbsoluteTemporalConstraint(index, NumericConstraint.EQ, at),
          IntervalTemporalConstraint(index - 1, index, NumericConstraint.GEQ, SolverSettings.settings.EPSILON))
    else
      temporalState.temporalConstraints + AbsoluteTemporalConstraint(index, NumericConstraint.EQ, at)

    val newTemporalState = TemporalState(
      temporalState.problem,
      newState.modalFacts,
      newState.numericFluents,
      temporalState.timedActions.tail, //we are assuming the head is this action!
      temporalState.runningActions,
      temporalState.plan ::: List(Happening(this, temporalState.state, newState, invariants)),
      newActionTemporalConstraints
    )

    newTemporalState.scheduleState()
  }

  def applyToRelaxedState(oldRelaxedState : RelaxedState, cumulativeRelaxedState : RelaxedState, elapsedTime : Double = 0) : RelaxedState = {
    effect.relaxedEffect(oldRelaxedState, cumulativeRelaxedState)
  }


  override def getLinearEffects(timeDependentVariables: Set[AtomicFormula], state: State): List[LinearEffect] = List[LinearEffect]()

  override def negativePropositionalPreconditions: Set[AtomicFormula] = Set[AtomicFormula]()

  override def positivePropositionalPreconditions: Set[AtomicFormula] = Set[AtomicFormula]()

  override def isApplicableRelaxed(relaxedState: RelaxedState): Boolean = true

  val propositionalEffects : (Set[AtomicFormula], Set[AtomicFormula]) = (tils collect {
      case modalEffect : Modal => modalEffect
    }).foldLeft((Set[AtomicFormula](), Set[AtomicFormula]()))((acc, modal) => (acc._1 ++ modal.atoms._1, acc._2 ++ modal.atoms._2))

  override val negativeEffects: Set[AtomicFormula] = propositionalEffects._2

  override def positiveEffects: Set[AtomicFormula] = propositionalEffects._1

  override val numericEffects: Set[AtomicFormula] = tifs.keySet

  override lazy val precondition: Condition = Empty()

  override lazy val invariant: Condition = Empty()

  override def getLinearPreconditions(timeDependentVariables: Set[AtomicFormula], state: State): Set[LinearCondition] = Set[LinearCondition]()
}
