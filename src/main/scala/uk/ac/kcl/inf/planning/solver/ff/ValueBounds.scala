/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.ff

import scala.collection.mutable.ListBuffer

/**
  * Represents the lower and upper bounds of a numeric fluent, in a relaxed state.
  * @author Josef Bajada
  */
case class ValueBounds (min : Double, max : Double) {
  def combine(that : ValueBounds): ValueBounds = {
    ValueBounds.assignCombiner(this, that)
  }

  def combine(that: Option[ValueBounds]) : ValueBounds = {
    that.map(vb => ValueBounds.assignCombiner(this, vb)).getOrElse(this)
  }

  def combine(that : ValueBounds, combiner: (ValueBounds, ValueBounds) => ValueBounds): ValueBounds = {
    combiner(this, that)
  }

  def combine(that: Option[ValueBounds], combiner: (ValueBounds, ValueBounds) => ValueBounds) : ValueBounds = {
    that.map(vb => combiner(this, vb)).getOrElse(this)
  }

  def >(that: ValueBounds) : Boolean = this.max > that.max

  def <(that: ValueBounds) : Boolean = this.min < that.min

  def <>(that: ValueBounds) : Boolean = this < that && this > that

  def ><(that: ValueBounds) : Boolean = that < this && that > this

  def ==(that: ValueBounds) : Boolean = this.min == that.min && this.max == that.max

  def !=(that: ValueBounds) : Boolean = this.min != that.min && this.max != that.max

  lazy val negative : ValueBounds = ValueBounds(- this.min, - this.max)

  def multiply(multiplier : Double) = ValueBounds(this.min * multiplier, this.max * multiplier)
}

object ValueBounds {

  def infinitize(oldBounds : Option[ValueBounds], newBounds : Option[ValueBounds]) : Option[ValueBounds] = {
    (oldBounds, newBounds) match {
      case (Some(oldVB), Some(newVB)) =>
        Some(infinitize(oldVB, newVB))

      case _ => None
    }
  }

  def infinitize(oldBounds : ValueBounds, newBounds : ValueBounds) : ValueBounds = {
    val min = if (newBounds < oldBounds) Double.NegativeInfinity else oldBounds.min
    val max = if (newBounds > oldBounds) Double.PositiveInfinity else oldBounds.max
    ValueBounds(min, max)
  }

  val assignCombiner = (vb1 : ValueBounds, vb2 : ValueBounds) =>
    ValueBounds(Math.min(vb1.min, vb2.min), Math.max(vb1.max, vb2.max))

  val increaseCombiner = (vb: ValueBounds, deltas : ValueBounds) => {
    val newMin = vb.min + deltas.min
    val newMax = vb.max + deltas.max

    ValueBounds(Math.min(vb.min, newMin),
                Math.max(vb.max, newMax))
  }

  val decreaseCombiner = (vb: ValueBounds, deltas : ValueBounds) => {
    val newMin = vb.min - deltas.max
    val newMax = vb.max - deltas.min

    ValueBounds(Math.min(vb.min, newMin),
                Math.max(vb.max, newMax))
  }

  val multiplyCombiner = (vb: ValueBounds, multipliers : ValueBounds) => {
    val v1 = vb.min * multipliers.min
    val v2 = vb.min * multipliers.max
    val v3 = vb.max * multipliers.min
    val v4 = vb.max * multipliers.max

    val l = List[Double](vb.min, vb.max, v1, v2, v3, v3)

    ValueBounds(l.min, l.max)
  }

  val divideCombiner = (vb: ValueBounds, divisors : ValueBounds) => {
    //todo: check if this is the correct way to handle division by 0
    //todo: if both divisor bounds are 0, should an effect be applicable?

    val list = ListBuffer(vb.min, vb.max)
    if (divisors.min != 0)
    {
      list += vb.min / divisors.min
      list += vb.max / divisors.min
    }

    if (divisors.max != 0)
    {
      list += vb.min / divisors.max
      list += vb.max / divisors.max
    }

    ValueBounds(list.min, list.max)
  }

  val INFINITY_BOUNDS : ValueBounds = ValueBounds(Double.NegativeInfinity, Double.PositiveInfinity)
}





