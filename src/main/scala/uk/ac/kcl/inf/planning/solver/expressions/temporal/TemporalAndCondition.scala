/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.temporal

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.expressions._
import uk.ac.kcl.inf.planning.solver.State
import uk.ac.kcl.inf.planning.solver.AtomicFormula
import uk.ac.kcl.inf.planning.solver.GroundedTerm
import uk.ac.kcl.inf.planning.solver.expressions.numeric.ExternalEvaluator
import scala.Some
import uk.ac.kcl.inf.planning.solver.TermType
import uk.ac.kcl.inf.planning.solver.LiftedTerm
import uk.ac.kcl.inf.planning.solver.expressions.temporal.TimeSpecifier.TimeSpecifier

/**
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 */
case class TemporalAndCondition(expressions : List[Expression]) extends Expression.Aux[TemporalAndCondition] with TimedCondition with TimedModal
{
  /**
   * Updates the type hierarchy of the specified type. While parsing the full type hierarchy might not be immediately
   * known, and determined from a separate type list. This method helps populate the type hierarchy properly.
   * @param termType - the full type to be populated
   * @return The expression updated with the right type information
   */
  override def updateTypeHierarchy(termType: TermType) =
    TemporalAndCondition(expressions.map(exp => exp.updateTypeHierarchy(termType)))

  /**
   * If one of the lifted terms is untyped and has the same name, it is replaced by the specified typed parameter.
   * This is used mainly in places where the expression is being parsed without type information (such as PDDL) and type
   * information is provided afterwards.
   * @param typedParameter - The typed parameter to replace the untyped one (which has the same parameter name)
   * @return an expression with the typed parameter
   */
  override def typify(typedParameter: Term) =
    TemporalAndCondition(expressions.map(exp => exp.typify(typedParameter)))

  /**
   * Substitutes the lifted parameter with the grounded argument term.
   * @param parameter The lifted parameter to be substituted
   * @param argument The grounded argument to substitute the parameter
   * @return A new instance of the expression with the parameter substituted by the argument
   */
  override def substitute(parameter: LiftedTerm, argument: GroundedTerm) =
    TemporalAndCondition(expressions = expressions.map(exp => exp.substitute(parameter, argument)))

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]) : TemporalAndCondition =
    copy(expressions = expressions.map(_.hookExternalEvaluator(evaluators)))

  /**
   * @return true if this expression is fully grounded, false otherwise
   */
  override def isGrounded: Boolean = expressions.forall(exp => exp.isGrounded)

  lazy val timedConditions : Map[TimeSpecifier.TimeSpecifier, List[TemporalConditionExpression]] = (expressions collect {
    case condition : TemporalConditionExpression => condition
  }).groupBy(tc => tc.when)

  lazy val subConjunctions : List[TemporalAndCondition] = expressions collect {
    case andCondition : TemporalAndCondition => andCondition
  }

  override def satisfiedBy(state: State, when: TimeSpecifier.TimeSpecifier): Boolean = {
    timedConditions.get(when) match {
      case Some(list) => list.forall(condition => condition.satisfiedBy(state, when))
      case _ => true
    }
  } && subConjunctions.forall(andCondition => andCondition.satisfiedBy(state, when))

  override def conditions(when: TimeSpecifier): Set[Condition] = {
    timedConditions.get(when) match {
      case Some(list) => list.foldLeft(Set[Condition]())((set, tce) => set ++ tce.conditions(when))
      case _ => Set[Condition]()
    }
  }

  def subConjunctionPositiveAtoms(when: TimeSpecifier) =
    subConjunctions.foldLeft(Set[AtomicFormula]())((s, a) => s union a.positiveAtoms(when))

  def subConjunctionNegativeAtoms(when: TimeSpecifier) =
    subConjunctions.foldLeft(Set[AtomicFormula]())((s, a) => s union a.negativeAtoms(when))


  lazy val temporalConditionExpressions = expressions collect {
    case effect : TemporalEffectExpression => effect
  }

  lazy val timedModalConditions : Map[TimeSpecifier, List[TimedModal]] = (temporalConditionExpressions collect {
    case timedModal : TimedModal => timedModal
  }) groupBy(_.when)

  override def positiveAtoms(when: TimeSpecifier.TimeSpecifier): Set[AtomicFormula] =
    timedModalConditions.getOrElse(when, List[TimedModal]()).foldLeft(Set[AtomicFormula]())(
      (s, a) => s union a.positiveAtoms(when)) union subConjunctionPositiveAtoms(when)


  override def negativeAtoms(when: TimeSpecifier.TimeSpecifier): Set[AtomicFormula] =
    timedModalConditions.getOrElse(when, List[TimedModal]()).foldLeft(Set[AtomicFormula]())(
      (s, a) => s union a.negativeAtoms(when)) union subConjunctionNegativeAtoms(when)

  override def toString = expressions.foldLeft("AND (")((str, exp) => str + " " + exp) + ")"
}
