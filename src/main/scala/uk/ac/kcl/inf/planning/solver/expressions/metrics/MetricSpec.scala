/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.metrics

/**
 *
 * Date: 06/06/14
 * Time: 16:19 
 * @author josef
 */
object OptimizationDirective extends Enumeration {
  type OptimizationDirective = Value
  val MINIMIZE, MAXIMIZE = Value
}
import OptimizationDirective._
import uk.ac.kcl.inf.planning.validator.PlanTimeline

case class MetricSpec(directive : OptimizationDirective, exp: MetricNumeric) extends Ordering[PlanTimeline] {

  def cost(pt : PlanTimeline) = {
    exp.value(pt)
  }

  override def compare(p1: PlanTimeline, p2: PlanTimeline): Int = {
    //we use the plantimeline cost, since it is lazy val, so we only compute it once
    val ord = if (p1.cost < p2.cost) -1
              else if (p1.cost > p2.cost) 1
              else 0

    directive match {
      case MINIMIZE => ord
      case MAXIMIZE => -ord
    }
  }
}