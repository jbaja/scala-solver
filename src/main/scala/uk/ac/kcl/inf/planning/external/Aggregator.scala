/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.external

import uk.ac.kcl.inf.planning.solver.expressions.numeric.{ExternalEvaluator, NumericFluent}
import uk.ac.kcl.inf.planning.solver.{AtomicFormula, State}

/**
 * External module implementing cost accounting for an energy aggregator.
 * @param aliasPrefix - the alias prefix in the PDDL file identifying this module.
 */
case class Aggregator(aliasPrefix : String) extends ExternalEvaluator
{
  val fullName = "Kcl.Planning.APS.Aggregator"

  override def value(fluent : NumericFluent, state: State, t: Double): Option[Double] = {
    //todo: rather than do this all the time, return a function on hook attachment that calls directly the respective function (as defined in PDDLx)

    val fluentName = fluent.functionPredicate.predicate.drop(aliasPrefix.length() + 1) //we add one to remove the .

    fluentName match {
      case "total-cost-dt" => totalCostDt(fluent, state, t)

      case _ => handleStaticFluent(fluent, state)
    }
  }

  //this is expecting fluent to be ("Aggregator.total-cost-dt")
  def totalCostDt(fluent : NumericFluent, state : State, t: Double)  = {
    val interval = if (t > 0) t else 1.0

    val inflexibleLoad = state.numericFluents(AtomicFormula(aliasedName("inflexible-load")))
    val inflexibleLoadDt = state.numericFluents(AtomicFormula(aliasedName("inflexible-load-dt")))
    val flexibleLoad = state.numericFluents(AtomicFormula(aliasedName("flexible-load")))
    val unitPrice = state.numericFluents(AtomicFormula(aliasedName("unit-price")))

    val inflexibleLoadAtT = inflexibleLoad + (inflexibleLoadDt * interval)

    //the difference in load per time due to the gradient (could be positive or negative)
    val deltaBillableInfLoad = ((inflexibleLoadAtT - inflexibleLoad) * interval) / 2
    //take the total if inflexible-load was constant, and add/deduct the delta triangle computed above
    val totalBillableLoad = (flexibleLoad * interval) + (inflexibleLoad * interval) + deltaBillableInfLoad
    val costForPeriod = totalBillableLoad * unitPrice

    //estimate it linearly
    Some(costForPeriod / interval)
  }

  override def isConstant(fluent: NumericFluent, state : State, t : Double): Boolean = {
    val fluentName = fluent.functionPredicate.predicate.drop(aliasPrefix.length() + 1) //we add one to remove the .

    fluentName match {
      case "total-cost-dt" => false

      case _ => true
    }
  }

}
