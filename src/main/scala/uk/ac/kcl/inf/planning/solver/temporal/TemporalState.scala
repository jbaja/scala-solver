/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.temporal

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.expressions.Condition
import uk.ac.kcl.inf.planning.solver.expressions.numeric.{NumericFluent, NumericConstraint}
import uk.ac.kcl.inf.planning.solver.expressions.temporal.{TimeSpecifier, ContinuousNumericEffect}
import uk.ac.kcl.inf.planning.solver.ff.ValueBounds

import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer
import scala.collection.{immutable, mutable, Map}

/**
  * Date: 21/01/15
  * Time: 15:20
  * @author josef
  */

case class TemporalState(problem : Problem,
                         propositions: Set[AtomicFormula],
                         numericFluents: immutable.Map[AtomicFormula, Double],
                         timedActions: List[TimedAction],
                         runningActions: List[RunningAction] = List[RunningAction](),
                         plan: List[Happening] = List[Happening](),
                         temporalConstraints: Set[TemporalConstraint] = Set[TemporalConstraint](),
                         schedule: Option[List[Double]] = None,
                         lpscheduler : Option[LPScheduler] = None
                         ) {

  TemporalState.incrementStateCount()

  lazy val actionSequence : ActionSequence = plan.map(_.temporalAction)

  lazy val trpg: Option[TemporalRPG] = TemporalRPG.reachabilityAnalysis(this)
  /**
    * The invariants that are required to hold at the current state (determined from the current running actions).
    */
  lazy val invariants: Set[Condition] = runningActions.flatMap(_.startAction.invariants).toSet

  /**
    * Checks that the non-temporal-dependent invariants are consistent
    */
  lazy val invariantConsistent: Boolean = {
    invariants.forall(invariant => {
      val conditions = invariant.getNonTemporallyDependentConditions(timeDependentFluents)
      conditions.forall(_.satisfiedBy(state))
    })
  }

  lazy val fluentBounds : Map[AtomicFormula, ValueBounds] = lpscheduler.map(scheduler => scheduler.numericFluentBounds)
                                                                       .getOrElse(Map[AtomicFormula, ValueBounds]())
  /**
    * The grounded end snap actions of running actions currently in the event queue.
    */
  lazy val endSnapActions = runningActions.map(_.endAction)

  lazy val state: State = {
    State(propositions, numericFluents)
  }

  lazy val heuristic = trpg match {
    case Some(temporalRPG) => temporalRPG.heuristic

    case None => Integer.MAX_VALUE
  }

  lazy val helpfulActions = trpg match {
      //todo: fix bug in RelaxedPlan where actions are featuring more than once, distinct here solves this, but should happen before
    case Some(temporalRPG) => temporalRPG.helpfulActions.distinct

    case None => Set[TemporalAction]()
  }

  lazy val continuousEffects: List[ContinuousNumericEffect] =
    planConcurrentEffects.getOrElse(plan.size - 1, List[ContinuousNumericEffect]())

  override lazy val hashCode: Int = state.hashCode()

  //just for debug diagnostics
  def printPlanString() = {
    if (plan.isEmpty)
      println("Plan (0)")
    else {
      schedule.foreach(sc => {
        print("Plan (" + plan.size + " [" + sc.size + "]) ::: ")
        for (i <- plan.indices) {
          val time = schedule.get(i)
          print("| " + time + ": " + plan(i) + ", ")
        }
        println()
      })
    }
  }


  /**
    * All the concurrent continuous effect expressions at each step
    */
  lazy val planConcurrentEffects: Map[Int, List[ContinuousNumericEffect]] = {
    val mapEffects = mutable.Map[Int, List[ContinuousNumericEffect]]()
    val runningList = ListBuffer[SnapAction]()

    for (i <- plan.indices) {
      val happening = plan(i)
      happening.temporalAction match {
        case snapAction: SnapAction =>
          if (snapAction.when.equals(TimeSpecifier.AT_START))
            runningList += snapAction
          else if (snapAction.when.equals(TimeSpecifier.AT_END))
            runningList -= snapAction.startSnapAction.get

        case _ =>
      }

      val concurrentEffectList: List[ContinuousNumericEffect] = runningList.foldLeft(List[ContinuousNumericEffect]())(
        (accList, snapAction) => {
          //todo: handle expressions which have ?duration in them
          accList ++ snapAction.durativeAction.continuousEffects
        })

      mapEffects(i) = concurrentEffectList
    }

    mapEffects.toMap
  }

  //todo: should we construct an STN to determine the bounds from constraints of previous actions?
  lazy val stateBounds: Map[Int, (Option[Double], Option[Double])] = {
    val constraintMap: immutable.Map[Int, Set[IntervalTemporalConstraint]] = (temporalConstraints collect {
      case itc: IntervalTemporalConstraint => itc
    }).filter(constraint => constraint.upperStep - constraint.lowerStep == 1)
      .groupBy(_.lowerStep)

    val bounds = constraintMap.map(entry => {
      entry._1 -> computeBounds(entry._2.toList)
    })

    bounds
  }

  def getStateBound(stateIndex: Int): (Double, Double) = {
    stateBounds.get(stateIndex) match {
      case Some(optBounds) => (optBounds._1.getOrElse(0.0), optBounds._2.getOrElse(1.0))

      case None => (0.0, 1.0)
    }
  }

  @tailrec
  private def computeBounds(temporalConstraints: List[IntervalTemporalConstraint], lowerBound: Option[Double] = None, upperBound: Option[Double] = None): (Option[Double], Option[Double]) = {
    temporalConstraints match {
      case Nil => (lowerBound, upperBound)

      case temporalConstraint :: tail =>
        temporalConstraint.constraint match {
          case NumericConstraint.EQ => (Some(temporalConstraint.bound), Some(temporalConstraint.bound))

          case NumericConstraint.GEQ =>
            if (temporalConstraint.bound > lowerBound.getOrElse(0.0))
              computeBounds(tail, Some(temporalConstraint.bound), upperBound)
            else
              computeBounds(tail, lowerBound, upperBound)

          case NumericConstraint.GRE =>
            if (temporalConstraint.bound > lowerBound.getOrElse(0.0))
              computeBounds(tail, Some(temporalConstraint.bound), upperBound)
            else
              computeBounds(tail, lowerBound, upperBound)

          case NumericConstraint.LEQ =>
            if (temporalConstraint.bound < upperBound.getOrElse(Double.MaxValue))
              computeBounds(tail, lowerBound, Some(temporalConstraint.bound))
            else
              computeBounds(tail, lowerBound, upperBound)

          case NumericConstraint.LES =>
            if (temporalConstraint.bound < upperBound.getOrElse(Double.MaxValue))
              computeBounds(tail, lowerBound, Some(temporalConstraint.bound))
            else
              computeBounds(tail, lowerBound, upperBound)
        }
    }
  }

  /**
    * All the numeric fluents modified concurrently for each step via continuous effects
    */
  lazy val concurrentlyModifiedFluents: Map[Int, immutable.Set[AtomicFormula]] = {
    planConcurrentEffects.map(entry =>
      entry._1 -> entry._2.map(contEffect => contEffect.fluent.functionPredicate).toSet
    )
  }

  //todo: fix this, if a fluent is assigned directly it becomes non-time-dependent again
  //todo: check what is happening to fluents which depend on time-dependent fluents
  /**
    * Cumulative map of time dependent effects, i.e. if one effect appeared in step i, it will appear for all steps j where j >= i
    */
  lazy val timeDependentFluentsAtHappening: Map[Int, immutable.Set[AtomicFormula]] = {

    val mapTimeDependentFluents = mutable.Map[Int, immutable.Set[AtomicFormula]]()
    for (step <- 0 until concurrentlyModifiedFluents.keySet.size) {
      mapTimeDependentFluents(step) = concurrentlyModifiedFluents(step)
      if (step > 0)
        mapTimeDependentFluents(step) = mapTimeDependentFluents(step) ++ mapTimeDependentFluents(step - 1)
    }

    mapTimeDependentFluents.toMap
  }

  lazy val timeDependentFluents: Set[AtomicFormula] = {
    if (plan.nonEmpty)
      timeDependentFluentsAtHappening(plan.size - 1)
    else
      Set[AtomicFormula]()
  }

  lazy val (linearGradients, nonLinearGradientApproximations) = compileGradients()


  lazy val nonLinearEffects: Map[Int, List[ContinuousNumericEffect]] = {
    nonLinearGradientApproximations.map(entry => {
      entry._1 -> entry._2.map(tuple => tuple._1)
    })
  }

  lazy val nonLinearFluents : Set[AtomicFormula] = nonLinearEffects.foldLeft(Set[NumericFluent]())((acc, entry) => acc ++ entry._2.map(_.fluent))
                                                                   .map(_.functionPredicate)
  @tailrec
  private def compileGradients(stateIndex: Int = 0,
                               currentLinearGradients: Map[Int, List[(ContinuousNumericEffect, Double)]] = Map[Int, List[(ContinuousNumericEffect, Double)]](),
                               currentNonLinearGradients: Map[Int, List[(ContinuousNumericEffect, Double)]] = Map[Int, List[(ContinuousNumericEffect, Double)]]())
  : (Map[Int, List[(ContinuousNumericEffect, Double)]], Map[Int, List[(ContinuousNumericEffect, Double)]]) = {
    if (stateIndex >= plan.size - 1) {
      (currentLinearGradients, currentNonLinearGradients)
    }
    else {
      val tdPreviousTimeDependentFluents: immutable.Set[AtomicFormula] =
        if (stateIndex > 0)
          timeDependentFluentsAtHappening(stateIndex - 1)
        else
          immutable.Set[AtomicFormula]()

      //if we know any bounds of this state use them, otherwise set it to 1 time unit (this is only to get the gradient for non-linear continuous effects)
      //we first try the upper bound (if specified) to avoid shooting out in convex functions

      val effectBound: Double = getStateBound(stateIndex)._2
      //      val effectBound = stateBounds(stateIndex)._2.getOrElse(1.0)

      val (linearContinuousUpdates, nonLinearContinuousUpdates) =
            planConcurrentEffects(stateIndex).partition(_.isLinear(tdPreviousTimeDependentFluents, plan(stateIndex).newState, effectBound))

      val linearGradients: List[(ContinuousNumericEffect, Double)] = linearContinuousUpdates.map(update => {
        val gradient = update.gradient(tdPreviousTimeDependentFluents, plan(stateIndex).newState, effectBound)
        (update,gradient)
      })

      val nonlinearGradients: List[(ContinuousNumericEffect, Double)] = nonLinearContinuousUpdates.map(update => {
        val gradient = update.gradient(tdPreviousTimeDependentFluents, plan(stateIndex).newState, effectBound)
        (update,gradient)
      })

      compileGradients(stateIndex + 1,
                       currentLinearGradients.updated(stateIndex, linearGradients),
                       currentNonLinearGradients.updated(stateIndex, nonlinearGradients))
    }
  }

  def scheduleState(): Option[TemporalState] = {
    if (invariantConsistent) {
      val scheduledState = LPScheduler.scheduleState(Some(this))
      scheduledState.flatMap(temporalState =>
          Some(temporalState)
      )
    }
    else {
      None
    }
  }
}

object TemporalState {
  var stateCount : Int = 0

  def incrementStateCount(): Int ={
    stateCount += 1
    if (stateCount % 1000 == 0)
      println("States explored: " + stateCount)
    stateCount
  }

  def resetStateCount() : Int = {
    stateCount = 0
    stateCount
  }

  def fromState(problem : Problem, state: State, timedActions: List[TimedAction] = List[TimedAction]()): TemporalState = {
    TemporalState(problem, state.modalFacts, state.numericFluents, timedActions)
  }
}


