/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.numeric

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.State
import uk.ac.kcl.inf.planning.solver.TermType
import uk.ac.kcl.inf.planning.solver.LiftedTerm
import uk.ac.kcl.inf.planning.solver.expressions.Expression
import uk.ac.kcl.inf.planning.solver.ff.ValueBounds
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState

/**
 *
 * Date: 10/02/14
 * Time: 19:56 
 * @author josef
 */
case class SubtractNumeric(expression1 : Expression, expression2 : Expression) extends Expression.Aux[SubtractNumeric] with Numeric with Linear
{
  val numeric1: Numeric = expression1.asInstanceOf[Numeric]
  val numeric2: Numeric = expression2.asInstanceOf[Numeric]

  override val isGrounded: Boolean = expression1.isGrounded && expression2.isGrounded

  override def value(state : State, context: Map[Expression, Double] = Map[Expression, Double]()): Option[Double] = {
    numeric1.value(state, context).flatMap(minuend => {
      numeric2.value(state, context).flatMap(subtrahend => {
        Some(minuend - subtrahend)
      })
    })
  }

  def relaxedBounds(relaxedState: RelaxedState) : Option[ValueBounds] = {
    numeric1.relaxedBounds(relaxedState).map(minuendBounds =>
      minuendBounds.combine(numeric2.relaxedBounds(relaxedState), ValueBounds.decreaseCombiner)
    )
  }

  override def updateTypeHierarchy(termType: TermType) : SubtractNumeric = SubtractNumeric(expression1.updateTypeHierarchy(termType), expression2.updateTypeHierarchy(termType))

  override def typify(typedParameter: Term) : SubtractNumeric = SubtractNumeric(expression1.typify(typedParameter), expression2.typify(typedParameter))

  override def substitute(parameter: LiftedTerm, argument: GroundedTerm) : SubtractNumeric = SubtractNumeric(expression1.substitute(parameter, argument), expression2.substitute(parameter, argument))

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): SubtractNumeric =
    copy(expression1 = expression1.hookExternalEvaluator(evaluators), expression2.hookExternalEvaluator(evaluators))

  override def fluents: Set[NumericFluent] = numeric1.fluents ++ numeric2.fluents

  //the coeffecients of all variables and a constant
  override def linearFormula(continuousVariables : Set[AtomicFormula], state: State, t : Double = 0.0): LinearFormula =
    (numeric1, numeric2) match {
      case (linear1 : Linear, linear2 : Linear) =>
        val linearFormula1 = linear1.linearFormula(continuousVariables, state, t)
        val linearFormula2 = linear2.linearFormula(continuousVariables, state, t)
        val mergedCoeff = linearFormula1.coefficients ++ linearFormula2.coefficients.map {
          case (key, value) => key -> (linearFormula1.coefficients.getOrElse(key, 0.0) - value)
        }
        LinearFormula(mergedCoeff, linearFormula1.constant - linearFormula2.constant)
    }


  override def isConstant(timeDependentVariables : Set[AtomicFormula], state : State, t : Double): Boolean = {
    val linears = (numeric1, numeric2) match {
      case (linear1 : Linear, linear2 : Linear) => List[Linear](linear1, linear2)

      case _ => List[Linear]()
    }
    linears.forall(_.isConstant(timeDependentVariables, state, t))
  }

  /**
   * @return The constant part of the expression, computed by adding the constant parts of the sub expressions.
   */
  override def constant : Double = numeric1.constant - numeric2.constant


  override def toString: String = "(- " + numeric1 + " " + numeric2 + ")"
}
