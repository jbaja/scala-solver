/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.ff

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.expressions.{Condition, Modal}

/**
 * Builds a Relaxed Planning Graph (i.e. ignoring delete effects)
 * Date: 14/11/13
 * Time: 19:45 
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 * @deprecated Refer to TemporalRPG for a temporal version of this.
 */
case class RelaxedPlanningGraph(state: State, goalState : Condition, preconditionActionMap : Map[AtomicFormula, Set[Action]])
{
  //if part of the goal is already satisfied remove it from the list of (positive) goal facts we need to achieve
  lazy val goalPositiveFacts = goalState match {
    case modalGoalState : Modal => modalGoalState.positiveAtoms diff state.modalFacts
    case _ => Set[AtomicFormula]()
  }

  val propositionLayer0 = PropositionLayer(0, Set[AtomicFormula](), state.modalFacts, preconditionActionMap)

  lazy val (lastPropositionLayer, layerGoalsMap) = expandGraph(propositionLayer0, goalPositiveFacts)

  lazy val (relaxedPlan, helpfulActions) = lastPropositionLayer match {
    case Some(propositionLayer) => extractPlan(propositionLayer, layerGoalsMap)

    case None => (List[Action](), Set[Action]()) //no relaxed plan, no helpful actions
  }

  lazy val hFF = relaxedPlan.size

  def expandGraph(startLayer: PropositionLayer, goalFacts : Set[AtomicFormula]) =
  {
    def expandGraphAcc(fromLayer: PropositionLayer, remainingGoalFacts : Set[AtomicFormula],
                         achievedGoals : Map[Int, Set[AtomicFormula]]) : (Option[PropositionLayer], Map[Int, Set[AtomicFormula]]) =
    {
      if (fromLayer.satisfies(goalState))
        (Option(fromLayer), achievedGoals)
      else
      {
     //   val start = System.nanoTime()
        val nextLayer = fromLayer.traverse()
     //   val afterTraverse = System.nanoTime()

        nextLayer match
        {
          case None => (None, achievedGoals)

          case Some(propositionLayer) =>
          {
       //     println("Time to traverse to layer " + propositionLayer.layerIndex + " " + ((afterTraverse - start) / 1000000) + "ms")

            val satisfiedNow = for {
              goalFact <- remainingGoalFacts
              if propositionLayer.facts.contains(goalFact)
            } yield goalFact

            expandGraphAcc(propositionLayer, remainingGoalFacts diff satisfiedNow,
                           achievedGoals + (propositionLayer.layerIndex -> satisfiedNow))
          }
        }
      }
    }

 //   val start = System.nanoTime()
    val exp = expandGraphAcc(startLayer, goalFacts, Map[Int, Set[AtomicFormula]]())
//    val end = System.nanoTime()

//    println("Time to expand graph " + ((end - start) / 1000000) + "ms");
    exp
  }

  def extractPlan(lastLayer: PropositionLayer, layerGoalsMap: Map[Int, Set[AtomicFormula]]) : (Plan, Set[Action]) = {

    def extractPlanAcc(currentLayer: PropositionLayer,
                       unsatisfiedPreconditions : Set[AtomicFormula] = Set(),
                       helpfulActions: Set[Action] = Set(),
                       actionsAcc : List[Action] = List()) : (Plan, Set[Action]) = {

       currentLayer.previousLayer match {
         case None => (actionsAcc, helpfulActions) //we're in the first layer

         case Some(actionLayer) =>
         {
           //todo: what if a satisfied condition was satisfied earlier?
           val achievedPreconditions = actionLayer.newPositiveEffects intersect unsatisfiedPreconditions
           val goalsInLayer = layerGoalsMap(currentLayer.layerIndex) ++ achievedPreconditions

           val selectedActions : Set[Action] = for {
             goal <- goalsInLayer
             possibleAction <- actionLayer.newAppliedActions.find(_.positiveEffects.contains(goal))
           } yield possibleAction


           val helpfulActions = actionLayer.layerIndex match {
             case 1 =>  for {
               goal <- goalsInLayer
               helpfulAction <- actionLayer.newAppliedActions.filter(_.positiveEffects.contains(goal))
             } yield helpfulAction

             case _ => Set[Action]()
           }

           val newPreconditions = (for {
             selectedAction <- selectedActions
             modalPrecondition = selectedAction.preconditionExpression match {
               case modal : Modal => modal
             }
           } yield modalPrecondition.positiveAtoms).flatten

//           selectedActions.flatMap(_.precondition.positiveAtoms)

           extractPlanAcc(actionLayer.previousLayer, unsatisfiedPreconditions -- achievedPreconditions ++ newPreconditions, helpfulActions, actionsAcc ++ selectedActions)
         }
       }
    }

 //   val start = System.nanoTime()

    val plan = extractPlanAcc(lastLayer)
 //   val end = System.nanoTime()
 //   println("Time to extract plan " + ((end - start) / 1000000) + "ms")
    plan
  }
}