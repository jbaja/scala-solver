/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.external

import uk.ac.kcl.inf.planning.solver.{AtomicFormula, State}
import uk.ac.kcl.inf.planning.solver.expressions.numeric.{ExternalEvaluator, NumericFluent}

/**
  * Class that models the cooling rate according to newton's law of cooling.
  * Date: 04/03/16
  * Time: 15:50 
  * @author josef
  */
case class Temperature(aliasPrefix : String) extends ExternalEvaluator  {
  override val fullName = "Kcl.Planning.Temperature"

  override def value(fluent : NumericFluent, state: State, t: Double): Option[Double] = {
    //todo: rather than do this all the time, return a function on hook attachment that calls directly the respective function (as defined in PDDLx)

    val method = fluent.functionPredicate.predicate.drop(aliasPrefix.length() + 1) //we add one to remove the .

    method match {
      case "cooling-rate" => coolingRate(fluent, state, t)

      case _ => handleStaticFluent(fluent, state)
    }
  }

  def coolingRate(fluent : NumericFluent, state : State, t : Double) : Option[Double] = {
    if (fluent.functionPredicate.parameters.size != 1) {
      throw new Exception("Invalid parameters for module method: " + fluent.functionPredicate + " Expecting 1 parameter of type " + aliasPrefix + ".Object")
    }

    //check if we are applying direct heat, in which case ambient temperature has negligible effect, assume 0 to avoid complications related to monotonicty and convergence
    if (state.modalFacts.contains(AtomicFormula(aliasedName("heating"), List(fluent.functionPredicate.parameters.head)))) {
      Some(0.0)
    }
    else {
      val temperatureAt0 = state.numericFluents(AtomicFormula(aliasedName("current-temp"), List(fluent.functionPredicate.parameters.head)))
      val ambientTemperature = state.numericFluents(AtomicFormula(aliasedName("ambient-temp")))
      val coolingConstant = state.numericFluents(AtomicFormula(aliasedName("cooling-constant"), List(fluent.functionPredicate.parameters.head)))
      if (t == 0) {
        //just return the raw gradient at this point in time (gradient is kept positive because it will be used in a decrease cont effect)
        val coolingRate = coolingConstant * (temperatureAt0 - ambientTemperature)
        Some(coolingRate)
      }
      else {
        //approximate the non-linear function as a linear one
        val temperatureAtT = ambientTemperature + (temperatureAt0 - ambientTemperature) * Math.exp(- coolingConstant * t)
        val delta = temperatureAt0 - temperatureAtT
        Some(delta / t)
      }
    }
  }

  override def isConstant(fluent: NumericFluent, state : State, t : Double): Boolean = {
    val fluentName = fluent.functionPredicate.predicate.drop(aliasPrefix.length() + 1) //we add one to remove the .

    fluentName match {
      case "cooling-rate" => false

      case _ => true
    }
  }
}
