/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.optimiser

import scala.annotation.tailrec
import scala.util.Random

import uk.ac.kcl.inf.planning.validator._
import uk.ac.kcl.inf.planning.solver.{SolverSettings, State, DurativeAction, Problem}
import java.util.Date
import uk.ac.kcl.inf.planning.utils.{NumericUtils, FixedSizeFifo}
import java.io.FileWriter
import java.text.SimpleDateFormat
import uk.ac.kcl.inf.planning.solver.expressions.temporal.TimeSpecifier
import scala.util.control.Breaks._

/**
  * Experimental Local Search implementation for plan improvement and repair.
  * @author Josef Bajada - josef.bajada@kcl.ac.uk
  */
object Optimiser {
  val random = new Random()
  val sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")


  def getRandomPeriod(periodDist : Map[Int, Int], excluded: Set[Int]) : Int =
  {
    //we raise all the distribution values by power of 0, to make them value 1, since we don't use this for now
    val adjustedDist: Map[Int, Int] = periodDist.map(pDist => (pDist._1, Math.pow(pDist._2, 0).toInt))

    //we calculate the total distribution
    val total = adjustedDist.foldLeft(0)((total, apDist) => total + apDist._2)

    val r = random.nextInt(total)

    var acc = 0
    val periods = adjustedDist.keySet
    for (period <- periods)
    {
      val prob = adjustedDist(period)
      if ((r >= acc) && (r < (acc + prob))) {
        return period
      }

      acc += prob
    }
    0 //should never happen
  }

  def chooseBestNeighbour(prevNeighbourPlan : GroundedPlan, prevNeighbourTimeline : PlanTimeline,
                          newNeighbourPlan : GroundedPlan, newNeighbourTimeline : PlanTimeline,
                          currentDepth : Int, maximumDepth : Int ) : (GroundedPlan, PlanTimeline, Integer) = {

    if (newNeighbourTimeline.violationCount < prevNeighbourTimeline.violationCount)
    {
      return (newNeighbourPlan, newNeighbourTimeline, currentDepth+1)
    }
    else if ((newNeighbourTimeline.violationCount == 0) && (prevNeighbourTimeline.violationCount== 0) && (newNeighbourTimeline betterThan prevNeighbourTimeline))
    {
      return (newNeighbourPlan, newNeighbourTimeline, currentDepth+1)
    }

    //accept with probability P depending on the depth

    //todo: make the probability depend on whether (newNeighbourTimeline.cost < previousNeighbourTimeline.cost)

    //square the error to obtain a skewness value that amplifies the more the violation difference
    val skewness : Double = if (newNeighbourTimeline.violationCount == 0 && prevNeighbourTimeline.violationCount == 0) {
      //both are valid plans, compare costs
      (newNeighbourTimeline.cost.toDouble + 1) / (prevNeighbourTimeline.cost.toDouble + 1)
    }
    else {
      //one of them is invalid, compare violations {
      (newNeighbourTimeline.violationCount.toDouble + 1) / (prevNeighbourTimeline.violationCount.toDouble + 1)
    }

    val temperature = maximumDepth-currentDepth
    val acceptanceProb : Double = math.pow(1.0 - (1.0 / (1.0 + Math.exp(temperature))), skewness)

    val r = random.nextDouble()
    if (r > acceptanceProb) //reject
    {
      (prevNeighbourPlan, prevNeighbourTimeline, currentDepth)
    }
    else //accept
    {
      (newNeighbourPlan, newNeighbourTimeline, currentDepth+1)
    }
  }

  def optimisePlan(seedPlan : GroundedPlan, maximumDepth : Int = 100) : (GroundedPlan, PlanTimeline) = {

    var bestPlan = seedPlan
    var bestPlanTimeline = seedPlan.timeline

    var previousNeighbourPlan = seedPlan
    var previousNeighbourTimeline = bestPlanTimeline
    var depthCounter = 0
    var tabuList = FixedSizeFifo[GroundedPlan](100)

    var visitedPlans = List[GroundedPlan]()

    //loop until either we find a better plan than the provided one (if it is non violated), or until we find a neighbour which is not violated
    while ((!previousNeighbourTimeline.betterThan(bestPlanTimeline) && bestPlanTimeline.violationCount == 0) || (previousNeighbourTimeline.violationCount > 0))
    {
        getUnvisitedNeighbour(seedPlan.problem, previousNeighbourPlan, visitedPlans) match {
          case None => return (seedPlan, bestPlanTimeline)

          case Some(newNeighbourPlan) =>
            if (!tabuList.contains(newNeighbourPlan)) {

              val newNeighbourTimeline = newNeighbourPlan.timeline

            if (((newNeighbourTimeline.violationCount == 0) && (bestPlanTimeline.violationCount == 0) && (newNeighbourTimeline betterThan bestPlanTimeline)) || //if the plan costs less than the best
                ((newNeighbourTimeline.violationCount == 0) && (bestPlanTimeline.violationCount > 0))) //or the best plan isn't a valid plan (but the new one is)
            {
              println("*** Found better plan! Cost " + newNeighbourTimeline.cost)
              previousNeighbourPlan = newNeighbourPlan
              previousNeighbourTimeline = newNeighbourTimeline

              //we can just return it
              return (newNeighbourPlan, newNeighbourTimeline)
            }
            else if (newNeighbourTimeline.violationCount < bestPlanTimeline.violationCount) {
              println(sdf.format(new Date(System.currentTimeMillis())))
              println("+++ Found better partial plan! Violations " + newNeighbourTimeline.violationCount)
              bestPlan = newNeighbourPlan
              bestPlanTimeline = newNeighbourTimeline
              previousNeighbourPlan = bestPlan
              previousNeighbourTimeline = bestPlanTimeline
              depthCounter = 0
              println(bestPlan.toPlanString)

              visitedPlans = List[GroundedPlan]()
              println("+++ Selecting it as best plan.")
            }
            else if (depthCounter >= maximumDepth) {
              //the plan is not better than the current plan, but we've traversed too deep, start again from the seed plan
              previousNeighbourPlan = bestPlan
              previousNeighbourTimeline = bestPlanTimeline
              depthCounter = 0
              visitedPlans = List[GroundedPlan]()
            }
            else
            {
              val toTraverse = chooseBestNeighbour(previousNeighbourPlan, previousNeighbourTimeline, newNeighbourPlan, newNeighbourTimeline, depthCounter, maximumDepth)

              previousNeighbourPlan = toTraverse._1
              previousNeighbourTimeline = toTraverse._2
              depthCounter = toTraverse._3

              if (toTraverse._1.equals(newNeighbourPlan)) {
                visitedPlans = toTraverse._1 :: visitedPlans
                tabuList = tabuList :+ toTraverse._1
              }
            }
          }
        }
    }

    println("Returning plan with " + previousNeighbourTimeline.violationCount)
    (previousNeighbourPlan, previousNeighbourTimeline)
  }

  def searchForBetterPlan(problem : Problem, seedPlan : GroundedPlan, seedPlanTimeline : PlanTimeline, iterations : Int = 1) : GroundedPlan = {

    println("Searching for a plan that has cost better than " + seedPlanTimeline.cost + ", violations " + seedPlanTimeline.violationCount + " ...")

    var lastPlan = seedPlan
    var lastPlanTimeline : PlanTimeline = seedPlanTimeline

    val sdf = new SimpleDateFormat("yyyyMMddHHmmss")
    val dt = sdf.format(new Date())
    val fw = new FileWriter(problem.domain.name + "_" + problem.name + "_" + dt + ".txt", false)

    if (seedPlanTimeline.violationCount == 0)
    {
      fw.write("0\t" + seedPlanTimeline.cost + "\t0\n")
    }

    //todo: this is ugly! make it properly using recursion or something!
    breakable { for (i <- 1 to iterations)
    {
      println("\nSearching for a better plan ("+i+") ... \n")
      val start = System.currentTimeMillis()
      val newPlan = optimisePlan(lastPlan)
      val end = System.currentTimeMillis()
      lastPlan = newPlan._1
      lastPlanTimeline = newPlan._2

      val elapsed = (end - start).toDouble
      println(new Date(System.currentTimeMillis()))
      println("***** New Plan " + lastPlan.toPlanString)
      println("****** Cheaper plan found with cost : " + lastPlanTimeline.cost + " violations: " + lastPlanTimeline.violationCount + " in " + elapsed + "ms")
      fw.write(i + "\t" + lastPlanTimeline.cost+"\t" + (elapsed / 1000) + "\n")
      fw.flush()

      if (lastPlanTimeline.cost == 0)
        break
    } }

    fw.close()

    lastPlan
  }

  @tailrec final def getUnvisitedNeighbour(problem: Problem, plan: GroundedPlan, visitedPlans : List[GroundedPlan] = List[GroundedPlan]()) : Option[GroundedPlan] = {
    val planOpt = getNeighbour(problem, plan)
    planOpt match {
      case Some(neighbourPlan) =>
        if (visitedPlans.contains(neighbourPlan)) {
          getUnvisitedNeighbour(problem, neighbourPlan)
        }
        else
          planOpt

      case None => println("getNeighbour returning None!"); None
    }
  }


  def getNeighbour(problem: Problem, plan : GroundedPlan) : Option[GroundedPlan] = {

    if (plan.futureIntervals.nonEmpty) {
      random.nextInt(3) match {
        case 0 => getAddNeighbour(problem, plan)
        case 1 =>  getRemoveNeighbour(problem, plan) match {
            case Some(newPlan) => Some(newPlan)
            case None => getAddNeighbour(problem, plan)
          }
        case 2 =>  getMoveNeighbour(problem, plan) match {
          case Some(newPlan) => Some(newPlan)
          case None => getAddNeighbour(problem, plan)
        }
      }
    }
    else {
       getAddNeighbour(problem, plan)
    }
  }

  def getRemoveNeighbour(problem: Problem, plan : GroundedPlan) : Option[GroundedPlan] = {

     val candidates = plan.timedApplicables.filter {
       case TimedApplicableAction(_, _) => true
       case TimedApplicableDurativeAction(at, _, TimeSpecifier.AT_START, _) => at >= 0
       case _ => false
     }

    if (candidates.isEmpty)
      return None

    val actionNumber = random.nextInt(candidates.size)
    val actionToDrop = candidates(actionNumber)

    val newApplicables = plan.timedApplicables.filterNot(p => {
      p.equals(actionToDrop) || (p match {
        case TimedApplicableDurativeAction(_, _, TimeSpecifier.AT_END, Some(parent)) => parent.equals(actionToDrop)
        case _ => false
      })
    })

    val newPlan = GroundedPlan(problem, newApplicables)
    Some(newPlan)
  }

  def getMoveNeighbour(problem : Problem, oldPlan : GroundedPlan) : Option[GroundedPlan] = {

    ///first we remove it
    val candidates = oldPlan.timedApplicables.filter {
      case TimedApplicableAction(_,_) => true
      case TimedApplicableDurativeAction(at, _, TimeSpecifier.AT_START, _) => at >= 0
      case _ => false
    }

    if (candidates.isEmpty)
      return None

    val actionNumber = random.nextInt(candidates.size)
    val actionToMove = candidates(actionNumber)

    val newApplicables = oldPlan.timedApplicables.filterNot(p => {
      p.equals(actionToMove) || (p match {
        case TimedApplicableDurativeAction(_, _, TimeSpecifier.AT_END, Some(parent)) => parent.equals(actionToMove)
        case _ => false
      })
    })

    val plan = GroundedPlan(problem, newApplicables)

    /////move it somewhere else
    val timeline = plan.timeline
    val periods = plan.futureIntervals.size
    var excludedPeriods = Set[Int]()

    val timepoints = timeline.futureTimeline.toList

    //todo: improve this to support durative actions with variable durations, use the LPScheduler!

    //if there are no periods, there is still 1 we can try (added at the end)
    while (excludedPeriods.size <= periods)
    {
      //compute a probability distribution proportional to the number of violations
      val dist: Map[Int, Int] = (for {
        i <- 0 until timepoints.size-1
        if !excludedPeriods.contains(i)
      } yield (i, timepoints(i).newViolations.size + 1)).toMap.updated(timepoints.size - 1, timeline.violationCount)

      val startPos = getRandomPeriod(dist, excludedPeriods)

      val periodStartTime = if (startPos < periods)
        plan.futureIntervals(startPos)._1
      else if (plan.futureIntervals.nonEmpty)
        plan.futureIntervals.last._2
      else 0

      val (timepoint, _) = timeline.getTimepointAndDurationAt(periodStartTime)
      val invariantConditions = plan.invariantConditionsAt(periodStartTime)

      //todo: handle case when it is a snap action
      val durativeAction = actionToMove.asInstanceOf[TimedApplicableDurativeAction].durativeAction

      if (durativeAction.isApplicable(timepoint.state, TimeSpecifier.AT_START) &&
          durativeAction.isApplicable(durativeAction.applyToState(timepoint.state, TimeSpecifier.AT_START), TimeSpecifier.OVER_ALL))
      {

        val newAction = durativeAction.computeDuration(timepoint.state)
        //check it is not mutex with the invariants
        if (newAction.duration.isDefined &&
          invariantConditions.forall(_.satisfiedBy(newAction.applyToState(timepoint.state, TimeSpecifier.AT_START)))) {

            //check that after applying the AT START effects, the invariant OVER ALL condition holds
            getActionStartTime(newAction, timepoint.state, plan, startPos) match {
              case Some(actionStartTime) =>
                val startTA = TimedApplicableDurativeAction(actionStartTime, newAction, TimeSpecifier.AT_START)
                val endTA = TimedApplicableDurativeAction(actionStartTime + newAction.duration.get, newAction, TimeSpecifier.AT_END, Some(startTA))

                val newTimedApplicables = (startTA :: endTA :: plan.timedApplicables).sortBy(ta => ta.at)
                val newPlan = GroundedPlan(problem, newTimedApplicables)

                return Some(newPlan)

              case None =>
            }
        }

      }
      excludedPeriods = excludedPeriods + startPos
    }

    None
  }

  @tailrec final def getAddNeighbour(problem: Problem, plan: GroundedPlan, excludedPeriods : Set[Int] = Set[Int]()) : Option[GroundedPlan] = {

    if (excludedPeriods.size <= plan.futureIntervals.size)  {
      //if there are no periods, there is still 1 we can try (added at the end)

      RandomPeriodSelector.selectPeriod(problem, plan, excludedPeriods) match {
        case Some(selectedPeriod) =>
          val startTime = plan.getPeriodStartTime(selectedPeriod)
          val (timepoint, _) =  plan.timeline.getTimepointAndDurationAt(startTime)

          val nonMutexActions = ApplicableActionSelector.getPossibleActionsAt(problem, plan, selectedPeriod)
          val additionalAction = selectDurativeAction(nonMutexActions, timepoint.state, plan, selectedPeriod)
          additionalAction match {

            //todo: the durations of subsequent actions need to be updated, the additional action might have an impact on their duration expression
            case Some((st, durativeAction)) =>
              val startTA = TimedApplicableDurativeAction(st, durativeAction, TimeSpecifier.AT_START)
              val endTA = TimedApplicableDurativeAction(st + durativeAction.duration.get, durativeAction, TimeSpecifier.AT_END, Some(startTA))
              val newTimedApplicables = (startTA :: endTA :: plan.timedApplicables).sortBy(ta => ta.at)

              Some(GroundedPlan(problem, newTimedApplicables))

            case None => getAddNeighbour(problem, plan, excludedPeriods + selectedPeriod)
          }

        case None => None
      }
    }
    else {
      None
    }
  }


  def remove[T](list: List[T], index : Int) : List[T] = {
    val (start, _ :: end) = list.splitAt(index)
    start ::: end
  }

  def getActionStartTime(durativeAction: DurativeAction, state: State, plan : GroundedPlan, startPos : Int): Option[Double] =
  {
    val (minEndPos, maxEndPos) = getMinMaxEndIntervals(plan.futureIntervals, startPos, durativeAction.duration.get)

    //corner cases
    if (plan.futureIntervals.isEmpty) //if there are no intervals in the plan, add the first action at 0.0
    {
      return Some(0.0) //add the first action
    }
    else if (startPos == plan.futureIntervals.size) //no action invariants to check, the plan has ended
    {
      //todo: make it configurable to some epsilon
      return Some(plan.futureIntervals.last._2 + 0.001)
    }

    val (validInvariants, validMaxEnd) = checkActionInvariants(durativeAction, plan, state, startPos, minEndPos, maxEndPos)

    if (validInvariants) {
      val endPos = if (validMaxEnd - minEndPos > 0) minEndPos + random.nextInt(validMaxEnd - minEndPos + 1) else validMaxEnd

      val startInterval = plan.futureIntervals(startPos)
      val startTime = if (endPos >= plan.futureIntervals.size)
      {
        startInterval._1 + (duration(startInterval) / 2)
      }
      else
      {
        val endInterval = plan.futureIntervals(endPos)
        //check how much slack we have and divide it by 2
        val periodSlack = (getIntervalSpanDuration(plan.futureIntervals, startPos, endPos) - durativeAction.duration.get) / 2
        //          println("Durative action duration: " + durativeAction.duration.get + " period slack " + periodSlack)
        if ((periodSlack < duration(startInterval)) && (periodSlack < duration(endInterval)))
        {
          //the slack fits in both start and end periods, distribute it evenly by deducting the slack from the start interval's end point
          startInterval._2 - periodSlack
        }
        else if (periodSlack >= duration(startInterval)) //the start interval is too tight, so put it in the middle of it
        {
          startInterval._1 + (duration(startInterval) / 2)
        }
        else         //the end interval is too tight, so put it in the middle of it
        {
          if ((endInterval._1 + (duration(endInterval) / 2)) - durativeAction.duration.get < 0)
          {
            println("--------------- Returning negative 2nd part!")
            println("** endInterval" + endInterval)
            println("** duration(endInterval)"  + duration(endInterval) + " / 2 = " + (duration(endInterval) / 2))
            println("** Action duration: " + durativeAction.duration.get)
          }

          (endInterval._1 + (duration(endInterval) / 2)) - durativeAction.duration.get
        }
      }

      Some(NumericUtils.round(startTime, 3))
    }
    else {
      None
    }
  }

  @tailrec final def selectDurativeAction(actionList: List[DurativeAction], state: State, plan : GroundedPlan, startPos : Int): Option[(Double, DurativeAction)] =
  {
    if (actionList.isEmpty)
      return None

    val actionNumber = random.nextInt(actionList.size)

    //todo: shuffle the list instead and use the stream lazily
    val newActionList = remove(actionList, actionNumber) //remove it from the list
    val durativeAction = actionList(actionNumber)
    val (minEndPos, maxEndPos) = getMinMaxEndIntervals(plan.futureIntervals, startPos, durativeAction.duration.get)

    //corner cases
    if (plan.futureIntervals.isEmpty) {
      //there are no intervals in the plan, add the first action at 0.0
      return Some((0.0, durativeAction)) //add the first action
    }
    else if (startPos == plan.futureIntervals.size) {
      //no action invariants to check, the plan has ended
      return Some((plan.futureIntervals.last._2 + SolverSettings.settings.EPSILON, durativeAction))
    }

    //todo: remove this mess and use the LPScheduler instead, it will also support flexible durations
    val (validInvariants, validMaxEnd) = checkActionInvariants(durativeAction, plan, state, startPos, minEndPos, maxEndPos)
    if (validInvariants) {
      val endPos = if (validMaxEnd - minEndPos > 0) minEndPos + random.nextInt(validMaxEnd - minEndPos + 1) else validMaxEnd
      if (startPos >= plan.futureIntervals.size)
        println("Chosen Start Pos " + startPos + " End Pos " + endPos + " intervals: " + " [" + plan.futureIntervals.size + "]")

      val startInterval = plan.futureIntervals(startPos)

      val startTime = if (endPos >= plan.futureIntervals.size) {
          startInterval._1 + (duration(startInterval) / 2)
        }
        else {
          val endInterval = plan.futureIntervals(endPos)
          //check how much slack we have and divide it by 2
          val periodSlack = (getIntervalSpanDuration(plan.futureIntervals, startPos, endPos) - durativeAction.duration.get) / 2
          if ((periodSlack < duration(startInterval)) && (periodSlack < duration(endInterval))) {
            //the slack fits in both start and end periods, distribute it evenly by deducting the slack from the start interval's end point
            startInterval._2 - periodSlack
          }
          else if (periodSlack >= duration(startInterval)) {
            //the start interval is too tight, so put it in the middle of it
            startInterval._1 + (duration(startInterval) / 2)
          }
          else {
             //the end interval is too tight, so put it in the middle of it
            if ((endInterval._1 + (duration(endInterval) / 2)) - durativeAction.duration.get < 0)
            {
              println("--------------- Returning negative 2nd part!")
              println("** endInterval" + endInterval)
              println("** duration(endInterval)"  + duration(endInterval) + " / 2 = " + (duration(endInterval) / 2))
              println("** Action duration: " + durativeAction.duration.get)
            }

            (endInterval._1 + (duration(endInterval) / 2)) - durativeAction.duration.get
          }
        }

      Some((NumericUtils.round(startTime, 3), durativeAction))
    }
    else
    {
      selectDurativeAction(newActionList, state, plan, startPos)
    }
  }

  def checkActionInvariants(durativeAction : DurativeAction, plan : GroundedPlan, stateAtStartPos : State, startPos : Int, minEndPos : Int, maxEndPos : Int) : (Boolean, Int) =
  {
    //check if it is going to be added at the end
    if (startPos >= plan.futureIntervals.size)
      return (true, startPos)

    val startIntervalTime = plan.futureIntervals(startPos)._1

    val newState = durativeAction.applyToState(stateAtStartPos, TimeSpecifier.AT_START)
    if (!durativeAction.isApplicable(newState, TimeSpecifier.OVER_ALL))
    {
      return (false, maxEndPos)
    }

    val lastIntervalTime = if (maxEndPos == plan.futureIntervals.size) plan.futureIntervals.last._2 else plan.futureIntervals(maxEndPos)._1
    val timedApplicables = plan.timedApplicables.filter(timedApplicable => timedApplicable.at > startIntervalTime && timedApplicable.at <= lastIntervalTime).sortBy(_.at)

    //todo: make this a bit clearer
    var currentState = newState
    var intervalIndex = startPos
    var valid : Boolean = false
    for (timedApplicable <- timedApplicables) {
      currentState = timedApplicable.applyToState(currentState)
      if (!durativeAction.isApplicable(newState, TimeSpecifier.OVER_ALL)) {
        return (valid, intervalIndex)
      }

      intervalIndex = intervalIndex + 1

      if (intervalIndex > minEndPos)
        valid = true

      if (intervalIndex > maxEndPos)
        return (valid, intervalIndex)
    }

    (true, maxEndPos)
  }

  def getIntervalSpanDuration(intervals: List[(Double, Double)], startPos : Int, endPos : Int) : Double = {
    intervals.slice(startPos, endPos+1).foldLeft(0.0)((duration, interval) => duration + (interval._2 - interval._1))
  }

  def duration(interval : (Double, Double)) : Double = interval._2 - interval._1


  def getMinMaxEndIntervals(intervals: List[(Double, Double)], startPos: Int, duration: Double): (Int, Int) = {
    val size = intervals.size
    val min = {
      def findMinDuration(): Int = {
        var intervalDur: Double = 0.0
        for (i <- startPos until size) {
          val interval = intervals(i)
          intervalDur += interval._2 - interval._1
          if (intervalDur > duration) return i
        }

        size
      }
      findMinDuration()
    }

    val max = {
      var intervalDur: Double = 0.0
      def findMaxDuration(): Int = {
        for (i <- startPos + 1 until size) {
          val interval = intervals(i)
          intervalDur += interval._2 - interval._1
          if (intervalDur > duration) return i
        }

        size
      }
      findMaxDuration()
    }

    (min, max)
  }
}
