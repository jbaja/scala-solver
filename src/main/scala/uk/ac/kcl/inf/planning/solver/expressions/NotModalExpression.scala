/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.expressions.numeric.ExternalEvaluator
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState

/**
 * Used only as a dummy predicate to represent the inverse of Modal Expressions.
 * Date: 10/02/14
 * Time: 17:19 
 * @author josef
 */
case class NotModalExpression(predicate: AtomicFormula) extends Expression.Aux[NotModalExpression] with Modal with Effect with Condition
{
  override lazy val positiveAtoms: Set[AtomicFormula] = Set[AtomicFormula]()
  override lazy val negativeAtoms: Set[AtomicFormula] = Set[AtomicFormula](predicate)

  /**
   * Updates the type hierarchy of the specified type. While parsing the full type hierarchy might not be immediately
   * known, and determined from a separate type list. This method helps populate the type hierarchy properly.
   * @param termType - the full type to be populated
   * @return The expression updated with the right type information
   */
  override def updateTypeHierarchy(termType: TermType): NotModalExpression = copy(predicate = predicate.updateTypeHierarchy(termType))

  /**
   * If one of the lifted terms is untyped and has the same name, it is replaced by the specified typed parameter.
   * This is used mainly in places where the expression is being parsed without type information (such as PDDL) and type
   * information is provided afterwards.
   * @param typedParameter - The typed parameter to replace the untyped one (which has the same parameter name)
   * @return an expression with the typed parameter
   */
  override def typify(typedParameter: Term): NotModalExpression = copy(predicate = predicate.typify(typedParameter))

  /**
   * Substitutes the lifted parameter with the grounded argument term.
   * @param parameter The lifted parameter to be substituted
   * @param argument The grounded argument to substitute the parameter
   * @return A new instance of the expression with the parameter substituted by the argument
   */
  override def substitute(parameter: LiftedTerm, argument: GroundedTerm): NotModalExpression = copy(predicate = predicate.substitute(parameter, argument))

  //todo: external function evaluation will be needed also in case of modal
  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): NotModalExpression = this

  /**
   * @return true if this expression is fully grounded, false otherwise
   */
  override def isGrounded: Boolean = predicate.isGrounded

  /**
   * Determines whether this condition is satisfied in the provided state.
   * @param state The state to be checked for this condition.
   * @param context The context in which this condition is operating (for local variables within actions)
   * @return true if the state satisfies the condition, false otherwise
   */
  override def satisfiedBy(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): Boolean = {
    !state.modalFacts.contains(predicate)
  }

  override def relaxedSatisfiedBy(relaxedState: RelaxedState): Boolean = {
    !relaxedState.modalFacts.contains(predicate) || relaxedState.negatedFacts.contains(predicate)
  }

  /**
   * Returns this condition if it is not satisfied by the provided state
   * @param state The state to be checked for this condition
   * @param context The context in which this condition is operating (for local variables within actions)
   * @return true if the state satisfies the condition, false otherwise
   */
  override def violations(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): Set[Condition] =
    if (satisfiedBy(state, context)) Set[Condition]() else Set[Condition](this)


  /**
   * Takes a state and produces a new state after applying the effects
   * @param state the current (old) state
   * @return the new updated state after applying the effect
   */
  override def applyToState(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): State =
    state.copy(modalFacts = state.modalFacts -- negativeAtoms ++ positiveAtoms)

  /**
   * @param oldRelaxedState - the relaxed state prior to the current layer containing upper bounds and lower bounds of each numeric variable
   * @param cumulativeRelaxedState - the relaxed state accumulated from the current layer before this effect
   * @return a map of updated values to the fluents, with the values being an upper and a lower bound
   */
  override def relaxedEffect(oldRelaxedState: RelaxedState, cumulativeRelaxedState: RelaxedState): RelaxedState = {
    cumulativeRelaxedState.copy(modalFacts = cumulativeRelaxedState.modalFacts ++ positiveAtoms,
                                negatedFacts = cumulativeRelaxedState.negatedFacts ++ negativeAtoms)
  }

  override def toString = predicate.toString

  override def equals(obj: scala.Any): Boolean = obj.getClass().equals(this.getClass()) && (obj.asInstanceOf[this.type].predicate.equals(this.predicate))

  override def invert: Expression with Condition = ModalExpression(predicate)

  override def atomicFormulas: Set[AtomicFormula] = Set[AtomicFormula](predicate)
}
