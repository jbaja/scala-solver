/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.numeric

import uk.ac.kcl.inf.planning.solver._

import uk.ac.kcl.inf.planning.solver.AtomicFormula
import uk.ac.kcl.inf.planning.solver.GroundedTerm
import uk.ac.kcl.inf.planning.solver.TermType
import uk.ac.kcl.inf.planning.solver.expressions.{NumericEffect, Expression}
import uk.ac.kcl.inf.planning.solver.ff.ValueBounds
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState


/**
 *
 * Date: 10/02/14
 * Time: 18:22 
 * @author josef
 */

case class DecreaseEffect(fluent: NumericFluent, expression: Expression) extends Expression.Aux[DecreaseEffect] with NumericEffect
{
  val numeric : Numeric = expression.asInstanceOf[Numeric]

  override val isGrounded: Boolean = fluent.isGrounded && expression.isGrounded

  override def substitute(parameter: LiftedTerm, argument: GroundedTerm) =
    DecreaseEffect(fluent.substitute(parameter, argument), expression.substitute(parameter, argument))

  override def typify(typedParameter: Term)  =
    DecreaseEffect(fluent.typify(typedParameter), expression.typify(typedParameter))

  override def updateTypeHierarchy(termType: TermType) =
    DecreaseEffect(fluent.updateTypeHierarchy(termType), expression.updateTypeHierarchy(termType))

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): DecreaseEffect =
    copy(expression = expression.hookExternalEvaluator(evaluators))


  /**
   * Evaluates the effect on the fluent in the state.
   * @param state - the state
   * @param context - contextual information, such as action duration
   * @return a mapping between the updated fluent and the new value of the fluent.
   */

  override def evaluateNumeric(state: State, context: Map[Expression, Double] = Map[Expression, Double]()) : Map[AtomicFormula, Double] = {
    fluent.value(state, context) match {
      case Some(currentValue) => Map[AtomicFormula, Double]((fluent.functionPredicate, currentValue - numeric.value(state, context).getOrElse(0.0)))
      case None => Map[AtomicFormula, Double]()
    }
  }

  /**
   * @param oldRelaxedState - the relaxed state prior to the current layer containing upper bounds and lower bounds of each numeric variable
   * @param cumulativeRelaxedState - the relaxed state accumulated from the current layer before this effect
   * @return a map of updated values to the fluents, with the values being an upper and a lower bound
   */
//  def relaxedEffect(oldRelaxedState: RelaxedState, cumulativeRelaxedState : RelaxedState) : RelaxedState = {
//    val (curMin, curMax) = (fluent.value(cumulativeRelaxedState.minState).getOrElse(0.0), fluent.value(cumulativeRelaxedState.maxState).getOrElse(0.0))
//    val (v1, v2) = (-numeric.value(oldRelaxedState.minState).getOrElse(0.0), -numeric.value(oldRelaxedState.maxState).getOrElse(0.0))
//
//    //same as IncreaseEffect, but we turned them to -ve above to serve as a decrease
//    val minIncrease = Math.min(0, Math.min(v1, v2)) //if the minimum increase is positive, than its redundant, because of the maximum increase
//    val maxIncrease = Math.max(0, Math.max(v1, v2)) //if the maximum increase is negative, than its redundant, because of the minimum increase
//
//    val newNumericFluents = cumulativeRelaxedState.numericFluentBounds.updated(fluent.functionPredicate, ValueBounds(curMin + minIncrease, curMax + maxIncrease))
//    cumulativeRelaxedState.copy(numericFluentBounds = newNumericFluents)
//  }

  /**
   * @param oldRelaxedState - the relaxed state prior to the current layer containing upper bounds and lower bounds of each numeric variable
   * @param cumulativeRelaxedState - the relaxed state accumulated from the current layer before this effect
   * @return a map of updated values to the fluents, with the values being an upper and a lower bound
   */
  def relaxedEffect(oldRelaxedState: RelaxedState, cumulativeRelaxedState : RelaxedState) : RelaxedState = {

    //get the old bounds (prior to the effect of the action)
    val oldBounds = fluent.relaxedBounds(oldRelaxedState)
    //calculate the delta bounds for the decrease operation
    val deltaBounds = numeric.relaxedBounds(oldRelaxedState)
    //get the current cumulative bounds
    val curBounds = fluent.relaxedBounds(cumulativeRelaxedState)

    //calculate the bounds from the effect
    val effectBounds = oldBounds.map(bounds => bounds.combine(deltaBounds, ValueBounds.decreaseCombiner))

    //todo: cater for one-shot actions, if this is part of a one-shot action, instead of infinity the real value must be used
    //extrapolate the change to infinity (since multiple actions can be applied in a relaxed state)
    val infBounds = ValueBounds.infinitize(oldBounds, effectBounds)

    //combine them with the cumulative bounds from other concurrent actions
    val newBounds = infBounds.map(bounds => bounds.combine(curBounds))

    //update the relaxed state
    newBounds.map { vb =>
      cumulativeRelaxedState.copy(
        numericFluentBounds = cumulativeRelaxedState.numericFluentBounds.updated(fluent.functionPredicate, vb))
    }.getOrElse(cumulativeRelaxedState)
  }

  override def numericFluents: Set[NumericFluent] = Set[NumericFluent](fluent)

  override def toLinearEffect(timeDependentVariables : Set[AtomicFormula], state : State, t : Double = 0.0) : List[LinearEffect] = {
    expression match {
      case (linear : Linear) =>
        List[LinearEffect](LinearEffect(fluent.functionPredicate,
          linear.linearFormula(timeDependentVariables, state, t),
          NumericUpdate.DECREASE))
    }
  }
}

