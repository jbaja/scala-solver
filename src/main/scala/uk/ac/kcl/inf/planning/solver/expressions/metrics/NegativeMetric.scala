/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.metrics

import uk.ac.kcl.inf.planning.solver.expressions.numeric.{ExternalEvaluator, Numeric}
import uk.ac.kcl.inf.planning.solver.expressions.Expression
import uk.ac.kcl.inf.planning.solver.{GroundedTerm, LiftedTerm, Term, TermType}
import uk.ac.kcl.inf.planning.validator.PlanTimeline

/**
 *
 * Date: 06/06/14
 * Time: 15:57 
 * @author josef
 */
case class NegativeMetric (expression : Expression) extends Expression.Aux[NegativeMetric] with MetricNumeric {

  override val isGrounded: Boolean = expression.isGrounded

  override def updateTypeHierarchy(termType: TermType) : NegativeMetric = NegativeMetric(expression.updateTypeHierarchy(termType))

  override def typify(typedParameter: Term) : NegativeMetric = NegativeMetric(expression.typify(typedParameter))

  override def substitute(parameter: LiftedTerm, argument: GroundedTerm) : NegativeMetric = NegativeMetric(expression.substitute(parameter, argument))

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): NegativeMetric =
    copy(expression = expression.hookExternalEvaluator(evaluators))

  /**
   * Provides the numeric value of this entity for a plan
   * @param planTimeline the whole plan timeline to be evaluated
   * @return An double value
   */
  override def value(planTimeline: PlanTimeline): Double = {
    expression match {
      case mn : MetricNumeric => (- mn.value(planTimeline))
      case n : Numeric => (- n.value(planTimeline.endState).getOrElse(0.0))
    }
  }


}
