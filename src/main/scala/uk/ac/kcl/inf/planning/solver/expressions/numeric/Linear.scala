/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.numeric

import uk.ac.kcl.inf.planning.solver.{State, AtomicFormula}

/**
 * Created by Josef on 26/01/2015.
 */
trait  Linear {
  //the coeffecients of all variables and a constant
  def linearFormula(timeDependentVariables : Set[AtomicFormula], state : State, t : Double) : LinearFormula

  /**
   * @return true if the actual underlying function is constant throughout the duration of the state, false it is not and the linear formula obtained is an approximation.
   */
  def isConstant(timeDependentVariables : Set[AtomicFormula], state : State, duration : Double): Boolean
}

object NumericConstraint extends Enumeration {
  type NumericConstraint = Value
  val LEQ, GEQ, LES, GRE, EQ = Value
}
import NumericConstraint._

object NumericUpdate extends Enumeration {
  type NumericUpdate = Value
  val INCREASE, DECREASE, ASSIGN, SCALEUP, SCALEDOWN = Value
}
import NumericUpdate._

case class LinearFormula(coefficients : Map[AtomicFormula, Double] = Map[AtomicFormula, Double](), constant : Double = 0.0)   {
  override def toString: String = coefficients.foldLeft("")((str, coeff) =>
    str + " (" + coeff._2 + " " + coeff._1 + ") + "
  ) + constant
}

case class LinearCondition(linearFormula1 : LinearFormula, linearFormula2 : LinearFormula, relation : NumericConstraint) {
  val hasVariables : Boolean = linearFormula1.coefficients.size + linearFormula2.coefficients.size > 0
}

case class LinearEffect(fluent : AtomicFormula, linearFormula : LinearFormula, numericUpdate : NumericUpdate)

case class NonLinearException(message : String) extends Exception