/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.ff

import uk.ac.kcl.inf.planning.solver._

/**
 * This Solver class does the searching using the respective heuristics.
 * Date: 14/11/13
 * Time: 19:35 
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 */
class FfSolver(problem: Problem) extends Solver
{
  /**
   * All the grounded actions for the problem.
   */

  //todo: the following is very slow, find another way
  println("Mapping " + problem.groundedPredicates.size + " predicates")
  val preconditionActionsMap : Map[AtomicFormula, Set[Action]] = problem.groundedPredicates.map(m => m -> { problem.groundedActions.filter(_.positivePropositionalPreconditions.contains(m))}).toMap
  println("Precondition Actions map ready")

  /**
   * Will try to solve the planning problem. Will run EHC with Helpful Actions to find the goal fast.
    * @return the list of actions in chronological order
   */
  def solve() : Plan =
  {
    findPathUsingEhcWithHa(problem.initialState)

    //todo: fall back to A* if we catch the GoalUnreachableException
  }

  /**
   * Tries to find a path to the goal using the given state and accumulates the actions with the action history stack.
   * Uses Enforced Hill Climbing with the FF heuristic and helpful actions.
   * @param state The state we want to find a goal from.
   * @return The the list of actions in chronological order to achieve the goal from the provided state.
   */
  def findPathUsingEhcWithHa(state: State) : Plan =
  {
    def findPathFrom(currentState: State, bestH : Int, helpfulActions: Set[Action], partialPlan: Plan, openList : List[ActionStateHeuristicInfo], closedList : Set[Int]) : (State, List[Action]) =
    {
      //check if we found the goal
      if (problem.goalCondition.satisfiedBy(currentState))
      {
        (currentState, partialPlan.reverse)     //if yes, just return the currentState and the stack of actions
      }
      else
      {
        //get all the successor states that we have not seen before (i.e. not in the closed list)
        val successorActionStates = (for {
          helpfulAction <- helpfulActions
          successorState = helpfulAction.applyToState(currentState)
          if !closedList.contains(successorState.hashCode())
        } yield (helpfulAction, successorState, partialPlan)).toList

        val ffSuccessors = computeFfForStates(successorActionStates, bestH)

        if (ffSuccessors.nonEmpty)         //are there any actions left to check?
        {
          //check if the head of the map is actually better than the bestH
          //in which case it will be the only one in the list because the previous call stops when finding a better H)
          val firstAction = ffSuccessors.head
          if (firstAction._2._1 < bestH)
          {
            //we have a better heuristic value! We can do EHC!
            val (nextAction, nextState, nextActionPlan) = firstAction._1
            val (newBestH, nextHelpfulActions) = firstAction._2
            println(newBestH)

            //we find the path from the current currentState while clearing all the open list, in EHC style
            //then we run find path again from the new currentState with the better H
            return findPathFrom(nextState, newBestH, nextHelpfulActions, nextAction :: nextActionPlan, List[ActionStateHeuristicInfo](), closedList + nextState.hashCode())
          }
        }

        val newOpenList = openList ++ ffSuccessors

        //check if there is anything left to check on the open list
        if (newOpenList.nonEmpty)
        {
          val ((nextAction, nextState, nextActionPlan), (_, nextHelpfulActions)) = newOpenList.head

          findPathFrom(nextState, bestH, nextHelpfulActions, nextAction :: nextActionPlan, newOpenList.tail, closedList + nextState.hashCode())
        }
        else
        { //out of options, just return the current currentState (which won't be the goal currentState) and the empty list
          //the outer function will detect this and throw an Exception that FF couldn't find the goal
          (currentState, List[Action]())
        }
      }
    }

    val (bestH : Int, helpfulActions : Set[Action]) = computeRpgHeuristic(state)
    val (endState : State, actions : List[Action]) = findPathFrom(state, bestH, helpfulActions, List[Action](), List[ActionStateHeuristicInfo](), Set[Int](state.hashCode()))
    println(endState)

    if (actions.isEmpty && !problem.goalCondition.satisfiedBy(endState))
      throw new FFGoalUnreachableException("Goal cannot be reached using FF", endState)

    actions
  }


  /**
   * Lazily computes the FF heuristic for each of the given Action-State pair.
   * It will filter out any states that have a dead-end heuristic value -1.
   * If there it finds one state which has a better heuristic value than bestH it will return it and stop looking for others,
   * returning only that in the list. (FF algorithm). Otherwise it returns all the successors.
   * Lazily means that the actual computation is postponed to until when the time of the next item comes.
   * @param actionStates : The list of action-state pairs
   * @param bestH - the best H value so far
   * @return A list of pairs of pairs, each has the original (action, state) pair and the matching (heuristic, helpful actions) pair
   */
  def computeFfForStates(actionStates : List[(Action, State, Plan)], bestH : Int) =
  {
    val stream = Stream(actionStates: _*).map(actionState => actionState -> { computeRpgHeuristic(actionState._2)}).filter(tuple => tuple._2._1 >= 0)
    stream.find(_._2._1 < bestH).map(List(_)).getOrElse(stream.sortBy(_._2._1).toList)
  }


  /**
   * Compute the Relaxed Planning Graph for a state and returns the distance (heuristic estimate) and helpful actions
   * @param state The state for which the FF RPG needs to be computed
   * @return the heuristic value and list of helpful actions
   */
  def computeRpgHeuristic(state: State) : (Int, Set[Action]) =
  {
 //   println(state)
 //   val start = System.nanoTime()
    val rpg = RelaxedPlanningGraph(state, problem.goalCondition, preconditionActionsMap)
    var ret = (rpg.hFF, rpg.helpfulActions)
 //   val end = System.nanoTime()

 //   println("RPG constructed in "  + ((end - start) / 1000000) + "ms")
 //   println("Helpful actions " + rpg.helpfulActions)
    ret
  }
}

class FFGoalUnreachableException(msg: String, endState: State) extends RuntimeException(msg)