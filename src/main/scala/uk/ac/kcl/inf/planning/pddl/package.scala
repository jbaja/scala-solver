/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning

import uk.ac.kcl.inf.planning.solver._

/**
 *
 * Date: 26/01/15
 * Time: 14:55 
 * @author josef
 */
package object pddl {

  def optionListToSet[A](o: Option[List[A]]) = o.fold(Set[A]())(_.toSet)

  def collectActions(structList : List[Any]) = (structList collect { case action : Action => action }).toSet

  def collectDurativeActions(structList : List[Any]) = (structList collect { case action : DurativeAction => action }).toSet

  def collectProcesses(structList : List[Any]) = (structList collect { case process : Process => process }).toSet

  def collectEvents(structList : List[Any]) = (structList collect { case event : Event => event }).toSet

}
