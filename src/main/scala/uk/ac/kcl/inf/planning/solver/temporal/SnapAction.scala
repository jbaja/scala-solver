/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.temporal

import uk.ac.kcl.inf.planning.solver.expressions.numeric.{LinearEffect, LinearCondition}
import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.expressions._
import uk.ac.kcl.inf.planning.solver.expressions.temporal.TimeSpecifier._
import uk.ac.kcl.inf.planning.solver.expressions.temporal.{TimedNumericEffect, TimeSpecifier}
import uk.ac.kcl.inf.planning.utils.Timer

class SnapAction(val durativeAction : DurativeAction, val when : TimeSpecifier, val startSnapAction : Option[SnapAction] = None) extends TemporalAction {
  val preconditions = durativeAction.conditionsAt(when)

  val positiveEffects = when match {
    case TimeSpecifier.AT_START => durativeAction.positiveEffects(when) + durativeAction.dummyProposition
    case TimeSpecifier.AT_END => durativeAction.positiveEffects(when)
  }

  private val timer : Timer = new Timer

  val negativeEffects = durativeAction.negativeEffects(when)

  lazy val precondition = when match {
    case TimeSpecifier.AT_START => AndCondition(preconditions.map(_.asInstanceOf[Expression]).toList)

    case TimeSpecifier.AT_END => AndCondition(ModalExpression(durativeAction.dummyProposition) :: preconditions.map(_.asInstanceOf[Expression]).toList)
  }

  lazy val invariant = AndCondition(invariants.map(_.asInstanceOf[Expression]).toList)

  lazy val numericEffects : Set[AtomicFormula] = durativeAction.numericEffects(when)

  /**
   * @return a pair with the positive effects followed with the negative effects
   */
  lazy val propositionalPreconditions = durativeAction.conditions(when)

  lazy val positivePropositionalPreconditions : Set[AtomicFormula] = propositionalPreconditions._1
  lazy val negativePropositionalPreconditions : Set[AtomicFormula] = propositionalPreconditions._2

  val invariants : Set[Condition] = {
    when match {
      case TimeSpecifier.AT_START =>
        durativeAction.conditionsAt(TimeSpecifier.OVER_ALL)

      case _ => Set[Condition]()
    }
  }

  def isApplicable(temporalState: TemporalState): Boolean = {
    preconditions.forall( precondition => {
      val conditions = precondition.getNonTemporallyDependentConditions(temporalState.timeDependentFluents)
      conditions.forall(_.satisfiedBy(temporalState.state))
    })
  }

  def isApplicableRelaxed(relaxedState: RelaxedState) : Boolean = {
    //if the snap action is a start snap action we need to check both the preconditions and invariants of its durative action
    when match {
      case TimeSpecifier.AT_START => if (preconditions.forall(_.relaxedSatisfiedBy(relaxedState))) {
          val newRelaxedState = applyToRelaxedState(relaxedState, relaxedState, SolverSettings.settings.EPSILON)
          invariants.forall(_.relaxedSatisfiedBy(newRelaxedState))
        }
        else {
          false
        }

      case TimeSpecifier.AT_END => preconditions.forall(_.relaxedSatisfiedBy(relaxedState)) &&
                                   ModalExpression(durativeAction.dummyProposition).relaxedSatisfiedBy(relaxedState) &&
                                    invariants.forall(_.relaxedSatisfiedBy(relaxedState))

      case _ => false
    }
  }

  def applyToRelaxedState(relaxedState : RelaxedState, cumulativeRelaxedState : RelaxedState, elapsed : Double) : RelaxedState = {
    durativeAction.applyToRelaxedState(relaxedState, cumulativeRelaxedState, when, elapsed)
  }

  override def applyToTemporalState(temporalState: TemporalState) : Option[TemporalState] = {
    if (!isApplicable(temporalState)) {
      return None
    }

    val numericUpdates = durativeAction.effect match {
      case numericEffect: TimedNumericEffect => numericEffect.evaluateNumeric(temporalState.state, when)
      case _ => Map[AtomicFormula, Double]()
    }

    val newState = State((temporalState.state.modalFacts --
      durativeAction.negativeEffects(when)) ++
      durativeAction.positiveEffects(when),
      temporalState.state.numericFluents ++ numericUpdates)

    val index = temporalState.plan.size //we are adding it at the end

    //todo: find the earliest interfering happening and only put the constraint after that (essentially achieving POPF functionality, but this requires changing how the helpful actions are deduced in the TemporalRPG)

    val newActionTemporalConstraints = getTemporalConstraints(index, temporalState)

    val (newRunningActions, newTemporalConstraints) = when match {
      case TimeSpecifier.AT_START =>
        val lbDuration = durativeAction.minimumDurationAtState(temporalState.state)
        val ubDuration = durativeAction.maximumDurationAtState(temporalState.state)
        //add the started action to the list of running actions
        val addedRunningActions = RunningAction(this, index, lbDuration, ubDuration) :: temporalState.runningActions
        (addedRunningActions, newActionTemporalConstraints)

      case TimeSpecifier.AT_END =>
        val runningAction = temporalState.runningActions.find(_.startAction.equals(startSnapAction.get)).get
        //remove the ended action from the list of running actions
        val subtractedRunningActions = temporalState.runningActions.filterNot(_.equals(runningAction))
        (subtractedRunningActions, newActionTemporalConstraints ++ runningAction.temporalConstraints(index))  //constraints on duration)
    }

    val invariants = newRunningActions.flatMap(_.startAction.invariants).toSet
    val newPlan = temporalState.plan ::: List(Happening(this, temporalState.state, newState, invariants))

    val newTemporalState = TemporalState(
      temporalState.problem,
      newState.modalFacts,
      newState.numericFluents,
      temporalState.timedActions,
      newRunningActions,
      newPlan,
      newTemporalConstraints
    )

    newTemporalState.scheduleState()
  }

  override def toString: String = durativeAction + " [" + when + "]"

  override def getLinearPreconditions(timeDependentVariables: Set[AtomicFormula], state: State): Set[LinearCondition] = {
    (durativeAction.conditionsAt(when) collect {
      case numericCondition: NumericCondition => numericCondition
    }).flatMap(_.toLinearCondition(timeDependentVariables, state))
  }

  def getLinearOverallConditions(timeDependentVariables: Set[AtomicFormula], state: State): Set[LinearCondition] = {
    if (when.equals(AT_START)) {
      (invariants collect {
        case numericCondition: NumericCondition => numericCondition
      }).flatMap(_.toLinearCondition(timeDependentVariables, state))
    }
    else
      Set[LinearCondition]()
  }

  override def getLinearEffects(timeDependentVariables: Set[AtomicFormula], state: State): List[LinearEffect] =
    durativeAction.effect match {
      case numericEffect: TimedNumericEffect =>
        numericEffect.toLinearEffect(timeDependentVariables, state, when)

      case _ => List[LinearEffect]()
  }

  override def equals(that: scala.Any): Boolean = System.identityHashCode(this) == System.identityHashCode(that.asInstanceOf[AnyRef])

  override def hashCode(): Int = System.identityHashCode(this)
}


