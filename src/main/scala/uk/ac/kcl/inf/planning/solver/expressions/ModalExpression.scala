/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.State
import uk.ac.kcl.inf.planning.solver.GroundedTerm
import uk.ac.kcl.inf.planning.solver.LiftedTerm
import uk.ac.kcl.inf.planning.solver.AtomicFormula
import uk.ac.kcl.inf.planning.solver.expressions.numeric.ExternalEvaluator
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState

/**
 *
 * Date: 10/02/14
 * Time: 17:19 
 * @author josef
 */
case class ModalExpression(predicate: AtomicFormula) extends Expression.Aux[ModalExpression] with Modal with Effect with Condition
{
  override lazy val positiveAtoms: Set[AtomicFormula] = Set[AtomicFormula](predicate)
  override lazy val negativeAtoms: Set[AtomicFormula] = Set[AtomicFormula]()

  /**
   * Updates the type hierarchy of the specified type. While parsing the full type hierarchy might not be immediately
   * known, and determined from a separate type list. This method helps populate the type hierarchy properly.
   * @param termType - the full type to be populated
   * @return The expression updated with the right type information
   */
  override def updateTypeHierarchy(termType: TermType): ModalExpression = copy(predicate = predicate.updateTypeHierarchy(termType))

  /**
   * If one of the lifted terms is untyped and has the same name, it is replaced by the specified typed parameter.
   * This is used mainly in places where the expression is being parsed without type information (such as PDDL) and type
   * information is provided afterwards.
   * @param typedParameter - The typed parameter to replace the untyped one (which has the same parameter name)
   * @return an expression with the typed parameter
   */
  override def typify(typedParameter: Term): ModalExpression = copy(predicate = predicate.typify(typedParameter))

  //todo: external modules can be used also for modal variables
  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): ModalExpression = this

  /**
   * Substitutes the lifted parameter with the grounded argument term.
   * @param parameter The lifted parameter to be substituted
   * @param argument The grounded argument to substitute the parameter
   * @return A new instance of the expression with the parameter substituted by the argument
   */
  override def substitute(parameter: LiftedTerm, argument: GroundedTerm): ModalExpression = copy(predicate = predicate.substitute(parameter, argument))

  /**
   * @return true if this expression is fully grounded, false otherwise
   */
  override def isGrounded: Boolean = predicate.isGrounded

  /**
   * Determines whether this condition is satisfied in the provided state.
   * @param state The state to be checked for this condition.
   * @param context The context in which this condition is operating (for local variables within actions)
   * @return true if the state satisfies the condition, false otherwise
   */
  override def satisfiedBy(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): Boolean = {
    val ret = state.modalFacts.contains(predicate)
    ret
  }

  override def relaxedSatisfiedBy(relaxedState: RelaxedState): Boolean = {
    relaxedState.modalFacts.contains(predicate)
  }

  /**
   * @param oldRelaxedState - the relaxed state prior to the current layer containing upper bounds and lower bounds of each numeric variable
   * @param cumulativeRelaxedState - the relaxed state accumulated from the current layer before this effect
   * @return a map of updated values to the fluents, with the values being an upper and a lower bound
   */
  override def relaxedEffect(oldRelaxedState: RelaxedState, cumulativeRelaxedState: RelaxedState): RelaxedState = {
    cumulativeRelaxedState.copy(modalFacts = cumulativeRelaxedState.modalFacts ++ positiveAtoms)
  }


  /**
   * Returns this condition if it is not satisfied by the provided state
   * @param state The state to be checked for this condition
   * @param context The context in which this condition is operating (for local variables within actions)
   * @return true if the state satisfies the condition, false otherwise
   */
  override def violations(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): Set[Condition] =
    if (satisfiedBy(state, context)) Set[Condition]() else Set[Condition](this)


  /**
   * Takes a state and produces a new state after applying the effects
   * @param state the current (old) state
   * @return the new updated state after applying the effect
   */
  override def applyToState(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): State =
    state.copy(modalFacts = state.modalFacts -- negativeAtoms ++ positiveAtoms)

  override def toString = predicate.toString

  override def equals(obj: scala.Any): Boolean = obj.getClass().equals(this.getClass()) && (obj.asInstanceOf[this.type].predicate.equals(this.predicate))

  override def invert: Expression with Condition = NotModalExpression(predicate)

  override def atomicFormulas: Set[AtomicFormula] = Set[AtomicFormula](predicate)
}
