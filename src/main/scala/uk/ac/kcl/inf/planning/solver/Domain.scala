/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver

import uk.ac.kcl.inf.planning.external._
import uk.ac.kcl.inf.planning.solver.expressions.numeric.ExternalEvaluator

import scala.util.parsing.input.Positional

/**
 *
 * Date: 12/12/13
 * Time: 14:37
 * @author josef
 */
class Domain(val name: String,
             val types : List[TermType],
             constant_defs : Set[GroundedTerm],
             liftedActions: Set[Action] = Set[Action](),
             liftedDurativeActions : Set[DurativeAction] = Set[DurativeAction](),
             liftedProcesses : Set[Process] = Set[Process](),
             liftedEvents : Set[Event] = Set[Event](),
             liftedPredicates: Set[AtomicFormula] = Set[AtomicFormula](),
             liftedFunctions : Set[AtomicFormula] = Set[AtomicFormula]()) extends Positional
{
  //todo: load these dynamically through configuration and mapping of class aliases
  val externalEvaluators : Map[String, ExternalEvaluator] = Map("Torricelli" -> Torricelli("Torricelli"),
                                                                "Vehicle" -> Vehicle("Vehicle"),
                                                                "Aggregator" -> Aggregator("Aggregator"),
                                                                "Storage" -> Storage("Storage"),
                                                                "Temperature" -> Temperature("Temperature"))

  val actions = liftedActions.map(a => a.hookExternalEvaluator(externalEvaluators).updateTypeHierarchy(types))
  val durativeActions = liftedDurativeActions.map(a => a.hookExternalEvaluator(externalEvaluators).updateTypeHierarchy(types))
  val processes = liftedProcesses.map(a => a.updateTypeHierarchy(types))
  val events = liftedEvents.map(a => a.updateTypeHierarchy(types))

  val predicates : Set[AtomicFormula] = liftedPredicates.map(p => p.updateTypeHierarchy(types)) collect {
    case af : AtomicFormula => af
  }

  val functions : Set[AtomicFormula] = liftedFunctions.map(f => f.updateTypeHierarchy(types)) collect {
    case af : AtomicFormula => af
  }

  val constants : Set[GroundedTerm] = constant_defs.map(entity => entity.updateTypeHierarchy(types)) collect {
    case gt : GroundedTerm => gt
  }

  override val toString: String = "(domain " + name + ")"
}
