/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.temporal

import java.io.{File, PrintWriter, FileWriter, FileReader}

import uk.ac.kcl.inf.planning.pddl.PddlParser
import uk.ac.kcl.inf.planning.solver.{AtomicFormula, SolverSettings, Action}
import uk.ac.kcl.inf.planning.solver.expressions.temporal.{Timed, TimeSpecifier}

import scala.annotation.tailrec

/**
 * Created by Josef on 04/01/14.
 */
object TemporalSolverLauncher
{
  @tailrec
  private def constructDurations(plan : List[Happening],
                         schedule : List[Double],
                         actionIndex : Int = 0,
                         mapRunningActionHashes : Map[Int, Int] = Map[Int, Int](),
                         mapActionDurations : Map[Int, Double] = Map[Int, Double]())
                         : Map[Int, Double] = {
    //todo: fix the way durations are being constructed
    plan match {
      case happening :: tail => {
        happening.temporalAction match {
          case goalAction: GoalDummyAction =>
            constructDurations(tail, schedule, actionIndex + 1, mapRunningActionHashes, mapActionDurations)

          case action: Action =>
            constructDurations(tail, schedule, actionIndex + 1, mapRunningActionHashes, mapActionDurations)

          case action: TimedAction =>
            constructDurations(tail, schedule, actionIndex + 1, mapRunningActionHashes, mapActionDurations)

          case snapAction: SnapAction =>
            if (snapAction.when.equals(TimeSpecifier.AT_START)) {
              constructDurations(tail, schedule, actionIndex + 1,
                mapRunningActionHashes.updated(snapAction.hashCode(), actionIndex),
                mapActionDurations)
            }
            else {
              //this is the end snap action, get the start one and compute the duration
              val startSnapAction = snapAction.startSnapAction.get
              val startActionIndex = mapRunningActionHashes(startSnapAction.hashCode())
              val duration = schedule(actionIndex) - schedule(startActionIndex)

              constructDurations(tail, schedule, actionIndex + 1,
                mapRunningActionHashes - startSnapAction.hashCode(),
                mapActionDurations.updated(startActionIndex, duration)
              )
            }
          }
        }

      case Nil => mapActionDurations
    }
  }

  @tailrec
  private def printPrettyPlan(plan : List[Happening], schedule : List[Double], mapActionDurations : Map[Int, Double], actionIndex : Int = 0) : Unit = {
    if (actionIndex == 0)
      printf("\n===============** PLAN [%.4f] **=========================\n\n", schedule.last)

    plan match {
      case happening :: tail =>
        val timestamp = if (schedule(actionIndex) < 0 && (0 - schedule(actionIndex)) < SolverSettings.settings.EPSILON) 0 else schedule(actionIndex)
        happening.temporalAction match {
          case action : Action =>
            printf("%.4f: %s\n", timestamp, action)

          case snapAction : SnapAction =>
            if (snapAction.when.equals(TimeSpecifier.AT_START))
              printf("%.4f: %s [%.4f]\n", timestamp, snapAction.durativeAction, mapActionDurations(actionIndex))

          case _ =>
        }
        printPrettyPlan(tail, schedule, mapActionDurations, actionIndex + 1)

      case Nil => println("\n===================================================================\n")
    }
  }

  //todo: construct a 'plan step' data structure that can be printed easily (each step has time, name and duration)

  def writeOutputPlan(output : PrintWriter, plan : List[Happening], schedule : List[Double], mapActionDurations : Map[Int, Double], actionIndex : Int = 0) : Unit = {
    plan match {
      case happening :: tail =>
        val timestamp = if (schedule(actionIndex) < 0 && (0 - schedule(actionIndex)) < SolverSettings.settings.EPSILON) 0 else schedule(actionIndex)
        happening.temporalAction match {
          case action: Action =>
            output.println("%.4f: %s".format(timestamp, action.toString))

          case snapAction: SnapAction =>
            if (snapAction.when.equals(TimeSpecifier.AT_START))
              output.println("%.4f: %s [%.4f]".format(timestamp, snapAction.durativeAction, mapActionDurations(actionIndex)))

          case _ =>
        }
        writeOutputPlan(output, tail, schedule, mapActionDurations, actionIndex + 1)

      case Nil =>
    }
  }

  def writeOutputToFile(filename : String, timeTaken : Long, plan : List[Happening], schedule : List[Double], mapActionDurations : Map[Int, Double]) : Unit = {
    val planOutput = new PrintWriter(new FileWriter(filename + ".plan", false))
    writeOutputPlan(planOutput, plan, schedule, mapActionDurations)
    planOutput.close()

    val infoOutput = new PrintWriter(new FileWriter(filename + ".info", false))
    writePlanInfo(infoOutput, timeTaken, plan, schedule)
    infoOutput.close()
  }


  def writePlanInfo(output : PrintWriter, timeTaken : Long, plan : List[Happening], schedule : List[Double]) = {
    output.println("Time taken to generate solution: " + timeTaken + "ms")
    output.println("States explored " + TemporalState.stateCount)
    output.println("Plan Steps: " + plan.length)
    output.println("Timed Happenings: " + plan.count(th => th.temporalAction match {
      case t: TimedAction => true
      case _ => false
    }))
  }

  def processCommandLineSettings(args : Array[String]): Unit = {
    var useBrfs = false
    var useNumericHeuristic = true
    var useLinearNumericHeuristic = true
    var epsilon = SolverSettings.settings.EPSILON
    var maxError = SolverSettings.settings.MAX_ERROR
    var maxImprovementIterations = SolverSettings.settings.MAX_IMPROVEMENT_ITERATIONS
    var useEhcAb = SolverSettings.settings.EHC_AB
    var ehchOrderThresh = SolverSettings.settings.EHC_H_ORDERING_THRESHOLD
    var parallel = SolverSettings.settings.PARALLEL

    if (args.length > 2) {
      for (i <- 2 until args.length) {
        args(i) match {
          case "-brfs" => useBrfs = true

          case "-numhOff" => {
            useNumericHeuristic = false
            useLinearNumericHeuristic = false
          }

          case "-numhLinOnly" => {
            useNumericHeuristic = false
            useLinearNumericHeuristic = true
          }

          case "-ehcAbOff" => useEhcAb = false

          case "-par" => parallel = true

          case "-ehchOrderTresh" => if (args.length > i + 1) ehchOrderThresh = args(i + 1).toInt
          else println("Invalid (missing) EHC H Order Threshold value. Using Default " + SolverSettings.settings.EHC_H_ORDERING_THRESHOLD)

//          case "-epsilon" => if (args.length > i + 1) epsilon = args(i + 1).toDouble
//          else println("Invalid (missing) EPSILON value. Using Default " + SolverSettings.settings.EPSILON)
//
//          case "-nonlinMaxErr" => if (args.length > i + 1) maxError = args(i + 1).toDouble
//          else println("Invalid (missing) NonLin Max Convergence Error value. Using Default " + SolverSettings.settings.MAX_ERROR)

          case "-nonlinMaxIt" => if (args.length > i + 1) maxImprovementIterations = args(i + 1).toInt
          else println("Invalid (missing) NonLin Max Convergence Iterations value. Using Default " + SolverSettings.settings.MAX_IMPROVEMENT_ITERATIONS)

          case _ =>
        }
      }
    }

    val newSolverSettings = SolverSettings(useBrfs, useEhcAb, ehchOrderThresh, epsilon, maxError, maxImprovementIterations, useNumericHeuristic, useLinearNumericHeuristic, parallel)
    SolverSettings.settings = newSolverSettings
    println("Using Solver Settings: " + newSolverSettings)
  }


  def main(args: Array[String])
  {
//    println(new File(".").getAbsolutePath())
    println("Starting planner")
    if (args.length >= 2)
    {
      println("Parsing domain")
      val parsedDomain = PddlParser.parseDomain(new FileReader(args(0)))
      parsedDomain match {
        case PddlParser.Success(r, n) => println(r)
        case PddlParser.Failure(msg, n) => println(msg + " in " + args(0) + ":" + n.pos)
        case PddlParser.Error(msg, n) => println(msg + " in " + args(0) + ":" + n.pos)
      }

      println("Parsing problem")
      val parsedProblem = PddlParser.parseProblem(parsedDomain.get, new FileReader(args(1)))
      parsedProblem match {
        case PddlParser.Success(r, n) => println(r)
        case PddlParser.Failure(msg, n) => println(msg + " in " + args(1) + ":" + n.pos)
        case PddlParser.Error(msg, n) => println(msg + " in " + args(1) + ":" + n.pos)
      }

      processCommandLineSettings(args)
      println("Settings: " + SolverSettings.settings)

      val problemFile = new File(args(1))
      println("Initialising solver for problem file ... " + problemFile)

      val solver = new TemporalSolver(parsedProblem.get)

      println("Starting search ...")
      var start = System.currentTimeMillis()
      val result = solver.solve()
      var end = System.currentTimeMillis()

      println("Time took %d ms".format(end - start))

      result match {
        case Some((plan, schedule)) =>
          for (i <- plan.indices)
          {
            printf("%.3f: Old State %s\n", schedule(i), plan(i).oldState)
            printf("%.3f: --> %s -->\n", schedule(i), plan(i).temporalAction)
            printf("%.3f: New State %s\n", schedule(i), plan(i).newState)
          }

          for (i <- plan.indices)
          {
             printf("%.3f, %s\n", schedule(i), plan(i).temporalAction)
          }

        println("\n*** Plan Happenings ***\n")

          for (i <- plan.indices)
          {
            plan(i).temporalAction match {
              case goal : GoalDummyAction =>
              case _ =>  printf("%.3f: %s\n", schedule(i), plan(i))
            }
          }
          println("\n** Numeric fluent values **")
          plan.last.newState.numericFluents.foreach(println(_))

          val timeTaken = end - start
          println("Time took %d ms".format(timeTaken))
          println("States explored " + TemporalState.stateCount)

          println("Plan Steps: " + (plan.last.temporalAction match {
            case t : GoalDummyAction => plan.length - 1
            case _ => plan.length
          }))

          println("Timed Happenings: " + plan.count(th => th.temporalAction match {
            case t: TimedAction => true
            case _ => false
          }))

          val happeningDurations = constructDurations(plan, schedule)
          printPrettyPlan(plan, schedule, happeningDurations)
          val outputFileTitle = problemFile.getName.replaceFirst("[.][^.]+$", "")
          writeOutputToFile(outputFileTitle, timeTaken, plan, schedule, happeningDurations)

        case None =>
          println("=======*** NO PLAN FOUND ***========")
      }
    }
    else
    {
      println("No PDDL domain and problem specified!")
    }
  }
}
