/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver

import scala.annotation.tailrec

/**
 * An atomic formula, the most basic type of expression
 * @param predicate The predicate name
 * @param parameters The terms for the predicate.
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 */
case class AtomicFormula(predicate : String, parameters: List[Term] = List[Term]())
{
  val isGrounded = parameters.forall(_.isGrounded)

  /**
   * Helper value for the parameters that have not yet been bound to a value
   */
  lazy val liftedParameters: List[LiftedTerm] =
    parameters collect {
      case pt: LiftedTerm => pt
    }

   def substitute(parameter: LiftedTerm, argument: GroundedTerm) : AtomicFormula =
  {
    //check if the parameter is actually included, if yes replace it and return a new AtomicFormula
    //we call substitute again recursively just in case the same parameter is used more than once in the predicate
    val i = parameters.indexOf(parameter)
    if (i >= 0)
      AtomicFormula(predicate, parameters.updated(i, argument)).substitute(parameter, argument)
    else
      this
  }

  def typify(typedParameter: Term) : AtomicFormula =
  {
    @tailrec def typifyFrom(typedParameter: Term, startFrom : Int, expAcc : AtomicFormula) : AtomicFormula = {
      val i = parameters.indexWhere(_.symbol == typedParameter.symbol, startFrom)
      if (i >= 0)
        typifyFrom(typedParameter, i+1, AtomicFormula(expAcc.predicate, expAcc.parameters.updated(i, typedParameter)))
      else
        expAcc
      }

    typifyFrom(typedParameter, 0, this)
  }

  final def updateTypeHierarchy(termType : TermType) : AtomicFormula =
  {
    @tailrec def updateFrom(startFrom : Int, expAcc : AtomicFormula) : AtomicFormula = {
      val i = parameters.indexWhere(_.termType.name == termType.name, startFrom)
      if (i >= 0)
      {
        updateFrom(i+1, AtomicFormula(expAcc.predicate, expAcc.parameters.updated(i, expAcc.parameters(i).updateTypeHierarchy(termType))))
      }
      else
        expAcc
    }

    updateFrom(0, this)
  }

  /**
   * Updates the type hierarchy of the specified type. While parsing the full type hierarchy might not be immediately
   * known, and determined from a separate type list. This method helps populate the type hierarchy properly.
   * @param typeList- list of types to be updated
   * @return The expression updated with the right type information
   */
   @tailrec final def updateTypeHierarchy(typeList : List[TermType]) : AtomicFormula = {
    typeList match
    {
      case termType :: tail =>
        updateTypeHierarchy(termType).updateTypeHierarchy(tail)

      case Nil => this
    }
  }


  @tailrec final def typifyAll(typedParameters : List[Term]) : AtomicFormula = {
    typedParameters match {
      case head :: tail =>
        typify(head).typifyAll(tail)

      case Nil => this //all typifications done
    }
  }

  /**
   * Generates the grounded atomic formulas for this atomic formula.
   * If its already grounded, it will return itself
   * @param possibleValues  the type to object mappings
   * @return The set of grounded atomic formulas
   */
  def generateGroundedAtomicFormulas(possibleValues : Map[TermType, Set[GroundedTerm]]) : Set[AtomicFormula] =
  {
    if (isGrounded) {
      Set[AtomicFormula](this)
    }
    else
    {
      def generateGroundedAcc(accSubstitutions: List[(LiftedTerm, GroundedTerm)], remainder: List[LiftedTerm]): Set[AtomicFormula] =
      {
        remainder match
        {
          case Nil => Set(substituteList(accSubstitutions))

          case parameter :: tail => for
          {
            argument <- possibleValues(parameter.termType) if possibleValues.contains(parameter.termType)
            rest <- generateGroundedAcc((parameter, argument) :: accSubstitutions, tail)
          } yield rest
        }
      }

      generateGroundedAcc(Nil, liftedParameters)
    }
  }

  /**
   * Substitutes a list of parameters with their respective grounded terms
   * @param pairList a list of pairs, the parameter and respective argument to be substituted with
   * @return the atomic formula with the specified parameters replaced by their respective arguments
   */
  @tailrec final def substituteList(pairList : List[(LiftedTerm, GroundedTerm)]) : AtomicFormula =
  {
    pairList match
    {
      case (parameter, argument) :: tail =>
        substitute(parameter, argument).substituteList(tail)

      case Nil => this //all substitutions done
    }
  }

  override val toString = parameters.foldLeft("(" + predicate)((str, parameter) => str + " " + parameter) + ")"

  def evaluateNumeric(state: State): Map[AtomicFormula, Double] = state.numericFluents.get(this) match {
    case Some(number) => Map[AtomicFormula, Double]((this, number))
    case None => Map[AtomicFormula, Double]()
  }
}

//todo: add EqualityAtomicFormula

class ExpressionUngroundedException(msg: String) extends RuntimeException(msg)