/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.external

import uk.ac.kcl.inf.planning.solver.{AtomicFormula, State}
import uk.ac.kcl.inf.planning.solver.expressions.numeric.{NumericFluent, ExternalEvaluator}

/**
 * External module representing a car, accelerating and decelerating.
 * Date: 25/02/15
 * Time: 16:35 
 * @author josef
 */
case class Vehicle(aliasPrefix : String) extends ExternalEvaluator {

  override def fullName: String = "Kcl.Planning.Vehicle"

  val AIR_RESISTANCE_START = 50

  override def value(fluent: NumericFluent, state: State, t: Double): Option[Double] = {
    val method = fluent.functionPredicate.predicate.drop(aliasPrefix.length() + 1) //we add one to remove the .
    method match {
      case "accelerate" => accelerate(fluent, state, t)

      case "travel-rate" => travelRate(fluent, state, t)

      case _ => handleStaticFluent(fluent, state)
    }
  }

  /**
   * Calculates a (linear approximation of the) gradient with which the velocity needs to be updated according to the current acceleration parameter.
   * The method takes care of wind resistance that starts to manifest as soon as the velocity exceeds 50.
   * @param fluent - the fluent semantically attached with this method
   * @param state - the state comprising of boolean and numeric fluents
   * @param t - time to spend in this state
   * @return The rate with which to update the velocity over time t spent in the state.
   */
  def travelRate(fluent : NumericFluent, state : State, t : Double) : Option[Double] = {
    if (fluent.functionPredicate.parameters.size != 1) {
      throw new Exception("Invalid parameters for module method: " + fluent.functionPredicate + " Expecting 1 parameter of type Vehicle.Car")
    }

    val distanceAt0 = state.numericFluents(AtomicFormula(aliasedName("distance-traveled"), List(fluent.functionPredicate.parameters.head)))
    val velocityAt0 = state.numericFluents(AtomicFormula(aliasedName("velocity"), List(fluent.functionPredicate.parameters.head)))
    val acceleration = state.numericFluents(AtomicFormula(aliasedName("acceleration"), List(fluent.functionPredicate.parameters.head)))

    val airResistanceTimePoint = if (velocityAt0 >= AIR_RESISTANCE_START) 0
                                 else if (acceleration > 0) (AIR_RESISTANCE_START - velocityAt0) / acceleration
                                 else Double.PositiveInfinity

    val distanceTraveledAtT = if (airResistanceTimePoint > t) {
      velocityAt0 * t + ((acceleration * t * t) / 2)
    }
    else {
      val draggedTime = t - airResistanceTimePoint
      val velocityAtDragStart = (acceleration * airResistanceTimePoint) + velocityAt0
      val distanceRateAfterDrag = (0.1 * acceleration * draggedTime * draggedTime) - (10 * acceleration * draggedTime * draggedTime) + (AIR_RESISTANCE_START * AIR_RESISTANCE_START) + velocityAtDragStart

      (velocityAt0 * airResistanceTimePoint) + ((acceleration * airResistanceTimePoint * airResistanceTimePoint) / 2) + (draggedTime * distanceRateAfterDrag)
    }

    Some(distanceTraveledAtT / t)
  }

  /**
   * Calculates a (linear approximation of the) gradient with which the distance traveled needs to be updated according to the current acceleration parameter.
   * The method takes care of wind resistance that starts to manifest as soon as the velocity exceeds 50.
   * @param fluent - the fluent semantically attached with this method
   * @param state - the state comprising of boolean and numeric fluents
   * @param t - time to spend in this state
   * @return The rate with which to update the velocity over time t spent in the state.
   */
  def accelerate(fluent : NumericFluent, state : State, t : Double) : Option[Double] = {
    if (fluent.functionPredicate.parameters.size != 1) {
      throw new Exception("Invalid parameters for module method: " + fluent.functionPredicate + " Expecting 1 parameter of type Vehicle.Car")
    }

    val acceleration = state.numericFluents(AtomicFormula(aliasedName("acceleration"), List(fluent.functionPredicate.parameters.head)))
    val velocityAt0 = state.numericFluents(AtomicFormula(aliasedName("velocity"), List(fluent.functionPredicate.parameters.head)))
    val velocityAtT = velocityAt(velocityAt0, acceleration, t)

    Some((velocityAtT - velocityAt0) / t)
  }

  private def velocityAt(initialVelocity : Double, acceleration : Double, t : Double): Double =
  {
    val airResistanceTimePoint = if (initialVelocity >= AIR_RESISTANCE_START) 0
                                 else if (acceleration > 0) (AIR_RESISTANCE_START - initialVelocity) / acceleration
                                 else Double.PositiveInfinity

    (acceleration * t) + initialVelocity

    if (airResistanceTimePoint > t) {
      (acceleration * t) + initialVelocity
    }
    else {
      val c0 = acceleration * airResistanceTimePoint + initialVelocity  //velocity at air resistance start point
      val remainingT = t - airResistanceTimePoint

      val c1 = Math.sqrt(10 * acceleration) / 5
      val e = Math.exp(-c1 * remainingT)

      val numerator = (c0 * (Math.sqrt(10 * acceleration) - AIR_RESISTANCE_START) * e) + AIR_RESISTANCE_START + Math.sqrt(10 * acceleration)
      val denominator = 1 - (c0 * e)

      numerator / denominator
    }
  }

  override def isConstant(fluent: NumericFluent, state: State, t: Double): Boolean = {
    val function = fluent.functionPredicate.predicate.drop(aliasPrefix.length() + 1) //we add one to remove the .
    val acceleration = state.numericFluents(AtomicFormula(aliasedName("acceleration"), List(fluent.functionPredicate.parameters.head)))

    acceleration == 0
  }
}
