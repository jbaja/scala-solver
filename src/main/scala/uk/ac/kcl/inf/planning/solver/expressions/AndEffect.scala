/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.GroundedTerm
import uk.ac.kcl.inf.planning.solver.LiftedTerm
import uk.ac.kcl.inf.planning.solver.expressions.numeric._
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState

/**
 *
 * Date: 10/02/14
 * Time: 17:51 
 * @author josef
 */
case class AndEffect(expressions: List[Expression]) extends Expression.Aux[AndEffect] with Modal with NumericEffect
{
  override lazy val isGrounded = expressions.forall(exp => exp.isGrounded)

  val modalExpressions = expressions collect {
    case modalExpression : Modal => modalExpression
  }

  override lazy val positiveAtoms =
    modalExpressions.foldLeft(Set[AtomicFormula]())((s, a) => s union a.positiveAtoms)

  override lazy val negativeAtoms =
    modalExpressions.foldLeft(Set[AtomicFormula]())((s, a) => s union a.negativeAtoms)


  override def substitute(parameter: LiftedTerm, argument: GroundedTerm)  =
    AndEffect(expressions.map(exp => exp.substitute(parameter, argument)))

  override def typify(typedParameter: Term): AndEffect =
    AndEffect(expressions.map(exp => exp.typify(typedParameter)))

  def updateTypeHierarchy(termType: TermType): AndEffect =
    AndEffect(expressions.map(exp => exp.updateTypeHierarchy(termType)))

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): AndEffect =
    AndEffect(expressions.map(exp => exp.hookExternalEvaluator(evaluators)))

  override def toString =
    expressions.foldLeft("AND (")((str, exp) => str + " " + exp) + ")"

  def evaluateNumeric(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): Map[AtomicFormula, Double] = (expressions collect {
    case numericEffect : NumericEffect => numericEffect
  }).flatMap(_.evaluateNumeric(state, context)).toMap


  /**
   * @param oldRelaxedState - the relaxed state prior to the current layer containing upper bounds and lower bounds of each numeric variable
   * @param cumulativeRelaxedState - the relaxed state accumulated from the current layer before this effect
   * @return a map of updates to fluents to a tuple with a possible positive and a possible negative effect on the variable in the relaxed state.
   */
  def relaxedEffect(oldRelaxedState: RelaxedState, cumulativeRelaxedState : RelaxedState) : RelaxedState = {
    expressions.foldLeft(cumulativeRelaxedState)((accState, expression) =>
      expression match {
        case effect : Effect => effect.relaxedEffect(oldRelaxedState, accState)
        case _ => accState
      })
  }


  /**
   * Takes a state and produces a new state after applying the effects
   * @param state the current (old) state
   * @return the new updated state after applying the effect
   */
  override def applyToState(state: State, context: Map[Expression, Double] = Map[Expression, Double]()) =  State(state.modalFacts -- negativeAtoms ++ positiveAtoms, state.numericFluents ++ evaluateNumeric(state, context))

  override def numericFluents: Set[NumericFluent] = {
    (for (expression <- expressions collect {
      case numericEffect : NumericEffect => numericEffect
    }) yield expression.numericFluents).flatten.toSet
  }

  override def toLinearEffect(timeDependentVariables : Set[AtomicFormula], state : State, t : Double = 0.0) : List[LinearEffect] = {
    (expressions collect {
      case numericEffect : NumericEffect => numericEffect
    }).flatMap(_.toLinearEffect(timeDependentVariables, state, t))
  }
}
