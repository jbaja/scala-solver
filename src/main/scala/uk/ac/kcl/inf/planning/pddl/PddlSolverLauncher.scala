/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.pddl

import java.io.FileReader

import uk.ac.kcl.inf.planning.solver.ff.FfSolver

/**
 * Created by Josef on 04/01/14.
 * @deprecated This class has been replaced by TemporalSolverLauncher.
 */
object PddlSolverLauncher
{
  def main(args: Array[String])
  {
    if (args.length >= 2)
    {
      val parsedDomain = PddlParser.parseDomain(new FileReader(args(0)))
      parsedDomain match {
        case PddlParser.Success(r, n) => println(r)
        case PddlParser.Failure(msg, n) => println(msg + " in " + args(0) + ":" + n.pos)
        case PddlParser.Error(msg, n) => println(msg + " in " + args(0) + ":" + n.pos)
      }

      val parsedProblem = PddlParser.parseProblem(parsedDomain.get, new FileReader(args(1)))
      parsedProblem match {
        case PddlParser.Success(r, n) => println(r)
        case PddlParser.Failure(msg, n) => println(msg + " in " + args(1) + ":" + n.pos)
        case PddlParser.Error(msg, n) => println(msg + " in " + args(1) + ":" + n.pos)
      }

      println("Initialising solver...")

      val solver = new FfSolver(parsedProblem.get)

      println("Starting search ...")
      var start = System.currentTimeMillis()
      val actions = solver.solve()
      var end = System.currentTimeMillis()

      println("Time took %d ms".format(end - start))

      actions.foreach(action => println(action))

    }
    else
    {
      println("No PDDL domain and problem specified!");
    }
  }
}
