/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.temporal

import uk.ac.kcl.inf.planning.solver.expressions.numeric.{LinearEffect, NumericFluent}
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState
import uk.ac.kcl.inf.planning.solver.{AtomicFormula, State}
import uk.ac.kcl.inf.planning.solver.expressions.{Expression, Condition}

/**
 *
 * Date: 11/02/14
 * Time: 11:51 
 * @author josef
 */
object TimeSpecifier extends Enumeration {
  type TimeSpecifier = Value
  val AT_START, AT_END, OVER_ALL = Value
}

import TimeSpecifier._

trait Timed {
  def when: Set[TimeSpecifier]
}

trait TimedCondition {
  def satisfiedBy(state: State, when: TimeSpecifier): Boolean

  def conditions(when: TimeSpecifier): Set[Condition]
}

trait TimedEffect {
  def applyToState(state: State, when: TimeSpecifier): State

  def evaluateRelaxed(oldRelaxedState : RelaxedState, cumulativeRelaxedState : RelaxedState, when : TimeSpecifier) : RelaxedState
}

trait ContinuousNumericEffect {
  def evaluateNumericDelta(state: State, t: Double, context: Map[Expression, Double] = Map[Expression, Double]()): Map[AtomicFormula, Double]

  def numericFluents: Set[NumericFluent]

  def continuousExpression: ContinuousTemporalExpression

  def gradient(timeDependent : Set[AtomicFormula], state : State, duration : Double) : Double

  def isLinear(timeDependent : Set[AtomicFormula], state : State, duration : Double) : Boolean

  def fluent : NumericFluent

  /**
   * @param oldRelaxedState - the relaxed state prior to the current layer containing upper bounds and lower bounds of each numeric variable
   * @param cumulativeRelaxedState - the relaxed state accumulated from the current layer before this effect
   * @return a map of updated values to the fluents, with the values being an upper and a lower bound
   */
  def relaxedEffect(oldRelaxedState: RelaxedState, cumulativeRelaxedState : RelaxedState, elapsedTime : Double) : RelaxedState

  def getRelaxedEffectPreconditions(relaxedState : RelaxedState, elapsedTime : Double) : Set[Condition]
}

trait TimedNumericEffect extends TimedEffect {
  def evaluateNumeric(state: State, when: TimeSpecifier, context: Map[Expression, Double] = Map[Expression, Double]()): Map[AtomicFormula, Double]

  def numericFluents(when: TimeSpecifier): Set[NumericFluent]

  def toLinearEffect(timeDependentVariables : Set[AtomicFormula], state : State, when : TimeSpecifier) : List[LinearEffect]
}

trait TimedModal {
  def atoms(when: TimeSpecifier) = (positiveAtoms(when), negativeAtoms(when))

  def negativeAtoms(when: TimeSpecifier): Set[AtomicFormula]

  def positiveAtoms(when: TimeSpecifier): Set[AtomicFormula]
}

trait TemporalNumeric {
  /**
   * Provides the numeric value of this entity for this state
   * @param state the current state
   * @param t the elapsed time
   * @return An option with a double value Some(Double), or None if it is undefined in the current state.
   */
  def value(state: State, t: Double, context: Map[Expression, Double] = Map[Expression, Double]()): Option[Double]

  def fluents: Set[NumericFluent]
}