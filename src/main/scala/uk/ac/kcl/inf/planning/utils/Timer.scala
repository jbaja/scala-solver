/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.utils

/**
  * Class used to debug performance and find how much time is elapsing between the different activities of the planner.
  * Created by Josef on 16/04/2015.
  * @author Josef Bajada
  */
class Timer {
  var startTime = System.currentTimeMillis()

  /**
   * @return The time elapsed since the last time this method was called. (Or since this object was first initialised.)
   */
  def lap() : Long = {     
    val elapsed = System.currentTimeMillis() - startTime
    startTime = System.currentTimeMillis() //we do it again to get the latest one and get a more accurate one for the next lap
    elapsed
  }

  def log(message : String) = {
    printf("%d: %s\n", lap(), message)
  }
}
