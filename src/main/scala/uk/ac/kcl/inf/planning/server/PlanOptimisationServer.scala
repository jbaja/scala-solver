/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.server

import java.io.{StringReader, FileReader}
import java.util.Date

import uk.ac.kcl.inf.planning.optimiser.Optimiser
import uk.ac.kcl.inf.planning.pddl.PddlParser
import uk.ac.kcl.inf.planning.solver.expressions.temporal.TimeSpecifier
import uk.ac.kcl.inf.planning.solver.{LiftedTerm, Problem, Domain}
import uk.ac.kcl.inf.planning.sppa.{Sppa, SppaMessage}
import uk.ac.kcl.inf.planning.sppa.server.{PlannerServer, PlannerClientListener, PlannerClientListenerFactory}
import uk.ac.kcl.inf.planning.validator._

/**
 *
 * Date: 01/09/14
 * Time: 14:25 
 * @author josef
 */
class PlanningClientListenerImpl extends PlannerClientListener {

  var domain : Domain = null
  var problem : Problem = null
  var timedInitialLiterals : Iterable[TimedInitialLiteral] = List[TimedInitialLiteral]()
  var timedInitialFluents : Iterable[TimedInitialFluent] = List[TimedInitialFluent]()

  var seedPlan : GroundedPlan = null
  var actionSteps : List[TimedApplicable] = List[TimedApplicable]()

  override def onDomain(domainPddl: String): SppaMessage = {
    print("Received domain ... ")
    val parsedDomain = PddlParser.parseDomain(new StringReader(domainPddl))

    parsedDomain match {
      case PddlParser.Success(result, nextPos) => {
        this.domain = result
        println("OK")
        new SppaMessage(Sppa.CODE_ERR_OK, Sppa.MSG_ERR_OK)
      }

      case PddlParser.Failure(msg, n) => {
        val payload = msg + " in domain line: " + n.pos;
        println("ERR " + Sppa.CODE_ERR_PARSE_DOMAIN + " " + Sppa.MSG_ERR_PARSE_DOMAIN)
        println(payload)
        new SppaMessage(Sppa.CODE_ERR_PARSE_DOMAIN, Sppa.MSG_ERR_PARSE_DOMAIN, payload)
      }

      case PddlParser.Error(msg, n) => {
        val payload = msg + " in domain line: " + n.pos;
        println("ERR " + Sppa.CODE_ERR_PARSE_DOMAIN + " " + Sppa.MSG_ERR_PARSE_DOMAIN)
        println(payload)
        new SppaMessage(Sppa.CODE_ERR_PARSE_DOMAIN, Sppa.MSG_ERR_PARSE_DOMAIN, payload)
      }
    }
  }

  override def onProblem(problemPddl: String): SppaMessage = {
    print("Received problem ... ");
    if (domain == null)
    {
       println("ERR " + Sppa.CODE_ERR_MISSING_DOMAIN + " " + Sppa.MSG_ERR_MISSING_DOMAIN);
       new SppaMessage(Sppa.CODE_ERR_MISSING_DOMAIN, Sppa.MSG_ERR_MISSING_DOMAIN, "The domain needs to be provided before providing the problem file.")
    }
    else
    {
      val parsedProblem = PddlParser.parseProblem(domain, new StringReader(problemPddl))

      parsedProblem match {
        case PddlParser.Success(result, nextPos) => {
          this.problem = result
          this.timedInitialLiterals = problem.timedInitialLiterals.flatMap(timedList => timedList._2 map (til => TimedInitialLiteral(timedList._1, til)))
          this.timedInitialFluents = problem.timedInitialFluents.flatMap(timedList => timedList._2 map (tif => TimedInitialFluent(timedList._1, tif._1, tif._2)))

          println("OK")
          new SppaMessage(Sppa.CODE_ERR_OK, Sppa.MSG_ERR_OK)
        }

        case PddlParser.Failure(msg, n) => {
          val payload = msg + " in problem line: " + n.pos;
          println("ERR " + Sppa.CODE_ERR_PARSE_PROBLEM + " " + Sppa.MSG_ERR_PARSE_PROBLEM)
          println(payload)
          new SppaMessage(Sppa.CODE_ERR_PARSE_PROBLEM, Sppa.MSG_ERR_PARSE_PROBLEM, payload)
        }

        case PddlParser.Error(msg, n) => {
          val payload = msg + " in problem line: " + n.pos;
          println("ERR " + Sppa.CODE_ERR_PARSE_PROBLEM + " " + Sppa.MSG_ERR_PARSE_PROBLEM)
          println(payload)
          new SppaMessage(Sppa.CODE_ERR_PARSE_PROBLEM, Sppa.MSG_ERR_PARSE_PROBLEM, payload)
        }
      }
    }
  }

  override def onSeedPlan(seedPlanText: String): SppaMessage = {
    print("Received seed plan ... ");
    if (domain == null)
    {
      new SppaMessage(Sppa.CODE_ERR_MISSING_DOMAIN, Sppa.MSG_ERR_MISSING_DOMAIN, "The domain needs to be provided before providing the seed plan.")
    }
    else if (problem == null)
    {
      new SppaMessage(Sppa.CODE_ERR_MISSING_PROBLEM, Sppa.MSG_ERR_MISSING_PROBLEM, "The problem needs to be provided before providing the seed plan.")
    }
    else
    {
      val parsedPlan = PlanParser.parsePlan(new StringReader(seedPlanText))
      parsedPlan match {
        case PlanParser.Success(result, next) => {

          actionSteps = result.planSteps.flatMap(planAction => {
            //match all the terms with their respective typed grounded terms from the problem file
            val groundedTerms = PlanAnalyserLauncher.getGroundedTerms(problem, planAction.terms)

            //find the action from the actions list of the domain file
            domain.actions.find(action => action.name equals planAction.actionSymbol) match {
              case Some(action) => {
                val pairList = (action.parameters collect {
                  case lt: LiftedTerm => lt
                }) zip groundedTerms
                val groundedAction = action.substituteList(pairList)
                List[TimedApplicable](TimedApplicableAction(planAction.at, groundedAction))
              }

              case None => {
                //not in the actions list, check if it is a durative action
                domain.durativeActions.find(durativeAction => durativeAction.name equals planAction.actionSymbol) match {
                  case Some(durativeAction) => {
                    val pairList = (durativeAction.parameters collect {
                      case lt: LiftedTerm => lt
                    }) zip groundedTerms

                    val groundedDurativeAction = durativeAction.substituteList(pairList).copy(duration = Some(planAction.duration))
                    val startAction = TimedApplicableDurativeAction(planAction.at, groundedDurativeAction, TimeSpecifier.AT_START)
                    val endAction = TimedApplicableDurativeAction(planAction.at + planAction.duration, groundedDurativeAction, TimeSpecifier.AT_END, Some(startAction))
                    List[TimedApplicable](startAction, endAction)
                  }

                  case None =>
                    val msg = "Unable to find action " + planAction.actionSymbol + " in domain!"
                    return new SppaMessage(Sppa.CODE_ERR_PLAN_MISMATCH, Sppa.MSG_ERR_PLAN_MISMATCH, msg)
                }
              }
            }
          })


          //todo: solve bug with TILs and TIFs that have time 0, and also what if 2 TILs have to happen at the same time?
          val allPlanSteps = (actionSteps ++ this.timedInitialLiterals ++ this.timedInitialFluents).sortBy(_.at)

          val groundedPlan = GroundedPlan(problem, allPlanSteps)
          println("\nGrounded Plan: " + groundedPlan)

          this.seedPlan = groundedPlan
          println("OK");

          new SppaMessage(Sppa.CODE_ERR_OK, Sppa.MSG_ERR_OK)
        }

        case PlanParser.Failure(msg, n) => {
          val payload = msg + " in seed plan line: " + n.pos;
          println(payload)
          new SppaMessage(Sppa.CODE_ERR_PARSE_PLAN, Sppa.MSG_ERR_PARSE_PLAN, payload)
        }

        case PlanParser.Error(msg, n) => {
          val payload = msg + " in seed plan line: " + n.pos;
          println(payload)
          new SppaMessage(Sppa.CODE_ERR_PARSE_PLAN, Sppa.MSG_ERR_PARSE_PLAN, payload)
        }
      }
    }
  }

  override def onStartPlan(): SppaMessage = {
    print("Received START PLAN ... ")
    if (domain == null)
    {
      new SppaMessage(Sppa.CODE_ERR_MISSING_DOMAIN, Sppa.MSG_ERR_MISSING_DOMAIN, "The domain needs to be provided before initiating the planning process.")
    }
    else if (problem == null)
    {
      new SppaMessage(Sppa.CODE_ERR_MISSING_PROBLEM, Sppa.MSG_ERR_MISSING_PROBLEM, "The problem needs to be provided before initiating the planning process.")
    }
    else
    {
      println("** Start Plan **")

      //if no plan was provided, create the empty plan
      if (this.seedPlan == null)
        this.seedPlan = GroundedPlan(problem, List[TimedApplicable]())

      val planTimeline = this.seedPlan.timeline

      val cost = planTimeline.cost
      val violations = planTimeline.violationCount

      println("\nPlan State Timeline:" + planTimeline + " cost " + cost + "\n")

      println("\nGoal condition " + problem.goalCondition + " satisfied? " + problem.goalCondition.satisfiedBy(planTimeline.endState))

      println(new Date(System.currentTimeMillis()))
      println("\n\n *** Searching ... \n\n")
      val newPlan = Optimiser.searchForBetterPlan(problem, seedPlan, planTimeline, 1)

      // println("\nNew plan: " + newPlan)
      val newPlanTimeline = newPlan.timeline
      val newCost = newPlanTimeline.cost

      println("\n\n\n:::::::::::Found Plan :::::::::::::")
      println(newPlan.toPlanString)

      this.seedPlan = newPlan

      new SppaMessage(Sppa.CODE_ERR_OK, Sppa.MSG_ERR_OK, seedPlan.toPlanString)
    }
  }

}


object PlanOptimisationServer extends PlannerClientListenerFactory {

  override def getInstance(): PlannerClientListener = new PlanningClientListenerImpl()

  def main(args: Array[String]): Unit =
  {
    println("Initialising Plan Optimisation Server")
    new PlannerServer(this)
  }

}

