/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.ff

import uk.ac.kcl.inf.planning.solver.{State, Action, AtomicFormula}
import uk.ac.kcl.inf.planning.solver.expressions.Condition

/**
 * The Layers to be used by the Planning Graph
 * Date: 14/11/13 Time: 14:45
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 * @deprecated Refer to TrpgLayer for a temporal version.
 */
sealed abstract class Layer(layerIndex : Int)

case class PropositionLayer(layerIndex : Int, oldFacts: Set[AtomicFormula], newFacts : Set[AtomicFormula], preconditionActionMap : Map[AtomicFormula, Set[Action]], previousLayer : Option[ActionLayer] = None) extends Layer(layerIndex)
{
  def satisfies(predicate: Condition) : Boolean =
    predicate.satisfiedBy(state)

  lazy val facts = oldFacts ++ newFacts
  //todo: put numeric facts
  lazy val state = State(facts, Map[AtomicFormula, Double]())

  lazy val applicableActions =
  {
    val alreadyAppliedActions = previousLayer match
    {
      case Some(actionLayer) => actionLayer.appliedActions
      case None => Set[Action]()
    }

//    val start = System.nanoTime()

    //todo: this seems to be the slowest thing
    val actions = (for {
      f <- newFacts
//      _ = println("Fact " + f + " actions " + preconditionActionMap(f).size)
      a <- preconditionActionMap(f)
      if a.isApplicable(state)
    } yield a) diff alreadyAppliedActions

//    val end = System.nanoTime()
//    println(actions.size + " Applicable actions took " +((end - start) / 1000000) + "ms")
    actions
  }

  /**
   * Generates the next Action and Proposition Layers
   * @return An Option with the new proposition layer, with None if there were no new positive effects
   */
  def traverse() : Option[PropositionLayer] =
  {
//    println("Traversing layer " + layerIndex)
    val actionLayer = ActionLayer(layerIndex + 1, this)
    //check if new facts were added, otherwise return nothing, adding new proposition layers is useless

    val nextNewFacts = actionLayer.newPositiveEffects// diff facts

    if (nextNewFacts.isEmpty)
      return None

    Some(PropositionLayer(layerIndex + 1, facts, nextNewFacts, preconditionActionMap, Some(actionLayer)))
  }

  override def toString =
    "Proposition Layer " + layerIndex + " facts: " + facts
}

/**
 * Represents the action Layer in the planning graph. Provides the new facts generated from the effects of the actions.
 * @param layerIndex The layer number
 * @param previousLayer the previous proposition layer
 */
case class ActionLayer(layerIndex : Int, previousLayer : PropositionLayer) extends Layer(layerIndex)
{
  lazy val appliedActions : Set[Action] =
  {
    previousLayer.previousLayer match {
      case Some(actionLayer) => actionLayer.appliedActions ++ newAppliedActions
      case None => newAppliedActions
    }
  }

  lazy val newAppliedActions : Set[Action] =
    previousLayer.applicableActions


  lazy val newPositiveEffects = {
//    val start = System.nanoTime()
    for {
      action <- previousLayer.applicableActions
      effect <- action.positiveEffects
      if !(previousLayer.facts.contains(effect))
    } yield effect

//    val end = System.nanoTime()
//    println("Time to get positive effects of action layer " + ((end - start) / 1000000) + "ms")
//    effects
  }

  //todo: support numeric effects

  lazy val negativeEffects = (for {
    action <- previousLayer.applicableActions
  } yield action.negativeEffects).flatten

  override def toString =
    "Action Layer " + layerIndex + " actions: " + previousLayer.applicableActions + " +ve effects: " + newPositiveEffects + " -ve effects: " + negativeEffects
}