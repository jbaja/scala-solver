/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions

import uk.ac.kcl.inf.planning.solver.expressions.numeric.{LinearEffect, NumericFluent}
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState
import uk.ac.kcl.inf.planning.solver.{AtomicFormula, State}

/**
 *
 * Date: 10/02/14
 * Time: 16:54 
 * @author josef
 */
trait Effect
{
  /**
   * Takes a state and produces a new state after applying the effects
   * @param state the current (old) state
   * @return the new updated state after applying the effect
   */
   def applyToState(state: State, context: Map[Expression, Double] = Map[Expression, Double]()) : State

  /**
   * @param oldRelaxedState - the relaxed state prior to the current layer containing upper bounds and lower bounds of each numeric variable
   * @param cumulativeRelaxedState - the relaxed state accumulated from the current layer before this effect
   * @return a map of updated values to the fluents, with the values being an upper and a lower bound
   */
  def relaxedEffect(oldRelaxedState: RelaxedState, cumulativeRelaxedState : RelaxedState) : RelaxedState
}

trait NumericEffect extends Effect
{
  override def applyToState(state : State, context: Map[Expression, Double] = Map[Expression, Double]()): State = state.copy(numericFluents = state.numericFluents ++ evaluateNumeric(state))

  /**
   * Evaluates the effect on the fluent in the state.
   * @param state - the state
   * @param context - contextual information, such as action duration
   * @return a mapping between the updated fluent and the new value of the fluent.
   */
  def evaluateNumeric(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): Map[AtomicFormula, Double]

  def numericFluents : Set[NumericFluent]

  def toLinearEffect(timeDependentVariables : Set[AtomicFormula], state : State, t : Double = 0.0) : List[LinearEffect]

  //  def evaluateIncreasingNumeric(state: State) : Map[AtomicFormula, Double]
//  def evaluateDecreasingNumeric(state: State) : Map[AtomicFormula, Double]
  //todo: return those that are changing positively and those that are changing negatively
}
