/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.validator

import uk.ac.kcl.inf.planning.solver.expressions.Condition
import uk.ac.kcl.inf.planning.solver.{Problem, State}
import scala.collection.SortedSet

/**
 * Represents the states that will occur at each timepoint of a Plan.
 * At each PlanTimepoint there will be the state, the achieving action that created that state, and any invariant conditions
 * that must hold during this point.
 *
 * Created by Josef on 26/02/14.
 */
case class PlanTimeline(problem : Problem,
                        plan : GroundedPlan,
                        endState: State,
                        stateTimeline: SortedSet[PlanTimepoint],
                        violationCount: Int = 0) extends Ordered[PlanTimeline] {

  def getTimepointAndDurationAt(at: Double): (PlanTimepoint, Option[Double]) = {
    val timepoint = stateTimeline.takeWhile(_.at <= at).last
    val following = stateTimeline.dropWhile(_.at <= at)

    if (following.nonEmpty) {
      (timepoint, Some(following.head.at - timepoint.at))
    }
    else {
      (timepoint, None)
    }
  }


  lazy val cost : Double = {
    problem.metric match {
      case Some(metricSpec) =>
        metricSpec.cost(this)

      case None => 0.0
    }
  }

  lazy val duration: Double = {
    if (stateTimeline.nonEmpty){
      stateTimeline.last.at
    }
    else
      0
  }

  override def compare(that: PlanTimeline): Int = {
    if ((this.violationCount > 0) || (that.violationCount > 0)) {
      this.violationCount - that.violationCount
    }
      else {
      problem.metric match {
        case Some(metricSpec) =>
          metricSpec.compare(this, that)

        case None => 0
      }
    }
  }

  def betterThan(that: PlanTimeline) : Boolean = this.compare(that) < 0

  lazy val futureTimeline : SortedSet[PlanTimepoint] = stateTimeline.filter(_.at >= 0.0)

  override def toString: String =
    stateTimeline.foldLeft("")((str, timedState) => str + "\n" + timedState.at + ": "
      + timedState.achievingAction.getOrElse("<Initial>")
      + " " + timedState.state.modalFacts.size + " facts "
      + timedState.invariantConditions.size + " invariants "
      + "Numeric Fluents " + timedState.state.numericFluents
      ) + "\n Violation Count: " + violationCount

}

case class PlanTimepoint(
                         at: Double,
                         state: State,
                         achievingAction: Option[TimedApplicable] = None,
                         invariantConditions: Set[Condition] = Set[Condition](),
                         cumulativeViolations : Set[Condition] = Set[Condition](),
                         newViolations: Set[Condition] = Set[Condition]())

object PlanTimepoint  {
  implicit def planTimepointOrdering : Ordering[PlanTimepoint] = Ordering.fromLessThan( _.at < _.at)
}

