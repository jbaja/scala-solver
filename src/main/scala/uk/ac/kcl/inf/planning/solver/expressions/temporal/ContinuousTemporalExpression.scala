/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.temporal

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.expressions.numeric._
import uk.ac.kcl.inf.planning.solver.expressions.Expression

/**
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 */
case class ContinuousTemporalExpression(multiplicand : Expression) extends Expression.Aux[ContinuousTemporalExpression] with TemporalNumeric with Linear {
  def rateNumeric : Numeric = multiplicand.asInstanceOf[Numeric]

  override def updateTypeHierarchy(termType: TermType) = this.copy(multiplicand = multiplicand.updateTypeHierarchy(termType))

  /**
   * If one of the lifted terms is untyped and has the same name, it is replaced by the specified typed parameter.
   * This is used mainly in places where the expression is being parsed without type information (such as PDDL) and type
   * information is provided afterwards.
   * @param typedParameter - The typed parameter to replace the untyped one (which has the same parameter name)
   * @return an expression with the typed parameter
   */
  override def typify(typedParameter: Term) = this.copy(multiplicand = multiplicand.typify(typedParameter))

  /**
   * Substitutes the lifted parameter with the grounded argument term.
   * @param parameter The lifted parameter to be substituted
   * @param argument The grounded argument to substitute the parameter
   * @return A new instance of the expression with the parameter substituted by the argument
   */
  override def substitute(parameter: LiftedTerm, argument: GroundedTerm) = this.copy(multiplicand = multiplicand.substitute(parameter, argument))

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): ContinuousTemporalExpression =
    copy(multiplicand = multiplicand.hookExternalEvaluator(evaluators))

  /**
   * @return true if this expression is fully grounded, false otherwise
   */
  override def isGrounded: Boolean = multiplicand.isGrounded

  /**
   * Provides the numeric value of this entity for this state
   * @param state the current state
   * @param t the elapsed time
   * @return An option with a double value Some(Double), or None if it is undefined in the current state.
   */
  override def value(state: State, t: Double, context: Map[Expression, Double] = Map[Expression, Double]()): Option[Double] =
    if (!isGrounded)
      None
    else
    {
      multiplicand match {
        case numeric : Numeric => numeric.value(state, context).map(v => t * v)
        case _ => None
      }
    }

  override def fluents: Set[NumericFluent] = multiplicand match {
    case numeric : Numeric => numeric.fluents
    case _ => Set[NumericFluent]()
  }

  //the coeffecients of all variables and a constant
  override def linearFormula(timeDepedentFluents: Set[AtomicFormula], state: State, t : Double = 0.0): LinearFormula = {
    multiplicand match {
      case linear : Linear => linear.linearFormula(timeDepedentFluents, state, t)

      case _ => throw NonLinearException("Continuous effect not of a linear form: " + multiplicand)
    }
  }

  override def isConstant(timeDependentVariables : Set[AtomicFormula], state : State, t : Double): Boolean = {
    multiplicand match {
      case linear : Linear => linear.isConstant(timeDependentVariables, state, t)

      case _ => true
    }
  }


  override def toString: String = "(* #t " + multiplicand + ")"

}
