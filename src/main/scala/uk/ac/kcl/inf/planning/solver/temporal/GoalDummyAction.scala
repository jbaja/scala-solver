/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.temporal

import uk.ac.kcl.inf.planning.solver.expressions._
import uk.ac.kcl.inf.planning.solver.expressions.numeric.{NumericConstraint, LinearEffect, LinearCondition}
import uk.ac.kcl.inf.planning.solver.{Problem, State, AtomicFormula}

/**
 * Virtual dummy action, whose precondition expression is the goal.
 * Created by Josef on 29/01/2015.
 * @author Josef Bajada
 */
case class GoalDummyAction(precondition : Condition) extends TemporalAction
{
  lazy val invariant = Empty()

  /**
   * @return a pair with the positive effects followed with the negative effects
   */
  lazy val propositionalPreconditions : (Set[AtomicFormula], Set[AtomicFormula]) =
  {
    precondition match {
      case modalCondition : Modal => modalCondition.atoms
      case _ => (Set[AtomicFormula](), Set[AtomicFormula]())
    }
  }
  lazy val positivePropositionalPreconditions : Set[AtomicFormula] = propositionalPreconditions._1
  lazy val negativePropositionalPreconditions : Set[AtomicFormula] = propositionalPreconditions._2

  lazy val numericEffects : Set[AtomicFormula] = Set[AtomicFormula]()

  def isApplicable(temporalState: TemporalState): Boolean = {
    if (temporalState.plan.nonEmpty && this.equals(temporalState.plan.last.temporalAction)) {
      false
    }
    else {
      val conditions = precondition.getNonTemporallyDependentConditions(temporalState.timeDependentFluents)
      conditions.forall(_.satisfiedBy(temporalState.state))
    }
  }

  override def isApplicableRelaxed(relaxedState : RelaxedState) : Boolean = {
    precondition.relaxedSatisfiedBy(relaxedState)
  }

  override def applyToTemporalState(temporalState: TemporalState): Option[TemporalState] = {
    //for the goal action to be applicable, no durative action must be running and the precondition (= goal condition) must be satisfied
    if (temporalState.runningActions.nonEmpty || (!isApplicable(temporalState))) {
      return None
    }

    //the goal dummy action will be added exactly at the end with the last real action
    val index = temporalState.plan.size //we are adding it at the end
    val newTemporalConstraints = if (index > 0)
        temporalState.temporalConstraints + IntervalTemporalConstraint(index - 1, index, NumericConstraint.EQ, 0.0)
      else
        temporalState.temporalConstraints + AbsoluteTemporalConstraint(index, NumericConstraint.EQ, 0.0)

    val newTemporalState = TemporalState(
      temporalState.problem,
      temporalState.state.modalFacts,
      temporalState.state.numericFluents,
      temporalState.timedActions,
      temporalState.runningActions,
      temporalState.plan ::: List(Happening(this, temporalState.state, temporalState.state, Set[Condition]())),
      newTemporalConstraints
    )

    //no need to check heuristic, it is the goal state, h = 0
    val goalState = LPScheduler.scheduleState(Some(newTemporalState))
//    val goalState = LPScheduler.scheduleState(TemporalRPG.reachabilityAnalysis(problem, newTemporalState))
/*    goalState match {
      case Some(ts) => println("Found goal in " + ts); println("Schedule: " + ts.schedule); ts.printPlanString();
      case None =>
    } */

    goalState
  }

  override def getLinearEffects(timeDependentVariables: Set[AtomicFormula], state: State): List[LinearEffect] = List[LinearEffect]()

  override def getLinearPreconditions(timeDependentVariables: Set[AtomicFormula], state: State): Set[LinearCondition] =
    precondition match {
      case numericCondition : NumericCondition => numericCondition.toLinearCondition(timeDependentVariables, state)
      case _ => Set[LinearCondition]()
    }

  override def toString: String = "<Goal>"

  override def equals(o: scala.Any): Boolean = o match {
    case gda : GoalDummyAction => true
    case _ => false
  }

  override def applyToRelaxedState(relaxedState: RelaxedState, cumulativeRelaxedState: RelaxedState, elapsedTime: Double): RelaxedState = {
   cumulativeRelaxedState
 }

  override def negativeEffects: Set[AtomicFormula] = Set[AtomicFormula]()

  override def positiveEffects: Set[AtomicFormula] = Set[AtomicFormula]()
}
