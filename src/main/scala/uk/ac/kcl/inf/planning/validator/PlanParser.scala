/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.validator

import scala.util.parsing.combinator.RegexParsers
import scala.language.postfixOps

import java.io.Reader
import scala.util.parsing.input.StreamReader

/**
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 */
object PlanParser extends RegexParsers {
  override protected val whiteSpace = """(\s|;.*)+""".r

  def plan : Parser[Plan] = plan_step.* ^^ { Plan }

  def plan_step : Parser[PlanStep] = time ~ action ~ (duration?) ^^ {
    case at ~ action ~ duration => PlanStep(at, action._1, action._2, duration.getOrElse(0.0))
  }

  def action : Parser[(String, List[String])] = "(" ~> action_symbol ~ (term*) <~ ")" ^^ {
    case action_symbol ~ terms => (action_symbol, terms)
  }

  def time : Parser[Double] = number <~ ":"
  def duration : Parser[Double] = "[" ~> number <~ "]"

  def number = """-?[0-9]+(\.[0-9]+)?""".r ^^ { _.toDouble }

  def action_symbol : Parser[String] = name
  def term = name

  def name: Parser[String] = """[a-zA-Z][a-zA-Z0-9_-]*""".r

  def parsePlan(planString: Reader) = {
    parseAll(plan, StreamReader(planString))
  }
}
