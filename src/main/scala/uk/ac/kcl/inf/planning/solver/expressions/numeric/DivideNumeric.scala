/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.numeric

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.State
import uk.ac.kcl.inf.planning.solver.ff.ValueBounds
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState
import scala.Some
import uk.ac.kcl.inf.planning.solver.TermType
import uk.ac.kcl.inf.planning.solver.LiftedTerm
import uk.ac.kcl.inf.planning.solver.expressions.Expression

/**
 *
 * Date: 10/02/14
 * Time: 19:56 
 * @author josef
 */
case class DivideNumeric(expression1 : Expression, expression2 : Expression) extends Expression.Aux[DivideNumeric] with Numeric with Linear
{
  val numeric1: Numeric = expression1.asInstanceOf[Numeric]
  val numeric2: Numeric = expression2.asInstanceOf[Numeric]

  override val isGrounded: Boolean = expression1.isGrounded && expression2.isGrounded

  override def value(state : State, context: Map[Expression, Double] = Map[Expression, Double]()): Option[Double] = {
    numeric1.value(state, context).flatMap(dividend => {
      numeric2.value(state, context).flatMap(divisor => {
        if (divisor == 0)
          None
        else
          Some(dividend / divisor)
      })
    })
  }

  def relaxedBounds(relaxedState: RelaxedState) : Option[ValueBounds] = {
    numeric1.relaxedBounds(relaxedState).map(dividendBounds =>
      dividendBounds.combine(numeric2.relaxedBounds(relaxedState), ValueBounds.divideCombiner)
    )
  }

  override def updateTypeHierarchy(termType: TermType) : DivideNumeric = DivideNumeric(expression1.updateTypeHierarchy(termType), expression2.updateTypeHierarchy(termType))

  override def typify(typedParameter: Term) : DivideNumeric = DivideNumeric(expression1.typify(typedParameter), expression2.typify(typedParameter))

  override def substitute(parameter: LiftedTerm, argument: GroundedTerm) : DivideNumeric = DivideNumeric(expression1.substitute(parameter, argument), expression2.substitute(parameter, argument))

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): DivideNumeric =
    copy(expression1 = expression1.hookExternalEvaluator(evaluators), expression2 = expression2.hookExternalEvaluator(evaluators))

  override def fluents: Set[NumericFluent] = numeric1.fluents ++ numeric2.fluents

  //the coeffecients of all variables and a constant
  override def linearFormula(continuousVariables: Set[AtomicFormula], state: State, t : Double = 0.0): LinearFormula = {
    val vars1 = numeric1.fluents.map(_.functionPredicate).intersect(continuousVariables)
    val vars2 = numeric2.fluents.map(_.functionPredicate).intersect(continuousVariables)

    //the two expressions can have 1 continuous variable in common to remain linear, as long as one of the expressions
    //doesn't have any other variables not on the other side
    if (((vars1 intersect vars2).size > 1) ||
        ((vars1 diff vars2).nonEmpty && (vars2 diff vars1).nonEmpty))
      throw NonLinearException("Divide expression cannot be resolved linearly: " + this)

    (numeric1, numeric2) match {
      case (linear1: Linear, linear2: Linear) => {
        val linearFormula1 = linear1.linearFormula(continuousVariables, state, t)
        val linearFormula2 = linear2.linearFormula(continuousVariables, state, t)
        if (linearFormula2.coefficients.nonEmpty)
          throw NonLinearException("Sorry, linear polynomial division with variables in the divisor not implemented yet: " + this)

        val coefficients = linearFormula1.coefficients.map(entry =>
          entry._1 -> entry._2 / linearFormula2.constant
        )

        val constant = linearFormula1.constant / linearFormula2.constant
        LinearFormula(coefficients, constant)
      }
    }
  }

  override def isConstant(timeDependentVariables : Set[AtomicFormula], state : State, t : Double): Boolean = {
    val linears = (numeric1, numeric2) match {
      case (linear1 : Linear, linear2 : Linear) => List[Linear](linear1, linear2)

      case _ => List[Linear]()
    }
    linears.forall(_.isConstant(timeDependentVariables, state, t))
  }

  /**
   * @return The constant part of the expression, computed by adding the constant parts of the sub expressions.
   */
  override def constant : Double = numeric1.constant / numeric2.constant


  override def toString: String = "(/ " + numeric1 + " " + numeric2 + ")"
}
