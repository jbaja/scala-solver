/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.GroundedTerm
import uk.ac.kcl.inf.planning.solver.LiftedTerm
import uk.ac.kcl.inf.planning.solver.expressions.numeric.ExternalEvaluator
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState

/**
 *
 * Date: 10/02/14
 * Time: 17:34 
 * @author josef
 */
case class NotEqualTermsCondition(term1 : Term, term2 : Term) extends Expression.Aux[NotEqualTermsCondition] with Condition
{
  /**
   * @return true if this expression is fully grounded, false otherwise
   */
  override def isGrounded: Boolean = term1.isGrounded && term2.isGrounded

  /**
   * Substitutes the lifted parameter with the grounded argument term.
   * @param parameter The lifted parameter to be substituted
   * @param argument The grounded argument to substitute the parameter
   * @return A new instance of the expression with the parameter substituted by the argument
   */
  override def substitute(parameter: LiftedTerm, argument: GroundedTerm) : NotEqualTermsCondition =
  {
    val newTerm1 = if (term1.equals(parameter)) argument else term1
    val newTerm2 = if (term2.equals(parameter)) argument else term2

    if ((newTerm1 != term1) || (newTerm2 != term2))
      copy(term1 = newTerm1, term2 = newTerm2)
    else
      this
  }

  override def typify(typedParameter: Term) : NotEqualTermsCondition =
  {
    val newTerm1 = if (term1.symbol.equals(typedParameter.symbol)) typedParameter else term1
    val newTerm2 = if (term2.symbol.equals(typedParameter.symbol)) typedParameter else term2

    if ((newTerm1 != term1) || (newTerm2 != term2))
      copy(term1 = newTerm1, term2 = newTerm2)
    else
      this
  }

  override def updateTypeHierarchy(termType : TermType) : NotEqualTermsCondition =
  {
    val newTerm1 = if (term1.termType.name.equals(termType.name)) term1.updateTypeHierarchy(termType) else term1
    val newTerm2 = if (term2.termType.name.equals(termType.name)) term2.updateTypeHierarchy(termType) else term2

    if ((newTerm1 != term1) || (newTerm2 != term2))
      copy(term1 = newTerm1, term2 = newTerm2)
    else
      this
  }

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): NotEqualTermsCondition = this

  /**
   * Determines whether this condition is satisfied in the provided state.
   * @param state The state to be checked for this condition.
   * @return true if the state satisfies the condition, false otherwise
   */
  override def satisfiedBy(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): Boolean = !term1.equals(term2)

  override def relaxedSatisfiedBy(relaxedState: RelaxedState): Boolean = !term1.equals(term2)

  override def violations(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): Set[Condition] =
    if (satisfiedBy(state, context)) Set[Condition]() else Set[Condition](this)

  override def invert: Expression with Condition = EqualTermsCondition(term1, term2)

  override def equals(obj: scala.Any): Boolean = obj.getClass().equals(this.getClass()) &&
    (obj.asInstanceOf[this.type].term1.equals(this.term1) && obj.asInstanceOf[this.type].term2.equals(this.term2))

  override def atomicFormulas: Set[AtomicFormula] = Set[AtomicFormula]()
}