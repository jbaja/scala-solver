/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.temporal

import uk.ac.kcl.inf.planning.solver._

import uk.ac.kcl.inf.planning.solver.AtomicFormula
import uk.ac.kcl.inf.planning.solver.GroundedTerm
import uk.ac.kcl.inf.planning.solver.TermType
import uk.ac.kcl.inf.planning.solver.expressions.numeric._
import uk.ac.kcl.inf.planning.solver.expressions.{Condition, Expression}
import uk.ac.kcl.inf.planning.solver.ff.ValueBounds
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState

/**
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 */

case class ContinuousIncreaseEffect(fluent: NumericFluent, temporalExpression: ContinuousTemporalExpression) extends Expression.Aux[ContinuousIncreaseEffect] with ContinuousNumericEffect
{
  override val isGrounded: Boolean = fluent.isGrounded && temporalExpression.isGrounded

  override def substitute(parameter: LiftedTerm, argument: GroundedTerm) =
    ContinuousIncreaseEffect(fluent.substitute(parameter, argument), temporalExpression.substitute(parameter, argument))

  override def typify(typedParameter: Term)  =
    ContinuousIncreaseEffect(fluent.typify(typedParameter), temporalExpression.typify(typedParameter))

  override def updateTypeHierarchy(termType: TermType) =
    ContinuousIncreaseEffect(fluent.updateTypeHierarchy(termType), temporalExpression.updateTypeHierarchy(termType))

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): ContinuousIncreaseEffect =
    copy(temporalExpression = temporalExpression.hookExternalEvaluator(evaluators))


  override def evaluateNumericDelta(state: State, t: Double, context: Map[Expression, Double] = Map[Expression, Double]()): Map[AtomicFormula, Double] =
    Map((fluent.functionPredicate, temporalExpression.value(state, t, context).get))

  /**
    * @param oldRelaxedState - the relaxed state prior to the current layer containing upper bounds and lower bounds of each numeric variable
    * @param cumulativeRelaxedState - the relaxed state accumulated from the current layer before this effect
    * @return a map of updated values to the fluents, with the values being an upper and a lower bound
    */
  override def relaxedEffect(oldRelaxedState: RelaxedState, cumulativeRelaxedState: RelaxedState, elapsedTime: Double): RelaxedState = {
    //get the old bounds (prior to the effect of the action)
    val oldBounds = fluent.relaxedBounds(oldRelaxedState)
    //get the current cumulative bounds
    val curBounds = fluent.relaxedBounds(cumulativeRelaxedState)
    //get the delta change
    //todo: fix here to fix heuristic for external evaluators, wtf is happening here?
    val deltaBounds = temporalExpression.rateNumeric.relaxedBounds(oldRelaxedState).map(_.multiply(elapsedTime))
    //calculate the bounds from the effect
    val effectBounds = oldBounds.map(bounds => bounds.combine(deltaBounds, ValueBounds.increaseCombiner))

    //todo: cater for one-shot actions, if this is part of a one-shot action, instead of infinity the real value must be used
    //extrapolate the change to infinity (since multiple actions can be applied in a relaxed state)
    val infBounds = ValueBounds.infinitize(oldBounds, effectBounds)
  //  val infBounds = effectBounds

    //combine them with the cumulative bounds from other concurrent actions
    val newBounds = infBounds.map(bounds => bounds.combine(curBounds))

    //update the relaxed state
    val newRelaxedState = newBounds.map { vb =>
      cumulativeRelaxedState.copy(
        numericFluentBounds = cumulativeRelaxedState.numericFluentBounds.updated(fluent.functionPredicate, vb))
    }.getOrElse(cumulativeRelaxedState)

    newRelaxedState
  }

  override def getRelaxedEffectPreconditions(relaxedState: RelaxedState, elapsedTime : Double): Set[Condition] = {
    val deltaBounds = temporalExpression.rateNumeric.relaxedBounds(relaxedState).map(_.multiply(elapsedTime))

    val preConditions : Option[Set[Condition]] = deltaBounds.map(db => {
      val decreaseCondition = if (db.min < 0) Some(GreaterThanCondition(NumericConstant(0), temporalExpression.multiplicand)) else None
      val increaseCondition = if (db.max > 0) Some(GreaterThanCondition(temporalExpression.multiplicand, NumericConstant(0))) else None

      Set[Option[Condition]](increaseCondition, decreaseCondition).flatten
    })

    preConditions.getOrElse(Set[Condition]())
  }


  override def numericFluents: Set[NumericFluent] = Set[NumericFluent](fluent)

  override def continuousExpression: ContinuousTemporalExpression = temporalExpression

  override def gradient(timeDependentFluents: Set[AtomicFormula], state: State, duration : Double): Double = {
    temporalExpression match {
      case linear : Linear => {
        val linearFormula = linear.linearFormula(timeDependentFluents, state, duration)

        if (linearFormula.coefficients.nonEmpty)
        {
          println("increase " + fluent + " dt " + linearFormula + " for " + duration)
          linearFormula.coefficients.foreach(coeff =>
            println("Value of " + coeff._1 + state.numericFluents.getOrElse(coeff._1, 0.0)))

          throw NonLinearException("Cannot handle non-linear continuous effect depends on " + linearFormula + " : " + this)
        }

        linearFormula.constant
      }
    }
  }

  def isLinear(timeDependent : Set[AtomicFormula], state : State, duration : Double) : Boolean = {
    temporalExpression match {
      case linear : Linear => linear.isConstant(timeDependent, state, duration)

      case _ => false
    }
  }

  override def toString: String = "(increase " + fluent + " " + temporalExpression + ")"
}

