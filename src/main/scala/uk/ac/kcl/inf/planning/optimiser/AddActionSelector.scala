/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.optimiser

import uk.ac.kcl.inf.planning.solver.expressions.temporal.TimeSpecifier
import uk.ac.kcl.inf.planning.solver.{DurativeAction, State, Problem}
import uk.ac.kcl.inf.planning.validator.GroundedPlan

/**
 * Created by Josef on 07/01/2015.
 */
trait AddActionSelector {
  def getPossibleActionsAt(problem : Problem, plan : GroundedPlan, period : Integer) : List[DurativeAction]
}

object ApplicableActionSelector extends AddActionSelector
{
  override def getPossibleActionsAt(problem: Problem, plan: GroundedPlan, period: Integer): List[DurativeAction] = {
    val startTime = plan.getPeriodStartTime(period)
    val (timepoint, _) = plan.timeline.getTimepointAndDurationAt(startTime)
    val invariantConditions = plan.invariantConditionsAt(startTime)

    val applicableActions = problem.groundedDurativeActions
        .withFilter(_.isApplicable(timepoint.state, TimeSpecifier.AT_START))   //we get only the starting ones
        .map(_.computeDuration(timepoint.state))                               //we compute their duration at this context
        .withFilter(_.duration.isDefined)              //we filter out those who cannot have their duration computed

    //filter out the ones that do not conflict with the invariant conditions of the selected period
    (for {action <- applicableActions
          if invariantConditions.forall(_.satisfiedBy(action.applyToState(timepoint.state, TimeSpecifier.AT_START)))
    } yield action).toList
  }
}

