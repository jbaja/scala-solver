/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.temporal

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.expressions._
import uk.ac.kcl.inf.planning.solver.State
import uk.ac.kcl.inf.planning.solver.AtomicFormula
import uk.ac.kcl.inf.planning.solver.GroundedTerm
import uk.ac.kcl.inf.planning.solver.TermType
import uk.ac.kcl.inf.planning.solver.LiftedTerm
import uk.ac.kcl.inf.planning.solver.expressions.numeric.ExternalEvaluator
import uk.ac.kcl.inf.planning.solver.expressions.temporal.TimeSpecifier.TimeSpecifier

/**
 *
 * Date: 11/02/14
 * Time: 12:03 
 * @author josef
 */
case class TemporalConditionExpression(expression : Expression, when: TimeSpecifier) extends Expression.Aux[TemporalConditionExpression] with TimedCondition with TimedModal{

  override def satisfiedBy(state: State, when: TimeSpecifier): Boolean =
    if (this.when.equals(when))
    {
      expression match {
        case condition : Condition => condition.satisfiedBy(state)
        case _ => true
      }
    }
    else true

  override def conditions(when: TimeSpecifier): Set[Condition] =
    if (this.when.equals(when))
    {
      expression match {
        case condition : Condition => Set[Condition](condition)
        case _ => Set[Condition]()
      }
    }
    else Set[Condition]()


  override def positiveAtoms(when: TimeSpecifier): Set[AtomicFormula] =
    if (this.when.equals(when))
    {
      expression match {
        case modal : Modal => modal.positiveAtoms
        case _ => Set[AtomicFormula]()
      }
    }
    else Set[AtomicFormula]()


  override def negativeAtoms(when: TimeSpecifier): Set[AtomicFormula] =
    if (this.when.equals(when))
    {
      expression match {
        case modal : Modal => modal.negativeAtoms
        case _ => Set[AtomicFormula]()
      }
    }
    else Set[AtomicFormula]()

  /**
   * Updates the type hierarchy of the specified type. While parsing the full type hierarchy might not be immediately
   * known, and determined from a separate type list. This method helps populate the type hierarchy properly.
   * @param termType - the full type to be populated
   * @return The expression updated with the right type information
   */
  override def updateTypeHierarchy(termType: TermType) = this.copy(expression = expression.updateTypeHierarchy(termType))

  /**
   * If one of the lifted terms is untyped and has the same name, it is replaced by the specified typed parameter.
   * This is used mainly in places where the expression is being parsed without type information (such as PDDL) and type
   * information is provided afterwards.
   * @param typedParameter - The typed parameter to replace the untyped one (which has the same parameter name)
   * @return an expression with the typed parameter
   */
  override def typify(typedParameter: Term) = this.copy(expression = expression.typify(typedParameter))

  /**
   * Substitutes the lifted parameter with the grounded argument term.
   * @param parameter The lifted parameter to be substituted
   * @param argument The grounded argument to substitute the parameter
   * @return A new instance of the expression with the parameter substituted by the argument
   */
  override def substitute(parameter: LiftedTerm, argument: GroundedTerm) = this.copy(expression = expression.substitute(parameter, argument))

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]) : TemporalConditionExpression =
    copy(expression = expression.hookExternalEvaluator(evaluators))

  /**
   * @return true if this expression is fully grounded, false otherwise
   */
  override def isGrounded: Boolean = expression.isGrounded

  override def toString = "(" + when + "(" + expression + "))"
}

