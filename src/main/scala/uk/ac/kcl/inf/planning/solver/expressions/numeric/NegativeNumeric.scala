/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.numeric

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.State
import uk.ac.kcl.inf.planning.solver.TermType
import uk.ac.kcl.inf.planning.solver.LiftedTerm
import uk.ac.kcl.inf.planning.solver.expressions.Expression
import uk.ac.kcl.inf.planning.solver.ff.ValueBounds
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState

/**
 *
 * Date: 10/02/14
 * Time: 18:31 
 * @author josef
 */
case class NegativeNumeric(expression : Expression) extends Expression.Aux[NegativeNumeric] with Numeric with Linear
{
  val numeric: Numeric = expression.asInstanceOf[Numeric]

  override val isGrounded: Boolean = expression.isGrounded

  override def value(state: State, context: Map[Expression, Double] = Map[Expression, Double]()) : Option[Double] =
     numeric.value(state, context).map(v => -v)

  override def relaxedBounds(relaxedState: RelaxedState): Option[ValueBounds] =
     numeric.relaxedBounds(relaxedState).map(_.negative)

  override def updateTypeHierarchy(termType: TermType) : NegativeNumeric = NegativeNumeric(expression.updateTypeHierarchy(termType))

  override def typify(typedParameter: Term) : NegativeNumeric = NegativeNumeric(expression.typify(typedParameter))

  override def substitute(parameter: LiftedTerm, argument: GroundedTerm) : NegativeNumeric = NegativeNumeric(expression.substitute(parameter, argument))

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): NegativeNumeric =
    copy(expression = expression.hookExternalEvaluator(evaluators))

  override def fluents: Set[NumericFluent] = expression match {
    case numeric : Numeric => numeric.fluents
    case _ => Set[NumericFluent]()
  }

  //the coeffecients of all variables and a constant
  override def linearFormula(continuousVariables : Set[AtomicFormula], state : State, t : Double = 0.0): LinearFormula = {
    expression match {
      case linear : Linear => {
        val subLinearFormula = linear.linearFormula(continuousVariables, state, t)
        val negativeCoefficients = subLinearFormula.coefficients.map( entry =>
          entry._1 -> (0.0 - entry._2)
        )
        LinearFormula(negativeCoefficients, 0.0 - subLinearFormula.constant)
      }
    }
  }

  override def isConstant(timeDependentVariables : Set[AtomicFormula], state : State, t : Double): Boolean = {
    expression match {
      case linear : Linear => linear.isConstant(timeDependentVariables, state, t)

      case _ => true
    }
  }

  /**
   * @return The constant part of the expression, computed by adding the constant parts of the sub expressions.
   */
  override def constant : Double = 0 - numeric.constant

  override def toString: String = "(- " + numeric + ")"

}
