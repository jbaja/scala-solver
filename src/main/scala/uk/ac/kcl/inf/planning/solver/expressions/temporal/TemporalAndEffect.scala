/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.temporal

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.expressions._
import uk.ac.kcl.inf.planning.solver.State
import uk.ac.kcl.inf.planning.solver.AtomicFormula
import uk.ac.kcl.inf.planning.solver.GroundedTerm
import uk.ac.kcl.inf.planning.solver.TermType
import uk.ac.kcl.inf.planning.solver.LiftedTerm
import uk.ac.kcl.inf.planning.solver.expressions.numeric.{ExternalEvaluator, LinearEffect, NumericFluent}
import uk.ac.kcl.inf.planning.solver.expressions.temporal.TimeSpecifier.TimeSpecifier
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState

/**
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 */
case class TemporalAndEffect(expressions : List[Expression]) extends Expression.Aux[TemporalAndEffect] with TimedModal with TimedNumericEffect
{
  /**
   * Updates the type hierarchy of the specified type. While parsing the full type hierarchy might not be immediately
   * known, and determined from a separate type list. This method helps populate the type hierarchy properly.
   * @param termType - the full type to be populated
   * @return The expression updated with the right type information
   */
  override def updateTypeHierarchy(termType: TermType) =
    TemporalAndEffect(expressions.map(exp => exp.updateTypeHierarchy(termType)))

  /**
   * If one of the lifted terms is untyped and has the same name, it is replaced by the specified typed parameter.
   * This is used mainly in places where the expression is being parsed without type information (such as PDDL) and type
   * information is provided afterwards.
   * @param typedParameter - The typed parameter to replace the untyped one (which has the same parameter name)
   * @return an expression with the typed parameter
   */
  override def typify(typedParameter: Term) =
    TemporalAndEffect(expressions.map(exp => exp.typify(typedParameter)))

  /**
   * Substitutes the lifted parameter with the grounded argument term.
   * @param parameter The lifted parameter to be substituted
   * @param argument The grounded argument to substitute the parameter
   * @return A new instance of the expression with the parameter substituted by the argument
   */
  override def substitute(parameter: LiftedTerm, argument: GroundedTerm) =
    TemporalAndEffect(expressions = expressions.map(exp => exp.substitute(parameter, argument)))


  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]) : TemporalAndEffect =
    copy(expressions = expressions.map(_.hookExternalEvaluator(evaluators)))

  /**
   * @return true if this expression is fully grounded, false otherwise
   */
  override def isGrounded: Boolean = expressions.forall(exp => exp.isGrounded)

  lazy val timedEffects : Map[TimeSpecifier, List[TemporalEffectExpression]] = (expressions collect {
    case effect : TemporalEffectExpression => effect
  }).groupBy(tc => tc.when)

  lazy val continuousEffects = (expressions collect {
    case effect : ContinuousNumericEffect => effect
  })

  lazy val subConjunctions : List[TemporalAndEffect] = expressions collect {
    case andEffect : TemporalAndEffect => andEffect
  }

  lazy val temporalEffectExpressions = expressions collect {
    case effect : TemporalEffectExpression => effect
  }


  lazy val timedNumericEffects : Map[TimeSpecifier, List[TimedNumericEffect]] = temporalEffectExpressions collect {
    case timedNumericEffect : TimedNumericEffect  => timedNumericEffect
  } groupBy(_.when)



  override def toString = expressions.foldLeft("AND (")((str, exp) => str + " " + exp) + ")"

  override def applyToState(state: State, when: TimeSpecifier.TimeSpecifier): State = State(state.modalFacts -- negativeAtoms(when) ++ positiveAtoms(when), state.numericFluents ++ evaluateNumeric(state, when))

  override def evaluateNumeric(state: State, when: TimeSpecifier, context: Map[Expression, Double] = Map[Expression, Double]()): Map[AtomicFormula, Double] =
    (expressions collect {
      case numericEffect : TimedNumericEffect => numericEffect
      }).flatMap(_.evaluateNumeric(state, when, context)).toMap


  override def evaluateRelaxed(oldRelaxedState: RelaxedState, cumulativeRelaxedState: RelaxedState, when: TimeSpecifier): RelaxedState =
  {
    val newRelaxedState = expressions.foldLeft(cumulativeRelaxedState)((accState, expression) =>
      expression match {
        case effect : TemporalEffectExpression => effect.evaluateRelaxed(oldRelaxedState, accState, when)
        case _ => accState
      })

    newRelaxedState
  }

  def subConjunctionPositiveAtoms(when: TimeSpecifier) =
    subConjunctions.foldLeft(Set[AtomicFormula]())((s, a) => s union a.positiveAtoms(when))

  def subConjunctionNegativeAtoms(when: TimeSpecifier) =
    subConjunctions.foldLeft(Set[AtomicFormula]())((s, a) => s union a.negativeAtoms(when))

  lazy val timedModalEffects : Map[TimeSpecifier, List[TimedModal]] = temporalEffectExpressions collect {
    case timedModal : TimedModal => timedModal
  } groupBy(_.when)

  override def positiveAtoms(when: TimeSpecifier): Set[AtomicFormula] =
    timedModalEffects.getOrElse(when, List[TimedModal]()).foldLeft(Set[AtomicFormula]())(
      (s, a) => s union a.positiveAtoms(when) union subConjunctionPositiveAtoms(when))


  override def negativeAtoms(when: TimeSpecifier.TimeSpecifier): Set[AtomicFormula] =
    timedModalEffects.getOrElse(when, List[TimedModal]()).foldLeft(Set[AtomicFormula]())(
      (s, a) => s union a.negativeAtoms(when)) union subConjunctionNegativeAtoms(when)

  override def numericFluents(when: TimeSpecifier): Set[NumericFluent] =  {
    (for (expression <- expressions collect {
      case numericEffect : TimedNumericEffect => numericEffect
    }) yield expression.numericFluents(when)).flatten.toSet
  }

  override def toLinearEffect(timeDependentVariables: Set[AtomicFormula], state: State, when: TimeSpecifier): List[LinearEffect] = {
    (expressions collect {
      case numericEffect : TimedNumericEffect => numericEffect
    }).flatMap(_.toLinearEffect(timeDependentVariables, state, when))
  }

}
