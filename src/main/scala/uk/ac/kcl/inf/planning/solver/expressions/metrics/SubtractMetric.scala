/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.metrics

import uk.ac.kcl.inf.planning.solver.expressions.Expression
import uk.ac.kcl.inf.planning.solver.expressions.numeric.ExternalEvaluator
import uk.ac.kcl.inf.planning.solver.{GroundedTerm, LiftedTerm, Term, TermType}
import uk.ac.kcl.inf.planning.validator.PlanTimeline

/**
 *
 * Date: 05/06/14
 * Time: 18:42 
 * @author josef
 */
case class SubtractMetric(exp1 : Expression, exp2 : Expression) extends Expression.Aux[SubtractMetric] with MetricNumeric
{
    override val isGrounded: Boolean = exp1.isGrounded && exp2.isGrounded

    override def updateTypeHierarchy(termType: TermType) : SubtractMetric = SubtractMetric(exp1.updateTypeHierarchy(termType), exp2.updateTypeHierarchy(termType))

    override def typify(typedParameter: Term) : SubtractMetric = SubtractMetric(exp1.typify(typedParameter), exp2.typify(typedParameter))

    override def substitute(parameter: LiftedTerm, argument: GroundedTerm) : SubtractMetric = SubtractMetric(exp1.substitute(parameter, argument), exp2.substitute(parameter, argument))

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): SubtractMetric =
    copy(exp1 = exp1.hookExternalEvaluator(evaluators), exp2 = exp2.hookExternalEvaluator(evaluators))


  /**
   * Provides the numeric value of this entity for a plan
   * @param planTimeline the whole plan timeline to be evaluated
   * @return An double value
   */
  override def value(planTimeline: PlanTimeline): Double = {
    (exp1, exp2) match {
      case (num1 : MetricNumeric, num2 : MetricNumeric) =>
        num1.value(planTimeline) - num2.value(planTimeline)
    }
  }
}
