/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.external

import org.apache.commons.math3.special.Erf
import uk.ac.kcl.inf.planning.solver.{AtomicFormula, State}
import uk.ac.kcl.inf.planning.solver.expressions.numeric.{NumericFluent, ExternalEvaluator}

/**
  * @author Josef Bajada
  */
case class Storage(aliasPrefix : String) extends ExternalEvaluator
{
  val fullName = "Kcl.Planning.APS.Storage"
  val fullPercentile = 0.9999
  val scalingFactor = Erf.erfInv(fullPercentile)

  override def value(fluent : NumericFluent, state: State, t: Double): Option[Double] = {
    //todo: rather than do this all the time, return a function on hook attachment that calls directly the respective function (as defined in PDDLx)

    val fluentName = fluent.functionPredicate.predicate.drop(aliasPrefix.length() + 1) //we add one to remove the .

    fluentName match {
      case "charge-rate" => chargeRate(fluent, state, t)

      case _ => handleStaticFluent(fluent, state)
    }
  }

  //this is expecting fluent to be (state-of-charge-dt ?b)
  def chargeRate(fluent : NumericFluent, state : State, t: Double) : Option[Double]  = {
    if (fluent.functionPredicate.parameters.size != 2)
    {
      throw new Exception("Invalid parameters for module method: " + fluent.functionPredicate + " Expecting 2 parameters of type Storage.state-of-charge-dt (battery and charge profile)")
    }

    val fullChargeTime = state.numericFluents(AtomicFormula(aliasedName("full-charge-time"), fluent.functionPredicate.parameters))
    val scale = fullChargeTime / scalingFactor //the Gauss Error function takes place over 3 units

    val batteryParam = fluent.functionPredicate.parameters.take(1)
    val currentStateOfCharge = state.numericFluents(AtomicFormula(aliasedName("state-of-charge"),  batteryParam))

    val scaledTimeSaved = Erf.erfInv(currentStateOfCharge / 100) //already charged in battery

    if (t > 0) {
      val scaledTotalTime = scaledTimeSaved + (t / scale)
      val stateOfChargeAtT = Erf.erf(scaledTotalTime) * 100 //convert to a percentage

      //estimate it linearly
      Some((stateOfChargeAtT - currentStateOfCharge) / t)
    }
    else {
      val timeSaved = scaledTimeSaved * scale
      val remainingFullChargeTime = fullChargeTime - timeSaved

      //estimate it linearly to full charge
      Some((100 - currentStateOfCharge) / remainingFullChargeTime)
    }
  }

  override def isConstant(fluent: NumericFluent, state : State, t : Double): Boolean = {
    val fluentName = fluent.functionPredicate.predicate.drop(aliasPrefix.length() + 1) //we add one to remove the .

    fluentName match {
      case "charge-rate" => false

      case _ => true
    }
  }
}
