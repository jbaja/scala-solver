/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.temporal

import org.apache.commons.math3.optim.nonlinear.scalar.GoalType
import org.apache.commons.math3.optim.PointValuePair
import org.apache.commons.math3.optim.linear._
import uk.ac.kcl.inf.planning.solver.expressions.temporal.ContinuousNumericEffect
import uk.ac.kcl.inf.planning.solver.ff.ValueBounds
import uk.ac.kcl.inf.planning.solver.{SolverSettings, State, AtomicFormula}
import uk.ac.kcl.inf.planning.solver.expressions.numeric._

import scala.annotation.tailrec
import scala.collection.JavaConversions._
import scala.collection.mutable.ListBuffer
import scala.collection._

/**
  * LPScheduler, solves an LP for a temporal state and finds the values of time-dependent numeric fluents and the schedule for the plan.
  *
  * Date: 23/01/15
  * Time: 13:56
  * @author josef
  */
case class LPScheduler(state: TemporalState) {

  //todo: decouple this class from the Apache Commons Math library.
  //todo: Create an intermediate interface and implementing class that maps variables to LP variables and subclass LP implementations from it
  //todo: This way we can add other solvers such as CPlex, LPSolve and Gurobi

  val solver: SimplexSolver = new SimplexSolver

  private lazy val indexFluentMap: Map[Int, AtomicFormula] = {
    //assign an index to each numeric function, so that we can look it up in the array of doubles of our LP
    state.timeDependentFluentsAtHappening(state.plan.size - 1).foldLeft((Map[Int, AtomicFormula](), 0))(
      (mapIndexTuple, fluent) => (mapIndexTuple._1.updated(mapIndexTuple._2, fluent), mapIndexTuple._2 + 1))._1
  }

  //assign an index to each numeric function, so that we can look it up in the array of doubles of our LP
  private lazy val fluentIndexMap: Map[AtomicFormula, Int] = indexFluentMap.map(_.swap)

  private lazy val numericVariableCount = state.timeDependentFluents.size

  //the actions that need to be performed, the variable values in the initial state, and the variables before and after application of each action
  private lazy val variableCount: Int = state.plan.size + (2 * numericVariableCount * state.plan.size)

  private def getVarIndex(fluent: AtomicFormula, stateIndex: Int, pre: Boolean = true): Int = {
    val skip = state.plan.size + (2 * numericVariableCount * stateIndex)
    val offset = fluentIndexMap(fluent)
    if (pre)
      skip + offset //the variable before state application
    else
      skip + numericVariableCount + offset //the variable after state application
  }

  private def getVarArray: Array[Double] = new Array[Double](variableCount)

  private def getFluentObjectiveFunction(fluent: AtomicFormula): LinearObjectiveFunction = {
    val objectiveFunctionCoefficients = getVarArray
    objectiveFunctionCoefficients(getVarIndex(fluent, state.plan.size - 1, false)) = 1 //the variable index of the fluent in the last happening
    new LinearObjectiveFunction(objectiveFunctionCoefficients, 0)
  }

  lazy val numericFluentBounds: Map[AtomicFormula, ValueBounds] = if (SolverSettings.settings.USE_NUMERIC_HEURISTIC) {
    variableObjectiveFunctions.map(pair => {
      val upperBound = computeBound(pair._1, GoalType.MAXIMIZE).getOrElse(Double.PositiveInfinity)
      val lowerBound = computeBound(pair._1, GoalType.MINIMIZE).getOrElse(Double.NegativeInfinity)
      (pair._1, ValueBounds(lowerBound, upperBound))
    })
  } else if (SolverSettings.settings.USE_NUMERIC_HEURISTIC_LINEARONLY) {
    variableObjectiveFunctions.map(pair => {
      if (state.nonLinearFluents.contains(pair._1)) {
        (pair._1, ValueBounds.INFINITY_BOUNDS)
      } else {
        val upperBound = computeBound(pair._1, GoalType.MAXIMIZE).getOrElse(Double.PositiveInfinity)
        val lowerBound = computeBound(pair._1, GoalType.MINIMIZE).getOrElse(Double.NegativeInfinity)
        (pair._1, ValueBounds(lowerBound, upperBound))
      }
    })
  }
  else {
    variableObjectiveFunctions.map(pair => (pair._1, ValueBounds.INFINITY_BOUNDS))
  }

  private lazy val variableObjectiveFunctions: Map[AtomicFormula, LinearObjectiveFunction] =
    state.timeDependentFluents.map(numericFluent => (numericFluent, getFluentObjectiveFunction(numericFluent))).toMap

  private lazy val allTDFluentsObjectiveFunction: LinearObjectiveFunction = {
    val objectiveFunctionCoefficients = getVarArray
    state.timeDependentFluents.foreach(numericFluent =>
      objectiveFunctionCoefficients(getVarIndex(numericFluent, state.plan.size - 1, false)) = 1 //the variable index of the fluent in the last happening
    )
    new LinearObjectiveFunction(objectiveFunctionCoefficients, 0)
  }

  private val makespanObjectiveFunction: LinearObjectiveFunction = {
    val objectiveFunctionCoefficients = getVarArray
    objectiveFunctionCoefficients(state.plan.size - 1) = 1 // the time of the last happening
    new LinearObjectiveFunction(objectiveFunctionCoefficients, 0)
  }

  /**
    * @return An array of doubles representing all the variables, with the required variable set to 1.
    */
  private def getSingleVarArray(index: Int) = {
    val varArray = getVarArray
    varArray(index) = 1
    varArray
  }

  //todo: handle actions running at the initial state (for provided plans with actions with a negative timestamp)

  private def verifyContinuousEffectLinearity: Boolean = {
    state.planConcurrentEffects.forall(entry => {
      val depended = state.concurrentlyModifiedFluents(entry._1)
      //for all continuous effects, and all numeric fluents effected by them, they cannot also be depended variables
      //otherwise a non-linearity is introduced
      entry._2.forall(effect => effect.numericFluents.forall(!depended.contains(_)))
    })
  }


  private def getIntervalVarArray(posIndex: Int, negIndex: Int) = {
    val varArray = getVarArray
    varArray(posIndex) = 1
    varArray(negIndex) = -1
    varArray
  }

  private def getTemporalLinearConstraint(temporalConstraint: TemporalConstraint): LinearConstraint = {
    temporalConstraint match {
      case itc: IntervalTemporalConstraint =>
        val temporalConsVarArray = getIntervalVarArray(itc.upperStep, itc.lowerStep)
        itc.constraint match {
          case NumericConstraint.GEQ => new LinearConstraint(temporalConsVarArray, Relationship.GEQ, itc.bound)

          case NumericConstraint.GRE => new LinearConstraint(temporalConsVarArray, Relationship.GEQ, itc.bound + SolverSettings.settings.EPSILON)

          case NumericConstraint.LES => new LinearConstraint(temporalConsVarArray, Relationship.LEQ, itc.bound - SolverSettings.settings.EPSILON)

          case NumericConstraint.LEQ => new LinearConstraint(temporalConsVarArray, Relationship.LEQ, itc.bound)

          case NumericConstraint.EQ => new LinearConstraint(temporalConsVarArray, Relationship.EQ, itc.bound)
        }

      case atc: AbsoluteTemporalConstraint =>
        val singleVarArray = getSingleVarArray(atc.step)
        atc.constraint match {
          case NumericConstraint.GEQ => new LinearConstraint(singleVarArray, Relationship.GEQ, atc.bound)

          case NumericConstraint.GRE => new LinearConstraint(singleVarArray, Relationship.GEQ, atc.bound + SolverSettings.settings.EPSILON)

          case NumericConstraint.LES => new LinearConstraint(singleVarArray, Relationship.LEQ, atc.bound - SolverSettings.settings.EPSILON)

          case NumericConstraint.LEQ => new LinearConstraint(singleVarArray, Relationship.LEQ, atc.bound)

          case NumericConstraint.EQ => new LinearConstraint(singleVarArray, Relationship.EQ, atc.bound)
        }
    }
  }

  private def getRunningLinearConstraint(runningAction: RunningAction): Option[LinearConstraint] = {
    if (runningAction.startIndex < state.plan.size - 1) {
      runningAction.ubDuration match {
        case Some(maxDuration) =>
          val temporalConsVarArray = getIntervalVarArray(state.plan.size - 1, runningAction.startIndex)
          //todo: if 2 actions can happen together (not MUTEX) the epsilon can be removed
          Some(new LinearConstraint(temporalConsVarArray, Relationship.LEQ, maxDuration + SolverSettings.settings.EPSILON))

        case None => None
      }
    }
    else {
      None
    }
  }

  private def getNumericConditionConstraint(numericCondition: LinearCondition, stateIndex: Int, pre: Boolean): LinearConstraint = {
    val variableArray = getVarArray

    numericCondition.linearFormula1.coefficients.foreach(coeff => {
      val varIndex = getVarIndex(coeff._1, stateIndex, pre)
      variableArray(varIndex) = coeff._2
    })

    numericCondition.linearFormula2.coefficients.foreach(coeff => {
      val varIndex = getVarIndex(coeff._1, stateIndex, pre)
      variableArray(varIndex) = variableArray(varIndex) - coeff._2
    })

    //constant is on the other side of the relationship, so we move it to the other side
    val constant = numericCondition.linearFormula2.constant - numericCondition.linearFormula1.constant
    numericCondition.relation match {
      case NumericConstraint.EQ => new LinearConstraint(variableArray, Relationship.EQ, constant)

      case NumericConstraint.GEQ => new LinearConstraint(variableArray, Relationship.GEQ, constant)

      case NumericConstraint.LEQ => new LinearConstraint(variableArray, Relationship.LEQ, constant)

      //a bit of a hack here to simulate strict >. We can't be too granular or the LP solver might suffer from overflow issues.
      case NumericConstraint.GRE => new LinearConstraint(variableArray, Relationship.GEQ, constant + 0.000001)
      //    case NumericConstraint.GRE => new LinearConstraint(variableArray, Relationship.GEQ, Math.nextAfter(Math.nextAfter(constant, Double.MaxValue), Double.MaxValue))

      //a bit of a hack here to simulate strict <. We can't be too granular or the LP solver might suffer from overflow issues.
      case NumericConstraint.LES => new LinearConstraint(variableArray, Relationship.LEQ, constant - 0.000001)
      //      case NumericConstraint.LES => new LinearConstraint(variableArray, Relationship.LEQ, Math.nextAfter(Math.nextAfter(constant, Double.MinValue), Double.MinValue))
    }
  }

  private def getContinuousEffectConstraint(stateIndex: Int, fluent: AtomicFormula, gradient: Double): Option[LinearConstraint] = {
    if (stateIndex < state.plan.size - 1) {
      val prevValIndex = getVarIndex(fluent, stateIndex, pre = false) //the value V'(i) after applying the action
      val nextValIndex = getVarIndex(fluent, stateIndex + 1, pre = true) //the value V(i+1) before applying the next action

      val variableArray = getVarArray

      variableArray(prevValIndex) = -1
      variableArray(nextValIndex) = 1
      variableArray(stateIndex + 1) = 0.0 - gradient
      variableArray(stateIndex) = gradient

      Some(new LinearConstraint(variableArray, Relationship.EQ, 0.0))
    }
    else {
      None
    }
  }

  private def getChainingValueConstraint(fluent: AtomicFormula, stateIndex: Int): LinearConstraint = {
    val oldValIndex = getVarIndex(fluent, stateIndex, pre = true)
    val newValIndex = getVarIndex(fluent, stateIndex, pre = false)
    val variableArray = getVarArray

    variableArray(oldValIndex) = -1
    variableArray(newValIndex) = 1

    new LinearConstraint(variableArray, Relationship.EQ, 0.0)
  }

  private def getNumericUpdateConstraint(linearEffect: LinearEffect, stateIndex: Int): LinearConstraint = {
    val fluent = linearEffect.fluent
    val oldValIndex = getVarIndex(fluent, stateIndex, pre = true) //the value V(i) before applying the action
    val newVaIndex = getVarIndex(fluent, stateIndex, pre = false) //the value V'(i) after applying the action
    val variableArray = getVarArray

    variableArray(newVaIndex) = 1

    linearEffect.numericUpdate match {
      case NumericUpdate.INCREASE =>
        variableArray(oldValIndex) = -1
        linearEffect.linearFormula.coefficients.foreach(coeff => {
          val coeffIndex = getVarIndex(coeff._1, stateIndex, pre = true)
          variableArray(coeffIndex) = 0.0 - coeff._2
        })
        new LinearConstraint(variableArray, Relationship.EQ, linearEffect.linearFormula.constant)

      case NumericUpdate.DECREASE =>
        variableArray(oldValIndex) = -1
        linearEffect.linearFormula.coefficients.foreach(coeff => {
          val coeffIndex = getVarIndex(coeff._1, stateIndex, pre = true)
          variableArray(coeffIndex) = coeff._2
        })
        new LinearConstraint(variableArray, Relationship.EQ, 0.0 - linearEffect.linearFormula.constant)

      case NumericUpdate.ASSIGN =>
        linearEffect.linearFormula.coefficients.foreach(coeff => {
          val coeffIndex = getVarIndex(coeff._1, stateIndex, pre = true)
          variableArray(coeffIndex) = 0.0 - coeff._2
        })
        new LinearConstraint(variableArray, Relationship.EQ, linearEffect.linearFormula.constant)

      case NumericUpdate.SCALEUP =>
        if (linearEffect.linearFormula.coefficients.nonEmpty)
          throw NonLinearException("Cannot scale-up non-linearly: " + linearEffect.linearFormula)

        variableArray(oldValIndex) = 0.0 - linearEffect.linearFormula.constant
        new LinearConstraint(variableArray, Relationship.EQ, 0.0)

      case NumericUpdate.SCALEDOWN =>
        if (linearEffect.linearFormula.coefficients.nonEmpty)
          throw NonLinearException("Cannot scale-down non-linearly: " + linearEffect.linearFormula)

        variableArray(oldValIndex) = 0.0 - (1 / linearEffect.linearFormula.constant)
        new LinearConstraint(variableArray, Relationship.EQ, 0.0)
    }
  }

  def constraints(nonLinearGradients: Map[Int, List[(ContinuousNumericEffect, Double)]] = state.nonLinearGradientApproximations,
                  additionalTemporalConstraints: List[TemporalConstraint] = List[TemporalConstraint]()) = {
    val constraints: ListBuffer[LinearConstraint] = ListBuffer[LinearConstraint]()

    //add the temporal constraints accumulated in the temporal state (action ordering and durations of actions)
    state.temporalConstraints.foreach(temporalConstraint =>
      constraints.add(getTemporalLinearConstraint(temporalConstraint))
    )

    additionalTemporalConstraints.foreach(temporalConstraint =>
      constraints.add(getTemporalLinearConstraint(temporalConstraint))
    )

    //add the constraints of the running actions
    constraints.addAll(
      state.runningActions.flatMap(runningAction => getRunningLinearConstraint(runningAction)).toList
    )

    //add the value constraints for the initial state
    indexFluentMap.foreach(entry => {
      val constVarArray = getSingleVarArray(entry._1 + state.plan.size)
      val initialValue = state.plan.head.oldState.numericFluents.getOrElse(entry._2, 0.0)
      constraints.add(new LinearConstraint(constVarArray, Relationship.EQ, initialValue))
    })


    //apply the action continuous effects
    (for {
      stateGradients <- compileAllGradients(nonLinearGradients)
      gradientUpdate <- stateGradients._2
      stateIndex = stateGradients._1
      linearConstraint <- getContinuousEffectConstraint(stateIndex, gradientUpdate._1, gradientUpdate._2)
    } yield linearConstraint).foreach(constraints.add)


    for (i <- state.plan.indices) {
      //todo: what about actions already started before the plan?
      val tdFluents: immutable.Set[AtomicFormula] = if (i > 0) state.timeDependentFluentsAtHappening(i - 1) else immutable.Set[AtomicFormula]()

      //action preconditions which have time-dependent variables
      val linearPreconditions = state.plan(i).temporalAction.getLinearPreconditions(tdFluents, state.plan(i).oldState).filter(_.hasVariables)
      linearPreconditions.foreach(linearCondition =>
        constraints.add(getNumericConditionConstraint(linearCondition, i, pre = true))
      )

      //overall conditions which have time-dependent variables

      if (i > 0) {
        // V(i) (before applying action) must satisfy the invariants of the previous happening (i-1)
        val previousLinearOverallConditions = state.plan(i - 1).getLinearInvariantConditions(tdFluents).filter(_.hasVariables)
        previousLinearOverallConditions.foreach(linearCondition => {
          constraints.add(getNumericConditionConstraint(linearCondition, i, pre = true))
        })
      }

      // V'(i) (after applying the action) must satisfy the invariants of this happening (i)
      val linearOverallConditions = state.plan(i).getLinearInvariantConditions(tdFluents).filter(_.hasVariables)
      linearOverallConditions.foreach(linearCondition => {
        constraints.add(getNumericConditionConstraint(linearCondition, i, pre = false))
      })

      //effects between the old state and the new state of non-time-dependent fluents so far, take them from the newState (after applying the action)
      val oldUneffectedFluents = if (i > 0) state.timeDependentFluentsAtHappening(state.plan.size - 1) diff state.timeDependentFluentsAtHappening(i - 1)
      else state.timeDependentFluentsAtHappening(state.plan.size - 1)
      oldUneffectedFluents.foreach(fluent => {
        //we update V'(i) according to the state information, because there were no continuous effects on this fluent
        val curVarIndex = getVarIndex(fluent, i, pre = false)
        val curVarArray = getVarArray
        curVarArray(curVarIndex) = 1
        constraints.add(new LinearConstraint(curVarArray, Relationship.EQ, state.plan(i).newState.numericFluents(fluent)))
      })

      //the ones that haven't been effected by continuous effects so far, can be updated according to the state information
      val uneffectedFluents = state.timeDependentFluentsAtHappening(state.plan.size - 1) diff state.timeDependentFluentsAtHappening(i)
      uneffectedFluents.foreach(fluent => {
        //no continuous effect running on this, so we can also add it on V(i+1) if i+1 exists (i.e. this is not the last state)
        if (i < state.plan.size - 1) {
          val nextVarIndex = getVarIndex(fluent, i + 1, pre = true)
          val nextVarArray = getVarArray
          nextVarArray(nextVarIndex) = 1
          constraints.add(new LinearConstraint(nextVarArray, Relationship.EQ, state.plan(i + 1).oldState.numericFluents(fluent)))
        }
      })

      //the ones that were modified earlier on need to be updated via the LP instead of the state
      if (i > 0) {
        val prevModifiedContinuously = state.timeDependentFluentsAtHappening(i - 1)
        val linearEffects = state.plan(i).temporalAction.getLinearEffects(prevModifiedContinuously, state.plan(i).oldState)
          .filter(linearEffect => prevModifiedContinuously.contains(linearEffect.fluent))
        linearEffects.foreach(linearEffect => {
          constraints.add(getNumericUpdateConstraint(linearEffect, i))
        })

        val remaining = prevModifiedContinuously diff linearEffects.map(_.fluent).toSet
        remaining.foreach(fluent =>
          constraints.add(getChainingValueConstraint(fluent, i))
        )
      }
    }

    constraints.toList
  }

  /**
    * Tries to find a schedule for this state.
    * Any time-dependent variables will also be computed.
    */
  lazy val schedule: Option[TemporalState] = {
    if (state.plan.nonEmpty) {
      //check that there is no non-linearity involved
      if (!verifyContinuousEffectLinearity) {
        println("Non-linearity detected!")
        None
      }
      else {
        computeSchedule()
      }
    }
    else {
      Some(state.copy(schedule = Some(List[Double]())))
    }
  }

  private def runLinearProgram(linearConstraints: List[LinearConstraint]): Option[PointValuePair] =
    findBound(makespanObjectiveFunction, linearConstraints, GoalType.MINIMIZE)

  @tailrec
  private def computeSchedule(nonLinearGradientApproximations: Map[Int, List[(ContinuousNumericEffect, Double)]] = state.nonLinearGradientApproximations,
                              improvementIteration: Int = 0,
                              additionalConstraints: List[TemporalConstraint] = List[TemporalConstraint]()
                             ): Option[TemporalState] = {

    //todo: solve the LP without non-linear constraints first, then add the linear ones with the bounds computed from the linear ones
    //todo: the current implementation risks not getting a solution due to some constraint or bound

    val linearConstraints = constraints(nonLinearGradientApproximations, additionalConstraints)
    runLinearProgram(linearConstraints) match {
      case Some(solution) => {
        //get the values
        val (preStateUpdate, postStateUpdate) = collectNumericUpdates(solution)
        //get the schedule
        val schedule = solution.getPoint.slice(0, state.plan.size).toList

        val errors = compileAllDeltas(schedule, postStateUpdate, nonLinearGradientApproximations)

        val gradientDiscrepancies = errors.groupBy(_.stateIndex).map(entry => {
          entry._1 -> entry._2.map(errorEntry => {
            (errorEntry.effect, errorEntry.realGradient, errorEntry.approximateGradient, errorEntry.nextGuessGradient)
          })
        })

        //check if the deltas are within the required range
        if ((improvementIteration < SolverSettings.settings.MAX_IMPROVEMENT_ITERATIONS) &&
          errors.exists(error => Math.abs(error.errorDelta) > SolverSettings.settings.MAX_ERROR)) {
          val newConstraints = gradientDiscrepancies.map(entry =>
            entry._1 -> {
              val duration = schedule(entry._1 + 1) - schedule(entry._1)
              entry._2.map(contEffEntry => {
                val valueAtDur = contEffEntry._2
                val prevValue = contEffEntry._3

                if (prevValue < 0) //negative gradient
                {
                  if (prevValue < valueAtDur)
                    IntervalTemporalConstraint(entry._1, entry._1 + 1, NumericConstraint.GEQ, duration)
                  else
                    IntervalTemporalConstraint(entry._1, entry._1 + 1, NumericConstraint.LEQ, duration)
                }
                else {
                  if (prevValue < valueAtDur)
                    IntervalTemporalConstraint(entry._1, entry._1 + 1, NumericConstraint.LEQ, duration)
                  else
                    IntervalTemporalConstraint(entry._1, entry._1 + 1, NumericConstraint.GEQ, duration)
                }
              })
            })

          //we filter out redundant bounds by taking the minimum and the maximum
          val filteredConstraintList = newConstraints.map(entry =>
            entry._1 -> entry._2.groupBy(_.constraint).map(list => {
              list._1 match {
                case NumericConstraint.LEQ =>
                  list._2.minBy(_.bound)

                case NumericConstraint.GEQ =>
                  list._2.maxBy(_.bound)
              }
            })
          ).values.flatten.toList

          val newGradientApproximations = gradientDiscrepancies.map(effectsAtIndex =>
            effectsAtIndex._1 -> effectsAtIndex._2.map(effectGradient =>
              (effectGradient._1, effectGradient._4))
          )

          computeSchedule(newGradientApproximations, improvementIteration + 1, filteredConstraintList)
        }
        else {
          val newPlan = updatePlan(state.plan, 0, preStateUpdate, postStateUpdate)
          if (improvementIteration > 0)
            println("Partial plan " + newPlan + " after " + improvementIteration + " improvement iterations")

          //check if the schedule is unfeasible when considering the next TIL/TIF
          if (state.timedActions.nonEmpty && (schedule.last > state.timedActions.head.at))
            None
          else
            Some(TemporalState(
              state.problem,
              state.propositions,
              state.numericFluents ++ postStateUpdate(state.plan.size - 1),
              state.timedActions,
              state.runningActions,
              newPlan,
              state.temporalConstraints,
              Some(schedule),
              Some(this)
            ))
        }
      }

      case None => None
    }
  }


  def collectNumericUpdates(solution: PointValuePair)
  : (Map[Int, Map[AtomicFormula, Double]], Map[Int, Map[AtomicFormula, Double]]) = {
    val preApplyNumericUpdates = mutable.Map[Int, Map[AtomicFormula, Double]]()
    val postApplyNumericUpdates = mutable.Map[Int, Map[AtomicFormula, Double]]()
    val values = solution.getPoint

    for (i <- state.plan.indices) {
      preApplyNumericUpdates(i) = Map[AtomicFormula, Double]()
      postApplyNumericUpdates(i) = Map[AtomicFormula, Double]()

      for (v <- 0 until numericVariableCount) {
        val fluent = indexFluentMap(v)
        val preIndex = state.plan.size + (2 * numericVariableCount * i) + v
        val postIndex = state.plan.size + (2 * numericVariableCount * i) + numericVariableCount + v

        preApplyNumericUpdates(i) = preApplyNumericUpdates(i).updated(fluent, values(preIndex)) //:: preApplyNumericUpdates(i)
        postApplyNumericUpdates(i) = postApplyNumericUpdates(i).updated(fluent, values(postIndex)) //:: postApplyNumericUpdates(i)
      }
    }

    (preApplyNumericUpdates, postApplyNumericUpdates)
  }


  @tailrec
  private def updatePlan(plan: List[Happening],
                         stateIndex: Int,
                         preStates: Map[Int, Map[AtomicFormula, Double]],
                         postStates: Map[Int, Map[AtomicFormula, Double]]): List[Happening] = {
    if (stateIndex >= plan.size) {
      plan
    }
    else {
      val oldHappening = plan(stateIndex)
      val newHappening = Happening(oldHappening.temporalAction,
        State(oldHappening.oldState.modalFacts, oldHappening.oldState.numericFluents ++ preStates(stateIndex)),
        State(oldHappening.newState.modalFacts, oldHappening.newState.numericFluents ++ postStates(stateIndex)),
        oldHappening.invariants)

      val newPlan = plan.updated(stateIndex, newHappening)
      updatePlan(newPlan, stateIndex + 1, preStates, postStates)
    }
  }

  @tailrec
  private def applyErrorCorrection(stateFluents: Map[Int, Map[AtomicFormula, Double]],
                                   errorCorrections: List[NonLinearError]
                                  ): Map[Int, Map[AtomicFormula, Double]] = {
    errorCorrections match {
      case Nil => stateFluents

      case errorCorrection :: tail => {
        val stateIndex = errorCorrection.stateIndex + 1 //we have to adjust V(i+1) of the wrong approximation at V'(i)
        val fluents = stateFluents(stateIndex)
        val erroneousFluent = errorCorrection.effect.fluent.functionPredicate
        val newFluents = fluents.updated(erroneousFluent, fluents(erroneousFluent) + errorCorrection.errorDelta)

        applyErrorCorrection(stateFluents.updated(stateIndex, newFluents), tail)
      }
    }
  }


  private def compileAllDeltas(schedule: List[Double],
                               postStateUpdates: Map[Int, Map[AtomicFormula, Double]],
                               approximations: Map[Int, List[(ContinuousNumericEffect, Double)]],
                               stateIndex: Int = 0,
                               accNonLinDeltas: List[NonLinearError] = List[NonLinearError]()
                              ): List[NonLinearError] = {
    if (stateIndex >= state.plan.size - 1) {
      //base case, we've reached the end of the plan
      accNonLinDeltas
    }
    else {
      val duration = schedule(stateIndex + 1) - schedule(stateIndex)
      val approximationsAtState: List[(ContinuousNumericEffect, Double)] = approximations.getOrElse(stateIndex, List[(ContinuousNumericEffect, Double)]())
      val errors = approximationsAtState.map(nonLinEffect => {
        val effect = nonLinEffect._1
        val approximateGradient = nonLinEffect._2
        //the actual time-dependent values might have changed, so we need to adjust them
        val newState = state.plan(stateIndex).newState.copy(numericFluents = state.plan(stateIndex).newState.numericFluents ++ postStateUpdates(stateIndex))

        val actualGradientAtDuration = nonLinEffect._1.gradient(state.timeDependentFluentsAtHappening(stateIndex), newState, duration)

        val errorDelta = (actualGradientAtDuration * duration) - (approximateGradient * duration)

        val nextGuessGradient = if (actualGradientAtDuration > 0) {
          //increasing
          if (approximateGradient > actualGradientAtDuration)
            (approximateGradient + actualGradientAtDuration) / 2 //curve is convex
          else if (approximateGradient < actualGradientAtDuration)
            actualGradientAtDuration - (approximateGradient - actualGradientAtDuration) / 2 //curve is concave
          else
            actualGradientAtDuration //curve is linear (no error)
        }
        else {
          //decreasing
          if (approximateGradient > actualGradientAtDuration)
            actualGradientAtDuration - (approximateGradient - actualGradientAtDuration) / 2 //curve is convex
          else if (approximateGradient < actualGradientAtDuration)
            (approximateGradient + actualGradientAtDuration) / 2 //curve is concave
          else
            actualGradientAtDuration //curve is linear (no error)
        }

        NonLinearError(stateIndex, duration, effect, approximateGradient, actualGradientAtDuration, errorDelta, nextGuessGradient)
      })

      compileAllDeltas(schedule, postStateUpdates, approximations, stateIndex + 1, accNonLinDeltas ++ errors)
    }
  }

  @tailrec
  private def compileAllGradients(nonLinearGradients: Map[Int, List[(ContinuousNumericEffect, Double)]],
                                  currentGradients: Map[Int, Map[AtomicFormula, Double]] = Map[Int, Map[AtomicFormula, Double]](),
                                  stateIndex: Int = 0
                                 ): Map[Int, Map[AtomicFormula, Double]] = {
    if (stateIndex >= state.plan.size - 1) {
      currentGradients
    }
    else {
      val tdPreviousTimeDependentFluents: immutable.Set[AtomicFormula] =
        if (stateIndex > 0)
          state.timeDependentFluentsAtHappening(stateIndex - 1)
        else
          immutable.Set[AtomicFormula]()

      //we should be able to merge them safely, because they are disjoint
      val linear = state.linearGradients.getOrElse(stateIndex, List[(ContinuousNumericEffect, Double)]())
      val nonLinear = nonLinearGradients.getOrElse(stateIndex, List[(ContinuousNumericEffect, Double)]())
      val allEffects = linear ++ nonLinear

      val groupedUpdates = allEffects.groupBy(contEffectGradient => contEffectGradient._1.fluent.functionPredicate)
        .map(effectGradient => effectGradient._1 -> effectGradient._2.map(_._2).sum)

      val noUpdates: Map[AtomicFormula, Double] = (state.timeDependentFluents diff groupedUpdates.keySet).map(_ -> 0.0).toMap //no change in the remaining

      //if we know any bounds of this state use them, otherwise set it to 1 time unit (this is only to get the gradient for non-linear continuous effects)
      val effectBound: Double = state.getStateBound(stateIndex)._2

      compileAllGradients(nonLinearGradients, currentGradients.updated(stateIndex, noUpdates ++ groupedUpdates), stateIndex + 1)
    }
  }

  private def findBound(linearObjectiveFunction: LinearObjectiveFunction, linearConstraints: List[LinearConstraint], goalType: GoalType): Option[PointValuePair] = {
    try {
      Some(solver.optimize(linearObjectiveFunction, new LinearConstraintSet(linearConstraints), goalType, new NonNegativeConstraint(false)))
    }
    catch {
      case ex: Throwable => None
    }
  }

  @tailrec
  private def computeBound(numericFluent: AtomicFormula,
                           goalType: GoalType,
                           nonLinearGradientApproximations: Map[Int, List[(ContinuousNumericEffect, Double)]] = state.nonLinearGradientApproximations,
                           improvementIteration: Int = 0,
                           additionalConstraints: List[TemporalConstraint] = List[TemporalConstraint]()
                          ): Option[Double] = {

    //todo: solve the LP without non-linear constraints first, then add the linear ones with the bounds computed from the linear ones
    val linearConstraints = constraints(nonLinearGradientApproximations, additionalConstraints)
    findBound(variableObjectiveFunctions(numericFluent), linearConstraints, goalType) match {
      case Some(solution) => {
        //get the values
        val (preStateUpdate, postStateUpdate) = collectNumericUpdates(solution)
        //get the schedule
        val schedule = solution.getPoint.slice(0, state.plan.size).toList

        val errors = compileAllDeltas(schedule, postStateUpdate, nonLinearGradientApproximations)

        val gradientDiscrepancies = errors.groupBy(_.stateIndex).map(entry => {
          entry._1 -> entry._2.map(errorEntry => {
            (errorEntry.effect, errorEntry.realGradient, errorEntry.approximateGradient, errorEntry.nextGuessGradient)
          })
        })

        //check if the deltas are within the required range
        if ((improvementIteration < SolverSettings.settings.MAX_IMPROVEMENT_ITERATIONS) &&
          errors.exists(error => Math.abs(error.errorDelta) > SolverSettings.settings.MAX_ERROR)) {
          val newConstraints = gradientDiscrepancies.map(entry =>
            entry._1 -> {
              val duration = schedule(entry._1 + 1) - schedule(entry._1)
              entry._2.map(contEffEntry => {
                val valueAtDur = contEffEntry._2
                val prevValue = contEffEntry._3

                if (prevValue < 0) {
                  //negative gradient
                  if (prevValue < valueAtDur)
                    IntervalTemporalConstraint(entry._1, entry._1 + 1, NumericConstraint.GEQ, duration)
                  else
                    IntervalTemporalConstraint(entry._1, entry._1 + 1, NumericConstraint.LEQ, duration)
                }
                else {
                  if (prevValue < valueAtDur)
                    IntervalTemporalConstraint(entry._1, entry._1 + 1, NumericConstraint.LEQ, duration)
                  else
                    IntervalTemporalConstraint(entry._1, entry._1 + 1, NumericConstraint.GEQ, duration)
                }
              })
            })

          //we filter out redundant bounds by taking the minimum and the maximum
          val filteredConstraintList = newConstraints.map(entry =>
            entry._1 -> entry._2.groupBy(_.constraint).map(list => {
              list._1 match {
                case NumericConstraint.LEQ =>
                  list._2.minBy(_.bound)

                case NumericConstraint.GEQ =>
                  list._2.maxBy(_.bound)
              }
            })
          ).values.flatten.toList

          val newGradientApproximations = gradientDiscrepancies.map(effectsAtIndex =>
            effectsAtIndex._1 -> effectsAtIndex._2.map(effectGradient =>
              (effectGradient._1, effectGradient._4))
          )

          computeBound(numericFluent, goalType, newGradientApproximations, improvementIteration + 1, filteredConstraintList)
        }
        else {
          val values = solution.getPoint
          Some(values(getVarIndex(numericFluent, state.plan.size - 1, false)))
        }
      }

      case None => None
    }
  }
}

object LPScheduler {
  def scheduleState(temporalStateOpt: Option[TemporalState]): Option[TemporalState] = {
    temporalStateOpt.flatMap(LPScheduler(_).schedule)
  }
}

case class NonLinearError(stateIndex: Int, duration: Double, effect: ContinuousNumericEffect, approximateGradient: Double, realGradient: Double, errorDelta: Double, nextGuessGradient: Double) {
  override val toString: String = "NonLinearError at " + stateIndex + "[" + duration + "] delta : " + errorDelta + " appG: " + approximateGradient + " realG: " + realGradient + " Effect: " + effect
}
