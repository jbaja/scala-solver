/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver

import uk.ac.kcl.inf.planning.solver.expressions._
import uk.ac.kcl.inf.planning.solver.expressions.numeric.{ExternalEvaluator, LinearEffect, LinearCondition}
import uk.ac.kcl.inf.planning.solver.temporal._

import scala.annotation.tailrec

/**
 * The actions that are possible to execute in a plan.
 * Each Action has a name, a sequence of parameters (terms), a precondition expression and an effect expression.
 * Date: 14/11/2013 Time: 15:43
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 */
case class Action(name: String, parameters: List[Term], preconditionExpression: Expression, effectExpression: Expression) extends TemporalAction
{
  val precondition : Condition = preconditionExpression.asInstanceOf[Condition]
  val effect : Effect = effectExpression.asInstanceOf[Effect]

  lazy val invariant = Empty()

  /**
   * @return true if all the parameters, preconditions and effects are grounded
   */
  lazy val isGrounded : Boolean =
    parameters.forall(term => term.isGrounded) &&
      preconditionExpression.isGrounded &&
      effectExpression.isGrounded

  /**
   * Helper value for the parameters that have not yet been bound to a value
   */
  lazy val liftedParameters: List[LiftedTerm] =
    parameters collect {
      case pt: LiftedTerm => pt
    }

  /**
   * Substitutes a lifted parameter with a grounded argument.
   * @param parameter The lifted parameter
   * @param argument The grounded argument
   * @return A new action instance with the parameter substituted with the argument if the lifted parameter was found,
   *         otherwise it returns itself.
   */
  def substitute(parameter: LiftedTerm, argument: GroundedTerm): Action =
  {
    //here we assume that a parameter only occurs once in the Action parameter list
    val i = parameters.indexOf(parameter)
    if (i >= 0)
    {
      Action(name, parameters.updated(i, argument),
        preconditionExpression.substitute(parameter, argument),
        effectExpression.substitute(parameter, argument))
    }
    else
      this
  }

  /**
   * Substitutes a list of parameters with their respective grounded terms
   * @param pairList a list of pairs, the parameter and respective argument to be substituted with
   * @return the action with the specified parameters replaced by their respective arguments
   */
  @tailrec final def substituteList(pairList : List[(LiftedTerm, GroundedTerm)]) : Action =
  {
    pairList match
    {
      case (parameter, argument) :: tail =>
        substitute(parameter, argument).substituteList(tail)

      case Nil => this //all substitutions done
    }
  }

  def updateTypeHierarchy(newTermType : TermType) : Action =
  {
    @tailrec def updateTypeHierarchyParameters(from : Int, actionAcc : Action) : Action =
    {
      val i = actionAcc.parameters.indexWhere(parameter => parameter.termType.name == newTermType.name, from)
      if (i >= 0)
      {
        updateTypeHierarchyParameters(i+1, Action(actionAcc.name,
          actionAcc.parameters.updated(i, actionAcc.parameters(i).updateTypeHierarchy(newTermType)),
          actionAcc.preconditionExpression, actionAcc.effectExpression))
      }
      else
        actionAcc
    }

    updateTypeHierarchyParameters(0,
      Action(name, parameters, preconditionExpression.updateTypeHierarchy(newTermType), effectExpression.updateTypeHierarchy(newTermType)))
  }

  @tailrec final def updateTypeHierarchy(typeList : List[TermType]) : Action =
  {
    typeList match
    {
      case termType :: tail =>
        updateTypeHierarchy(termType).updateTypeHierarchy(tail)

      case Nil => this
    }
  }

  def hookExternalEvaluator(evaluators : Map[String, ExternalEvaluator]) : Action = {
    Action(name, parameters, preconditionExpression.hookExternalEvaluator(evaluators), effectExpression.hookExternalEvaluator(evaluators))
  }

  /**
   * a pair, with the set of positive effects followed by the set of negative effects
   */
  val propositionalEffects : (Set[AtomicFormula], Set[AtomicFormula]) =
    effectExpression match {
      case modalEffect : Modal => modalEffect.atoms
      case _ => (Set[AtomicFormula](), Set[AtomicFormula]())
    }


  lazy val numericEffects : Set[AtomicFormula] =
  {
    effectExpression match {
      case numericEffect : NumericEffect => numericEffect.numericFluents.map(_.functionPredicate)
      case _ => Set[AtomicFormula]()
    }
  }

  lazy val positiveEffects  = propositionalEffects._1
  lazy val negativeEffects = propositionalEffects._2

  /**
   * @return a pair with the positive effects followed with the negative effects
   */
  lazy val propositionalPreconditions : (Set[AtomicFormula], Set[AtomicFormula]) =
  {
    preconditionExpression match {
      case modalCondition : Modal => modalCondition.atoms
      case _ => (Set[AtomicFormula](), Set[AtomicFormula]())
    }
  }

  lazy val positivePropositionalPreconditions : Set[AtomicFormula] = propositionalPreconditions._1
  lazy val negativePropositionalPreconditions : Set[AtomicFormula] = propositionalPreconditions._2

  /**
   * Checks if this action is applicable against a set of facts
   * @param state the facts that are holding true
   * @return true if the action is applicable, false otherwise
   */
  def isApplicable(state : State) : Boolean = precondition.satisfiedBy(state)

  def isApplicable(temporalState: TemporalState) =
    precondition.getNonTemporallyDependentConditions(temporalState.timeDependentFluents)
                .forall(_.satisfiedBy(temporalState.state))

  override def isApplicableRelaxed(relaxedState : RelaxedState) = precondition.relaxedSatisfiedBy(relaxedState)


  /**
   * Generates the grounded actions for this action
   * @param possibleValues The possible values for each type
   * @return
   */
  def generateGroundedActions(possibleValues : Map[TermType, Set[GroundedTerm]]) : Set[Action] = {
    def generateGroundedActionsAcc(accSubstitutions: List[(LiftedTerm, GroundedTerm)], remainder: List[LiftedTerm]): Set[Action] =  {
      remainder match {
        case Nil => Set(substituteList(accSubstitutions))

        case parameter :: tail => for {
          argument <- possibleValues(parameter.termType) if possibleValues.contains(parameter.termType)
          rest <- generateGroundedActionsAcc((parameter, argument) :: accSubstitutions, tail)
        } yield rest
      }
    }

    generateGroundedActionsAcc(Nil, liftedParameters)
  }


  /**
   * Apply an action to a state, which will give us another state.
   * This follows the FF rule that first negative effects are removed, than positive effects are added
   * @param state The state to which this action will be applied
   * @return The new state after applying this action
   */
  def applyToState(state: State) : State = {
    val numericUpdates = effectExpression match {
      case numericEffect : NumericEffect => numericEffect.evaluateNumeric(state)
      case _ => Map[AtomicFormula, Double]()
    }

    State((state.modalFacts -- negativeEffects) ++ positiveEffects, state.numericFluents ++ numericUpdates )
  }

  /**
   * @param oldRelaxedState - the relaxed state prior to the current layer containing upper bounds and lower bounds of each numeric variable
   * @param cumulativeRelaxedState - the relaxed state accumulated from the current layer before this effect
   * @return a map of updates to fluents to a tuple with a possible positive and a possible negative effect on the variable in the relaxed state.
   */
  def applyToRelaxedState(oldRelaxedState : RelaxedState, cumulativeRelaxedState : RelaxedState, elapsedTime : Double = 0) : RelaxedState = {
    effect.relaxedEffect(oldRelaxedState, cumulativeRelaxedState)
  }

  override def applyToTemporalState(temporalState: TemporalState) : Option[TemporalState] = {
    if (!isApplicable(temporalState)) {
      return None
    }

    val newState = applyToState(temporalState.state)
    val index = temporalState.plan.size //we are adding it at the end
    val newTemporalConstraints = getTemporalConstraints(index, temporalState)

    val newTemporalState = TemporalState(
      temporalState.problem,
      newState.modalFacts,
      newState.numericFluents,
      temporalState.timedActions,
      temporalState.runningActions,
      temporalState.plan ::: List(Happening(this, temporalState.state, newState, temporalState.invariants)),
      newTemporalConstraints
    )

    newTemporalState.scheduleState()
  }

  override def getLinearPreconditions(timeDependentVariables: Set[AtomicFormula], state: State): Set[LinearCondition] =
    precondition match {
      case numericCondition : NumericCondition => numericCondition.toLinearCondition(timeDependentVariables, state)
      case _ => Set[LinearCondition]()
  }

  override def getLinearEffects(timeDependentVariables: Set[AtomicFormula], state: State): List[LinearEffect] =
    effect match {
      case numericEffect : NumericEffect => numericEffect.toLinearEffect(timeDependentVariables, state)
      case _ => List[LinearEffect]()
    }

  override val toString = parameters.foldLeft("(" + name)((str, term) => str + " " + term) + ")"
}
