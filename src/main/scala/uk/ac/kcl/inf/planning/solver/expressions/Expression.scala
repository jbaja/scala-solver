/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions

import uk.ac.kcl.inf.planning.solver.expressions.numeric.ExternalEvaluator
import uk.ac.kcl.inf.planning.solver.{TermType, Term, GroundedTerm, LiftedTerm}

/**
 *
 * Date: 10/02/14
 * Time: 16:42 
 * @author josef
 */

trait Expression
{
  type Repr <: Expression

  /**
   * @return true if this expression is fully grounded, false otherwise
   */
  def isGrounded : Boolean

  /**
   * Substitutes the lifted parameter with the grounded argument term.
   * @param parameter The lifted parameter to be substituted
   * @param argument The grounded argument to substitute the parameter
   * @return A new instance of the expression with the parameter substituted by the argument
   */
  def substitute(parameter : LiftedTerm, argument : GroundedTerm) : Repr

  /**
   * If one of the lifted terms is untyped and has the same name, it is replaced by the specified typed parameter.
   * This is used mainly in places where the expression is being parsed without type information (such as PDDL) and type
   * information is provided afterwards.
   * @param typedParameter - The typed parameter to replace the untyped one (which has the same parameter name)
   * @return an expression with the typed parameter
   */
  def typify(typedParameter: Term) : Repr

  /**
   * Typify a list of parameters
   * @param typedParameters - all the (presumably untyped) parameters to be typed
   * @return an Expression with the specified parameters typed
   */
  def typifyAll(typedParameters : List[Term]) : Repr

  /**
   * Updates the type hierarchy of the specified type. While parsing the full type hierarchy might not be immediately
   * known, and determined from a separate type list. This method helps populate the type hierarchy properly.
   * @param termType - the full type to be populated
   * @return The expression updated with the right type information
   */
  def updateTypeHierarchy(termType : TermType) : Repr

  /**
   * Updates the type hierarchy of the specified type. While parsing the full type hierarchy might not be immediately
   * known, and determined from a separate type list. This method helps populate the type hierarchy properly.
   * @param typeList- list of types to be updated
   * @return The expression updated with the right type information
   */

  def updateTypeHierarchy(typeList : List[TermType]) : Repr

  def hookExternalEvaluator(evaluators : Map[String, ExternalEvaluator]) : Repr
}

object Expression {
  //type alias helper to view the type member as though it were a param
  //A neat trick, shamelessly borrowed from the shapeless library
  trait Aux[T <: Expression.Aux[T]] extends Expression {
    self : T =>
    type Repr = T

    def updateTypeHierarchy(typeList : List[TermType]) : T =
    {
      typeList match
      {
        case termType :: tail =>
          updateTypeHierarchy(termType).updateTypeHierarchy(tail)

        case Nil => this
      }
    }

    def typifyAll(typedParameters : List[Term]): T  = {
      typedParameters match
      {
        case head :: tail =>
          typify(head).typifyAll(tail)

        case Nil => this //all typifications done
      }
    }
  }
}


