/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.temporal

import uk.ac.kcl.inf.planning.solver.{SolverSettings, State, Problem}

import scala.annotation.tailrec
import scala.collection.immutable.Queue

/**
 *
 * Date: 21/01/15
 * Time: 14:53 
 * @author josef
 */
class TemporalSolver(problem : Problem) {

  val initialTemporalState = TemporalState.fromState(problem, problem.initialState, problem.timedHappenings)

  def solve(): Option[(List[Happening], List[Double])] = {
    if (SolverSettings.settings.USE_Brfs) {
      breadthFirstSearch()
    }
    else {
      ehcWithBfsFallback().orElse({
        println("********************************************************************************************")
        println("*** EHC with Ascent Backtracking yielded no result. Falling to Best First Search with A* ***")
        println("********************************************************************************************")
        System.gc()
        TemporalState.resetStateCount()
        bestFirstSearch()
      }).orElse({
        println("**********************************************************")
        println("*** A* Failed. Last resort trying Breadth First Search ***")
        println("**********************************************************")
        System.gc()
        TemporalState.resetStateCount()
        breadthFirstSearch(Queue[TemporalState](initialTemporalState), Set[State](initialTemporalState.state))
      })
    }
  }

  def breadthFirstSearch(): Option[(List[Happening], List[Double])] = {
    breadthFirstSearch(Queue[TemporalState](initialTemporalState), Set[State](initialTemporalState.state))
  }


  def ehcWithBfsFallback() : Option[(List[Happening], List[Double])] =
        ehcAbSearch(Queue[TemporalState](initialTemporalState),
          Set[State](initialTemporalState.state),
          initialTemporalState.heuristic, List[TemporalState](initialTemporalState))

  def bestFirstSearch() : Option[(List[Happening], List[Double])] =
        bestFirstSearch(Queue[TemporalState](initialTemporalState),
                        Set[State](initialTemporalState.state))



  @tailrec final def breadthFirstSearch(openList : Queue[TemporalState], seen : Set[State] = Set[State]()) : Option[(List[Happening], List[Double])] = {
    if (openList.isEmpty) //base-case, we don't have anything else to explore
      return None

    openList.dequeue match  {

      case (temporalState, remainingOpenList) =>
        if (problem.goalCondition.satisfiedBy(temporalState.state) &&
                  temporalState.runningActions.isEmpty) {
          println("Goal!! in state " + temporalState.state)
          Some((temporalState.plan, temporalState.schedule.getOrElse(List[Double](0.0))))
        }
        else {
          val goalState = if (temporalState.endSnapActions.isEmpty)
            GoalDummyAction(problem.goalCondition).applyToTemporalState(temporalState)
          else
            None

          val (newOpenList, newSeen) = if (goalState.isDefined) {
            val finalState = goalState.get
            return Some((finalState.plan, finalState.schedule.getOrElse(List[Double](0.0))))
          }
          else  {
            val groundedTemporalStartActions = problem.groundedDurativeStartActions
            val timedAction = temporalState.timedActions.headOption.toList

            //we get the list of all possible actions, they will be filtered out if they are not applicable and schedulable by the applyToTemporalState
            val possibleActions : List[TemporalAction] =  if (problem.goalCondition.satisfiedBy(temporalState.state))
              temporalState.endSnapActions ++ problem.groundedActionsList ++ groundedTemporalStartActions ++ timedAction
            else
              problem.groundedActionsList ++ groundedTemporalStartActions ++ temporalState.endSnapActions ++ timedAction

            //todo: make the seen list maintain a list of 'running' soon to be closed states too

            val newStates = for {
              action <- possibleActions
              newState <- action.applyToTemporalState(temporalState)
              if newState.runningActions.nonEmpty || (!seen.contains(newState.state))
            } yield newState

            (remainingOpenList ++ newStates, seen ++ newStates.filter(_.runningActions.isEmpty).map(_.state))
          }

          breadthFirstSearch(newOpenList, newSeen)
        }
    }
  }

  @tailrec final def bestFirstSearch(openList : Queue[TemporalState], seenStates : Set[State] = Set[State](), useHAOnly : Boolean = false) : Option[(List[Happening], List[Double])] = {
    if (openList.isEmpty) //base-case, we don't have anything else to explore
      return None

    openList.dequeue match  {

      case (temporalState, remainingOpenList) => {
        if (problem.goalCondition.satisfiedBy(temporalState.state) &&
          temporalState.runningActions.isEmpty)
        {
          println("Goal!! in state " + temporalState.state)
          Some((temporalState.plan, temporalState.schedule.getOrElse(List[Double](0.0))))
        }
        else {
          //if no actions are running check if we can schedule the plan in a way that achieves the goal
          val goalState = if (temporalState.endSnapActions.isEmpty)
            GoalDummyAction(problem.goalCondition).applyToTemporalState(temporalState)
          else
            None

          goalState match {
            case Some(finalState) => Some((finalState.plan, finalState.schedule.getOrElse(List[Double](0.0))))

            case None =>
              val groundedTemporalStartActions = if (useHAOnly) temporalState.helpfulActions else problem.groundedDurativeStartActions
              val timedAction = temporalState.timedActions.headOption.toList

              //we get the list of all possible actions, they will be filtered out if they are not applicable and schedulable by the applyToTemporalState
              val possibleActions : List[TemporalAction] =  if (problem.goalCondition.satisfiedBy(temporalState.state))
                temporalState.endSnapActions ++ timedAction ++ problem.groundedActionsList ++ groundedTemporalStartActions
              else
                problem.groundedActionsList ++ groundedTemporalStartActions ++ temporalState.endSnapActions ++ timedAction

              //todo: make the seen list maintain a list of 'running' soon to be closed states too (maybe use A&A paper about Memoization in temporal planning?)

              val newStates = for {
                action <- possibleActions.distinct
                newState <- action.applyToTemporalState(temporalState)
                if newState.runningActions.nonEmpty || (!seenStates.contains(newState.state))
              } yield newState

              val newOpenList = (remainingOpenList ++ newStates).sortBy(state => state.heuristic + state.plan.size)
              val newSeenStates = seenStates ++ newStates.filter(_.runningActions.isEmpty).map(_.state)

              bestFirstSearch(newOpenList, newSeenStates, useHAOnly)
          }
        }
      }
    }
  }


  /**
    * Experimental EHC search with Ascent Backtracking. Keeps track of the enforced hill climbs where the EHC ignored
    * the rest of the helpful actions and if EHC fails backtrack back to the state.
    * @param openList - open list of states to explore
    * @param seenStates - seen states
    * @param bestH - best H
    * @return the list of happenings and the schedule for them.
    */
  @tailrec
  final def ehcAbSearch(openList : Queue[TemporalState], seenStates : Set[State] = Set[State](), bestH : Int = Integer.MAX_VALUE, climbs : List[TemporalState] = List[TemporalState]()) :
                  Option[(List[Happening], List[Double])] = {
    if (openList.isEmpty) //base-case, we don't have anything else to explore
      return ehcAbFallback(climbs, seenStates)

    openList.dequeue match  {

      case (temporalState, remainingOpenList) => {
        if (problem.goalCondition.satisfiedBy(temporalState.state) &&
          temporalState.runningActions.isEmpty)
        {
          println("Goal!! in state " + temporalState.state)
          Some((temporalState.plan, temporalState.schedule.getOrElse(List[Double](0.0))))
        }
        else
        {
          //if no actions are running check if we can schedule the plan in a way that achieves the goal
          val goalState = if (temporalState.endSnapActions.isEmpty)
            GoalDummyAction(problem.goalCondition).applyToTemporalState(temporalState)
          else
            None

          goalState match {
            case Some(finalState) => Some((finalState.plan, finalState.schedule.getOrElse(List[Double](0.0))))

            case None =>
              val timedAction = temporalState.timedActions.headOption.toList
              val possibleActions : List[TemporalAction] = (temporalState.helpfulActions.toList ++ timedAction).distinct
              val newStates = (for {
                action <- possibleActions.view
                newState <- action.applyToTemporalState(temporalState)
                if newState.runningActions.nonEmpty || (!seenStates.contains(newState.state))
              } yield newState).sortWith(TemporalRPG.sortByHeuristicWithTieBreaks)

              val successors = newStates.toStream //stream with cached states
              newStates.find(_.heuristic < bestH) match {
                case Some(newBestState) =>
                  printf("H: %d < %d climbing ...\n", newBestState.heuristic, bestH)
                  newBestState.printPlanString()
                  val newSeen = if (newBestState.runningActions.isEmpty) seenStates + newBestState.state else seenStates
                  ehcAbSearch(Queue[TemporalState](newBestState), newSeen, newBestState.heuristic, temporalState :: climbs)

                case None =>
                  val newSeen = seenStates ++ successors.filter(_.runningActions.isEmpty).map(_.state)
                  ehcAbSearch(remainingOpenList ++ successors, newSeen, bestH, climbs)
              }
          }
        }
      }
    }
  }

  /**
    * This is called when EHC fails, to do an BFS.
    * It performs a BFS (using A*) from the last state in the stack of enforced hill climbs.
    * @param climbs - The stack of enforced hill climbs.
    * @param seen - The list of seen states.
    * @param backtracks - The number of backtracks (just to print it out in the end)
    * @return the plan, as a list of happenings and a schedule
    */
  @tailrec
  final def ehcAbFallback(climbs : List[TemporalState], seen : Set[State] = Set[State](), backtracks : Int = 1) :
    Option[(List[Happening], List[Double])] = {

    //todo: we could switch back to EHC if we find a state that has a better H than what was seen so far (if we pass the bestH as a parameter)
    climbs match {
      case Nil => {
        printf("Failed after %d Ascent Backtracks\n", backtracks)
        None
      }

      case temporalState :: tail => {
         println("Backtracking EHC to previous hill climb.")
         temporalState.printPlanString()
         bestFirstSearch(Queue[TemporalState](temporalState), seen + temporalState.state, true) match {
           case Some(plan) => {
             printf("Ascent Backtracks: %d\n", backtracks)
             Some(plan)
           }

           case None => ehcAbFallback(tail, seen + temporalState.state, backtracks + 1)
         }
      }
    }
  }
}
