/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.temporal

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.expressions.numeric.ExternalEvaluator
import uk.ac.kcl.inf.planning.solver.expressions.temporal.TimeSpecifier._
import uk.ac.kcl.inf.planning.solver.expressions.{Condition, Expression}
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState

/**
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 */
case class EmptyTemporal() extends Expression.Aux[EmptyTemporal] with TimedCondition with TimedEffect
{
  override def updateTypeHierarchy(termType: TermType)  = this

  override def typify(typedParameter: Term)  = this

  override def substitute(parameter: LiftedTerm, argument: GroundedTerm) = this

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]) = this

  override def isGrounded: Boolean = true

  override def applyToState(state: State, when: TimeSpecifier.TimeSpecifier): State = state

  override def satisfiedBy(state: State, when: TimeSpecifier.TimeSpecifier): Boolean = true

  override def conditions(when: TimeSpecifier.TimeSpecifier): Set[Condition] = Set[Condition]()

  override def evaluateRelaxed(oldRelaxedSate: RelaxedState, cumulativeRelaxedState: RelaxedState, when: TimeSpecifier): RelaxedState = cumulativeRelaxedState
}
