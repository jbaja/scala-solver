/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.numeric

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.State
import uk.ac.kcl.inf.planning.solver.AtomicFormula
import uk.ac.kcl.inf.planning.solver.GroundedTerm
import uk.ac.kcl.inf.planning.solver.TermType
import uk.ac.kcl.inf.planning.solver.LiftedTerm
import uk.ac.kcl.inf.planning.solver.expressions.{NumericEffect, Expression}
import uk.ac.kcl.inf.planning.solver.ff.ValueBounds
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState

/**
 *
 * Date: 10/02/14
 * Time: 18:16 
 * @author josef
 */
case class AssignEffect(fluent: NumericFluent, expression: Expression) extends Expression.Aux[AssignEffect] with NumericEffect
{
  val numeric : Numeric = expression.asInstanceOf[Numeric]

  override val isGrounded: Boolean = fluent.isGrounded && expression.isGrounded

  override def substitute(parameter: LiftedTerm, argument: GroundedTerm) : AssignEffect =
      AssignEffect(fluent.substitute(parameter, argument), expression.substitute(parameter, argument))

  override def typify(typedParameter: Term) : AssignEffect =
      AssignEffect(fluent.typify(typedParameter), expression.typify(typedParameter))

  override def updateTypeHierarchy(termType: TermType) : AssignEffect =
      AssignEffect(fluent.updateTypeHierarchy(termType), expression.updateTypeHierarchy(termType))

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): AssignEffect =
      copy(expression = expression.hookExternalEvaluator(evaluators))

  /**
   * Evaluates the effect on the fluent in the state.
   * @param state - the state
   * @param context - contextual information, such as action duration
   * @return a mapping between the updated fluent and the new value of the fluent.
   */
  override def evaluateNumeric(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): Map[AtomicFormula, Double] =
    numeric.value(state, context) match {
      case Some(value) => Map[AtomicFormula, Double]((fluent.functionPredicate, value))
      case None => Map[AtomicFormula, Double]()
    }

  /**
   * @param oldRelaxedState - the relaxed state prior to the current layer containing upper bounds and lower bounds of each numeric variable
   * @param cumulativeRelaxedState - the relaxed state accumulated from the current layer before this effect
   * @return a map of updates to fluents to a tuple with a possible positive and a possible negative effect on the variable in the relaxed state.
   */
  def relaxedEffect(oldRelaxedState: RelaxedState, cumulativeRelaxedState : RelaxedState) : RelaxedState = {
    //get the old bounds (prior to the effect of the action)
    val oldBounds = fluent.relaxedBounds(oldRelaxedState)
    //calculate the bounds from the assign operation
    val assignBounds = oldBounds.flatMap(bounds => numeric.relaxedBounds(oldRelaxedState))
    //get the current cumulative bounds
    val curBounds = fluent.relaxedBounds(cumulativeRelaxedState)

    //combine the assign bounds with the current cumulative bounds
    val newBounds = assignBounds.map(bounds => bounds.combine(curBounds, ValueBounds.assignCombiner))

    newBounds.map { vb =>
      cumulativeRelaxedState.copy(
        numericFluentBounds = cumulativeRelaxedState.numericFluentBounds.updated(fluent.functionPredicate, vb))
    }.getOrElse(cumulativeRelaxedState)
  }

  override def numericFluents: Set[NumericFluent] = Set[NumericFluent](fluent)

  override def toLinearEffect(timeDependentVariables : Set[AtomicFormula], state : State, t : Double = 0.0) : List[LinearEffect] = {
    expression match {
      case (linear : Linear) =>
        List[LinearEffect](LinearEffect(fluent.functionPredicate,
          linear.linearFormula(timeDependentVariables, state, t),
          NumericUpdate.ASSIGN))
    }
  }


}
