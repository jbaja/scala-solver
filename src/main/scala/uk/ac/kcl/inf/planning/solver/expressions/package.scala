/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver

import uk.ac.kcl.inf.planning.solver.expressions.numeric.{NotEqualsCondition, EqualsCondition, GreaterThanEqualCondition, GreaterThanCondition}

/**
 *
 * Date: 09/06/15
 * Time: 18:47 
 * @author josef
 */
package object expressions {
  /**
   * Converts a negated expression into Negated Normal Form
   * @param notExpression - the Not expression (the negation)
   * @return an expression in NNF that includes the negation
   */
  private def nnfNegation(notExpression : NotExpression) : Expression = {
    //if the negated expression needs to be converted to NNF call it recursively
    notExpression.expression match {
      case NotExpression(exp) => toNNF(exp) //not not cancel

      case AndCondition(explist) =>
        val negatedList = explist.map(NotExpression)
        toNNF(OrCondition(negatedList))

      case OrCondition(explist) =>
        val negatedList = explist.map(NotExpression)
        toNNF(AndCondition(negatedList))

      case ImpliesCondition(exp1, exp2) => AndCondition(List[Expression](toNNF(exp1), toNNF(NotExpression(exp2))))

      case GreaterThanCondition(exp1, exp2) =>
        GreaterThanEqualCondition(exp2, exp1) //invert inequality

      case GreaterThanEqualCondition(exp1, exp2) =>
        GreaterThanCondition(exp2, exp1) //invert inequality

      case af: AtomicFormula => notExpression //its already NNF

      case EqualsCondition(exp1, exp2) => notExpression //it is already NNF

      case _ => notExpression //unknown cases?? this case shouldn't be
    }
  }

  /**
   * Converts an expression to Negated Normal Form.
   * It also removes any Implies conditions.
   * @param expression - the expression to convert to NNF
   * @return - an expression in NNF
   */
  def toNNF(expression : Expression) : Expression = {
    expression match {
      case OrCondition(explist) => OrCondition(explist.map(toNNF))

      //while here remove any duplicates
      case AndCondition(explist) => AndCondition(explist.map(toNNF).distinct)

      //while here remove the Implies from the way, and convert Implies(A, B) to Or(Not(A), B)
      case ImpliesCondition(exp1, exp2) => OrCondition(List[Expression](toNNF(NotExpression(exp1)), toNNF(exp2)))

      case af : AtomicFormula => af  //cannot break it further

      case neg : NotExpression => nnfNegation(neg)

      case _ => expression //Greater Than, Empty etc. cannot be broken up further
    }
  }


  def isClause(expression : Expression ) : Boolean = {
   expression match {
      case and : AndCondition => and.isClause

      case not : NotExpression => not.isClause

      case af : AtomicFormula => true

      case nc : NumericCondition => true

      case _ => false
   }
  }


  def isDNF(expression : Expression ) : Boolean = {
    expression match {
      case OrCondition(explist) => explist.forall(isClause)

      case exp => isClause(exp)
    }
  }

  def isCNF(expression : Expression ) : Boolean = {
    expression match {
      case AndCondition(explist) => explist.forall(isClause)

      case exp => isClause(exp)
    }
  }

}
