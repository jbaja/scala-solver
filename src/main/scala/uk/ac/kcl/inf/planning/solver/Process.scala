/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver

import uk.ac.kcl.inf.planning.solver.expressions._

import scala.annotation.tailrec

/**
  * Experimental, non-working implementation!!
  *
  * @author Josef Bajada - josef.bajada@kcl.ac.uk
  */
case class Process(name: String, parameters: List[Term], preconditionExpression: Expression, effect: Expression)
{
  val precondition : Condition = preconditionExpression.asInstanceOf[Condition]

  /**
   * @return true if all the parameters, preconditions and effects are grounded
   */
  lazy val isGrounded : Boolean =
  {
    parameters.forall(term => term.isGrounded) &&
      preconditionExpression.isGrounded &&
      effect.isGrounded
  }

  /**
   * Helper value for the parameters that have not yet been bound to a value
   */
  lazy val liftedParameters: List[LiftedTerm] =
    parameters collect {
      case pt: LiftedTerm => pt
    }

  /**
   * Substitutes a lifted parameter with a grounded argument.
   * @param parameter The lifted parameter
   * @param argument The grounded argument
   * @return A new process instance with the parameter substituted with the argument if the lifted parameter was found,
   *         otherwise it returns itself.
   */
  def substitute(parameter: LiftedTerm, argument: GroundedTerm): Process = {
    //here we assume that a parameter only occurs once in the Process parameter list
    val i = parameters.indexOf(parameter)
    if (i >= 0) {
      Process(name, parameters.updated(i, argument),
        preconditionExpression.substitute(parameter, argument),
        effect.substitute(parameter, argument))
    }
    else
      this
  }

  /**
   * Substitutes a list of parameters with their respective grounded terms
   * @param pairList a list of pairs, the parameter and respective argument to be substituted with
   * @return the process with the specified parameters replaced by their respective arguments
   */
  @tailrec final def substituteList(pairList : List[(LiftedTerm, GroundedTerm)]) : Process = {
    pairList match
    {
      case (parameter, argument) :: tail =>
        substitute(parameter, argument).substituteList(tail)

      case Nil => this //all substitutions done
    }
  }

  def updateTypeHierarchy(newTermType : TermType) : Process = {
    @tailrec def updateTypeHierarchyParameters(from : Int, processAcc : Process) : Process =
    {
      val i = processAcc.parameters.indexWhere(parameter => parameter.termType.name == newTermType.name, from)
      if (i >= 0)
      {
        updateTypeHierarchyParameters(i+1, Process(processAcc.name,
          processAcc.parameters.updated(i, processAcc.parameters(i).updateTypeHierarchy(newTermType)),
          processAcc.preconditionExpression, processAcc.effect))
      }
      else
        processAcc
    }

    updateTypeHierarchyParameters(0,
      Process(name, parameters, preconditionExpression.updateTypeHierarchy(newTermType), effect.updateTypeHierarchy(newTermType)))
  }

  @tailrec final def updateTypeHierarchy(typeList : List[TermType]) : Process = {
    typeList match
    {
      case termType :: tail =>
        updateTypeHierarchy(termType).updateTypeHierarchy(tail)

      case Nil => this
    }
  }


  /**
   * @return a pair with the positive effects followed with the negative effects
   */
  lazy val preconditions : (Set[AtomicFormula], Set[AtomicFormula]) =
  {
    preconditionExpression match {
      case modalCondition : Modal => modalCondition.atoms
      case _ => (Set[AtomicFormula](), Set[AtomicFormula]())
    }
  }

  lazy val positivePreconditions : Set[AtomicFormula] = preconditions._1
  lazy val negativePreconditions : Set[AtomicFormula] = preconditions._2


  /**
   * Checks if this process is applicable against a set of facts
   * @param state the facts that are holding true
   * @return true if the event is applicable, false otherwise
   */
  def isApplicable(state : State) : Boolean = {
    //todo: this may be slowing down things a bit
    precondition.satisfiedBy(state)
  }

  /**
   * Generates the grounded processes for this action
   * @param possibleValues The possible values for each type
   * @return
   */
  def generateGroundedProcesses(possibleValues : Map[TermType, Set[GroundedTerm]]) : Set[Process] = {
    def generateGroundedProcessesAcc(accSubstitutions: List[(LiftedTerm, GroundedTerm)], remainder: List[LiftedTerm]): Set[Process] = {
      remainder match
      {
        case Nil => Set(substituteList(accSubstitutions))

        case parameter :: tail =>
          if (possibleValues.contains(parameter.termType)) {
            for {
              argument <- possibleValues(parameter.termType)
              rest <- generateGroundedProcessesAcc((parameter, argument) :: accSubstitutions, tail)
            } yield rest
          }
          else {
           //this process is not applicable for the problem because there is no object with the required parameter term type
            Set[Process]()
          }

      }
    }

    generateGroundedProcessesAcc(Nil, liftedParameters)
  }

  //todo: implement continuous effects

  override def toString =
    parameters.foldLeft("(" + name)((str, term) => str + " " + term) + ")"
}
