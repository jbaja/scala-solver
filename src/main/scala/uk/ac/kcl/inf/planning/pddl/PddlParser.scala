/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.pddl

import java.io.Reader

import scala.util.parsing.combinator._
import scala.util.parsing.input.{StreamReader, CharArrayReader}
import scala.language.postfixOps

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.expressions._
import uk.ac.kcl.inf.planning.solver.expressions.temporal._
import uk.ac.kcl.inf.planning.solver.expressions.numeric._
import uk.ac.kcl.inf.planning.solver.expressions.metrics._


/**
 * The PDDL domain and problem parser
 * Date: 12/12/13
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 */
object PddlParser extends RegexParsers {

  override protected val whiteSpace = """(\s|;.*)+""".r

  def domain: Parser[Domain] = positioned("(" ~> "define" ~> ("(" ~> "domain" ~> name <~ ")") ~ (require_def ?) ~ (types_def ?) ~
    (constants_def ?) ~ (predicates_def ?) ~ (functions_def ?) ~ (structure_def *) <~ ")" ^^ {
    case name ~ requirements_def ~ types_def ~ constants_def ~ predicates_def ~ functions_def ~ structure_def =>
      println("Initialising domain")
      new Domain(name, types_def.getOrElse(List[TermType]()), optionListToSet(constants_def),
        collectActions(structure_def), collectDurativeActions(structure_def),
        collectProcesses(structure_def), collectEvents(structure_def),
        optionListToSet(predicates_def), optionListToSet(functions_def))
  })

  def problem(domain: Domain): Parser[Problem] = positioned("(" ~> "define" ~> ("(" ~> "problem" ~> name <~ ")") ~ ("(" ~> ":domain" ~> name <~ ")") ~ (require_def ?) ~
    (object_declaration ?) ~ init ~ goal ~ (metric_spec ?) <~ ")" ^^ {
    case problem_name ~ domain_name ~ requirements_def ~ objects ~ init ~ goal ~ metric_spec =>
      println("Initialising problem")
      new Problem(domain, problem_name, optionListToSet(objects), goal, init._1, init._2, metric_spec)
  })

  def require_def = "(" ~> ":requirements" ~> (require_key +) <~ ")"

  def types_def = "(" ~> ":types" ~> type_list <~ ")"

  def constants_def = "(" ~> ":constants" ~> typed_list_names <~ ")"

  def predicates_def = "(" ~> ":predicates" ~> (atomic_formula_skeleton +) <~ ")"

  def functions_def = "(" ~> ":functions" ~> function_typed_list <~ ")"

  def structure_def = action_def | durative_action_def | process_def | event_def

  // | derived_def
  def object_declaration = "(" ~> ":objects" ~> typed_list_names <~ ")"

  //the initial state can contain normal predicates, numeric functions, timed initial literals and timed initial fluents
  def init: Parser[(Map[Double, List[Expression with Modal]], Map[Double, List[(AtomicFormula, Double)]])] = "(" ~> ":init" ~> (init_el *) <~ ")" ^^ {
    case init_els =>
      val timedInitialLiterals: Map[Double, List[Expression with Modal]] = (init_els collect {
        case init_state_modal: Expression with Modal => (0.0, init_state_modal)
        case (time: Double, exp: Expression with Modal) => (time, exp)
      }).groupBy(_._1) mapValues (_ map (_._2))

      val timedInitialFluents: Map[Double, List[(AtomicFormula, Double)]] = (init_els collect {
        case (init_state_af: AtomicFormula, value: Double) => (0.0, (init_state_af, value))
        case (time: Double, (fluent: AtomicFormula, value: Double)) => (time, (fluent, value))
      }).groupBy(_._1) mapValues (_ map (_._2))

      (timedInitialLiterals, timedInitialFluents)
  }

  def init_el = literal_name | assign_init_el | timed_initial_literal | timed_initial_fluent

  def timed_initial_literal: Parser[(Double, Expression with Modal)] = "(" ~> "at" ~> number ~ literal_name <~ ")" ^^ {
    case number ~ exp => (number, exp)
  }

  def timed_initial_fluent: Parser[(Double, (AtomicFormula, Double))] = "(" ~> "at" ~> number ~ assign_init_el <~ ")" ^^ {
    case number ~ exp => (number, exp)
  }

  /* | ("(=" ~ basic_function_term ~ name <~ ")" ) */
  //todo: support equality on functions

  def assign_init_el = "(" ~> "=" ~> basic_function_term ~ number <~ ")" ^^ {
    case af ~ number => (af, number)
  }

  def goal = "(" ~> ":goal" ~> pre_gd <~ ")"

  def parameters_opt = ((":parameters" ~> ("(" ~> typed_list_variables <~ ")"))?) ^^ {
    case Some(list) => list
    case None => List[LiftedTerm]()
  }

  def action_def = "(" ~> ":action" ~> action_symbol ~ parameters_opt ~
    ((":precondition" ~> pre_gd) ?) ~ ((":effect" ~> effect) ?) <~ ")" ^^ {
    case action_name ~ parameters ~ precondition ~ effect =>
      Action(action_name, parameters,
        expressions.toNNF(precondition.getOrElse(Empty())).typifyAll(parameters),
        effect.getOrElse(Empty()).typifyAll(parameters))
  }

  def durative_action_def = "(" ~> ":durative-action" ~> da_symbol ~ parameters_opt ~
    (":duration" ~> duration_constraint) ~ (":condition" ~> da_gd) ~ (":effect" ~> da_effect) <~ ")" ^^ {
    case action_name ~ parameters ~ duration_constraint ~ condition ~ effect =>
      DurativeAction(action_name, parameters,
        duration_constraint.typifyAll(parameters),
        condition.typifyAll(parameters),
        effect.typifyAll(parameters), None)
  }

  def process_def = "(" ~> ":process" ~> process_symbol ~ parameters_opt ~
                    ((":precondition" ~> pre_gd) ?) ~ (":effect" ~> process_effect) <~ ")" ^^ {
    case process_name ~ parameters ~ precondition ~ effect =>
      Process(process_name, parameters,
        precondition.getOrElse(Empty()).typifyAll(parameters),
        effect.typifyAll(parameters))
  }


  def event_def = "(" ~> ":event" ~> event_symbol ~ parameters_opt ~
    ((":precondition" ~> pre_gd) ?) ~ ((":effect" ~> effect) ?) <~ ")" ^^ {
    case event_name ~ parameters ~ precondition ~ effect =>
      Event(event_name, parameters,
        precondition.getOrElse(Empty()).typifyAll(parameters),
        effect.getOrElse(Empty()).typifyAll(parameters))
  }


  def pre_gd: Parser[Expression with Condition] = gd | and_pre_gd | empty
 /*   pref_gd | "(forall" <~ "(" ~> typed_list_variables <~ ")" ~> pre_gd <~ ")" |
            ^^ {  case e : Expression => e
                    case "(and" ~ list => AndExpression(list)
                  }*/
//todo: support preference goal conditions and forall

  def and_pre_gd: Parser[AndCondition] = "(" ~> "and" ~> (pre_gd *) <~ ")" ^^ {
    case exp_list: List[Expression with Condition] => AndCondition(exp_list)
  }

  def effect = and_c_effect | c_effect | empty

  def and_c_effect = "(" ~> "and" ~> (c_effect *) <~ ")" ^^ { AndEffect }

  def pref_gd = "(" ~> "preference" ~ (pref_name ?) ~ gd <~ ")" | gd

  def gd: Parser[Expression with Condition] = atomic_formula_term | literal_term |
    and_gd | or_gd | not_gd | imply_gd | f_comp

  //todo: support  exists, forall goal definitions
  /*   | ("(exists" ~ "(" ~> typed_list_variables <~ ")" ~ gd <~ ")") |
        ("(forall" ~ "(" ~> typed_list_variables <~ ")" ~ gd <~ ")") */

  def not_gd: Parser[NotExpression] = ("(" ~> "not" ~> gd <~ ")") ^^ { NotExpression }
  def and_gd: Parser[AndCondition] = "(" ~> "and" ~> (gd *) <~ ")" ^^ { AndCondition }
  def or_gd: Parser[OrCondition] = "(" ~> "or" ~> (gd *) <~ ")" ^^ { OrCondition }

  def imply_gd: Parser[ImpliesCondition] = "(" ~> "imply" ~> gd ~ gd <~ ")" ^^ {
    case exp1 ~ exp2 => ImpliesCondition(exp1, exp2)
  }

  def da_gd : Parser[Expression with TimedCondition] = pref_timed_gd | da_gd_conjunction | empty_temporal

  def da_gd_conjunction : Parser[TemporalAndCondition] = ("(and" ~> (da_gd*) <~ ")")  ^^ {
    case da_gd_list => TemporalAndCondition(da_gd_list)
  }

  //   | ("(forall" <~ "(" ~> typed_list_variables <~ ")" ~ da_gd <~ ")")
  //todo: support forall timed expression

  def f_comp : Parser[Expression with Condition] = "(" ~> binary_comp ~ f_exp ~ f_exp <~ ")" ^^ {
    case ">=" ~ f_exp1 ~ f_exp2 => GreaterThanEqualCondition(f_exp1, f_exp2)
    case ">"  ~ f_exp1 ~ f_exp2 => GreaterThanCondition(f_exp1, f_exp2)
    case "=" ~ f_exp1 ~ f_exp2 => EqualsCondition(f_exp1, f_exp2)
    case "<=" ~ f_exp1 ~ f_exp2 => GreaterThanEqualCondition(f_exp2, f_exp1)
    case "<"  ~ f_exp1 ~ f_exp2 => GreaterThanCondition(f_exp2, f_exp1)
  }

  def c_effect =  p_effect
  //("(forall" <~ "(" ~> typed_list_variables <~ ")" ~> effect <~ ")") |
  //("(when" ~ gd ~ cond_effect <~ ")")
  //todo: support ADL effects

  def p_effect : Parser[Expression with Effect] = not_atomic_formula_term | atomic_formula_term | p_effect_assign_op
  /*| ("(" ~> "assign" ~ function_term ~ term ~ ")") |
                             ("(" ~> "assign" ~ function_term ~ "undefined" ~ ")") */

  def p_effect_assign_op : Parser[Expression with NumericEffect] = ("(" ~> assign_op ~ f_head ~ f_exp <~ ")") ^^ {
    case "assign" ~ f_head ~ f_exp => AssignEffect(f_head, f_exp)
    case "scale-up" ~ f_head ~ f_exp => ScaleUpEffect(f_head, f_exp)
    case "scale-down" ~ f_head ~ f_exp => ScaleDownEffect(f_head, f_exp)
    case "increase" ~ f_head ~ f_exp => IncreaseEffect(f_head, f_exp)
    case "decrease" ~ f_head ~ f_exp => DecreaseEffect(f_head, f_exp)
  }

  def f_assign_da : Parser[Expression with NumericEffect] = "(" ~> assign_op ~ f_head ~ f_exp_da <~ ")" ^^ {
    case "assign" ~ f_head ~ f_exp => AssignEffect(f_head, f_exp)
    case "scale-up" ~ f_head ~ f_exp => ScaleUpEffect(f_head, f_exp)
    case "scale-down" ~ f_head ~ f_exp => ScaleDownEffect(f_head, f_exp)
    case "increase" ~ f_head ~ f_exp => IncreaseEffect(f_head, f_exp)
    case "decrease" ~ f_head ~ f_exp => DecreaseEffect(f_head, f_exp)
  }

  def cond_effect : Parser[Expression with Effect] =  cond_effect_conj | p_effect

  def cond_effect_conj : Parser[Expression with Effect] = "(and" ~> (p_effect *) <~ ")" ^^ { AndEffect }

  def process_effect : Parser[Expression] =  process_effect_conjunction | continuous_effect | empty_temporal

  def process_effect_conjunction : Parser[TemporalAndEffect] = ("(and" ~> (process_effect *) <~ ")") ^^ { TemporalAndEffect }

  def da_effect: Parser[Expression] = da_effect_conjunction | timed_effect | empty_temporal
/* |   ("(forall" <~ "(" ~> typed_list_variables <~ ")" ~ da_effect) |
    ("(when" ~ da_gd ~ timed_effect <~ ")") | */

  def da_effect_conjunction : Parser[TemporalAndEffect] =  ("(and" ~> (da_effect *) <~ ")") ^^ { TemporalAndEffect }

  def timed_effect : Parser[Expression] = timed_cond_effect |  timed_numeric_effect | continuous_effect

  def continuous_effect : Parser[Expression with ContinuousNumericEffect] = "(" ~> assign_op_t ~ f_head ~ f_exp_t <~ ")" ^^ {
    case "increase" ~ f_head ~ f_exp_t => ContinuousIncreaseEffect(f_head, f_exp_t)
    case "decrease" ~ f_head ~ f_exp_t => ContinuousDecreaseEffect(f_head, f_exp_t)
  }

  def timed_cond_effect : Parser[Expression with TimedEffect] = "(at" ~> time_specifier ~ cond_effect <~ ")"  ^^ {
    case "start" ~ effect => TemporalEffectExpression(effect, TimeSpecifier.AT_START)
    case "end" ~ effect => TemporalEffectExpression(effect, TimeSpecifier.AT_END)
  }

  def timed_numeric_effect : Parser[Expression with TimedNumericEffect] = "(at" ~> time_specifier ~ f_assign_da <~ ")"  ^^ {
    case "start" ~ effect => TemporalEffectExpression(effect, TimeSpecifier.AT_START)
    case "end" ~ effect => TemporalEffectExpression(effect, TimeSpecifier.AT_END)
  }

  def atomic_formula_skeleton = "(" ~> name ~ typed_list_variables <~ ")" ^^ {
    case name ~ typed_list_variables => new AtomicFormula(name, typed_list_variables)
  }

  def derived_def = "(" ~> ":derived" ~> atomic_formula_skeleton ~ gd <~ ")"

  def atomic_formula_term: Parser[ModalExpression] = ("(" ~> predicate ~ (term *) <~ ")") ^^ {
    case predicate ~ terms => ModalExpression(AtomicFormula(predicate, terms))
  }

  def atomic_formula_name: Parser[ModalExpression] = ("(" ~> predicate ~ (name_term *) <~ ")") ^^ {
    case predicate ~ name_terms => ModalExpression(AtomicFormula(predicate, name_terms))
  }

  def literal_term: Parser[Expression with Condition] = atomic_formula_term | not_atomic_formula_term | equals_atomic_formula_term

  def not_atomic_formula_term : Parser[Expression with Condition with Effect] = "(" ~> "not" ~> atomic_formula_term <~ ")" ^^ { NotExpression }

  def equals_atomic_formula_term : Parser[Expression with Condition] = "(" ~> "=" ~> term ~ term <~ ")" ^^ {
    case term1 ~ term2 => EqualTermsCondition(term1, term2)
  }

  def literal_name : Parser[Expression with Modal] = atomic_formula_name | not_atomic_formula_name /*| equals_atomic_formula_name*/

  def not_atomic_formula_name : Parser[NotExpression] = "(" ~> "not" ~> atomic_formula_name <~ ")" ^^ { NotExpression }

  //not clear what does this mean in the initial state
/*  def equals_atomic_formula_name = "(" ~> "=" ~> atomic_formula_name ~ atomic_formula_name<~ ")" ^^ {
    case term1 ~ term2 => EqualsExpression(term1, term2)
  }*/

  def type_list = typed_names.+ ^^ {
      case list =>
        //we first resolve the object types
        val (concluded, pending) = list.flatten.partition(entity => entity._2 == "object")
        val resolvedTypes : Map[String, TermType] = concluded.map(entity => (entity._1, TermType(entity._1, Some(ObjectType)))).toMap

        //we then resolve the other intermediate types
        def resolveTypeHierarchies(concludedTypes : Map[String, TermType], pending : List[(String, String)]) : Map[String, TermType] = {
          if (pending.isEmpty)
            concludedTypes
          else {
            val canConclude = pending.filter(entity => concludedTypes.contains(entity._2))
            val newConcludedTypes = canConclude.map(entity => (entity._1, TermType(entity._1, Some(concludedTypes(entity._2))))).toMap
            resolveTypeHierarchies(concludedTypes ++ newConcludedTypes, pending diff canConclude)
          }
        }

        resolveTypeHierarchies(resolvedTypes, pending).values.toList
  }

  def typed_list_names = typed_names.+ ^^ {
    case list => list.flatten.map(entity =>
    {
      GroundedTerm(entity._1, entity._2 match {
        case "object" => ObjectType
        case _ => TermType(entity._2)
      })})
  }

  def typed_names = (name+) ~ (("-" ~> primitive_type) ?) ^^ {
    case names ~ primitive_type => for {
      name <- names
    } yield (name, primitive_type.getOrElse("object"))
  }

  def typed_list_variables: Parser[List[LiftedTerm]] = typed_variables.* ^^ {
    case list => list.flatten.map(variable =>
      LiftedTerm(variable._1, variable._2 match {
        case "object" => ObjectType
        case _ => TermType(variable._2)
      }))
  }

  def typed_variables = ((variable+) ~ (("-" ~> primitive_type) ?)) ^^ {
    case variables ~ primitive_type => for (variable <- variables) yield variable -> primitive_type.getOrElse("object")
  }

  def f_exp : Parser[Expression with Numeric] = f_exp_number | f_exp_binary_op | f_exp_multi_op | f_exp_negative_op | f_head

  def f_exp_number : Parser[Expression with Numeric] = number ^^ { NumericConstant }

  def f_exp_binary_op : Parser[Expression with Numeric] = "(" ~> binary_op ~ f_exp ~ f_exp <~ ")" ^^ {
    case "-" ~ fexp1 ~ fexp2 => SubtractNumeric(fexp1, fexp2)
    case "/" ~ fexp1 ~ fexp2 => DivideNumeric(fexp1, fexp2)
    case "+" ~ fexp1 ~ fexp2 => AddNumeric(List[Expression with Numeric](fexp1, fexp2))
    case "*" ~ fexp1 ~ fexp2 => MultiplyNumeric(List[Expression with Numeric](fexp1, fexp2))
  }

  def f_exp_multi_op : Parser[Expression with Numeric] = "(" ~> multi_op ~ f_exp ~ (f_exp +) <~ ")" ^^ {
    case "+" ~ first ~ rest => AddNumeric(first :: rest)
    case "*" ~ first ~ rest => MultiplyNumeric(first :: rest)
  }

  def f_exp_negative_op : Parser[Expression with Numeric] = "(" ~> "-" ~> f_exp <~ ")" ^^ { NegativeNumeric }

  def f_head : Parser[NumericFluent] = f_head_args | f_head_atom

  //numeric fluents with arguments
  def f_head_args : Parser[NumericFluent] = "(" ~> function_symbol ~ (term *) <~ ")" ^^ {
    case function_name ~ terms => NumericFluent(AtomicFormula(function_name, terms))
  }

  //empty numeric fluent (without arguments)
  def f_head_atom : Parser[NumericFluent] = function_symbol ^^ {
    case function_name => NumericFluent(AtomicFormula(function_name, List[Term]()))
  }

  def f_exp_da: Parser[Expression with Numeric] = f_exp_binary_op_da |
    f_exp_multi_op_da |
    f_exp_negative_op_da |
    duration |
    f_exp

  def f_exp_binary_op_da : Parser[Expression with Numeric] = "(" ~> binary_op ~ f_exp_da ~ f_exp_da <~ ")" ^^ {
    case "-" ~ fexp1 ~ fexp2 => SubtractNumeric(fexp1, fexp2)
    case "/" ~ fexp1 ~ fexp2 => DivideNumeric(fexp1, fexp2)
    case "+" ~ fexp1 ~ fexp2 => AddNumeric(List[Expression with Numeric](fexp1, fexp2))
    case "*" ~ fexp1 ~ fexp2 => MultiplyNumeric(List[Expression with Numeric](fexp1, fexp2))
  }

  def f_exp_multi_op_da : Parser[Expression with Numeric] = ("(" ~> multi_op ~ f_exp_da ~ (f_exp_da *) <~ ")") ^^ {
    case "+" ~ first ~ rest => AddNumeric(first :: rest)
    case "*" ~ first ~ rest => MultiplyNumeric(first :: rest)
  }

  def f_exp_negative_op_da : Parser[Expression with Numeric] = "(" ~> "-" ~> f_exp_da <~ ")" ^^ { NegativeNumeric }

  def duration: Parser[NumericVariable] = "?duration" ^^ { s => NumericVariable("?duration") }

  def f_exp_t : Parser[ContinuousTemporalExpression] = f_t_multiply_exp | f_exp_multiply_t | f_exp_t_constant

  def f_t_multiply_exp : Parser[ContinuousTemporalExpression] = ("(" ~> "*" ~> "#t" ~> f_exp <~ ")") ^^ {
    multiplicand => ContinuousTemporalExpression(multiplicand)
  }

  def f_exp_multiply_t : Parser[ContinuousTemporalExpression] = ("(" ~> "*" ~> f_exp <~ "#t" <~ ")") ^^ {
    multiplicand => ContinuousTemporalExpression(multiplicand)
  }

  def f_exp_t_constant : Parser[ContinuousTemporalExpression] = "#t" ^^ { t => ContinuousTemporalExpression(NumericConstant(1.0))}

  def duration_constraint : Parser[Expression with Condition] = duration_constraint_conjunction |
    simple_duration_constraint | empty


  def duration_constraint_conjunction : Parser[Expression with Condition] = "(and" ~> (simple_duration_constraint +) <~ ")" ^^ {
    list => AndCondition(list)
  }

  def simple_duration_constraint: Parser[Expression with Condition] = ("(" ~> d_op ~ "?duration" ~ d_value <~ ")") ^^ {
    case "<" ~ "?duration" ~ d_value => GreaterThanCondition(d_value, NumericVariable("?duration"))
    case "<=" ~ "?duration" ~ d_value => GreaterThanEqualCondition(d_value, NumericVariable("?duration"))
    case ">" ~ "?duration" ~ d_value => GreaterThanCondition(NumericVariable("?duration"), d_value)
    case ">=" ~ "?duration" ~ d_value => GreaterThanEqualCondition(NumericVariable("?duration"), d_value)
    case "=" ~ "?duration" ~ d_value => EqualsCondition(NumericVariable("?duration"), d_value)
  }

  /*| ("(at" ~> time_specifier ~ simple_duration_constraint <~ ")") */
  //todo: support Duration time specifier (AT START or AT END)

  def binary_comp = "<=" | ">=" | "<" | ">" | "="
  def binary_op = "-" | "/" | multi_op
  def multi_op = "*" | "+"
  def assign_op = "assign" | "scale-up" | "scale-down" | "increase" | "decrease"
  def assign_op_t = "increase" | "decrease"
  def d_op = "<=" | ">=" | "<" | ">" | "="
  def d_value : Parser[Expression with Numeric] = numeric_constant | f_exp

  def timed_gd : Parser[TemporalConditionExpression] = (("(" ~> "at" ~ time_specifier ~ gd <~ ")") |
    ("(" ~> "over" ~ interval ~ gd <~ ")"))  ^^ {
      case "at" ~ "start" ~ gd => TemporalConditionExpression(expressions.toNNF(gd), TimeSpecifier.AT_START)
      case "at" ~ "end" ~ gd => TemporalConditionExpression(expressions.toNNF(gd), TimeSpecifier.AT_END)
      case "over" ~ "all" ~ gd => TemporalConditionExpression(expressions.toNNF(gd), TimeSpecifier.OVER_ALL)
    }

  def pref_timed_gd : Parser[TemporalConditionExpression] = timed_gd // | ("(preference" ~ (pref_name ?) ~ timed_gd <~ ")")
  def time_specifier = "start" | "end"
  def interval = "all"

  def metric_spec : Parser[MetricSpec] = "(" ~> ":metric" ~> optimization ~ metric_f_exp <~ ")" ^^ {
    case "minimize" ~ exp => MetricSpec(OptimizationDirective.MINIMIZE, exp)
    case "maximize" ~ exp => MetricSpec(OptimizationDirective.MAXIMIZE, exp)
  }

  def optimization = "minimize" | "maximize"

  def metric_f_exp : Parser[Expression with MetricNumeric] = binary_metric_exp | multi_metric_op | negative_numeric_metric | metric_numeric | metric_t_multiply_exp | metric_multiply_t_exp | metric_t_func_exp

  def metric_t_multiply_exp : Parser[ContinuousTemporalMetric] = ("(" ~> "*" ~> "#t" ~> f_exp <~ ")") ^^ {
    multiplicand => ContinuousTemporalMetric(multiplicand)
  }

  def metric_t_func_exp : Parser[Expression with MetricNumeric] = ("(:fn" ~> ("min" | "max") ~ f_exp <~ ")") ^^ {
    case "min" ~ exp => MinTemporalMetric(exp)
    case "max" ~ exp => MaxTemporalMetric(exp)
  }

  def metric_multiply_t_exp : Parser[ContinuousTemporalMetric] = ("(" ~> "*" ~> f_exp <~ "#t" <~ ")") ^^ {
    case multiplicand => ContinuousTemporalMetric(multiplicand)
  }

  def negative_numeric_metric : Parser[Expression with MetricNumeric] = "(" ~> "-" ~> metric_f_exp <~ ")" ^^ { NegativeMetric }

  def binary_metric_exp : Parser[Expression with MetricNumeric] = "(" ~> binary_op ~ metric_f_exp ~ metric_f_exp <~ ")" ^^ {
    case "-" ~ exp1 ~ exp2 => SubtractMetric(exp1, exp2)
    case "/" ~ exp1 ~ exp2 => DivideMetric(exp1, exp2)
    case "+" ~ exp1 ~ exp2 => AddMetric(List[Expression](exp1, exp2))
    case "*" ~ exp1 ~ exp2 => MultiplyMetric(List[Expression](exp1, exp2))
  }

  def multi_metric_op : Parser[Expression with MetricNumeric] = "(" ~> multi_op ~ metric_f_exp ~ (metric_f_exp *) <~ ")" ^^ {
    case "*" ~ first ~ rest => MultiplyMetric(first :: rest)
    case "+" ~ first ~ rest => AddMetric(first :: rest)
  }

  def metric_numeric : Parser[Expression with MetricNumeric] = metric_total_time | metric_expression

  def metric_total_time : Parser[TotalTimeMetric] = "total-time" ^^ { _ => TotalTimeMetric() }

  def metric_expression : Parser[ExpressionMetric] = numeric_expression ^^ { ExpressionMetric }

  def numeric_expression : Parser[Expression with Numeric] = numeric_constant | grounded_fluent | function_symbol_fluent

  def grounded_fluent : Parser[NumericFluent] = function_symbol_names ^^ { NumericFluent(_) }

  def function_symbol_names : Parser[AtomicFormula] = "(" ~> function_symbol ~ (name_term *) <~ ")" ^^ {
    case fs ~ name_terms => AtomicFormula(fs, name_terms)
  }

  def function_symbol_fluent : Parser[NumericFluent] = function_symbol ^^ {
    case function_name => NumericFluent(AtomicFormula(function_name, List[Term]()))
  }

  def basic_function_term = function_symbol_noterms | function_symbol_terms
  def function_symbol_noterms = function_symbol ^^ { case fs => AtomicFormula(fs, List[Term]())}
  def function_symbol_terms = ("(" ~> function_symbol ~ (term *) <~ ")")  ^^ { case fs ~ terms => AtomicFormula(fs, terms)}

  def function_term = "(" ~> function_symbol ~ (term *) <~ ")"

  def term: Parser[Term] = name_term | variable_term /* | function_term  */

  def variable_term: Parser[LiftedTerm] = variable ^^ { LiftedTerm(_) }
  def name_term: Parser[GroundedTerm] = name ^^ { GroundedTerm(_) }

  def function_typed_list = function_def.+
  def action_symbol: Parser[String] = name
  def da_symbol : Parser[String] = name
  def process_symbol : Parser[String] = name
  def event_symbol : Parser[String] = name
  def function_symbol = name
  def predicate = name
  def primitive_type = name | "object"
  def pref_name = name

  def function_def = atomic_formula_skeleton | "(" ~> function_symbol_noterms <~ ")"

  def empty: Parser[Empty] = "(" ~ ")" ^^ { _ => Empty() }
  def empty_temporal : Parser[EmptyTemporal] = "(" ~ ")" ^^ { _ => EmptyTemporal() }

  def variable: Parser[String] = """\?[a-zA-Z][a-zA-Z0-9_-]*""".r ^^ { _.substring(1) }

  def name: Parser[String] = """[a-zA-Z][\.a-zA-Z0-9_-]*""".r
  def number : Parser[Double] = """-?[0-9]+(\.[0-9]+)?""".r ^^ { _.toDouble }
  def numeric_constant : Parser[NumericConstant] = number ^^ { NumericConstant }

  val require_key = ":strips" | ":typing" | ":negative-preconditions" | ":disjunctive-preconditions" | ":equality " |
    ":existential-preconditions" | ":universal-preconditions" | ":quantified-preconditions" |
    ":conditional-effects" | ":fluents" | ":numeric-fluents" | ":adl" | ":durative-actions" |
    ":duration-inequalities" | ":continuous-effects" | ":derived-predicates" | ":timed-initial-literals" |
    ":preferences" | ":constraints" | ":action-costs" | ":timed-initial-fluents" | ":class-modules"

  //todo: support these for non-numeric functions (object fluents)
  def function_type = "number" | pddl_type

  def pddl_type = "(either" ~> (primitive_type +) <~ ")" | primitive_type

  //todo: support preferences in parser
  //todo: support constraints_def for full PDDL 3.1 support
  /*def constraints_def = "(:constraints" ~> con_gd <~ ")"
  def con_gd = "(and" ~ (con_gd.*) <~ ")" |
               "(forall" ~ ("(" ~> typed_list_variables <~ ")") ~ (con_gd) <~ ")" |
               "(at end " ~ gd <~ ")"*/

  def parseDomain(pddlDomain: String) = parseAll(domain, new CharArrayReader(pddlDomain.toCharArray))

  def parseDomain(pddlDomain: Reader) = parseAll(domain, StreamReader(pddlDomain))

  def parseProblem(domain : Domain, pddlProblem: String) = parseAll(problem(domain), new CharArrayReader(pddlProblem.toCharArray))

  def parseProblem(domain : Domain, pddlProblem: Reader) = parseAll(problem(domain), StreamReader(pddlProblem))
}

