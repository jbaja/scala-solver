/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions

import uk.ac.kcl.inf.planning.solver.expressions.numeric.{LinearCondition, Numeric}
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState
import uk.ac.kcl.inf.planning.solver.{AtomicFormula, State}

/**
 *
 * Date: 10/02/14
 * Time: 16:55 
 * @author josef
 */
trait Condition
{
  /**
   * Determines whether this condition is satisfied in the provided state.
   * @param state The state to be checked for this condition.
   * @return true if the state satisfies the condition, false otherwise
   */
  def satisfiedBy(state : State, context: Map[Expression, Double] = Map[Expression, Double]()) : Boolean

  def relaxedSatisfiedBy(relaxedState : RelaxedState) : Boolean

  def violationCount(state : State) : Int = if (satisfiedBy(state)) 0 else 1

  def violations(state: State, context: Map[Expression, Double] = Map[Expression, Double]()) : Set[Condition]

  def getNonTemporallyDependentConditions(timeDependentFluents : Set[AtomicFormula]) : Set[Condition] = Set[Condition](this)

  def atomicConditions : Set[Condition] = Set[Condition](this)

  def atomicFormulas : Set[AtomicFormula]

  def invert : Expression with Condition

  def isClause : Boolean = true
}

trait NumericCondition extends Condition
{
  def toLinearCondition(timeDependentVariables : Set[AtomicFormula], state : State, t : Double = 0.0) : Set[LinearCondition]

  def isTemporallyDependent(timeDependentFluents: Set[AtomicFormula]): Boolean

  def getNonTemporallyDependentConditions(timeDependentFluents : Set[AtomicFormula]) : Set[Condition]

  def atomicFormulas = fluents

  /**
   * @return all the fluents in this numeric expression
   */
  def fluents : Set[AtomicFormula]
}

trait ComparisonCondition extends NumericCondition
{
  def normalisedConstant : Double
  def normalisedExpression : Expression with Numeric
}
