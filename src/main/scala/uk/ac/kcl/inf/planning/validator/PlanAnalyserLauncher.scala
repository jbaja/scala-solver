/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.validator

import java.io.FileReader
import uk.ac.kcl.inf.planning.pddl.PddlParser
import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.GroundedTerm
import uk.ac.kcl.inf.planning.solver.LiftedTerm
import uk.ac.kcl.inf.planning.optimiser.Optimiser
import java.util.Date
import uk.ac.kcl.inf.planning.solver.expressions.temporal.TimeSpecifier

import scala.collection.immutable.Iterable

/**
 * @author Josef Bajada - josef.bajada@kcl.ac.uk
 */
object PlanAnalyserLauncher {

  def getGroundedTerms(problem : Problem, termNames : List[String]) : List[GroundedTerm] =
  {
    for {
      objectName <- termNames
      groundedTerm <- problem.objects.find(gTerm => gTerm.name equals objectName)
    } yield groundedTerm

//    termNames.map( objectName => {
//      problem.objects.find(groundedTerm => groundedTerm.name equals objectName) match {
//        case Some(groundedTerm) =>  groundedTerm //we found it
//        case None => println("Unable to find object " + objectName + " in problem definition ... ignoring!")
//      }
//    })
  }

  def parsePlan(filename : String) : Plan =
  {
    val parsedPlan = PlanParser.parsePlan(new FileReader(filename))
    parsedPlan match {
      case PlanParser.Success(r, n) => println(r)
      case PlanParser.Failure(msg, n) => println(msg + " in " + filename + ":" + n.pos)
      case PlanParser.Error(msg, n) => println(msg + " in " + filename + ":" + n.pos)
    }

    parsedPlan.getOrElse(Plan(List[PlanStep]()))
  }

  def main(args: Array[String])
  {
    if (args.length >= 2)
    {
      val parsedDomain = PddlParser.parseDomain(new FileReader(args(0)))
      parsedDomain match {
        case PddlParser.Success(r, n) => println(r)
        case PddlParser.Failure(msg, n) => println(msg + " in " + args(0) + ":" + n.pos)
        case PddlParser.Error(msg, n) => println(msg + " in " + args(0) + ":" + n.pos)
      }
      val domain : Domain = parsedDomain.get

      val parsedProblem = PddlParser.parseProblem(parsedDomain.get, new FileReader(args(1)))
      parsedProblem match {
        case PddlParser.Success(r, n) => println(r)
        case PddlParser.Failure(msg, n) => println(msg + " in " + args(1) + ":" + n.pos)
        case PddlParser.Error(msg, n) => println(msg + " in " + args(1) + ":" + n.pos)
      }

      val problem : Problem = parsedProblem.get

      val plan : Plan = if (args.length >= 3) parsePlan(args(2)) else Plan(List[PlanStep]())

      val actionSteps : List[TimedApplicable] = plan.planSteps.flatMap(planAction => {
        //match all the terms with their respective typed grounded terms from the problem file
        val groundedTerms = getGroundedTerms(problem, planAction.terms)

        //find the action from the actions list of the domain file
        domain.actions.find(action => action.name equals planAction.actionSymbol) match {
          case Some(action) => {
            val pairList = (action.parameters collect {
              case lt: LiftedTerm => lt
            }) zip groundedTerms
            val groundedAction = action.substituteList(pairList)
            List[TimedApplicable](TimedApplicableAction(planAction.at, groundedAction))
          }
          case None => {
            //not in the actions list, check if it is a durative action
            domain.durativeActions.find(durativeAction => durativeAction.name equals planAction.actionSymbol) match {
              case Some(durativeAction) => {
                val pairList = (durativeAction.parameters collect {
                  case lt: LiftedTerm => lt
                }) zip groundedTerms

                val groundedDurativeAction = durativeAction.substituteList(pairList).copy(duration = Some(planAction.duration))
                val startAction = TimedApplicableDurativeAction(planAction.at, groundedDurativeAction, TimeSpecifier.AT_START)
                val endAction = TimedApplicableDurativeAction(planAction.at + planAction.duration, groundedDurativeAction, TimeSpecifier.AT_END, Some(startAction))
                List[TimedApplicable](startAction, endAction)
              }

              case None =>
                throw new Exception("Unable to find action " + planAction.actionSymbol + " in domain!")
            }
          }
        }
      })


      val timedInitialLiterals = problem.timedInitialLiterals
                                        .flatMap(timedList => timedList._2 map (til => TimedInitialLiteral(timedList._1, til)))

      val timedInitialFluents = problem.timedInitialFluents
                                       .flatMap(timedList => timedList._2 map (tif => TimedInitialFluent(timedList._1, tif._1, tif._2)))

      val allPlanSteps = (actionSteps ++ timedInitialLiterals ++ timedInitialFluents).sortBy(_.at)

      val groundedPlan = GroundedPlan(problem, allPlanSteps)
      println("\nGrounded Plan: " + groundedPlan)

      val planTimeline = groundedPlan.timeline
      val cost = planTimeline.cost
      val violations = planTimeline.violationCount

      println("\nPlan State Timeline:" + planTimeline + " cost " + cost + "\n")

      println("\nGoal condition " + problem.goalCondition + " satisfied? " + problem.goalCondition.satisfiedBy(planTimeline.endState))

      println(new Date(System.currentTimeMillis()))
      println("\n\n *** Searching ... \n\n")
      val newPlan = Optimiser.searchForBetterPlan(problem, groundedPlan, planTimeline, 100)


     // println("\nNew plan: " + newPlan)
      val newPlanTimeline = newPlan.timeline
      val newCost = newPlanTimeline.cost

      println("\n\n\n:::::::::::Found Plan :::::::::::::")
      println(newPlan.toPlanString)

      if (planTimeline.violationCount == 0) {
        println("\nNew Plan Cost: " + newCost + " (< " + cost + ")\n")
      }
      else
      {
        println("\nPlan Cost: " + newCost)
      }
    }
    else
    {
      println("No Plan file specified!")
    }
  }

}


