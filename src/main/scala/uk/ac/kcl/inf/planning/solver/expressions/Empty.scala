/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.expressions.numeric.ExternalEvaluator
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState

/**
  * @author Josef Bajada - josef.bajada@kcl.ac.uk
  */
case class Empty() extends Expression.Aux[Empty] with Modal with Effect with Condition
{
  override def updateTypeHierarchy(termType: TermType): Empty = this

  override def typify(typedParameter: Term): Empty = this

  override def substitute(parameter: LiftedTerm, argument: GroundedTerm): Empty = this

  override def isGrounded: Boolean = true

  override def satisfiedBy(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): Boolean = true

  override def relaxedSatisfiedBy(relaxedState: RelaxedState): Boolean = true

  override def positiveAtoms: Set[AtomicFormula] = Set[AtomicFormula]()

  override def negativeAtoms: Set[AtomicFormula] = Set[AtomicFormula]()

  override def atomicConditions : Set[Condition] = Set[Condition]()

  override def atomicFormulas : Set[AtomicFormula] = Set[AtomicFormula]()

  override def applyToState(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): State = state

  override def violationCount(state: State): Int = 0

  override def violations(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): Set[Condition] = Set[Condition]()

  //inverting empty remains empty, this is a special case
  override def invert: Expression with Condition = this

  override def equals(obj: scala.Any): Boolean = obj.getClass().equals(this.getClass())

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): Empty = this

  /**
   * @param oldRelaxedState - the relaxed state prior to the current layer containing upper bounds and lower bounds of each numeric variable
   * @param cumulativeRelaxedState - the relaxed state accumulated from the current layer before this effect
   * @return a map of updated values to the fluents, with the values being an upper and a lower bound
   */
  override def relaxedEffect(oldRelaxedState: RelaxedState, cumulativeRelaxedState: RelaxedState): RelaxedState = cumulativeRelaxedState
}
