/*
 *     Copyright (C) 2016 Josef Bajada
 *     josef.bajada@gmail.com
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package uk.ac.kcl.inf.planning.solver.expressions.numeric

import uk.ac.kcl.inf.planning.solver._
import uk.ac.kcl.inf.planning.solver.State
import uk.ac.kcl.inf.planning.solver.GroundedTerm
import uk.ac.kcl.inf.planning.solver.LiftedTerm
import uk.ac.kcl.inf.planning.solver.expressions.Expression
import uk.ac.kcl.inf.planning.solver.ff.ValueBounds
import uk.ac.kcl.inf.planning.solver.temporal.RelaxedState

/**
 *
 * Date: 10/02/14
 * Time: 19:50 
 * @author josef
 */
case class AddNumeric(expressions: List[Expression]) extends Expression.Aux[AddNumeric] with Numeric with Linear
{
  val numerics : List[Numeric] = expressions collect { case numeric : Numeric => numeric }

  override val isGrounded: Boolean = expressions.forall(num => num.isGrounded)

  override def value(state: State, context: Map[Expression, Double] = Map[Expression, Double]()): Option[Double] = {
      val values = numerics.map(_.value(state, context))
      //todo: should we ignore undefined values? or should we return None (and maybe log an error)
      Some(numerics.map(_.value(state, context).getOrElse(0.0)).sum)
  }

  def relaxedBounds(relaxedState: RelaxedState) : Option[ValueBounds] =  {
      val allOperands = numerics.map(_.relaxedBounds(relaxedState))
      if (allOperands.isEmpty || allOperands.exists(_.isEmpty))
        None
      else
        allOperands.foldLeft(Some(ValueBounds(0.0, 0.0)) : Option[ValueBounds]){
          (acc, operand) => acc.map(accVb => accVb.combine(operand, ValueBounds.increaseCombiner))
        }
  }

  override def updateTypeHierarchy(termType: TermType) = AddNumeric(expressions.map(exp => exp.updateTypeHierarchy(termType)))

  override def typify(typedParameter: Term) = AddNumeric(expressions.map(numeric => numeric.typify(typedParameter)))

  override def substitute(parameter: LiftedTerm, argument: GroundedTerm) = AddNumeric(expressions.map(numeric => numeric.substitute(parameter, argument)))

  override def hookExternalEvaluator(evaluators: Map[String, ExternalEvaluator]): AddNumeric =
    copy(expressions = expressions.map(_.hookExternalEvaluator(evaluators)))


  override def fluents: Set[NumericFluent] = (expressions collect {
    case numeric : Numeric => numeric}).foldLeft(Set[NumericFluent]())((accSet, numeric) => accSet ++ numeric.fluents)

  //the coeffecients of all variables and a constant
  override def linearFormula(continuousVariables : Set[AtomicFormula], state : State, t : Double = 0.0): LinearFormula = {
    (expressions collect {
      case linear : Linear => linear
    }).foldLeft(LinearFormula())((accLinearFormula, linearExp) => {
         val subLinearFormula = linearExp.linearFormula(continuousVariables, state, t)
         val mergedCoeff = accLinearFormula.coefficients ++ subLinearFormula.coefficients.map {
           case (key, value) => key -> (value + accLinearFormula.coefficients.getOrElse(key, 0.0))
         }

         LinearFormula(mergedCoeff, accLinearFormula.constant + subLinearFormula.constant)
      })
  }

  override def isConstant(timeDependentVariables : Set[AtomicFormula], state : State, t : Double): Boolean = {
    (expressions collect {
      case linear : Linear => linear
    }).forall(_.isConstant(timeDependentVariables, state, t))
  }

  /**
   * @return The constant part of the expression, computed by adding the constant parts of the sub expressions.
   */
  override def constant : Double = {
    (expressions collect {case n : Numeric => n})
         .foldLeft(0.0)((accSum, numeric) => accSum + numeric.constant)
  }

  override def toString: String = "(+ " + expressions.foldLeft("")((str, exp) => str + " " + exp) + ")"
}
