name := "scala-solver"

version := "0.2"

scalaVersion := "2.12.1"
fork := true

scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature")

libraryDependencies += "org.scala-lang.modules" % "scala-parser-combinators_2.12" % "1.0.5"
libraryDependencies += "org.apache.commons" % "commons-math3" % "3.6"

javaOptions += "-Xmx8192m"
javaOptions += "-XX:+UseG1GC"
javaOptions += "-XX:-UseGCOverheadLimit"

mainClass in (Compile, run) := Some("uk.ac.kcl.inf.planning.solver.temporal.TemporalSolverLauncher")
