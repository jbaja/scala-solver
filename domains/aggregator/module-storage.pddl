;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Definition of the Storage module.
; The namespace must follow the convention:
; <Organisation>(.<Sub-entity>)*[.<Project>](.<Feature>)*.<Module>

(define (module Kcl.Planning.APS.Storage)
  (:requirements :typing :fluents)

  ;types introduced by this module
  (:types battery)

  ;predicates (these are immutable)
  (:predicates
        (available ?b - battery)  ;whether the battery is being used
  )

  ;numeric fluents (these are immutable)
  (:functions
    (state-of-charge ?b - battery) ;the current state-of-charge of the battery
    (capacity ?b - battery)        ;the total capacity of a battery
    (charge-power ?b - battery)    ;instant power needed to charge the battery
    (discharge-power ?b - battery) ;instant power extracted from discharging the battery
    (peukert ?b - battery)       ;the Peukert Constant of the battery
  )

  ;external calls to the module
  (:methods
    ;;;; Mutator Methods - if changing state is allowed  ;;;;;;

    ;charge the battery for ?d time and apply the changes
    (start-charging ?b - battery) - battery ;to be called to start the charging
    (stop-charge ?b - battery ?duration - Number) - battery ;to be called to stop the charging

    ;discharge the battery for ?d time and apply the changes
    (start-discharging ?b - battery) - battery
    (stop-discharging ?b - battery ?duration - Number) - battery

    ;;;; Aggregate Methods ;;;;;;;;;;

    ;aggregate function that calculates some kind of utilisation ratio
    (utilisation ?b - battery) - Number
  )
)

; To clarify
; 1. Does it make sense to allow mutable predicates provided by this module?
; For example: The (available ?b) predicate, can it be modified? Or should it only be accessible through a method?
; 2. Is safety in concurrency going to be enforced by Epsilon separation?

