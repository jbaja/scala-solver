(define (problem aggregator-prob)
     (:domain aggregator)
     
     (:objects 
        wash-dishes-h1 wash-dishes-h2 wash-dishes-h3 - activity
        wash-dishes-h4 - activity
        wash-dishes-h5 - activity
        wash-dishes-h6 - activity
        wash-dishes-h7 - activity
        wash-dishes-h8 - activity
        wash-dishes-h9 - activity
        wash-dishes-h10 - activity

        wash-dishes-normal wash-dishes-fast - activity-profile
        
        battery-s1 battery-s2 - battery
        battery-s3 - battery
        battery-s4 - battery
        battery-s5 - battery
        battery-s6 - battery
        battery-s7 - battery
        battery-s8 - battery
        battery-s9 - battery
        battery-s10 - battery

        charge-fast charge-normal - charge-profile
        discharge-fast discharge-normal - discharge-profile
     )
     
     (:init
        ;Not metering initially, the start-metering has to start it
                        
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;Appliance configurations        
        ;Household 2 has a normal dishwasher with 2 modes
        (can-perform wash-dishes-h1)
        
        (valid-activity-profile wash-dishes-h1 wash-dishes-normal)
        (= (power-needed wash-dishes-h1 wash-dishes-normal) 1000)
        (= (duration-needed wash-dishes-h1 wash-dishes-normal) 240)

        (valid-activity-profile wash-dishes-h1 wash-dishes-fast)
        (= (power-needed wash-dishes-h1 wash-dishes-fast) 3000)
        (= (duration-needed wash-dishes-h1 wash-dishes-fast) 90)
        

        ;todo: deadline using can-perform
        
        ;Household 2 has a new dishwasher, efficient, fast and with 2 modes
        (can-perform wash-dishes-h2)
        
        (valid-activity-profile wash-dishes-h2 wash-dishes-normal)
        (= (power-needed wash-dishes-h2 wash-dishes-normal) 900)
        (= (duration-needed wash-dishes-h2 wash-dishes-normal) 200)

        (valid-activity-profile wash-dishes-h2 wash-dishes-fast)
        (= (power-needed wash-dishes-h2 wash-dishes-fast) 2500)
        (= (duration-needed wash-dishes-h2 wash-dishes-fast) 80)
                
        ;Household 3 has an old dishwasher, inefficient and slow, with one config
        (can-perform wash-dishes-h3)
                
        (valid-activity-profile wash-dishes-h3 wash-dishes-normal)
        (= (power-needed wash-dishes-h3 wash-dishes-normal) 1200)
        (= (duration-needed wash-dishes-h3 wash-dishes-normal) 110)               
        
        (valid-activity-profile wash-dishes-h3 wash-dishes-normal)
        (= (power-needed wash-dishes-h3 wash-dishes-fast) 2400)
        (= (duration-needed wash-dishes-h3 wash-dishes-fast) 90)


        ;Household 4 has a new dishwasher, efficient, fast and with 2 modes
        (can-perform wash-dishes-h4)

        (valid-activity-profile wash-dishes-h4 wash-dishes-normal)
        (= (power-needed wash-dishes-h4 wash-dishes-normal) 900)
        (= (duration-needed wash-dishes-h4 wash-dishes-normal) 200)

        (valid-activity-profile wash-dishes-h4 wash-dishes-fast)
        (= (power-needed wash-dishes-h4 wash-dishes-fast) 2500)
        (= (duration-needed wash-dishes-h4 wash-dishes-fast) 80)

        ;Household 4 has a new dishwasher, efficient, fast and with 2 modes
        (can-perform wash-dishes-h5)

        (valid-activity-profile wash-dishes-h5 wash-dishes-normal)
        (= (power-needed wash-dishes-h5 wash-dishes-normal) 900)
        (= (duration-needed wash-dishes-h5 wash-dishes-normal) 200)

        (valid-activity-profile wash-dishes-h5 wash-dishes-fast)
        (= (power-needed wash-dishes-h5 wash-dishes-fast) 2500)
        (= (duration-needed wash-dishes-h5 wash-dishes-fast) 80)

        (can-perform wash-dishes-h6)

        (valid-activity-profile wash-dishes-h6 wash-dishes-normal)
        (= (power-needed wash-dishes-h6 wash-dishes-normal) 900)
        (= (duration-needed wash-dishes-h6 wash-dishes-normal) 200)

        (valid-activity-profile wash-dishes-h6 wash-dishes-fast)
        (= (power-needed wash-dishes-h6 wash-dishes-fast) 2500)
        (= (duration-needed wash-dishes-h6 wash-dishes-fast) 80)

        (can-perform wash-dishes-h7)

        (valid-activity-profile wash-dishes-h7 wash-dishes-normal)
        (= (power-needed wash-dishes-h7 wash-dishes-normal) 900)
        (= (duration-needed wash-dishes-h7 wash-dishes-normal) 200)

        (valid-activity-profile wash-dishes-h7 wash-dishes-fast)
        (= (power-needed wash-dishes-h7 wash-dishes-fast) 2500)
        (= (duration-needed wash-dishes-h7 wash-dishes-fast) 80)

        (can-perform wash-dishes-h8)

        (valid-activity-profile wash-dishes-h8 wash-dishes-normal)
        (= (power-needed wash-dishes-h8 wash-dishes-normal) 900)
        (= (duration-needed wash-dishes-h8 wash-dishes-normal) 200)

        (valid-activity-profile wash-dishes-h8 wash-dishes-fast)
        (= (power-needed wash-dishes-h8 wash-dishes-fast) 2500)
        (= (duration-needed wash-dishes-h8 wash-dishes-fast) 80)

        (can-perform wash-dishes-h9)

        (valid-activity-profile wash-dishes-h9 wash-dishes-normal)
        (= (power-needed wash-dishes-h9 wash-dishes-normal) 900)
        (= (duration-needed wash-dishes-h9 wash-dishes-normal) 200)

        (valid-activity-profile wash-dishes-h9 wash-dishes-fast)
        (= (power-needed wash-dishes-h9 wash-dishes-fast) 2500)
        (= (duration-needed wash-dishes-h9 wash-dishes-fast) 80)

        (can-perform wash-dishes-h10)

        (valid-activity-profile wash-dishes-h10 wash-dishes-normal)
        (= (power-needed wash-dishes-h10 wash-dishes-normal) 900)
        (= (duration-needed wash-dishes-h10 wash-dishes-normal) 200)

        (valid-activity-profile wash-dishes-h10 wash-dishes-fast)
        (= (power-needed wash-dishes-h10 wash-dishes-fast) 2500)
        (= (duration-needed wash-dishes-h10 wash-dishes-fast) 80)

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;battery capacity
        (= (max-energy battery-s1)  100000) ;in 100,000mAh
        (= (max-energy battery-s2)   80000) ;in  80,000mAh
        (= (max-energy battery-s3)   80000) ;in  80,000mAh
        (= (max-energy battery-s4)   80000) ;in  80,000mAh
        (= (max-energy battery-s5)   80000) ;in  80,000mAh
        (= (max-energy battery-s6)   80000) ;in  80,000mAh
        (= (max-energy battery-s7)   80000) ;in  80,000mAh
        (= (max-energy battery-s8)   80000) ;in  80,000mAh
        (= (max-energy battery-s9)   80000) ;in  80,000mAh
        (= (max-energy battery-s10)   80000) ;in  80,000mAh

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;battery charging and discharging profiles (following Peukert properties)

        ;battery-s1
        (valid-charge-profile battery-s1 charge-normal)
        (= (charge-power battery-s1 charge-normal) 1000) ;1kW instant power 
        (= (charge-energy battery-s1 charge-normal) 600)   ;per minute = 10mAh per second

        (valid-charge-profile battery-s1 charge-fast)
        (= (charge-power battery-s1 charge-fast) 2500) ;2.5kW instant power 
        (= (charge-energy battery-s1 charge-fast) 1200)   ;20mAh per second
                        
        (valid-discharge-profile battery-s1 discharge-normal)
        (= (discharge-power battery-s1 discharge-normal) 800) ;0.8kW instant power 
        (= (discharge-energy battery-s1 discharge-normal) 600)   ;10mAh per second

        (valid-discharge-profile battery-s1 discharge-fast)
        (= (discharge-power battery-s1 discharge-fast) 2000) ;2KW instant power 
        (= (discharge-energy battery-s1 discharge-fast) 1200)   ;20mAh per second
                        
        ;battery-s2
        (valid-charge-profile battery-s2 charge-normal)
        (= (charge-power battery-s2 charge-normal) 1400) ;1.4kW instant power 
        (= (charge-energy battery-s2 charge-normal) 1200)   ;20mAh per second

        (valid-charge-profile battery-s2 charge-fast)
        (= (charge-power battery-s2 charge-fast) 2700) ;2.7kW instant power 
        (= (charge-energy battery-s2 charge-fast) 1800)   ;30mAh per second
        
        (valid-discharge-profile battery-s2 discharge-normal)
        (= (discharge-power battery-s2 discharge-normal) 1200) ;1.2kW instant power 
        (= (discharge-energy battery-s2 discharge-normal) 1200)   ;20mAh per second

        (valid-discharge-profile battery-s2 discharge-fast)
        (= (discharge-power battery-s2 discharge-fast) 1800) ;1.8KW instant power 
        (= (discharge-energy battery-s2 discharge-fast) 1800)   ;30mAh per second
        
        ;battery-s3
        (valid-charge-profile battery-s3 charge-normal)
        (= (charge-power battery-s3 charge-normal) 1400) ;1.4kW instant power
        (= (charge-energy battery-s3 charge-normal) 1200)   ;20mAh per second

        (valid-charge-profile battery-s3 charge-fast)
        (= (charge-power battery-s3 charge-fast) 2700) ;2.7kW instant power
        (= (charge-energy battery-s3 charge-fast) 1800)   ;30mAh per second

        (valid-discharge-profile battery-s3 discharge-normal)
        (= (discharge-power battery-s3 discharge-normal) 1200) ;1.2kW instant power
        (= (discharge-energy battery-s3 discharge-normal) 1200)   ;20mAh per second

        (valid-discharge-profile battery-s3 discharge-fast)
        (= (discharge-power battery-s3 discharge-fast) 1800) ;1.8KW instant power
        (= (discharge-energy battery-s3 discharge-fast) 1800)   ;30mAh per second

        ;battery-s4
        (valid-charge-profile battery-s4 charge-normal)
        (= (charge-power battery-s4 charge-normal) 1400) ;1.4kW instant power
        (= (charge-energy battery-s4 charge-normal) 1200)   ;20mAh per second

        (valid-charge-profile battery-s4 charge-fast)
        (= (charge-power battery-s4 charge-fast) 2700) ;2.7kW instant power
        (= (charge-energy battery-s4 charge-fast) 1800)   ;30mAh per second

        (valid-discharge-profile battery-s4 discharge-normal)
        (= (discharge-power battery-s4 discharge-normal) 1200) ;1.2kW instant power
        (= (discharge-energy battery-s4 discharge-normal) 1200)   ;20mAh per second

        (valid-discharge-profile battery-s4 discharge-fast)
        (= (discharge-power battery-s4 discharge-fast) 1800) ;1.8KW instant power
        (= (discharge-energy battery-s4 discharge-fast) 1800)   ;30mAh per second

        ;battery-s5
        (valid-charge-profile battery-s5 charge-normal)
        (= (charge-power battery-s5 charge-normal) 1400) ;1.4kW instant power
        (= (charge-energy battery-s5 charge-normal) 1200)   ;20mAh per second

        (valid-charge-profile battery-s5 charge-fast)
        (= (charge-power battery-s5 charge-fast) 2700) ;2.7kW instant power
        (= (charge-energy battery-s5 charge-fast) 1800)   ;30mAh per second

        (valid-discharge-profile battery-s5 discharge-normal)
        (= (discharge-power battery-s5 discharge-normal) 1200) ;1.2kW instant power
        (= (discharge-energy battery-s5 discharge-normal) 1200)   ;20mAh per second

        (valid-discharge-profile battery-s5 discharge-fast)
        (= (discharge-power battery-s5 discharge-fast) 1800) ;1.8KW instant power
        (= (discharge-energy battery-s5 discharge-fast) 1800)   ;30mAh per second

        ;battery-s6
        (valid-charge-profile battery-s6 charge-normal)
        (= (charge-power battery-s6 charge-normal) 1400) ;1.4kW instant power
        (= (charge-energy battery-s6 charge-normal) 1200)   ;20mAh per second

        (valid-charge-profile battery-s6 charge-fast)
        (= (charge-power battery-s6 charge-fast) 2700) ;2.7kW instant power
        (= (charge-energy battery-s6 charge-fast) 1800)   ;30mAh per second

        (valid-discharge-profile battery-s6 discharge-normal)
        (= (discharge-power battery-s6 discharge-normal) 1200) ;1.2kW instant power
        (= (discharge-energy battery-s6 discharge-normal) 1200)   ;20mAh per second

        (valid-discharge-profile battery-s6 discharge-fast)
        (= (discharge-power battery-s6 discharge-fast) 1800) ;1.8KW instant power
        (= (discharge-energy battery-s6 discharge-fast) 1800)   ;30mAh per second

        ;battery-s7
        (valid-charge-profile battery-s7 charge-normal)
        (= (charge-power battery-s7 charge-normal) 1400) ;1.4kW instant power
        (= (charge-energy battery-s7 charge-normal) 1200)   ;20mAh per second

        (valid-charge-profile battery-s7 charge-fast)
        (= (charge-power battery-s7 charge-fast) 2700) ;2.7kW instant power
        (= (charge-energy battery-s7 charge-fast) 1800)   ;30mAh per second

        (valid-discharge-profile battery-s7 discharge-normal)
        (= (discharge-power battery-s7 discharge-normal) 1200) ;1.2kW instant power
        (= (discharge-energy battery-s7 discharge-normal) 1200)   ;20mAh per second

        (valid-discharge-profile battery-s7 discharge-fast)
        (= (discharge-power battery-s7 discharge-fast) 1800) ;1.8KW instant power
        (= (discharge-energy battery-s7 discharge-fast) 1800)   ;30mAh per second

        ;battery-s8
        (valid-charge-profile battery-s8 charge-normal)
        (= (charge-power battery-s8 charge-normal) 1400) ;1.4kW instant power
        (= (charge-energy battery-s8 charge-normal) 1200)   ;20mAh per second

        (valid-charge-profile battery-s8 charge-fast)
        (= (charge-power battery-s8 charge-fast) 2700) ;2.7kW instant power
        (= (charge-energy battery-s8 charge-fast) 1800)   ;30mAh per second

        (valid-discharge-profile battery-s8 discharge-normal)
        (= (discharge-power battery-s8 discharge-normal) 1200) ;1.2kW instant power
        (= (discharge-energy battery-s8 discharge-normal) 1200)   ;20mAh per second

        (valid-discharge-profile battery-s8 discharge-fast)
        (= (discharge-power battery-s8 discharge-fast) 1800) ;1.8KW instant power
        (= (discharge-energy battery-s8 discharge-fast) 1800)   ;30mAh per second

        ;battery-s9
        (valid-charge-profile battery-s9 charge-normal)
        (= (charge-power battery-s9 charge-normal) 1400) ;1.4kW instant power
        (= (charge-energy battery-s9 charge-normal) 1200)   ;20mAh per second

        (valid-charge-profile battery-s9 charge-fast)
        (= (charge-power battery-s9 charge-fast) 2700) ;2.7kW instant power
        (= (charge-energy battery-s9 charge-fast) 1800)   ;30mAh per second

        (valid-discharge-profile battery-s9 discharge-normal)
        (= (discharge-power battery-s9 discharge-normal) 1200) ;1.2kW instant power
        (= (discharge-energy battery-s9 discharge-normal) 1200)   ;20mAh per second

        (valid-discharge-profile battery-s9 discharge-fast)
        (= (discharge-power battery-s9 discharge-fast) 1800) ;1.8KW instant power
        (= (discharge-energy battery-s9 discharge-fast) 1800)   ;30mAh per second

        ;battery-s10
        (valid-charge-profile battery-s10 charge-normal)
        (= (charge-power battery-s10 charge-normal) 1400) ;1.4kW instant power
        (= (charge-energy battery-s10 charge-normal) 1200)   ;20mAh per second

        (valid-charge-profile battery-s10 charge-fast)
        (= (charge-power battery-s10 charge-fast) 2700) ;2.7kW instant power
        (= (charge-energy battery-s10 charge-fast) 1800)   ;30mAh per second

        (valid-discharge-profile battery-s10 discharge-normal)
        (= (discharge-power battery-s10 discharge-normal) 1200) ;1.2kW instant power
        (= (discharge-energy battery-s10 discharge-normal) 1200)   ;20mAh per second

        (valid-discharge-profile battery-s10 discharge-fast)
        (= (discharge-power battery-s10 discharge-fast) 1800) ;1.8KW instant power
        (= (discharge-energy battery-s10 discharge-fast) 1800)   ;30mAh per second


        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;battery availability (always connected)
        (available battery-s1)
        (available battery-s2)
        (available battery-s3)
        (available battery-s4)
        (available battery-s5)
        (available battery-s6)
        (available battery-s7)
        (available battery-s8)
        (available battery-s9)
        (available battery-s10)

        ;battery initialisation
        (= (energy battery-s1) 0)
        (= (energy battery-s2) 0)
        (= (energy battery-s3) 0)
        (= (energy battery-s4) 0)
        (= (energy battery-s5) 0)
        (= (energy battery-s6) 0)
        (= (energy battery-s7) 0)
        (= (energy battery-s8) 0)
        (= (energy battery-s9) 0)
        (= (energy battery-s10) 0)

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        (= (energy-cost) 0)
        (= (flexible-load) 0)
        
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;fluctuation of inflexible load
        
        (= (inflexible-load) 400000)
        (at 15 (= (inflexible-load) 320000))
        (at 30 (= (inflexible-load) 300000))
        (at 45 (= (inflexible-load) 250000))
        (at 60 (= (inflexible-load) 200000))
        (at 300 (= (inflexible-load) 350000))
        (at 360 (= (inflexible-load) 450000))
        (at 420 (= (inflexible-load) 700000))  ;people waking up, massive load
        (at 540(= (inflexible-load) 650000))
        (at 720 (= (inflexible-load) 600000))
        (at 1020 (= (inflexible-load) 650000))
        (at 1080 (= (inflexible-load) 720000))
        (at 1140 (= (inflexible-load) 800000))
        (at 1200 (= (inflexible-load) 700000))
        (at 1320 (= (inflexible-load) 500000))
                
        ;marginal cost at each hour 
        ;todo: this is in reality a step function not a linear cost function
        (= (unit-price) 0.45)
        (at 250 (= (unit-price) 0.08))     ;cheapest price during the night
        (at 420 (= (unit-price) 0.20))  ;price goes up at 8am
        (at 720 (= (unit-price) 0.14))  ;price goes a bit lower at noon
        (at 1080 (= (unit-price) 0.45)) ;price peaks at 6pm till 8pm
        (at 1200 (= (unit-price) 0.35)) ;price goes down a bit at 8pm till midnight
    )
    
    (:goal (and 
              (completed wash-dishes-h1)
              (completed wash-dishes-h2)
              (completed wash-dishes-h3)
              (completed wash-dishes-h3)
              (completed wash-dishes-h4)
              (completed wash-dishes-h5)
              (completed wash-dishes-h6)
              (completed wash-dishes-h7)
              (completed wash-dishes-h8)
              (completed wash-dishes-h9)
              (completed wash-dishes-h10)
        )
    )
    
    (:metric minimize (/ (* #t (* unit-price (+ inflexible-load flexible-load))) 60000))
)
