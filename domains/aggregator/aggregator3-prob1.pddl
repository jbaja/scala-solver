(define (problem aggregator3-prob1)
(:domain aggregator)
(:objects
activity-h0 activity-h1 activity-h2 activity-h3 activity-h4 activity-h5 activity-h6 activity-h7 activity-h8 activity-h9 activity-h10 activity-h11 activity-h12 activity-h13 activity-h14 activity-h15 activity-h16 activity-h17 activity-h18 activity-h19 activity-h20 activity-h21 activity-h22 activity-h23 activity-h24 activity-h25 activity-h26 activity-h27 activity-h28 activity-h29 activity-h30 activity-h31 activity-h32 activity-h33 activity-h34 activity-h35 activity-h36 activity-h37 activity-h38 activity-h39 activity-h40 activity-h41 activity-h42 activity-h43 activity-h44 activity-h45 activity-h46 activity-h47 activity-h48 activity-h49 activity-h50 activity-h51 activity-h52 activity-h53 activity-h54 activity-h55 activity-h56 activity-h57 activity-h58 activity-h59 activity-h60 activity-h61 activity-h62 activity-h63 activity-h64 activity-h65 activity-h66 activity-h67 activity-h68 activity-h69 activity-h70 activity-h71 activity-h72 activity-h73 activity-h74 activity-h75 activity-h76 activity-h77 activity-h78 activity-h79 activity-h80 activity-h81 activity-h82 activity-h83 activity-h84 activity-h85 activity-h86 activity-h87 activity-h88 activity-h89 activity-h90 activity-h91 activity-h92 activity-h93 activity-h94 activity-h95 activity-h96 activity-h97 activity-h98 activity-h99  - activity
perform-activity-fast perform-activity-slow  - activity-profile

)

(:init
     (= (inflexible-load) 400000)
        (at 15 (= (inflexible-load) 320000))
        (at 30 (= (inflexible-load) 300000))
        (at 45 (= (inflexible-load) 250000))
        (at 60 (= (inflexible-load) 200000))
        (at 300 (= (inflexible-load) 350000))  
        (at 360 (= (inflexible-load) 450000))          
        (at 420 (= (inflexible-load) 700000))  ;people waking up, massive load
        (at 540(= (inflexible-load) 650000))  
        (at 720 (= (inflexible-load) 600000))  
        (at 1020 (= (inflexible-load) 650000)) 
        (at 1080 (= (inflexible-load) 720000)) 
        (at 1140 (= (inflexible-load) 800000))                 
        (at 1200 (= (inflexible-load) 700000))
        (at 1320 (= (inflexible-load) 500000))
                
        ;marginal cost at each hour 
        ;todo: this is in reality a step function not a linear cost function
        (= (unit-price) 0.45)
        (at 250 (= (unit-price) 0.08))     ;cheapest price during the night
        (at 420 (= (unit-price) 0.20))  ;price goes up at 8am
        (at 720 (= (unit-price) 0.14))  ;price goes a bit lower at noon
        (at 1080 (= (unit-price) 0.45)) ;price peaks at 6pm till 8pm
        (at 1200 (= (unit-price) 0.35)) ;price goes down a bit at 8pm till midnight




;activity-h0
(valid-activity-profile activity-h0 perform-activity-slow)
(= (power-needed activity-h0 perform-activity-slow) 994)
(= (duration-needed activity-h0 perform-activity-slow) 605)

(valid-activity-profile activity-h0 perform-activity-fast)
(= (power-needed activity-h0 perform-activity-fast) 941)
(= (duration-needed activity-h0 perform-activity-fast) 181)

;;;;;;;;;;;;;;;;;


;activity-h0
(at 79 (can-perform activity-h0))
(at 930 (not (can-perform activity-h0)))


;activity-h1
(valid-activity-profile activity-h1 perform-activity-slow)
(= (power-needed activity-h1 perform-activity-slow) 927)
(= (duration-needed activity-h1 perform-activity-slow) 600)

(valid-activity-profile activity-h1 perform-activity-fast)
(= (power-needed activity-h1 perform-activity-fast) 504)
(= (duration-needed activity-h1 perform-activity-fast) 227)

;;;;;;;;;;;;;;;;;


;activity-h1
(at 189 (can-perform activity-h1))
(at 1209 (not (can-perform activity-h1)))


;activity-h2
(valid-activity-profile activity-h2 perform-activity-slow)
(= (power-needed activity-h2 perform-activity-slow) 786)
(= (duration-needed activity-h2 perform-activity-slow) 737)

(valid-activity-profile activity-h2 perform-activity-fast)
(= (power-needed activity-h2 perform-activity-fast) 1004)
(= (duration-needed activity-h2 perform-activity-fast) 179)

;;;;;;;;;;;;;;;;;


;activity-h2
(at 118 (can-perform activity-h2))
(at 1232 (not (can-perform activity-h2)))


;activity-h3
(valid-activity-profile activity-h3 perform-activity-slow)
(= (power-needed activity-h3 perform-activity-slow) 323)
(= (duration-needed activity-h3 perform-activity-slow) 478)

(valid-activity-profile activity-h3 perform-activity-fast)
(= (power-needed activity-h3 perform-activity-fast) 953)
(= (duration-needed activity-h3 perform-activity-fast) 263)

;;;;;;;;;;;;;;;;;


;activity-h3
(at 262 (can-perform activity-h3))
(at 1224 (not (can-perform activity-h3)))


;activity-h4
(valid-activity-profile activity-h4 perform-activity-slow)
(= (power-needed activity-h4 perform-activity-slow) 540)
(= (duration-needed activity-h4 perform-activity-slow) 504)

(valid-activity-profile activity-h4 perform-activity-fast)
(= (power-needed activity-h4 perform-activity-fast) 1437)
(= (duration-needed activity-h4 perform-activity-fast) 43)

;;;;;;;;;;;;;;;;;


;activity-h4
(at 167 (can-perform activity-h4))
(at 698 (not (can-perform activity-h4)))


;activity-h5
(valid-activity-profile activity-h5 perform-activity-slow)
(= (power-needed activity-h5 perform-activity-slow) 1040)
(= (duration-needed activity-h5 perform-activity-slow) 746)

(valid-activity-profile activity-h5 perform-activity-fast)
(= (power-needed activity-h5 perform-activity-fast) 896)
(= (duration-needed activity-h5 perform-activity-fast) 22)

;;;;;;;;;;;;;;;;;


;activity-h5
(at 216 (can-perform activity-h5))
(at 1226 (not (can-perform activity-h5)))


;activity-h6
(valid-activity-profile activity-h6 perform-activity-slow)
(= (power-needed activity-h6 perform-activity-slow) 580)
(= (duration-needed activity-h6 perform-activity-slow) 778)

(valid-activity-profile activity-h6 perform-activity-fast)
(= (power-needed activity-h6 perform-activity-fast) 1228)
(= (duration-needed activity-h6 perform-activity-fast) 61)

;;;;;;;;;;;;;;;;;


;activity-h6
(at 53 (can-perform activity-h6))
(at 1052 (not (can-perform activity-h6)))


;activity-h7
(valid-activity-profile activity-h7 perform-activity-slow)
(= (power-needed activity-h7 perform-activity-slow) 1269)
(= (duration-needed activity-h7 perform-activity-slow) 764)

(valid-activity-profile activity-h7 perform-activity-fast)
(= (power-needed activity-h7 perform-activity-fast) 811)
(= (duration-needed activity-h7 perform-activity-fast) 7)

;;;;;;;;;;;;;;;;;


;activity-h7
(at 41 (can-perform activity-h7))
(at 1264 (not (can-perform activity-h7)))


;activity-h8
(valid-activity-profile activity-h8 perform-activity-slow)
(= (power-needed activity-h8 perform-activity-slow) 365)
(= (duration-needed activity-h8 perform-activity-slow) 700)

(valid-activity-profile activity-h8 perform-activity-fast)
(= (power-needed activity-h8 perform-activity-fast) 1349)
(= (duration-needed activity-h8 perform-activity-fast) 297)

;;;;;;;;;;;;;;;;;


;activity-h8
(at 13 (can-perform activity-h8))
(at 1070 (not (can-perform activity-h8)))


;activity-h9
(valid-activity-profile activity-h9 perform-activity-slow)
(= (power-needed activity-h9 perform-activity-slow) 1218)
(= (duration-needed activity-h9 perform-activity-slow) 735)

(valid-activity-profile activity-h9 perform-activity-fast)
(= (power-needed activity-h9 perform-activity-fast) 1128)
(= (duration-needed activity-h9 perform-activity-fast) 154)

;;;;;;;;;;;;;;;;;


;activity-h9
(at 250 (can-perform activity-h9))
(at 1010 (not (can-perform activity-h9)))


;activity-h10
(valid-activity-profile activity-h10 perform-activity-slow)
(= (power-needed activity-h10 perform-activity-slow) 375)
(= (duration-needed activity-h10 perform-activity-slow) 790)

(valid-activity-profile activity-h10 perform-activity-fast)
(= (power-needed activity-h10 perform-activity-fast) 806)
(= (duration-needed activity-h10 perform-activity-fast) 151)

;;;;;;;;;;;;;;;;;


;activity-h10
(at 366 (can-perform activity-h10))
(at 1366 (not (can-perform activity-h10)))


;activity-h11
(valid-activity-profile activity-h11 perform-activity-slow)
(= (power-needed activity-h11 perform-activity-slow) 786)
(= (duration-needed activity-h11 perform-activity-slow) 566)

(valid-activity-profile activity-h11 perform-activity-fast)
(= (power-needed activity-h11 perform-activity-fast) 1379)
(= (duration-needed activity-h11 perform-activity-fast) 256)

;;;;;;;;;;;;;;;;;


;activity-h11
(at 387 (can-perform activity-h11))
(at 1377 (not (can-perform activity-h11)))


;activity-h12
(valid-activity-profile activity-h12 perform-activity-slow)
(= (power-needed activity-h12 perform-activity-slow) 464)
(= (duration-needed activity-h12 perform-activity-slow) 579)

(valid-activity-profile activity-h12 perform-activity-fast)
(= (power-needed activity-h12 perform-activity-fast) 1021)
(= (duration-needed activity-h12 perform-activity-fast) 119)

;;;;;;;;;;;;;;;;;


;activity-h12
(at 383 (can-perform activity-h12))
(at 1445 (not (can-perform activity-h12)))


;activity-h13
(valid-activity-profile activity-h13 perform-activity-slow)
(= (power-needed activity-h13 perform-activity-slow) 479)
(= (duration-needed activity-h13 perform-activity-slow) 602)

(valid-activity-profile activity-h13 perform-activity-fast)
(= (power-needed activity-h13 perform-activity-fast) 538)
(= (duration-needed activity-h13 perform-activity-fast) 283)

;;;;;;;;;;;;;;;;;


;activity-h13
(at 243 (can-perform activity-h13))
(at 933 (not (can-perform activity-h13)))


;activity-h14
(valid-activity-profile activity-h14 perform-activity-slow)
(= (power-needed activity-h14 perform-activity-slow) 631)
(= (duration-needed activity-h14 perform-activity-slow) 555)

(valid-activity-profile activity-h14 perform-activity-fast)
(= (power-needed activity-h14 perform-activity-fast) 1154)
(= (duration-needed activity-h14 perform-activity-fast) 0)

;;;;;;;;;;;;;;;;;


;activity-h14
(at 286 (can-perform activity-h14))
(at 927 (not (can-perform activity-h14)))


;activity-h15
(valid-activity-profile activity-h15 perform-activity-slow)
(= (power-needed activity-h15 perform-activity-slow) 1279)
(= (duration-needed activity-h15 perform-activity-slow) 746)

(valid-activity-profile activity-h15 perform-activity-fast)
(= (power-needed activity-h15 perform-activity-fast) 763)
(= (duration-needed activity-h15 perform-activity-fast) 104)

;;;;;;;;;;;;;;;;;


;activity-h15
(at 25 (can-perform activity-h15))
(at 804 (not (can-perform activity-h15)))


;activity-h16
(valid-activity-profile activity-h16 perform-activity-slow)
(= (power-needed activity-h16 perform-activity-slow) 1276)
(= (duration-needed activity-h16 perform-activity-slow) 730)

(valid-activity-profile activity-h16 perform-activity-fast)
(= (power-needed activity-h16 perform-activity-fast) 1464)
(= (duration-needed activity-h16 perform-activity-fast) 239)

;;;;;;;;;;;;;;;;;


;activity-h16
(at 35 (can-perform activity-h16))
(at 1251 (not (can-perform activity-h16)))


;activity-h17
(valid-activity-profile activity-h17 perform-activity-slow)
(= (power-needed activity-h17 perform-activity-slow) 1084)
(= (duration-needed activity-h17 perform-activity-slow) 323)

(valid-activity-profile activity-h17 perform-activity-fast)
(= (power-needed activity-h17 perform-activity-fast) 1284)
(= (duration-needed activity-h17 perform-activity-fast) 37)

;;;;;;;;;;;;;;;;;


;activity-h17
(at 330 (can-perform activity-h17))
(at 787 (not (can-perform activity-h17)))


;activity-h18
(valid-activity-profile activity-h18 perform-activity-slow)
(= (power-needed activity-h18 perform-activity-slow) 1299)
(= (duration-needed activity-h18 perform-activity-slow) 625)

(valid-activity-profile activity-h18 perform-activity-fast)
(= (power-needed activity-h18 perform-activity-fast) 556)
(= (duration-needed activity-h18 perform-activity-fast) 25)

;;;;;;;;;;;;;;;;;


;activity-h18
(at 120 (can-perform activity-h18))
(at 915 (not (can-perform activity-h18)))


;activity-h19
(valid-activity-profile activity-h19 perform-activity-slow)
(= (power-needed activity-h19 perform-activity-slow) 665)
(= (duration-needed activity-h19 perform-activity-slow) 629)

(valid-activity-profile activity-h19 perform-activity-fast)
(= (power-needed activity-h19 perform-activity-fast) 584)
(= (duration-needed activity-h19 perform-activity-fast) 0)

;;;;;;;;;;;;;;;;;


;activity-h19
(at 297 (can-perform activity-h19))
(at 1022 (not (can-perform activity-h19)))


;activity-h20
(valid-activity-profile activity-h20 perform-activity-slow)
(= (power-needed activity-h20 perform-activity-slow) 1072)
(= (duration-needed activity-h20 perform-activity-slow) 336)

(valid-activity-profile activity-h20 perform-activity-fast)
(= (power-needed activity-h20 perform-activity-fast) 752)
(= (duration-needed activity-h20 perform-activity-fast) 222)

;;;;;;;;;;;;;;;;;


;activity-h20
(at 364 (can-perform activity-h20))
(at 986 (not (can-perform activity-h20)))


;activity-h21
(valid-activity-profile activity-h21 perform-activity-slow)
(= (power-needed activity-h21 perform-activity-slow) 1072)
(= (duration-needed activity-h21 perform-activity-slow) 432)

(valid-activity-profile activity-h21 perform-activity-fast)
(= (power-needed activity-h21 perform-activity-fast) 1234)
(= (duration-needed activity-h21 perform-activity-fast) 80)

;;;;;;;;;;;;;;;;;


;activity-h21
(at 180 (can-perform activity-h21))
(at 984 (not (can-perform activity-h21)))


;activity-h22
(valid-activity-profile activity-h22 perform-activity-slow)
(= (power-needed activity-h22 perform-activity-slow) 1141)
(= (duration-needed activity-h22 perform-activity-slow) 315)

(valid-activity-profile activity-h22 perform-activity-fast)
(= (power-needed activity-h22 perform-activity-fast) 512)
(= (duration-needed activity-h22 perform-activity-fast) 130)

;;;;;;;;;;;;;;;;;


;activity-h22
(at 348 (can-perform activity-h22))
(at 855 (not (can-perform activity-h22)))


;activity-h23
(valid-activity-profile activity-h23 perform-activity-slow)
(= (power-needed activity-h23 perform-activity-slow) 871)
(= (duration-needed activity-h23 perform-activity-slow) 778)

(valid-activity-profile activity-h23 perform-activity-fast)
(= (power-needed activity-h23 perform-activity-fast) 970)
(= (duration-needed activity-h23 perform-activity-fast) 86)

;;;;;;;;;;;;;;;;;


;activity-h23
(at 376 (can-perform activity-h23))
(at 1320 (not (can-perform activity-h23)))


;activity-h24
(valid-activity-profile activity-h24 perform-activity-slow)
(= (power-needed activity-h24 perform-activity-slow) 783)
(= (duration-needed activity-h24 perform-activity-slow) 762)

(valid-activity-profile activity-h24 perform-activity-fast)
(= (power-needed activity-h24 perform-activity-fast) 859)
(= (duration-needed activity-h24 perform-activity-fast) 165)

;;;;;;;;;;;;;;;;;


;activity-h24
(at 21 (can-perform activity-h24))
(at 1202 (not (can-perform activity-h24)))


;activity-h25
(valid-activity-profile activity-h25 perform-activity-slow)
(= (power-needed activity-h25 perform-activity-slow) 987)
(= (duration-needed activity-h25 perform-activity-slow) 633)

(valid-activity-profile activity-h25 perform-activity-fast)
(= (power-needed activity-h25 perform-activity-fast) 1309)
(= (duration-needed activity-h25 perform-activity-fast) 244)

;;;;;;;;;;;;;;;;;


;activity-h25
(at 171 (can-perform activity-h25))
(at 1118 (not (can-perform activity-h25)))


;activity-h26
(valid-activity-profile activity-h26 perform-activity-slow)
(= (power-needed activity-h26 perform-activity-slow) 536)
(= (duration-needed activity-h26 perform-activity-slow) 700)

(valid-activity-profile activity-h26 perform-activity-fast)
(= (power-needed activity-h26 perform-activity-fast) 982)
(= (duration-needed activity-h26 perform-activity-fast) 140)

;;;;;;;;;;;;;;;;;


;activity-h26
(at 370 (can-perform activity-h26))
(at 1186 (not (can-perform activity-h26)))


;activity-h27
(valid-activity-profile activity-h27 perform-activity-slow)
(= (power-needed activity-h27 perform-activity-slow) 709)
(= (duration-needed activity-h27 perform-activity-slow) 400)

(valid-activity-profile activity-h27 perform-activity-fast)
(= (power-needed activity-h27 perform-activity-fast) 618)
(= (duration-needed activity-h27 perform-activity-fast) 162)

;;;;;;;;;;;;;;;;;


;activity-h27
(at 106 (can-perform activity-h27))
(at 614 (not (can-perform activity-h27)))


;activity-h28
(valid-activity-profile activity-h28 perform-activity-slow)
(= (power-needed activity-h28 perform-activity-slow) 926)
(= (duration-needed activity-h28 perform-activity-slow) 662)

(valid-activity-profile activity-h28 perform-activity-fast)
(= (power-needed activity-h28 perform-activity-fast) 577)
(= (duration-needed activity-h28 perform-activity-fast) 119)

;;;;;;;;;;;;;;;;;


;activity-h28
(at 358 (can-perform activity-h28))
(at 1038 (not (can-perform activity-h28)))


;activity-h29
(valid-activity-profile activity-h29 perform-activity-slow)
(= (power-needed activity-h29 perform-activity-slow) 576)
(= (duration-needed activity-h29 perform-activity-slow) 738)

(valid-activity-profile activity-h29 perform-activity-fast)
(= (power-needed activity-h29 perform-activity-fast) 1151)
(= (duration-needed activity-h29 perform-activity-fast) 61)

;;;;;;;;;;;;;;;;;


;activity-h29
(at 247 (can-perform activity-h29))
(at 1471 (not (can-perform activity-h29)))


;activity-h30
(valid-activity-profile activity-h30 perform-activity-slow)
(= (power-needed activity-h30 perform-activity-slow) 761)
(= (duration-needed activity-h30 perform-activity-slow) 782)

(valid-activity-profile activity-h30 perform-activity-fast)
(= (power-needed activity-h30 perform-activity-fast) 652)
(= (duration-needed activity-h30 perform-activity-fast) 122)

;;;;;;;;;;;;;;;;;


;activity-h30
(at 62 (can-perform activity-h30))
(at 1069 (not (can-perform activity-h30)))


;activity-h31
(valid-activity-profile activity-h31 perform-activity-slow)
(= (power-needed activity-h31 perform-activity-slow) 996)
(= (duration-needed activity-h31 perform-activity-slow) 367)

(valid-activity-profile activity-h31 perform-activity-fast)
(= (power-needed activity-h31 perform-activity-fast) 1282)
(= (duration-needed activity-h31 perform-activity-fast) 220)

;;;;;;;;;;;;;;;;;


;activity-h31
(at 9 (can-perform activity-h31))
(at 706 (not (can-perform activity-h31)))


;activity-h32
(valid-activity-profile activity-h32 perform-activity-slow)
(= (power-needed activity-h32 perform-activity-slow) 946)
(= (duration-needed activity-h32 perform-activity-slow) 711)

(valid-activity-profile activity-h32 perform-activity-fast)
(= (power-needed activity-h32 perform-activity-fast) 1052)
(= (duration-needed activity-h32 perform-activity-fast) 196)

;;;;;;;;;;;;;;;;;


;activity-h32
(at 398 (can-perform activity-h32))
(at 1167 (not (can-perform activity-h32)))


;activity-h33
(valid-activity-profile activity-h33 perform-activity-slow)
(= (power-needed activity-h33 perform-activity-slow) 852)
(= (duration-needed activity-h33 perform-activity-slow) 344)

(valid-activity-profile activity-h33 perform-activity-fast)
(= (power-needed activity-h33 perform-activity-fast) 773)
(= (duration-needed activity-h33 perform-activity-fast) 137)

;;;;;;;;;;;;;;;;;


;activity-h33
(at 39 (can-perform activity-h33))
(at 846 (not (can-perform activity-h33)))


;activity-h34
(valid-activity-profile activity-h34 perform-activity-slow)
(= (power-needed activity-h34 perform-activity-slow) 1175)
(= (duration-needed activity-h34 perform-activity-slow) 729)

(valid-activity-profile activity-h34 perform-activity-fast)
(= (power-needed activity-h34 perform-activity-fast) 967)
(= (duration-needed activity-h34 perform-activity-fast) 275)

;;;;;;;;;;;;;;;;;


;activity-h34
(at 349 (can-perform activity-h34))
(at 1315 (not (can-perform activity-h34)))


;activity-h35
(valid-activity-profile activity-h35 perform-activity-slow)
(= (power-needed activity-h35 perform-activity-slow) 763)
(= (duration-needed activity-h35 perform-activity-slow) 674)

(valid-activity-profile activity-h35 perform-activity-fast)
(= (power-needed activity-h35 perform-activity-fast) 1130)
(= (duration-needed activity-h35 perform-activity-fast) 30)

;;;;;;;;;;;;;;;;;


;activity-h35
(at 138 (can-perform activity-h35))
(at 1095 (not (can-perform activity-h35)))


;activity-h36
(valid-activity-profile activity-h36 perform-activity-slow)
(= (power-needed activity-h36 perform-activity-slow) 499)
(= (duration-needed activity-h36 perform-activity-slow) 603)

(valid-activity-profile activity-h36 perform-activity-fast)
(= (power-needed activity-h36 perform-activity-fast) 1335)
(= (duration-needed activity-h36 perform-activity-fast) 257)

;;;;;;;;;;;;;;;;;


;activity-h36
(at 374 (can-perform activity-h36))
(at 1289 (not (can-perform activity-h36)))


;activity-h37
(valid-activity-profile activity-h37 perform-activity-slow)
(= (power-needed activity-h37 perform-activity-slow) 1165)
(= (duration-needed activity-h37 perform-activity-slow) 398)

(valid-activity-profile activity-h37 perform-activity-fast)
(= (power-needed activity-h37 perform-activity-fast) 732)
(= (duration-needed activity-h37 perform-activity-fast) 147)

;;;;;;;;;;;;;;;;;


;activity-h37
(at 311 (can-perform activity-h37))
(at 892 (not (can-perform activity-h37)))


;activity-h38
(valid-activity-profile activity-h38 perform-activity-slow)
(= (power-needed activity-h38 perform-activity-slow) 843)
(= (duration-needed activity-h38 perform-activity-slow) 383)

(valid-activity-profile activity-h38 perform-activity-fast)
(= (power-needed activity-h38 perform-activity-fast) 1243)
(= (duration-needed activity-h38 perform-activity-fast) 160)

;;;;;;;;;;;;;;;;;


;activity-h38
(at 164 (can-perform activity-h38))
(at 592 (not (can-perform activity-h38)))


;activity-h39
(valid-activity-profile activity-h39 perform-activity-slow)
(= (power-needed activity-h39 perform-activity-slow) 1065)
(= (duration-needed activity-h39 perform-activity-slow) 384)

(valid-activity-profile activity-h39 perform-activity-fast)
(= (power-needed activity-h39 perform-activity-fast) 1406)
(= (duration-needed activity-h39 perform-activity-fast) 126)

;;;;;;;;;;;;;;;;;


;activity-h39
(at 94 (can-perform activity-h39))
(at 605 (not (can-perform activity-h39)))


;activity-h40
(valid-activity-profile activity-h40 perform-activity-slow)
(= (power-needed activity-h40 perform-activity-slow) 972)
(= (duration-needed activity-h40 perform-activity-slow) 326)

(valid-activity-profile activity-h40 perform-activity-fast)
(= (power-needed activity-h40 perform-activity-fast) 872)
(= (duration-needed activity-h40 perform-activity-fast) 298)

;;;;;;;;;;;;;;;;;


;activity-h40
(at 146 (can-perform activity-h40))
(at 940 (not (can-perform activity-h40)))


;activity-h41
(valid-activity-profile activity-h41 perform-activity-slow)
(= (power-needed activity-h41 perform-activity-slow) 732)
(= (duration-needed activity-h41 perform-activity-slow) 651)

(valid-activity-profile activity-h41 perform-activity-fast)
(= (power-needed activity-h41 perform-activity-fast) 949)
(= (duration-needed activity-h41 perform-activity-fast) 181)

;;;;;;;;;;;;;;;;;


;activity-h41
(at 362 (can-perform activity-h41))
(at 1046 (not (can-perform activity-h41)))


;activity-h42
(valid-activity-profile activity-h42 perform-activity-slow)
(= (power-needed activity-h42 perform-activity-slow) 761)
(= (duration-needed activity-h42 perform-activity-slow) 780)

(valid-activity-profile activity-h42 perform-activity-fast)
(= (power-needed activity-h42 perform-activity-fast) 1311)
(= (duration-needed activity-h42 perform-activity-fast) 205)

;;;;;;;;;;;;;;;;;


;activity-h42
(at 22 (can-perform activity-h42))
(at 1128 (not (can-perform activity-h42)))


;activity-h43
(valid-activity-profile activity-h43 perform-activity-slow)
(= (power-needed activity-h43 perform-activity-slow) 673)
(= (duration-needed activity-h43 perform-activity-slow) 489)

(valid-activity-profile activity-h43 perform-activity-fast)
(= (power-needed activity-h43 perform-activity-fast) 1433)
(= (duration-needed activity-h43 perform-activity-fast) 3)

;;;;;;;;;;;;;;;;;


;activity-h43
(at 365 (can-perform activity-h43))
(at 912 (not (can-perform activity-h43)))


;activity-h44
(valid-activity-profile activity-h44 perform-activity-slow)
(= (power-needed activity-h44 perform-activity-slow) 625)
(= (duration-needed activity-h44 perform-activity-slow) 443)

(valid-activity-profile activity-h44 perform-activity-fast)
(= (power-needed activity-h44 perform-activity-fast) 919)
(= (duration-needed activity-h44 perform-activity-fast) 206)

;;;;;;;;;;;;;;;;;


;activity-h44
(at 66 (can-perform activity-h44))
(at 788 (not (can-perform activity-h44)))


;activity-h45
(valid-activity-profile activity-h45 perform-activity-slow)
(= (power-needed activity-h45 perform-activity-slow) 396)
(= (duration-needed activity-h45 perform-activity-slow) 332)

(valid-activity-profile activity-h45 perform-activity-fast)
(= (power-needed activity-h45 perform-activity-fast) 610)
(= (duration-needed activity-h45 perform-activity-fast) 252)

;;;;;;;;;;;;;;;;;


;activity-h45
(at 304 (can-perform activity-h45))
(at 868 (not (can-perform activity-h45)))


;activity-h46
(valid-activity-profile activity-h46 perform-activity-slow)
(= (power-needed activity-h46 perform-activity-slow) 654)
(= (duration-needed activity-h46 perform-activity-slow) 398)

(valid-activity-profile activity-h46 perform-activity-fast)
(= (power-needed activity-h46 perform-activity-fast) 755)
(= (duration-needed activity-h46 perform-activity-fast) 23)

;;;;;;;;;;;;;;;;;


;activity-h46
(at 158 (can-perform activity-h46))
(at 866 (not (can-perform activity-h46)))


;activity-h47
(valid-activity-profile activity-h47 perform-activity-slow)
(= (power-needed activity-h47 perform-activity-slow) 517)
(= (duration-needed activity-h47 perform-activity-slow) 493)

(valid-activity-profile activity-h47 perform-activity-fast)
(= (power-needed activity-h47 perform-activity-fast) 1366)
(= (duration-needed activity-h47 perform-activity-fast) 162)

;;;;;;;;;;;;;;;;;


;activity-h47
(at 380 (can-perform activity-h47))
(at 1036 (not (can-perform activity-h47)))


;activity-h48
(valid-activity-profile activity-h48 perform-activity-slow)
(= (power-needed activity-h48 perform-activity-slow) 1112)
(= (duration-needed activity-h48 perform-activity-slow) 753)

(valid-activity-profile activity-h48 perform-activity-fast)
(= (power-needed activity-h48 perform-activity-fast) 862)
(= (duration-needed activity-h48 perform-activity-fast) 119)

;;;;;;;;;;;;;;;;;


;activity-h48
(at 27 (can-perform activity-h48))
(at 819 (not (can-perform activity-h48)))


;activity-h49
(valid-activity-profile activity-h49 perform-activity-slow)
(= (power-needed activity-h49 perform-activity-slow) 369)
(= (duration-needed activity-h49 perform-activity-slow) 665)

(valid-activity-profile activity-h49 perform-activity-fast)
(= (power-needed activity-h49 perform-activity-fast) 1255)
(= (duration-needed activity-h49 perform-activity-fast) 79)

;;;;;;;;;;;;;;;;;


;activity-h49
(at 32 (can-perform activity-h49))
(at 831 (not (can-perform activity-h49)))


;activity-h50
(valid-activity-profile activity-h50 perform-activity-slow)
(= (power-needed activity-h50 perform-activity-slow) 665)
(= (duration-needed activity-h50 perform-activity-slow) 608)

(valid-activity-profile activity-h50 perform-activity-fast)
(= (power-needed activity-h50 perform-activity-fast) 521)
(= (duration-needed activity-h50 perform-activity-fast) 286)

;;;;;;;;;;;;;;;;;


;activity-h50
(at 127 (can-perform activity-h50))
(at 1041 (not (can-perform activity-h50)))


;activity-h51
(valid-activity-profile activity-h51 perform-activity-slow)
(= (power-needed activity-h51 perform-activity-slow) 300)
(= (duration-needed activity-h51 perform-activity-slow) 707)

(valid-activity-profile activity-h51 perform-activity-fast)
(= (power-needed activity-h51 perform-activity-fast) 1442)
(= (duration-needed activity-h51 perform-activity-fast) 289)

;;;;;;;;;;;;;;;;;


;activity-h51
(at 23 (can-perform activity-h51))
(at 1073 (not (can-perform activity-h51)))


;activity-h52
(valid-activity-profile activity-h52 perform-activity-slow)
(= (power-needed activity-h52 perform-activity-slow) 1018)
(= (duration-needed activity-h52 perform-activity-slow) 781)

(valid-activity-profile activity-h52 perform-activity-fast)
(= (power-needed activity-h52 perform-activity-fast) 1191)
(= (duration-needed activity-h52 perform-activity-fast) 171)

;;;;;;;;;;;;;;;;;


;activity-h52
(at 258 (can-perform activity-h52))
(at 1465 (not (can-perform activity-h52)))


;activity-h53
(valid-activity-profile activity-h53 perform-activity-slow)
(= (power-needed activity-h53 perform-activity-slow) 1043)
(= (duration-needed activity-h53 perform-activity-slow) 564)

(valid-activity-profile activity-h53 perform-activity-fast)
(= (power-needed activity-h53 perform-activity-fast) 1323)
(= (duration-needed activity-h53 perform-activity-fast) 14)

;;;;;;;;;;;;;;;;;


;activity-h53
(at 317 (can-perform activity-h53))
(at 1203 (not (can-perform activity-h53)))


;activity-h54
(valid-activity-profile activity-h54 perform-activity-slow)
(= (power-needed activity-h54 perform-activity-slow) 530)
(= (duration-needed activity-h54 perform-activity-slow) 686)

(valid-activity-profile activity-h54 perform-activity-fast)
(= (power-needed activity-h54 perform-activity-fast) 824)
(= (duration-needed activity-h54 perform-activity-fast) 131)

;;;;;;;;;;;;;;;;;


;activity-h54
(at 138 (can-perform activity-h54))
(at 1074 (not (can-perform activity-h54)))


;activity-h55
(valid-activity-profile activity-h55 perform-activity-slow)
(= (power-needed activity-h55 perform-activity-slow) 917)
(= (duration-needed activity-h55 perform-activity-slow) 710)

(valid-activity-profile activity-h55 perform-activity-fast)
(= (power-needed activity-h55 perform-activity-fast) 916)
(= (duration-needed activity-h55 perform-activity-fast) 41)

;;;;;;;;;;;;;;;;;


;activity-h55
(at 278 (can-perform activity-h55))
(at 1164 (not (can-perform activity-h55)))


;activity-h56
(valid-activity-profile activity-h56 perform-activity-slow)
(= (power-needed activity-h56 perform-activity-slow) 584)
(= (duration-needed activity-h56 perform-activity-slow) 416)

(valid-activity-profile activity-h56 perform-activity-fast)
(= (power-needed activity-h56 perform-activity-fast) 1386)
(= (duration-needed activity-h56 perform-activity-fast) 295)

;;;;;;;;;;;;;;;;;


;activity-h56
(at 32 (can-perform activity-h56))
(at 873 (not (can-perform activity-h56)))


;activity-h57
(valid-activity-profile activity-h57 perform-activity-slow)
(= (power-needed activity-h57 perform-activity-slow) 817)
(= (duration-needed activity-h57 perform-activity-slow) 719)

(valid-activity-profile activity-h57 perform-activity-fast)
(= (power-needed activity-h57 perform-activity-fast) 981)
(= (duration-needed activity-h57 perform-activity-fast) 158)

;;;;;;;;;;;;;;;;;


;activity-h57
(at 211 (can-perform activity-h57))
(at 1394 (not (can-perform activity-h57)))


;activity-h58
(valid-activity-profile activity-h58 perform-activity-slow)
(= (power-needed activity-h58 perform-activity-slow) 558)
(= (duration-needed activity-h58 perform-activity-slow) 422)

(valid-activity-profile activity-h58 perform-activity-fast)
(= (power-needed activity-h58 perform-activity-fast) 1453)
(= (duration-needed activity-h58 perform-activity-fast) 157)

;;;;;;;;;;;;;;;;;


;activity-h58
(at 167 (can-perform activity-h58))
(at 870 (not (can-perform activity-h58)))


;activity-h59
(valid-activity-profile activity-h59 perform-activity-slow)
(= (power-needed activity-h59 perform-activity-slow) 402)
(= (duration-needed activity-h59 perform-activity-slow) 709)

(valid-activity-profile activity-h59 perform-activity-fast)
(= (power-needed activity-h59 perform-activity-fast) 1014)
(= (duration-needed activity-h59 perform-activity-fast) 114)

;;;;;;;;;;;;;;;;;


;activity-h59
(at 26 (can-perform activity-h59))
(at 1173 (not (can-perform activity-h59)))


;activity-h60
(valid-activity-profile activity-h60 perform-activity-slow)
(= (power-needed activity-h60 perform-activity-slow) 950)
(= (duration-needed activity-h60 perform-activity-slow) 450)

(valid-activity-profile activity-h60 perform-activity-fast)
(= (power-needed activity-h60 perform-activity-fast) 868)
(= (duration-needed activity-h60 perform-activity-fast) 77)

;;;;;;;;;;;;;;;;;


;activity-h60
(at 189 (can-perform activity-h60))
(at 944 (not (can-perform activity-h60)))


;activity-h61
(valid-activity-profile activity-h61 perform-activity-slow)
(= (power-needed activity-h61 perform-activity-slow) 543)
(= (duration-needed activity-h61 perform-activity-slow) 713)

(valid-activity-profile activity-h61 perform-activity-fast)
(= (power-needed activity-h61 perform-activity-fast) 791)
(= (duration-needed activity-h61 perform-activity-fast) 98)

;;;;;;;;;;;;;;;;;


;activity-h61
(at 137 (can-perform activity-h61))
(at 1002 (not (can-perform activity-h61)))


;activity-h62
(valid-activity-profile activity-h62 perform-activity-slow)
(= (power-needed activity-h62 perform-activity-slow) 1066)
(= (duration-needed activity-h62 perform-activity-slow) 607)

(valid-activity-profile activity-h62 perform-activity-fast)
(= (power-needed activity-h62 perform-activity-fast) 1025)
(= (duration-needed activity-h62 perform-activity-fast) 183)

;;;;;;;;;;;;;;;;;


;activity-h62
(at 57 (can-perform activity-h62))
(at 815 (not (can-perform activity-h62)))


;activity-h63
(valid-activity-profile activity-h63 perform-activity-slow)
(= (power-needed activity-h63 perform-activity-slow) 447)
(= (duration-needed activity-h63 perform-activity-slow) 569)

(valid-activity-profile activity-h63 perform-activity-fast)
(= (power-needed activity-h63 perform-activity-fast) 1010)
(= (duration-needed activity-h63 perform-activity-fast) 58)

;;;;;;;;;;;;;;;;;


;activity-h63
(at 208 (can-perform activity-h63))
(at 831 (not (can-perform activity-h63)))


;activity-h64
(valid-activity-profile activity-h64 perform-activity-slow)
(= (power-needed activity-h64 perform-activity-slow) 619)
(= (duration-needed activity-h64 perform-activity-slow) 643)

(valid-activity-profile activity-h64 perform-activity-fast)
(= (power-needed activity-h64 perform-activity-fast) 609)
(= (duration-needed activity-h64 perform-activity-fast) 245)

;;;;;;;;;;;;;;;;;


;activity-h64
(at 360 (can-perform activity-h64))
(at 1108 (not (can-perform activity-h64)))


;activity-h65
(valid-activity-profile activity-h65 perform-activity-slow)
(= (power-needed activity-h65 perform-activity-slow) 457)
(= (duration-needed activity-h65 perform-activity-slow) 396)

(valid-activity-profile activity-h65 perform-activity-fast)
(= (power-needed activity-h65 perform-activity-fast) 1047)
(= (duration-needed activity-h65 perform-activity-fast) 82)

;;;;;;;;;;;;;;;;;


;activity-h65
(at 144 (can-perform activity-h65))
(at 776 (not (can-perform activity-h65)))


;activity-h66
(valid-activity-profile activity-h66 perform-activity-slow)
(= (power-needed activity-h66 perform-activity-slow) 468)
(= (duration-needed activity-h66 perform-activity-slow) 704)

(valid-activity-profile activity-h66 perform-activity-fast)
(= (power-needed activity-h66 perform-activity-fast) 1495)
(= (duration-needed activity-h66 perform-activity-fast) 279)

;;;;;;;;;;;;;;;;;


;activity-h66
(at 381 (can-perform activity-h66))
(at 1346 (not (can-perform activity-h66)))


;activity-h67
(valid-activity-profile activity-h67 perform-activity-slow)
(= (power-needed activity-h67 perform-activity-slow) 1169)
(= (duration-needed activity-h67 perform-activity-slow) 736)

(valid-activity-profile activity-h67 perform-activity-fast)
(= (power-needed activity-h67 perform-activity-fast) 1077)
(= (duration-needed activity-h67 perform-activity-fast) 153)

;;;;;;;;;;;;;;;;;


;activity-h67
(at 123 (can-perform activity-h67))
(at 1130 (not (can-perform activity-h67)))


;activity-h68
(valid-activity-profile activity-h68 perform-activity-slow)
(= (power-needed activity-h68 perform-activity-slow) 784)
(= (duration-needed activity-h68 perform-activity-slow) 606)

(valid-activity-profile activity-h68 perform-activity-fast)
(= (power-needed activity-h68 perform-activity-fast) 1196)
(= (duration-needed activity-h68 perform-activity-fast) 246)

;;;;;;;;;;;;;;;;;


;activity-h68
(at 348 (can-perform activity-h68))
(at 1410 (not (can-perform activity-h68)))


;activity-h69
(valid-activity-profile activity-h69 perform-activity-slow)
(= (power-needed activity-h69 perform-activity-slow) 1126)
(= (duration-needed activity-h69 perform-activity-slow) 474)

(valid-activity-profile activity-h69 perform-activity-fast)
(= (power-needed activity-h69 perform-activity-fast) 682)
(= (duration-needed activity-h69 perform-activity-fast) 274)

;;;;;;;;;;;;;;;;;


;activity-h69
(at 369 (can-perform activity-h69))
(at 871 (not (can-perform activity-h69)))


;activity-h70
(valid-activity-profile activity-h70 perform-activity-slow)
(= (power-needed activity-h70 perform-activity-slow) 1110)
(= (duration-needed activity-h70 perform-activity-slow) 676)

(valid-activity-profile activity-h70 perform-activity-fast)
(= (power-needed activity-h70 perform-activity-fast) 611)
(= (duration-needed activity-h70 perform-activity-fast) 243)

;;;;;;;;;;;;;;;;;


;activity-h70
(at 170 (can-perform activity-h70))
(at 1311 (not (can-perform activity-h70)))


;activity-h71
(valid-activity-profile activity-h71 perform-activity-slow)
(= (power-needed activity-h71 perform-activity-slow) 1152)
(= (duration-needed activity-h71 perform-activity-slow) 515)

(valid-activity-profile activity-h71 perform-activity-fast)
(= (power-needed activity-h71 perform-activity-fast) 760)
(= (duration-needed activity-h71 perform-activity-fast) 261)

;;;;;;;;;;;;;;;;;


;activity-h71
(at 172 (can-perform activity-h71))
(at 938 (not (can-perform activity-h71)))


;activity-h72
(valid-activity-profile activity-h72 perform-activity-slow)
(= (power-needed activity-h72 perform-activity-slow) 581)
(= (duration-needed activity-h72 perform-activity-slow) 773)

(valid-activity-profile activity-h72 perform-activity-fast)
(= (power-needed activity-h72 perform-activity-fast) 714)
(= (duration-needed activity-h72 perform-activity-fast) 185)

;;;;;;;;;;;;;;;;;


;activity-h72
(at 379 (can-perform activity-h72))
(at 1555 (not (can-perform activity-h72)))


;activity-h73
(valid-activity-profile activity-h73 perform-activity-slow)
(= (power-needed activity-h73 perform-activity-slow) 1214)
(= (duration-needed activity-h73 perform-activity-slow) 371)

(valid-activity-profile activity-h73 perform-activity-fast)
(= (power-needed activity-h73 perform-activity-fast) 1056)
(= (duration-needed activity-h73 perform-activity-fast) 294)

;;;;;;;;;;;;;;;;;


;activity-h73
(at 97 (can-perform activity-h73))
(at 635 (not (can-perform activity-h73)))


;activity-h74
(valid-activity-profile activity-h74 perform-activity-slow)
(= (power-needed activity-h74 perform-activity-slow) 1115)
(= (duration-needed activity-h74 perform-activity-slow) 605)

(valid-activity-profile activity-h74 perform-activity-fast)
(= (power-needed activity-h74 perform-activity-fast) 652)
(= (duration-needed activity-h74 perform-activity-fast) 286)

;;;;;;;;;;;;;;;;;


;activity-h74
(at 336 (can-perform activity-h74))
(at 1069 (not (can-perform activity-h74)))


;activity-h75
(valid-activity-profile activity-h75 perform-activity-slow)
(= (power-needed activity-h75 perform-activity-slow) 565)
(= (duration-needed activity-h75 perform-activity-slow) 656)

(valid-activity-profile activity-h75 perform-activity-fast)
(= (power-needed activity-h75 perform-activity-fast) 1163)
(= (duration-needed activity-h75 perform-activity-fast) 1)

;;;;;;;;;;;;;;;;;


;activity-h75
(at 140 (can-perform activity-h75))
(at 865 (not (can-perform activity-h75)))


;activity-h76
(valid-activity-profile activity-h76 perform-activity-slow)
(= (power-needed activity-h76 perform-activity-slow) 536)
(= (duration-needed activity-h76 perform-activity-slow) 742)

(valid-activity-profile activity-h76 perform-activity-fast)
(= (power-needed activity-h76 perform-activity-fast) 1108)
(= (duration-needed activity-h76 perform-activity-fast) 119)

;;;;;;;;;;;;;;;;;


;activity-h76
(at 148 (can-perform activity-h76))
(at 1220 (not (can-perform activity-h76)))


;activity-h77
(valid-activity-profile activity-h77 perform-activity-slow)
(= (power-needed activity-h77 perform-activity-slow) 845)
(= (duration-needed activity-h77 perform-activity-slow) 380)

(valid-activity-profile activity-h77 perform-activity-fast)
(= (power-needed activity-h77 perform-activity-fast) 1387)
(= (duration-needed activity-h77 perform-activity-fast) 238)

;;;;;;;;;;;;;;;;;


;activity-h77
(at 31 (can-perform activity-h77))
(at 676 (not (can-perform activity-h77)))


;activity-h78
(valid-activity-profile activity-h78 perform-activity-slow)
(= (power-needed activity-h78 perform-activity-slow) 776)
(= (duration-needed activity-h78 perform-activity-slow) 317)

(valid-activity-profile activity-h78 perform-activity-fast)
(= (power-needed activity-h78 perform-activity-fast) 1203)
(= (duration-needed activity-h78 perform-activity-fast) 262)

;;;;;;;;;;;;;;;;;


;activity-h78
(at 12 (can-perform activity-h78))
(at 648 (not (can-perform activity-h78)))


;activity-h79
(valid-activity-profile activity-h79 perform-activity-slow)
(= (power-needed activity-h79 perform-activity-slow) 1261)
(= (duration-needed activity-h79 perform-activity-slow) 711)

(valid-activity-profile activity-h79 perform-activity-fast)
(= (power-needed activity-h79 perform-activity-fast) 1185)
(= (duration-needed activity-h79 perform-activity-fast) 84)

;;;;;;;;;;;;;;;;;


;activity-h79
(at 143 (can-perform activity-h79))
(at 963 (not (can-perform activity-h79)))


;activity-h80
(valid-activity-profile activity-h80 perform-activity-slow)
(= (power-needed activity-h80 perform-activity-slow) 462)
(= (duration-needed activity-h80 perform-activity-slow) 594)

(valid-activity-profile activity-h80 perform-activity-fast)
(= (power-needed activity-h80 perform-activity-fast) 1346)
(= (duration-needed activity-h80 perform-activity-fast) 70)

;;;;;;;;;;;;;;;;;


;activity-h80
(at 352 (can-perform activity-h80))
(at 1202 (not (can-perform activity-h80)))


;activity-h81
(valid-activity-profile activity-h81 perform-activity-slow)
(= (power-needed activity-h81 perform-activity-slow) 310)
(= (duration-needed activity-h81 perform-activity-slow) 723)

(valid-activity-profile activity-h81 perform-activity-fast)
(= (power-needed activity-h81 perform-activity-fast) 1008)
(= (duration-needed activity-h81 perform-activity-fast) 282)

;;;;;;;;;;;;;;;;;


;activity-h81
(at 19 (can-perform activity-h81))
(at 774 (not (can-perform activity-h81)))


;activity-h82
(valid-activity-profile activity-h82 perform-activity-slow)
(= (power-needed activity-h82 perform-activity-slow) 1093)
(= (duration-needed activity-h82 perform-activity-slow) 679)

(valid-activity-profile activity-h82 perform-activity-fast)
(= (power-needed activity-h82 perform-activity-fast) 1123)
(= (duration-needed activity-h82 perform-activity-fast) 127)

;;;;;;;;;;;;;;;;;


;activity-h82
(at 385 (can-perform activity-h82))
(at 1079 (not (can-perform activity-h82)))


;activity-h83
(valid-activity-profile activity-h83 perform-activity-slow)
(= (power-needed activity-h83 perform-activity-slow) 693)
(= (duration-needed activity-h83 perform-activity-slow) 703)

(valid-activity-profile activity-h83 perform-activity-fast)
(= (power-needed activity-h83 perform-activity-fast) 1231)
(= (duration-needed activity-h83 perform-activity-fast) 50)

;;;;;;;;;;;;;;;;;


;activity-h83
(at 210 (can-perform activity-h83))
(at 1091 (not (can-perform activity-h83)))


;activity-h84
(valid-activity-profile activity-h84 perform-activity-slow)
(= (power-needed activity-h84 perform-activity-slow) 1297)
(= (duration-needed activity-h84 perform-activity-slow) 795)

(valid-activity-profile activity-h84 perform-activity-fast)
(= (power-needed activity-h84 perform-activity-fast) 1433)
(= (duration-needed activity-h84 perform-activity-fast) 195)

;;;;;;;;;;;;;;;;;


;activity-h84
(at 303 (can-perform activity-h84))
(at 1308 (not (can-perform activity-h84)))


;activity-h85
(valid-activity-profile activity-h85 perform-activity-slow)
(= (power-needed activity-h85 perform-activity-slow) 1105)
(= (duration-needed activity-h85 perform-activity-slow) 721)

(valid-activity-profile activity-h85 perform-activity-fast)
(= (power-needed activity-h85 perform-activity-fast) 629)
(= (duration-needed activity-h85 perform-activity-fast) 34)

;;;;;;;;;;;;;;;;;


;activity-h85
(at 144 (can-perform activity-h85))
(at 935 (not (can-perform activity-h85)))


;activity-h86
(valid-activity-profile activity-h86 perform-activity-slow)
(= (power-needed activity-h86 perform-activity-slow) 906)
(= (duration-needed activity-h86 perform-activity-slow) 632)

(valid-activity-profile activity-h86 perform-activity-fast)
(= (power-needed activity-h86 perform-activity-fast) 600)
(= (duration-needed activity-h86 perform-activity-fast) 17)

;;;;;;;;;;;;;;;;;


;activity-h86
(at 366 (can-perform activity-h86))
(at 1039 (not (can-perform activity-h86)))


;activity-h87
(valid-activity-profile activity-h87 perform-activity-slow)
(= (power-needed activity-h87 perform-activity-slow) 1189)
(= (duration-needed activity-h87 perform-activity-slow) 640)

(valid-activity-profile activity-h87 perform-activity-fast)
(= (power-needed activity-h87 perform-activity-fast) 982)
(= (duration-needed activity-h87 perform-activity-fast) 156)

;;;;;;;;;;;;;;;;;


;activity-h87
(at 188 (can-perform activity-h87))
(at 1226 (not (can-perform activity-h87)))


;activity-h88
(valid-activity-profile activity-h88 perform-activity-slow)
(= (power-needed activity-h88 perform-activity-slow) 369)
(= (duration-needed activity-h88 perform-activity-slow) 560)

(valid-activity-profile activity-h88 perform-activity-fast)
(= (power-needed activity-h88 perform-activity-fast) 1310)
(= (duration-needed activity-h88 perform-activity-fast) 20)

;;;;;;;;;;;;;;;;;


;activity-h88
(at 271 (can-perform activity-h88))
(at 1036 (not (can-perform activity-h88)))


;activity-h89
(valid-activity-profile activity-h89 perform-activity-slow)
(= (power-needed activity-h89 perform-activity-slow) 351)
(= (duration-needed activity-h89 perform-activity-slow) 518)

(valid-activity-profile activity-h89 perform-activity-fast)
(= (power-needed activity-h89 perform-activity-fast) 738)
(= (duration-needed activity-h89 perform-activity-fast) 13)

;;;;;;;;;;;;;;;;;


;activity-h89
(at 321 (can-perform activity-h89))
(at 890 (not (can-perform activity-h89)))


;activity-h90
(valid-activity-profile activity-h90 perform-activity-slow)
(= (power-needed activity-h90 perform-activity-slow) 1171)
(= (duration-needed activity-h90 perform-activity-slow) 575)

(valid-activity-profile activity-h90 perform-activity-fast)
(= (power-needed activity-h90 perform-activity-fast) 1224)
(= (duration-needed activity-h90 perform-activity-fast) 44)

;;;;;;;;;;;;;;;;;


;activity-h90
(at 366 (can-perform activity-h90))
(at 1332 (not (can-perform activity-h90)))


;activity-h91
(valid-activity-profile activity-h91 perform-activity-slow)
(= (power-needed activity-h91 perform-activity-slow) 678)
(= (duration-needed activity-h91 perform-activity-slow) 309)

(valid-activity-profile activity-h91 perform-activity-fast)
(= (power-needed activity-h91 perform-activity-fast) 1181)
(= (duration-needed activity-h91 perform-activity-fast) 255)

;;;;;;;;;;;;;;;;;


;activity-h91
(at 336 (can-perform activity-h91))
(at 1038 (not (can-perform activity-h91)))


;activity-h92
(valid-activity-profile activity-h92 perform-activity-slow)
(= (power-needed activity-h92 perform-activity-slow) 563)
(= (duration-needed activity-h92 perform-activity-slow) 439)

(valid-activity-profile activity-h92 perform-activity-fast)
(= (power-needed activity-h92 perform-activity-fast) 994)
(= (duration-needed activity-h92 perform-activity-fast) 177)

;;;;;;;;;;;;;;;;;


;activity-h92
(at 148 (can-perform activity-h92))
(at 875 (not (can-perform activity-h92)))


;activity-h93
(valid-activity-profile activity-h93 perform-activity-slow)
(= (power-needed activity-h93 perform-activity-slow) 1090)
(= (duration-needed activity-h93 perform-activity-slow) 421)

(valid-activity-profile activity-h93 perform-activity-fast)
(= (power-needed activity-h93 perform-activity-fast) 705)
(= (duration-needed activity-h93 perform-activity-fast) 169)

;;;;;;;;;;;;;;;;;


;activity-h93
(at 30 (can-perform activity-h93))
(at 844 (not (can-perform activity-h93)))


;activity-h94
(valid-activity-profile activity-h94 perform-activity-slow)
(= (power-needed activity-h94 perform-activity-slow) 581)
(= (duration-needed activity-h94 perform-activity-slow) 327)

(valid-activity-profile activity-h94 perform-activity-fast)
(= (power-needed activity-h94 perform-activity-fast) 606)
(= (duration-needed activity-h94 perform-activity-fast) 205)

;;;;;;;;;;;;;;;;;


;activity-h94
(at 241 (can-perform activity-h94))
(at 822 (not (can-perform activity-h94)))


;activity-h95
(valid-activity-profile activity-h95 perform-activity-slow)
(= (power-needed activity-h95 perform-activity-slow) 812)
(= (duration-needed activity-h95 perform-activity-slow) 578)

(valid-activity-profile activity-h95 perform-activity-fast)
(= (power-needed activity-h95 perform-activity-fast) 640)
(= (duration-needed activity-h95 perform-activity-fast) 56)

;;;;;;;;;;;;;;;;;


;activity-h95
(at 375 (can-perform activity-h95))
(at 1091 (not (can-perform activity-h95)))


;activity-h96
(valid-activity-profile activity-h96 perform-activity-slow)
(= (power-needed activity-h96 perform-activity-slow) 1238)
(= (duration-needed activity-h96 perform-activity-slow) 324)

(valid-activity-profile activity-h96 perform-activity-fast)
(= (power-needed activity-h96 perform-activity-fast) 925)
(= (duration-needed activity-h96 perform-activity-fast) 57)

;;;;;;;;;;;;;;;;;


;activity-h96
(at 255 (can-perform activity-h96))
(at 903 (not (can-perform activity-h96)))


;activity-h97
(valid-activity-profile activity-h97 perform-activity-slow)
(= (power-needed activity-h97 perform-activity-slow) 982)
(= (duration-needed activity-h97 perform-activity-slow) 580)

(valid-activity-profile activity-h97 perform-activity-fast)
(= (power-needed activity-h97 perform-activity-fast) 598)
(= (duration-needed activity-h97 perform-activity-fast) 228)

;;;;;;;;;;;;;;;;;


;activity-h97
(at 71 (can-perform activity-h97))
(at 738 (not (can-perform activity-h97)))


;activity-h98
(valid-activity-profile activity-h98 perform-activity-slow)
(= (power-needed activity-h98 perform-activity-slow) 384)
(= (duration-needed activity-h98 perform-activity-slow) 754)

(valid-activity-profile activity-h98 perform-activity-fast)
(= (power-needed activity-h98 perform-activity-fast) 1458)
(= (duration-needed activity-h98 perform-activity-fast) 132)

;;;;;;;;;;;;;;;;;


;activity-h98
(at 39 (can-perform activity-h98))
(at 1264 (not (can-perform activity-h98)))


;activity-h99
(valid-activity-profile activity-h99 perform-activity-slow)
(= (power-needed activity-h99 perform-activity-slow) 581)
(= (duration-needed activity-h99 perform-activity-slow) 774)

(valid-activity-profile activity-h99 perform-activity-fast)
(= (power-needed activity-h99 perform-activity-fast) 820)
(= (duration-needed activity-h99 perform-activity-fast) 287)

;;;;;;;;;;;;;;;;;


;activity-h99
(at 104 (can-perform activity-h99))
(at 1060 (not (can-perform activity-h99)))


)

(:goal (and 
(completed activity-h0)

(completed activity-h1)

(completed activity-h2)

(completed activity-h3)

(completed activity-h4)

(completed activity-h5)

(completed activity-h6)

(completed activity-h7)

(completed activity-h8)

(completed activity-h9)

(completed activity-h10)

(completed activity-h11)

(completed activity-h12)

(completed activity-h13)

(completed activity-h14)

(completed activity-h15)

(completed activity-h16)

(completed activity-h17)

(completed activity-h18)

(completed activity-h19)

(completed activity-h20)

(completed activity-h21)

(completed activity-h22)

(completed activity-h23)

(completed activity-h24)

(completed activity-h25)

(completed activity-h26)

(completed activity-h27)

(completed activity-h28)

(completed activity-h29)

(completed activity-h30)

(completed activity-h31)

(completed activity-h32)

(completed activity-h33)

(completed activity-h34)

(completed activity-h35)

(completed activity-h36)

(completed activity-h37)

(completed activity-h38)

(completed activity-h39)

(completed activity-h40)

(completed activity-h41)

(completed activity-h42)

(completed activity-h43)

(completed activity-h44)

(completed activity-h45)

(completed activity-h46)

(completed activity-h47)

(completed activity-h48)

(completed activity-h49)

(completed activity-h50)

(completed activity-h51)

(completed activity-h52)

(completed activity-h53)

(completed activity-h54)

(completed activity-h55)

(completed activity-h56)

(completed activity-h57)

(completed activity-h58)

(completed activity-h59)

(completed activity-h60)

(completed activity-h61)

(completed activity-h62)

(completed activity-h63)

(completed activity-h64)

(completed activity-h65)

(completed activity-h66)

(completed activity-h67)

(completed activity-h68)

(completed activity-h69)

(completed activity-h70)

(completed activity-h71)

(completed activity-h72)

(completed activity-h73)

(completed activity-h74)

(completed activity-h75)

(completed activity-h76)

(completed activity-h77)

(completed activity-h78)

(completed activity-h79)

(completed activity-h80)

(completed activity-h81)

(completed activity-h82)

(completed activity-h83)

(completed activity-h84)

(completed activity-h85)

(completed activity-h86)

(completed activity-h87)

(completed activity-h88)

(completed activity-h89)

(completed activity-h90)

(completed activity-h91)

(completed activity-h92)

(completed activity-h93)

(completed activity-h94)

(completed activity-h95)

(completed activity-h96)

(completed activity-h97)

(completed activity-h98)

(completed activity-h99)
))

    (:metric minimize (/ (* #t (* unit-price (+ inflexible-load flexible-load))) 60000))
)
