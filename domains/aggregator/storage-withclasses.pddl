(define (domain storage-withclasses)

    (:requirements :typing :durative-actions :fluents :duration-inequalities :modules)

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;Import classes supported with external modules attached to the planner

    (:classes
                Storage - KCL.Planning.APS.Storage)

    (:functions
                (current-power)
    )

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;By importing the PDDL class Kcl.Planning.APS.Storage, and giving it an alias Storage
    ;the class can embelish the domain with Types, Predicates, Numeric Fluents, together with
    ;Methods, and Aggregate methods.
    ;Predicates and Numeric Fluents imported from the Module are immutable.
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    (:durative-action charge
      :parameters (?b - Storage.battery)
      :duration (> ?duration 0)
      :condition (and
                      (at start (Storage.available ?b))
                      (at start (< (Storage.state-of-charge ?b) (Storage.capacity ?b)))
                      (at end (= (Storage.state-of-charge ?b) (Storage.capacity ?b)))
                 )
      :effect (and
                    (at start (assign (?b) (Storage.start-charging ?b))) ;method call
                    (at start (increase (current-power) (Storage.charge-power ?b)))
                    (at end (decrease (current-power) (Storage.charge-power ?b)))
                    (at end (assign (?b) (Storage.stop-charging ?b ?duration))) ;method call
              )
    )

    (:durative-action discharge
      :parameters (?b - Storage.battery)
      :duration (> ?duration 0)
      :condition (and
                      (at start (Storage.available ?b))
                      (at start (> (Storage.state-of-charge ?b) 0))
                      (at end (>= (Storage.state-of-charge ?b) 0))
                 )
      :effect (and
                    (at start (assign (?b) (Storage.start-discharging ?b))) ;method call
                    (at start (decrease (current-power) (Storage.charge-power ?b)))
                    (at start (increase (current-power) (Storage.charge-power ?b)))
                    (at end (invoke (?b) (Storage.stop-discharging ?b ?duration))) ;method call
              )
    )

)



