(define (problem storage-withclasses-prob1)

    (:domain storage-withclasses)

    (:requirements :typing :durative-actions :fluents :duration-inequalities :modules)

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;Some generic functions might also be recommended to be implemented by numeric planners.
    ;These could have a standardised namespace such as PDDLx

    (:classes
              Storage - KCL.Planning.APS.Storage
              Math - Pddlx.Math
    )

    (:objects

        ;use the imported types that the methods know about
        battery1 - Storage.battery
        battery2 - Storage.battery)

    (:init
        (= (current-power) 0)

        ;** New Predicates from Storage class **
        ;These are the initialisation parameters of the module. It is the only time that they can be initialised.

        (Storage.available battery1)

        ;** New Numeric Fluents from Storage class **
        (= (Storage.state-of-charge battery1) 30)  ;the current state-of-charge of the battery
        (= (Storage.capacity battery1) 100) ;the total capacity of a battery
        (= (Storage.charge-power battery1) 2000) ;power needed to charge the battery
        (= (Storage.peukert-k battery1) 1.2) ;the Peukert Constant

        (= (Storage.state-of-charge battery2) 10)  ;the current state-of-charge of the battery
        (= (Storage.capacity battery1) 100) ;the total capacity of a battery
        (= (Storage.charge-power battery1) 2000) ;power needed to charge the battery
        (= (Storage.peukert-k battery1) 1.2) ;the Peukert Constant
    )

    ;** Methods - these resolve to external calls **
    (:goal
        ;we want both batteries to be fully charged
        (and (= (Storage.state-of-charge battery1) (Storage.capacity battery1))
             (= (Storage.state-of-charge battery2) (Storage.capacity battery2)))
    )

    ;we want the average charge level of both batteries to be as high as possible throughout
    (:metric maximize (/ (+ (Math.avg (Storage.state-of-charge battery1))
                            (Math.avg (Storage.state-of-charge battery2))) 2
                      )
    )
)