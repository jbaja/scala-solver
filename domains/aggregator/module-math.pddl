;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Definition of the Math module.
; This could be a 'standard' module provided by Planners supporting this external call functionality

(define (module Pddlx.Math)
  (:requirements :typing :fluents)

  (:methods

    (ceiling ?n - Number) - Number
    (floor ?n - Number) - Number
    (round ?n - Number) - Number
    (abs ?n - Number) - Number

    (power ?n ?p - Number) - Number
    (sqrt ?n - Number) - Number
    (exp ?n - Number) - Number
    (log ?n - Number) - Number

    (sin ?n - Number) - Number
    (cos ?n - Number) - Number
    (tan ?n - Number) - Number
    (asin ?n - Number) - Number
    (acos ?n - Number) - Number
    (atan ?n - Number) - Number
    (cosh ?n - Number) - Number
    (sinh ?n - Number) - Number
    (tanh ?n - Number) - Number

    ;;;; Aggregate Methods ;;;;;;;;;;;;;;;;;;;;;;;
    ; A Function refers to a PDDL Numeric Fluent
    (avg ?f - Function) - Number
    (max ?f - Function) - Number
    (min ?f - Function) - Number
  )
)

;;;;;;;;;;;;;;;;;;;;;;;;;
;To clarify:
;For generic Functions (that refer to PDDL Numeric Fluents)
;at which point will the function be invoked?
;There is a difference between these kinds of methods (which operate on generic fluents) and those methods
;which only interact with internal (immutable) fluents.


