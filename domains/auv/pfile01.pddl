(define (problem auvprob1234) (:domain auv)
(:objects
	ship1 - ship
	auv0 auv1 - auv
	torch0 torch1 - torch
	auv0gulper auv1gulper - gulper
	waypoint0 waypoint1 waypoint2 waypoint3 - Waypoint
	camera0 - Camera
	fish0 fish1 - Objective
	)
(:init
	(ready)
	(= (drift-rate) 0.5)
	(at_water_sample waypoint0)
	(at_water_sample waypoint1)
	(at_water_sample waypoint2)
	(at_surface waypoint0)
	(is-ship waypoint0)
	(channel_free)
	(= (energy auv0) 1000)
	(= (recharge-rate auv0) 11)
	(at auv0 waypoint0)
	(on_board_torch torch0 auv0)
	(off torch0)
	(= (distance-from-waypoint auv0) 0)
	(= (recharge-rate auv0) 10)
	(available auv0)
	(on_board_gulper auv0gulper auv0)
	(empty auv0gulper)

	(= (distance waypoint3 waypoint0) 4)
	(can_traverse waypoint3 waypoint0)
	(= (distance waypoint0 waypoint3) 4)
	(can_traverse waypoint0 waypoint3)
	(= (distance waypoint3 waypoint1) 10)
	(can_traverse waypoint3 waypoint1)
	(= (distance waypoint1 waypoint3) 3)
	(can_traverse waypoint1 waypoint3)
	(= (distance waypoint1 waypoint2) 1)
	(can_traverse waypoint1 waypoint2)
	(= (distance waypoint2 waypoint1) 6)
	(can_traverse waypoint2 waypoint1)
	(= (energy auv1) 1000)
	(= (recharge-rate auv1) 18)
	(at auv1 waypoint0)
	(on_board_torch torch1 auv1)
	(off torch1)
	(= (distance-from-waypoint auv1) 0)
	(= (recharge-rate auv1) 10)
	(available auv1)
	(on_board_gulper auv1gulper auv1)
	(empty auv1gulper)

	(equipped_for_imaging auv1)
	(= (distance waypoint3 waypoint0) 4)
	(can_traverse waypoint3 waypoint0)
	(= (distance waypoint0 waypoint3) 4)
	(can_traverse waypoint0 waypoint3)
	(= (distance waypoint3 waypoint2) 6)
	(can_traverse waypoint3 waypoint2)
	(= (distance waypoint2 waypoint3) 3)
	(can_traverse waypoint2 waypoint3)
	(= (distance waypoint2 waypoint1) 6)
	(can_traverse waypoint2 waypoint1)
	(= (distance waypoint1 waypoint2) 1)
	(can_traverse waypoint1 waypoint2)
	(on_board_camera camera0 auv1)
	(calibration_target camera0 fish1)
	(visible_from fish0 waypoint0)
	(visible_from fish0 waypoint1)
	(visible_from fish1 waypoint0)
	(visible_from fish1 waypoint1)
	(free waypoint0)
	(free waypoint1)
	(free waypoint2)
	(free waypoint3)
	(= (light-level fish0) 0)
	(= (light-level fish1) 0)
	(need_image fish1)
	(need_water_sample waypoint1)
)

(:goal (and
(have_water_sample waypoint1)
(communicated_image_data fish1)
	)
)

(:metric minimize (total-time))
)
