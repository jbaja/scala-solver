(define (problem auvprob6152) (:domain auv)
(:objects
	ship1 - ship
	auv0 auv1 auv2 auv3 - auv
	torch0 torch1 torch2 torch3 - torch
	auv0gulper auv1gulper auv2gulper auv3gulper - gulper
	waypoint0 waypoint1 waypoint2 waypoint3 waypoint4 waypoint5 waypoint6 waypoint7 waypoint8 - Waypoint
	camera0 camera1 camera2 camera3 camera4 - Camera
	fish0 fish1 fish2 fish3 - Objective
	)
(:init
	(ready)
	(= (drift-rate) 0.5)
	(at_water_sample waypoint0)
	(at_water_sample waypoint1)
	(at_water_sample waypoint2)
	(at_water_sample waypoint4)
	(at_water_sample waypoint6)
	(at_water_sample waypoint7)
	(at_water_sample waypoint8)
	(at_surface waypoint2)
	(is-ship waypoint2)
	(channel_free)
	(= (energy auv0) 1000)
	(= (recharge-rate auv0) 15)
	(at auv0 waypoint2)
	(on_board_torch torch0 auv0)
	(off torch0)
	(= (distance-from-waypoint auv0) 0)
	(= (recharge-rate auv0) 10)
	(available auv0)
	(on_board_gulper auv0gulper auv0)
	(empty auv0gulper)
	(equipped_for_imaging auv0)
	(= (distance waypoint7 waypoint0) 9)
	(can_traverse waypoint7 waypoint0)
	(= (distance waypoint0 waypoint7) 5)
	(can_traverse waypoint0 waypoint7)
	(= (distance waypoint7 waypoint1) 4)
	(can_traverse waypoint7 waypoint1)
	(= (distance waypoint1 waypoint7) 9)
	(can_traverse waypoint1 waypoint7)
	(= (distance waypoint7 waypoint2) 7)
	(can_traverse waypoint7 waypoint2)
	(= (distance waypoint2 waypoint7) 3)
	(can_traverse waypoint2 waypoint7)
	(= (distance waypoint7 waypoint8) 1)
	(can_traverse waypoint7 waypoint8)
	(= (distance waypoint8 waypoint7) 5)
	(can_traverse waypoint8 waypoint7)
	(= (distance waypoint0 waypoint6) 8)
	(can_traverse waypoint0 waypoint6)
	(= (distance waypoint6 waypoint0) 2)
	(can_traverse waypoint6 waypoint0)
	(= (distance waypoint1 waypoint3) 4)
	(can_traverse waypoint1 waypoint3)
	(= (distance waypoint3 waypoint1) 7)
	(can_traverse waypoint3 waypoint1)
	(= (distance waypoint1 waypoint5) 10)
	(can_traverse waypoint1 waypoint5)
	(= (distance waypoint5 waypoint1) 8)
	(can_traverse waypoint5 waypoint1)
	(= (distance waypoint2 waypoint4) 4)
	(can_traverse waypoint2 waypoint4)
	(= (distance waypoint4 waypoint2) 7)
	(can_traverse waypoint4 waypoint2)
	(= (energy auv1) 1000)
	(= (recharge-rate auv1) 20)
	(at auv1 waypoint2)
	(on_board_torch torch1 auv1)
	(off torch1)
	(= (distance-from-waypoint auv1) 0)
	(= (recharge-rate auv1) 10)
	(available auv1)
	(on_board_gulper auv1gulper auv1)
	(empty auv1gulper)
	(equipped_for_imaging auv1)
	(= (distance waypoint6 waypoint0) 2)
	(can_traverse waypoint6 waypoint0)
	(= (distance waypoint0 waypoint6) 8)
	(can_traverse waypoint0 waypoint6)
	(= (distance waypoint0 waypoint2) 2)
	(can_traverse waypoint0 waypoint2)
	(= (distance waypoint2 waypoint0) 2)
	(can_traverse waypoint2 waypoint0)
	(= (distance waypoint0 waypoint3) 8)
	(can_traverse waypoint0 waypoint3)
	(= (distance waypoint3 waypoint0) 9)
	(can_traverse waypoint3 waypoint0)
	(= (distance waypoint0 waypoint7) 5)
	(can_traverse waypoint0 waypoint7)
	(= (distance waypoint7 waypoint0) 9)
	(can_traverse waypoint7 waypoint0)
	(= (distance waypoint2 waypoint4) 4)
	(can_traverse waypoint2 waypoint4)
	(= (distance waypoint4 waypoint2) 7)
	(can_traverse waypoint4 waypoint2)
	(= (distance waypoint2 waypoint8) 4)
	(can_traverse waypoint2 waypoint8)
	(= (distance waypoint8 waypoint2) 5)
	(can_traverse waypoint8 waypoint2)
	(= (distance waypoint3 waypoint1) 7)
	(can_traverse waypoint3 waypoint1)
	(= (distance waypoint1 waypoint3) 4)
	(can_traverse waypoint1 waypoint3)
	(= (energy auv2) 1000)
	(= (recharge-rate auv2) 12)
	(at auv2 waypoint2)
	(on_board_torch torch2 auv2)
	(off torch2)
	(= (distance-from-waypoint auv2) 0)
	(= (recharge-rate auv2) 10)
	(available auv2)
	(on_board_gulper auv2gulper auv2)
	(empty auv2gulper)

	(equipped_for_imaging auv2)
	(= (distance waypoint6 waypoint0) 2)
	(can_traverse waypoint6 waypoint0)
	(= (distance waypoint0 waypoint6) 8)
	(can_traverse waypoint0 waypoint6)
	(= (distance waypoint6 waypoint2) 6)
	(can_traverse waypoint6 waypoint2)
	(= (distance waypoint2 waypoint6) 8)
	(can_traverse waypoint2 waypoint6)
	(= (distance waypoint6 waypoint7) 5)
	(can_traverse waypoint6 waypoint7)
	(= (distance waypoint7 waypoint6) 2)
	(can_traverse waypoint7 waypoint6)
	(= (distance waypoint0 waypoint1) 5)
	(can_traverse waypoint0 waypoint1)
	(= (distance waypoint1 waypoint0) 1)
	(can_traverse waypoint1 waypoint0)
	(= (distance waypoint0 waypoint3) 8)
	(can_traverse waypoint0 waypoint3)
	(= (distance waypoint3 waypoint0) 9)
	(can_traverse waypoint3 waypoint0)
	(= (distance waypoint0 waypoint4) 1)
	(can_traverse waypoint0 waypoint4)
	(= (distance waypoint4 waypoint0) 2)
	(can_traverse waypoint4 waypoint0)
	(= (distance waypoint0 waypoint5) 10)
	(can_traverse waypoint0 waypoint5)
	(= (distance waypoint5 waypoint0) 1)
	(can_traverse waypoint5 waypoint0)
	(= (distance waypoint2 waypoint8) 4)
	(can_traverse waypoint2 waypoint8)
	(= (distance waypoint8 waypoint2) 5)
	(can_traverse waypoint8 waypoint2)
	(= (energy auv3) 1000)
	(= (recharge-rate auv3) 16)
	(at auv3 waypoint2)
	(on_board_torch torch3 auv3)
	(off torch3)
	(= (distance-from-waypoint auv3) 0)
	(= (recharge-rate auv3) 10)
	(available auv3)
	(on_board_gulper auv3gulper auv3)
	(empty auv3gulper)

	(equipped_for_imaging auv3)
	(= (distance waypoint3 waypoint0) 9)
	(can_traverse waypoint3 waypoint0)
	(= (distance waypoint0 waypoint3) 8)
	(can_traverse waypoint0 waypoint3)
	(= (distance waypoint3 waypoint1) 7)
	(can_traverse waypoint3 waypoint1)
	(= (distance waypoint1 waypoint3) 4)
	(can_traverse waypoint1 waypoint3)
	(= (distance waypoint3 waypoint5) 10)
	(can_traverse waypoint3 waypoint5)
	(= (distance waypoint5 waypoint3) 2)
	(can_traverse waypoint5 waypoint3)
	(= (distance waypoint3 waypoint7) 10)
	(can_traverse waypoint3 waypoint7)
	(= (distance waypoint7 waypoint3) 7)
	(can_traverse waypoint7 waypoint3)
	(= (distance waypoint3 waypoint8) 4)
	(can_traverse waypoint3 waypoint8)
	(= (distance waypoint8 waypoint3) 3)
	(can_traverse waypoint8 waypoint3)
	(= (distance waypoint0 waypoint4) 1)
	(can_traverse waypoint0 waypoint4)
	(= (distance waypoint4 waypoint0) 2)
	(can_traverse waypoint4 waypoint0)
	(= (distance waypoint0 waypoint6) 8)
	(can_traverse waypoint0 waypoint6)
	(= (distance waypoint6 waypoint0) 2)
	(can_traverse waypoint6 waypoint0)
	(= (distance waypoint1 waypoint2) 6)
	(can_traverse waypoint1 waypoint2)
	(= (distance waypoint2 waypoint1) 9)
	(can_traverse waypoint2 waypoint1)
	(on_board_camera camera0 auv2)
	(calibration_target camera0 fish1)
	(on_board_camera camera1 auv2)
	(calibration_target camera1 fish1)
	(on_board_camera camera2 auv3)
	(calibration_target camera2 fish1)
	(on_board_camera camera3 auv1)
	(calibration_target camera3 fish2)
	(on_board_camera camera4 auv0)
	(calibration_target camera4 fish1)
	(visible_from fish0 waypoint0)
	(visible_from fish1 waypoint0)
	(visible_from fish1 waypoint1)
	(visible_from fish1 waypoint2)
	(visible_from fish1 waypoint3)
	(visible_from fish1 waypoint4)
	(visible_from fish1 waypoint5)
	(visible_from fish1 waypoint6)
	(visible_from fish1 waypoint7)
	(visible_from fish2 waypoint0)
	(visible_from fish2 waypoint1)
	(visible_from fish2 waypoint2)
	(visible_from fish2 waypoint3)
	(visible_from fish2 waypoint4)
	(visible_from fish2 waypoint5)
	(visible_from fish3 waypoint0)
	(visible_from fish3 waypoint1)
	(visible_from fish3 waypoint2)
	(visible_from fish3 waypoint3)
	(visible_from fish3 waypoint4)
	(visible_from fish3 waypoint5)
	(visible_from fish3 waypoint6)
	(visible_from fish3 waypoint7)
	(free waypoint0)
	(free waypoint1)
	(free waypoint2)
	(free waypoint3)
	(free waypoint4)
	(free waypoint5)
	(free waypoint6)
	(free waypoint7)
	(free waypoint8)
	(= (light-level fish0) 0)
	(= (light-level fish1) 0)
	(= (light-level fish2) 0)
	(= (light-level fish3) 0)
	(visible_from_ship fish0 ship1)
	(visible_from_ship fish1 ship1)
	(need_image fish2)
	(need_image fish1)
	(need_image fish3)
	(need_water_sample waypoint1)
	(need_water_sample waypoint8)
	(need_water_sample waypoint7)
	(need_water_sample waypoint6)
	(need_water_sample waypoint4)
)

(:goal (and
(have_water_sample waypoint4)
(have_water_sample waypoint6)
(have_water_sample waypoint7)
(have_water_sample waypoint8)
(have_water_sample waypoint1)
(communicated_image_data fish3)
(communicated_image_data fish1)
(communicated_image_data fish2)
	)
)

(:metric minimize (total-time))
)
