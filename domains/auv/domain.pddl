(define (domain AUV)
(:requirements :typing :durative-actions :fluents :duration-inequalities :conditional-effects)
(:types auv ship waypoint gulper camera objective)

(:predicates 
        (at ?x - auv ?w - waypoint)
        (drifting)
        (moving ?x - auv ?w - waypoint ?to - waypoint)
        (is-ship ?w - waypoint)
        (can_traverse ?x - waypoint ?y - waypoint)
        (equipped_with_gulper ?v - auv)
        (equipped_for_imaging ?v - auv)
        (empty ?g - gulper)
        (have_water_sample ?w - waypoint)
        (full ?g - gulper)
        (calibrated ?c - camera ?v - auv)
        (supports ?c - camera ?m - mode)
        (available ?v - auv)
        (have_image ?v - auv ?o - objective)
        (communicated_image_data ?o - objective)
        (at_water_sample ?w - waypoint)
        (visible_from ?o - objective ?w - waypoint)
        (visible_from_ship ?o - objective ?s - ship)
        (on_board_camera ?i - camera ?v - auv)
        (on_board_torch ?t - torch ?v - auv)
        (on_board_gulper ?g - gulper ?v - auv)
        (channel_free)
        (on ?t - torch)
        (off ?t - torch)
        (on_light ?s - ship)
        (off_light ?s - ship)	
        (cantrickle ?r - rover)
        (has_light ?s - ship)
        (free ?w - waypoint)
        (at_surface ?w - waypoint)
        (calibration_target ?i - camera ?o - objective)
        (need_image ?o - objective)
        (need_water_sample ?w - waypoint)
        (ready)


)
(:functions (energy ?v - auv) 
        (recharge-rate ?v - auv) 
        (distance ?x - waypoint ?y - waypoint) 
        (light-level ?o - objective) 
        (drift-rate) 
	(energy-used)
        (exposure ?v - auv ?o - objective) 
	(light-from ?t - torch)
        (distance-from-waypoint ?v - auv))



(:durative-action drift
:parameters ()
:duration (and (>= ?duration 0.1) (<= ?duration 100000))
:condition (and
                (at start (ready))
            )
:effect (and 
            (at start (not (ready)))
            (at start (drifting))
            (at end (not (drifting)))
            (forall (?v - auv) (increase (distance-from-waypoint ?v) (* #t (* 1 (drift-rate)))))
	)
)

(:durative-action localise_at_waypoint_for_imaging
:parameters (?v - auv ?w - waypoint ?o - objective)
:duration (= ?duration (distance-from-waypoint ?v))
:condition (and 
                (over all (at ?v ?w)) 
                (over all (drifting)) 
                (at start (available ?v)) 
                (at start (>= (energy ?v) (* (distance-from-waypoint ?v) 5)))
                (at start (visible_from ?o ?w)) 
                (at start (need_image ?o))
        )
:effect (and 
            (at start (decrease (energy ?v) (* (distance-from-waypoint ?v) 5)))
            (at start (increase (energy-used) (* (distance-from-waypoint ?v) 5)))
            (at start (not (available ?v)))
            (at end (available ?v))
            (at end (assign (distance-from-waypoint ?v) 0))
	)
)


(:durative-action localise_at_waypoint_for_sampling
:parameters (?v - auv ?g - gulper ?w - waypoint)
:duration (= ?duration (distance-from-waypoint ?v))
:condition (and
                (over all (at ?v ?w))
                (over all (drifting))
                (at start (available ?v))
                (at start (>= (energy ?v) (* (distance-from-waypoint ?v) 5)))
                (at start (need_water_sample ?w))
                (at start (on_board_gulper ?g ?v))
                (at start (empty ?g))
        )
:effect (and
            (at start (decrease (energy ?v) (* (distance-from-waypoint ?v) 5)))
            (at start (increase (energy-used) (* (distance-from-waypoint ?v) 5)))
            (at start (not (available ?v)))
            (at end (available ?v))
            (at end (assign (distance-from-waypoint ?v) 0))
        )
)


(:durative-action recharge
:parameters (?x - auv ?w - waypoint)
:duration (= ?duration (/ (- 1000 (energy ?x)) (recharge-rate ?x)))
:condition (and 
                (over all (drifting)) 
                (at start (available ?x))
                (over all (at ?x ?w)) 
                (at start (<= (energy ?x) 1000))
	   )
:effect (and 
            (at start (not (available ?x)))
            (at end (assign (energy ?x) 1000))
            (at end (available ?x))
	)
)


(:durative-action sample_water
:parameters (?v - auv ?g - gulper ?w - waypoint)
:duration (= ?duration 10)
:condition (and 
            (over all (drifting)) 
            (over all (at ?v ?w)) 
            (at start (at_water_sample ?w)) 
            (at start (on_board_gulper ?g ?v)) 
            (at start (empty ?g))
            (at start (available ?v))
            (at start (>= (energy ?v) 30))
            (at start (<= (distance-from-waypoint ?v) 2))
           )
:effect (and 
            (at start (increase (energy-used) 30))
            (at start (not (empty ?g)))
            (at start (not (available ?v)))  
            (at end (full ?g)) 
            (at end (available ?v)) 
            (at start (decrease (energy ?v) 30))
            (at start (assign (distance-from-waypoint ?v) 0))
            (at end (have_water_sample ?w)) 
            (at start (not (need_water_sample ?w)))
            (at start (not (at_water_sample ?w)))
        )
)

(:durative-action deliver_sample
:parameters (?v - auv ?g - gulper ?w - waypoint)
:duration (= ?duration 5)
:condition (and 
            (over all (drifting)) 
            (over all (at ?v ?w)) 
            (at start (is-ship ?w))
            (at start (on_board_gulper ?g ?v)) 
            (at start (full ?g))
            (at start (available ?v))
            (at start (>= (energy ?v) 5))
        )
:effect (and 
            (at start (increase (energy-used) 5))
            (at end (empty ?g))
            (at start (not (full ?g))) 
            (at start (not (available ?v)))  
            (at end (available ?v)) 
            (at start (decrease (energy ?v) 5))
        )
)


(:durative-action shine_torch
:parameters (?v ?v2 - auv ?w - waypoint ?w2 - waypoint ?o - objective ?t - torch)
:duration (and (>= ?duration (+ (distance-from-waypoint ?v2) (+ 4.515 (* (- 1000 (distance-from-waypoint ?v)) -0.0025))))  (<= ?duration (/ 1000 10 )) )
:condition (and 
                (over all (drifting)) 
                (over all (visible_from ?o ?w)) 
                (over all (visible_from ?o ?w2)) 
                (over all (>= (light-from ?t) 0))
                (at start (off ?t)) 
                (at start (on_board_torch ?t ?v))
                (over all (at ?v ?w))  
                (over all (at ?v2 ?w2))
                (over all (>= (energy ?v) 0))
                (at start (<= (distance-from-waypoint ?v) 5))
                (at start (available ?v))
                (at end (have_image ?v2 ?o))
                (at start (need_image ?o))
           )
:effect (and 
	      (decrease (energy ?v) (* #t 10))
	      (increase (energy-used) (* #t 10))
	      (at start (not (off ?t)))
	      (at end (off ?t))
	      (at end (not (on ?t)))
	      (at start (on ?t))
	      (at start (not (available ?v)))
	      (at end (available ?v))
	      (at start (assign (light-from ?t) (- 1000 (distance-from-waypoint ?v))))
	      (decrease (light-from ?t) (* #t (drift-rate)))
	      (at start (increase (light-level ?o) (- 1000 (distance-from-waypoint ?v) ) ) )
	      (decrease (light-level ?o) (* #t (drift-rate)))
	      (at end (decrease (light-level ?o) (light-from ?t)))
       )
)

(:durative-action shine_ship_light
:parameters (?s - ship ?o - objective ?v - auv)
:duration (<= ?duration 4000)
:condition (and (over all (visible_from_ship ?o ?s)) 
		(at start (off_light ?s)) 
		(at start (need_image ?o))
		(at end (have_image ?v ?o))
        (over all (drifting))
            )
:effect (and 
        (at start (not (off_light ?s)))
        (at end (off_light ?s))
        (at end (not (on_light ?s)))
        (at start (on_light ?s))
        (at start (increase (light-level ?o) 1000))
        (at end (decrease (light-level ?o) 1000))
        )
)


(:durative-action take_image
 :parameters (?v - auv ?w - waypoint ?o - objective ?c - camera)
 :duration (= ?duration (+ 4.51 (* (light-level ?o) -0.0025)) ) ;approximated linear function based on initial light level for this range
 :condition (and 
                (over all (drifting)) 
                (at start (need_image ?o))
                (at start (>= (light-level ?o) 400))
                (at start (on_board_camera ?c ?v))
                (at start (available ?v))
                (over all (visible_from ?o ?w))
                (over all (at ?v ?w))
                (at start (>= (energy ?v) (* 20 (+ 4.51 (* (light-level ?o) -0.0025)))))
                (at start (<= (distance-from-waypoint ?v) 2))
            )
 :effect (and 
		(at start (not (available ?v)))
		(at end (available ?v))		
		(at start (decrease (energy ?v) (* 20 (+ 4.51 (* (light-level ?o) -0.0025)))))
		(at start (increase (energy-used) (* 20 (+ 4.51 (* (light-level ?o) -0.0025)))))
		(at end (have_image ?v ?o))
		(at start (not (need_image ?o)))
       )
)





(:durative-action communicate_image_data
 :parameters (?v - auv ?o - objective ?w - waypoint)
 :duration (= ?duration 15)
 :condition (and 
                (over all (at ?v ?w)) 
                (over all (drifting)) 
                (over all (at_surface ?w)) 
                (at start (have_image ?v ?o))
                (at start (available ?v)) 
                (at start (channel_free)) 
                (at start (>= (energy ?v) 60))
            )
 :effect (and 
            (at start (not (available ?v))) 
            (at start (not (channel_free)))
            (at end (channel_free)) 
            (at end (communicated_image_data ?o)) 
            (at end (available ?v))
            (at start (decrease (energy ?v) 60))
            (at start (increase (energy-used) 60))
            )
)


(:durative-action navigate
:parameters (?v - auv ?y - waypoint ?z - waypoint)
:duration (= ?duration (distance ?y ?z))
:condition (and 
        (at start (can_traverse ?y ?z)) 
        (at start (available ?v)) 
        (at start (at ?v ?y))  
        (at start (>= (energy ?v) (* (distance ?y ?z) 5)))
        (at start (free ?z))
        (over all (drifting))
            )
:effect (and 
            (at start (decrease (energy ?v) (* (distance ?y ?z) 5)))
            (at start (increase (energy-used) (* (distance ?y ?z) 5)))
            (at start (not (at ?v ?y)))
            (at start (free ?y))
            (at start (not (available ?v))) 
            (at end (available ?v))
            (at end (at ?v ?z))
            (at start (not (free ?z)))
    )
)


)
