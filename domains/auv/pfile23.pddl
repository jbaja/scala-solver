(define (problem auvprob5742) (:domain auv)
(:objects
	ship1 - ship
	auv0 auv1 auv2 auv3 auv4 - auv
	torch0 torch1 torch2 torch3 torch4 - torch
	auv0gulper auv1gulper auv2gulper auv3gulper auv4gulper - gulper
	waypoint0 waypoint1 waypoint2 waypoint3 waypoint4 waypoint5 waypoint6 waypoint7 waypoint8 waypoint9 waypoint10 waypoint11 - Waypoint
	camera0 camera1 camera2 camera3 camera4 - Camera
	fish0 fish1 fish2 fish3 fish4 - Objective
	)
(:init
	(ready)
	(= (drift-rate) 0.5)
	(at_water_sample waypoint0)
	(at_water_sample waypoint1)
	(at_water_sample waypoint6)
	(at_water_sample waypoint7)
	(at_water_sample waypoint10)
	(at_surface waypoint7)
	(is-ship waypoint7)
	(channel_free)
	(= (energy auv0) 1000)
	(= (recharge-rate auv0) 14)
	(at auv0 waypoint7)
	(on_board_torch torch0 auv0)
	(off torch0)
	(= (distance-from-waypoint auv0) 0)
	(= (recharge-rate auv0) 10)
	(available auv0)
	(on_board_gulper auv0gulper auv0)
	(empty auv0gulper)
	(equipped_for_imaging auv0)
	(= (distance waypoint3 waypoint1) 9)
	(can_traverse waypoint3 waypoint1)
	(= (distance waypoint1 waypoint3) 3)
	(can_traverse waypoint1 waypoint3)
	(= (distance waypoint3 waypoint4) 10)
	(can_traverse waypoint3 waypoint4)
	(= (distance waypoint4 waypoint3) 3)
	(can_traverse waypoint4 waypoint3)
	(= (distance waypoint3 waypoint5) 6)
	(can_traverse waypoint3 waypoint5)
	(= (distance waypoint5 waypoint3) 9)
	(can_traverse waypoint5 waypoint3)
	(= (distance waypoint3 waypoint7) 4)
	(can_traverse waypoint3 waypoint7)
	(= (distance waypoint7 waypoint3) 8)
	(can_traverse waypoint7 waypoint3)
	(= (distance waypoint3 waypoint9) 6)
	(can_traverse waypoint3 waypoint9)
	(= (distance waypoint9 waypoint3) 2)
	(can_traverse waypoint9 waypoint3)
	(= (distance waypoint3 waypoint11) 2)
	(can_traverse waypoint3 waypoint11)
	(= (distance waypoint11 waypoint3) 8)
	(can_traverse waypoint11 waypoint3)
	(= (distance waypoint1 waypoint10) 1)
	(can_traverse waypoint1 waypoint10)
	(= (distance waypoint10 waypoint1) 1)
	(can_traverse waypoint10 waypoint1)
	(= (distance waypoint4 waypoint0) 1)
	(can_traverse waypoint4 waypoint0)
	(= (distance waypoint0 waypoint4) 10)
	(can_traverse waypoint0 waypoint4)
	(= (distance waypoint4 waypoint8) 8)
	(can_traverse waypoint4 waypoint8)
	(= (distance waypoint8 waypoint4) 7)
	(can_traverse waypoint8 waypoint4)
	(= (distance waypoint5 waypoint2) 10)
	(can_traverse waypoint5 waypoint2)
	(= (distance waypoint2 waypoint5) 2)
	(can_traverse waypoint2 waypoint5)
	(= (energy auv1) 1000)
	(= (recharge-rate auv1) 19)
	(at auv1 waypoint7)
	(on_board_torch torch1 auv1)
	(off torch1)
	(= (distance-from-waypoint auv1) 0)
	(= (recharge-rate auv1) 10)
	(available auv1)
	(on_board_gulper auv1gulper auv1)
	(empty auv1gulper)

	(equipped_for_imaging auv1)
	(= (distance waypoint1 waypoint3) 3)
	(can_traverse waypoint1 waypoint3)
	(= (distance waypoint3 waypoint1) 9)
	(can_traverse waypoint3 waypoint1)
	(= (distance waypoint1 waypoint4) 2)
	(can_traverse waypoint1 waypoint4)
	(= (distance waypoint4 waypoint1) 1)
	(can_traverse waypoint4 waypoint1)
	(= (distance waypoint1 waypoint5) 2)
	(can_traverse waypoint1 waypoint5)
	(= (distance waypoint5 waypoint1) 4)
	(can_traverse waypoint5 waypoint1)
	(= (distance waypoint1 waypoint7) 2)
	(can_traverse waypoint1 waypoint7)
	(= (distance waypoint7 waypoint1) 5)
	(can_traverse waypoint7 waypoint1)
	(= (distance waypoint1 waypoint10) 1)
	(can_traverse waypoint1 waypoint10)
	(= (distance waypoint10 waypoint1) 1)
	(can_traverse waypoint10 waypoint1)
	(= (distance waypoint3 waypoint9) 6)
	(can_traverse waypoint3 waypoint9)
	(= (distance waypoint9 waypoint3) 2)
	(can_traverse waypoint9 waypoint3)
	(= (distance waypoint4 waypoint0) 1)
	(can_traverse waypoint4 waypoint0)
	(= (distance waypoint0 waypoint4) 10)
	(can_traverse waypoint0 waypoint4)
	(= (distance waypoint4 waypoint2) 1)
	(can_traverse waypoint4 waypoint2)
	(= (distance waypoint2 waypoint4) 2)
	(can_traverse waypoint2 waypoint4)
	(= (distance waypoint4 waypoint8) 8)
	(can_traverse waypoint4 waypoint8)
	(= (distance waypoint8 waypoint4) 7)
	(can_traverse waypoint8 waypoint4)
	(= (distance waypoint5 waypoint11) 6)
	(can_traverse waypoint5 waypoint11)
	(= (distance waypoint11 waypoint5) 2)
	(can_traverse waypoint11 waypoint5)
	(= (energy auv2) 1000)
	(= (recharge-rate auv2) 20)
	(at auv2 waypoint7)
	(on_board_torch torch2 auv2)
	(off torch2)
	(= (distance-from-waypoint auv2) 0)
	(= (recharge-rate auv2) 10)
	(available auv2)
	(on_board_gulper auv2gulper auv2)
	(empty auv2gulper)

	(equipped_for_imaging auv2)
	(= (distance waypoint1 waypoint3) 3)
	(can_traverse waypoint1 waypoint3)
	(= (distance waypoint3 waypoint1) 9)
	(can_traverse waypoint3 waypoint1)
	(= (distance waypoint1 waypoint4) 2)
	(can_traverse waypoint1 waypoint4)
	(= (distance waypoint4 waypoint1) 1)
	(can_traverse waypoint4 waypoint1)
	(= (distance waypoint1 waypoint5) 2)
	(can_traverse waypoint1 waypoint5)
	(= (distance waypoint5 waypoint1) 4)
	(can_traverse waypoint5 waypoint1)
	(= (distance waypoint1 waypoint7) 2)
	(can_traverse waypoint1 waypoint7)
	(= (distance waypoint7 waypoint1) 5)
	(can_traverse waypoint7 waypoint1)
	(= (distance waypoint1 waypoint10) 1)
	(can_traverse waypoint1 waypoint10)
	(= (distance waypoint10 waypoint1) 1)
	(can_traverse waypoint10 waypoint1)
	(= (distance waypoint3 waypoint11) 2)
	(can_traverse waypoint3 waypoint11)
	(= (distance waypoint11 waypoint3) 8)
	(can_traverse waypoint11 waypoint3)
	(= (distance waypoint4 waypoint0) 1)
	(can_traverse waypoint4 waypoint0)
	(= (distance waypoint0 waypoint4) 10)
	(can_traverse waypoint0 waypoint4)
	(= (distance waypoint4 waypoint8) 8)
	(can_traverse waypoint4 waypoint8)
	(= (distance waypoint8 waypoint4) 7)
	(can_traverse waypoint8 waypoint4)
	(= (distance waypoint5 waypoint2) 10)
	(can_traverse waypoint5 waypoint2)
	(= (distance waypoint2 waypoint5) 2)
	(can_traverse waypoint2 waypoint5)
	(= (distance waypoint5 waypoint9) 3)
	(can_traverse waypoint5 waypoint9)
	(= (distance waypoint9 waypoint5) 6)
	(can_traverse waypoint9 waypoint5)
	(= (distance waypoint8 waypoint6) 5)
	(can_traverse waypoint8 waypoint6)
	(= (distance waypoint6 waypoint8) 9)
	(can_traverse waypoint6 waypoint8)
	(= (energy auv3) 1000)
	(= (recharge-rate auv3) 12)
	(at auv3 waypoint7)
	(on_board_torch torch3 auv3)
	(off torch3)
	(= (distance-from-waypoint auv3) 0)
	(= (recharge-rate auv3) 10)
	(available auv3)
	(on_board_gulper auv3gulper auv3)
	(empty auv3gulper)
	(equipped_for_imaging auv3)
	(= (distance waypoint11 waypoint3) 8)
	(can_traverse waypoint11 waypoint3)
	(= (distance waypoint3 waypoint11) 2)
	(can_traverse waypoint3 waypoint11)
	(= (distance waypoint11 waypoint5) 2)
	(can_traverse waypoint11 waypoint5)
	(= (distance waypoint5 waypoint11) 6)
	(can_traverse waypoint5 waypoint11)
	(= (distance waypoint11 waypoint6) 10)
	(can_traverse waypoint11 waypoint6)
	(= (distance waypoint6 waypoint11) 6)
	(can_traverse waypoint6 waypoint11)
	(= (distance waypoint11 waypoint7) 5)
	(can_traverse waypoint11 waypoint7)
	(= (distance waypoint7 waypoint11) 2)
	(can_traverse waypoint7 waypoint11)
	(= (distance waypoint11 waypoint8) 10)
	(can_traverse waypoint11 waypoint8)
	(= (distance waypoint8 waypoint11) 6)
	(can_traverse waypoint8 waypoint11)
	(= (distance waypoint11 waypoint9) 10)
	(can_traverse waypoint11 waypoint9)
	(= (distance waypoint9 waypoint11) 5)
	(can_traverse waypoint9 waypoint11)
	(= (distance waypoint3 waypoint4) 10)
	(can_traverse waypoint3 waypoint4)
	(= (distance waypoint4 waypoint3) 3)
	(can_traverse waypoint4 waypoint3)
	(= (distance waypoint3 waypoint10) 3)
	(can_traverse waypoint3 waypoint10)
	(= (distance waypoint10 waypoint3) 9)
	(can_traverse waypoint10 waypoint3)
	(= (distance waypoint5 waypoint1) 4)
	(can_traverse waypoint5 waypoint1)
	(= (distance waypoint1 waypoint5) 2)
	(can_traverse waypoint1 waypoint5)
	(= (distance waypoint6 waypoint2) 4)
	(can_traverse waypoint6 waypoint2)
	(= (distance waypoint2 waypoint6) 10)
	(can_traverse waypoint2 waypoint6)
	(= (distance waypoint9 waypoint0) 3)
	(can_traverse waypoint9 waypoint0)
	(= (distance waypoint0 waypoint9) 3)
	(can_traverse waypoint0 waypoint9)
	(= (energy auv4) 1000)
	(= (recharge-rate auv4) 14)
	(at auv4 waypoint7)
	(on_board_torch torch4 auv4)
	(off torch4)
	(= (distance-from-waypoint auv4) 0)
	(= (recharge-rate auv4) 10)
	(available auv4)
	(on_board_gulper auv4gulper auv4)
	(empty auv4gulper)

	(equipped_for_imaging auv4)
	(= (distance waypoint10 waypoint1) 1)
	(can_traverse waypoint10 waypoint1)
	(= (distance waypoint1 waypoint10) 1)
	(can_traverse waypoint1 waypoint10)
	(= (distance waypoint10 waypoint2) 10)
	(can_traverse waypoint10 waypoint2)
	(= (distance waypoint2 waypoint10) 5)
	(can_traverse waypoint2 waypoint10)
	(= (distance waypoint10 waypoint7) 7)
	(can_traverse waypoint10 waypoint7)
	(= (distance waypoint7 waypoint10) 9)
	(can_traverse waypoint7 waypoint10)
	(= (distance waypoint10 waypoint9) 4)
	(can_traverse waypoint10 waypoint9)
	(= (distance waypoint9 waypoint10) 6)
	(can_traverse waypoint9 waypoint10)
	(= (distance waypoint1 waypoint3) 3)
	(can_traverse waypoint1 waypoint3)
	(= (distance waypoint3 waypoint1) 9)
	(can_traverse waypoint3 waypoint1)
	(= (distance waypoint1 waypoint5) 2)
	(can_traverse waypoint1 waypoint5)
	(= (distance waypoint5 waypoint1) 4)
	(can_traverse waypoint5 waypoint1)
	(= (distance waypoint2 waypoint0) 7)
	(can_traverse waypoint2 waypoint0)
	(= (distance waypoint0 waypoint2) 3)
	(can_traverse waypoint0 waypoint2)
	(= (distance waypoint2 waypoint6) 10)
	(can_traverse waypoint2 waypoint6)
	(= (distance waypoint6 waypoint2) 4)
	(can_traverse waypoint6 waypoint2)
	(= (distance waypoint2 waypoint8) 9)
	(can_traverse waypoint2 waypoint8)
	(= (distance waypoint8 waypoint2) 7)
	(can_traverse waypoint8 waypoint2)
	(= (distance waypoint7 waypoint4) 2)
	(can_traverse waypoint7 waypoint4)
	(= (distance waypoint4 waypoint7) 5)
	(can_traverse waypoint4 waypoint7)
	(= (distance waypoint7 waypoint11) 2)
	(can_traverse waypoint7 waypoint11)
	(= (distance waypoint11 waypoint7) 5)
	(can_traverse waypoint11 waypoint7)
	(on_board_camera camera0 auv2)
	(calibration_target camera0 fish3)
	(on_board_camera camera1 auv4)
	(calibration_target camera1 fish2)
	(on_board_camera camera2 auv1)
	(calibration_target camera2 fish0)
	(on_board_camera camera3 auv3)
	(calibration_target camera3 fish4)
	(on_board_camera camera4 auv0)
	(calibration_target camera4 fish1)
	(visible_from fish0 waypoint0)
	(visible_from fish0 waypoint1)
	(visible_from fish0 waypoint2)
	(visible_from fish0 waypoint3)
	(visible_from fish0 waypoint4)
	(visible_from fish1 waypoint0)
	(visible_from fish1 waypoint1)
	(visible_from fish1 waypoint2)
	(visible_from fish1 waypoint3)
	(visible_from fish2 waypoint0)
	(visible_from fish2 waypoint1)
	(visible_from fish2 waypoint2)
	(visible_from fish2 waypoint3)
	(visible_from fish2 waypoint4)
	(visible_from fish2 waypoint5)
	(visible_from fish2 waypoint6)
	(visible_from fish2 waypoint7)
	(visible_from fish2 waypoint8)
	(visible_from fish2 waypoint9)
	(visible_from fish2 waypoint10)
	(visible_from fish3 waypoint0)
	(visible_from fish3 waypoint1)
	(visible_from fish3 waypoint2)
	(visible_from fish3 waypoint3)
	(visible_from fish3 waypoint4)
	(visible_from fish3 waypoint5)
	(visible_from fish3 waypoint6)
	(visible_from fish3 waypoint7)
	(visible_from fish3 waypoint8)
	(visible_from fish3 waypoint9)
	(visible_from fish3 waypoint10)
	(visible_from fish3 waypoint11)
	(visible_from fish4 waypoint0)
	(visible_from fish4 waypoint1)
	(visible_from fish4 waypoint2)
	(visible_from fish4 waypoint3)
	(visible_from fish4 waypoint4)
	(visible_from fish4 waypoint5)
	(visible_from fish4 waypoint6)
	(visible_from fish4 waypoint7)
	(visible_from fish4 waypoint8)
	(visible_from fish4 waypoint9)
	(visible_from fish4 waypoint10)
	(visible_from fish4 waypoint11)
	(free waypoint0)
	(free waypoint1)
	(free waypoint2)
	(free waypoint3)
	(free waypoint4)
	(free waypoint5)
	(free waypoint6)
	(free waypoint7)
	(free waypoint8)
	(free waypoint9)
	(free waypoint10)
	(free waypoint11)
	(= (light-level fish0) 0)
	(= (light-level fish1) 0)
	(= (light-level fish2) 0)
	(= (light-level fish3) 0)
	(= (light-level fish4) 0)
	(visible_from_ship fish1 ship1)
	(need_image fish4)
	(need_image fish2)
	(need_water_sample waypoint0)
	(need_water_sample waypoint7)
	(need_water_sample waypoint1)
)

(:goal (and
(have_water_sample waypoint1)
(have_water_sample waypoint7)
(have_water_sample waypoint0)
(communicated_image_data fish2)
(communicated_image_data fish4)
	)
)

(:metric minimize (total-time))
)
