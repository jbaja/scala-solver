(define (domain mousetrap)
     (:requirements :durative-actions :typing :fluents) 
     (:types mouse junction cheese - object 
          trap part - contraption 
          contraption - object)
     (:predicates
          (at ?m - mouse ?j - junction)
          (cheese_loc ?c - cheese ?j - junction)
          (connected ?j1 ?j2 - junction)
          (trigger_connected ?j1 ?j2 - junction ?p - contraption)
          (eaten ?c - cheese ?m - mouse)
          (clipx)
          (clipy)
          (causes ?p1 ?p2 - contraption)
          (triggered ?p - contraption)
          (trap_up ?t - trap ?c - cheese))
     (:functions
          (contraption_time ?p - contraption)
          (run_time ?j1 ?j2 - junction))

     (:durative-action RUN
          :parameters
               (?m - mouse
               ?from ?to - junction)
          :duration (= ?duration (run_time ?from ?to))
          :condition (and 
               (at start (connected ?from ?to))
               (at start (at ?m ?from)))
          :effect (and 
               (at start (not (at ?m ?from)))
               (at end (at ?m ?to))))

     (:durative-action RUN_TRIGGER
          :parameters
               (?m - mouse
               ?from ?to - junction
               ?p - contraption)
          :duration (= ?duration (run_time ?from ?to))
          :condition (and 
               (at start (trigger_connected ?from ?to ?p))
               (at start (at ?m ?from))
               (at end (clipx)))
          :effect (and 
               (at start (not (at ?m ?from)))
               (at end (at ?m ?to))
               (at end (triggered ?p))))

     (:durative-action CLIP
          :parameters
               ()
          :duration (= ?duration 1)
          :condition (and 
               (at start (clipy))
               (at end (clipy)))
          :effect (and 
               (at start (clipx))
               (at end (not (clipx)))
               (at start (not (clipy)))))

     (:durative-action EAT_CHEESE
          :parameters
               (?m - mouse
               ?j - junction
               ?c - cheese
               ?t - trap)
          :duration (= ?duration 5)
          :condition (and 
               (at start (cheese_loc ?c ?j))
               (at start (at ?m ?j))
               (over all (trap_up ?t ?c)))
          :effect (and 
               (at end (eaten ?c ?m))))

     (:durative-action PERFORM
          :parameters
               (?p1 - part ?p2 - contraption)
          :duration (= ?duration (contraption_time ?p1))
          :condition (and
               (at start (causes ?p1 ?p2))
               (at end (clipx))
               (at start (triggered ?p1)))
          :effect (and
               (at start (clipy))
               (at end (triggered ?p2))))

     (:durative-action DROP_TRAP
          :parameters
               (?t - trap
               ?c - cheese)
          :duration (= ?duration (contraption_time ?t))
          :condition (and
               (at start (trap_up ?t ?c))
               (at start (triggered ?t)))
          :effect (and
               (at start (clipy))
               (at end (not (trap_up ?t ?c))))))
