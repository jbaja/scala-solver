
; Version LPG-td-1.0
; Seed 34107397
; Command line: lpg -o domain.pddl -f p01b.pddl -n 4 
; Problem p01b.pddl
; Time 0.02
; Search time 0.02
; Parsing time 0.00
; Mutex time 0.00
; MetricValue 188.00

0.0002:   (PICK-UP TRUCK-2 CITY-LOC-4 PACKAGE-2) [1.0000]
1.0006:   (DRIVE TRUCK-2 CITY-LOC-4 CITY-LOC-3) [45.0000]
46.0008:   (DROP TRUCK-2 CITY-LOC-3 PACKAGE-2) [1.0000]
47.0010:   (PICK-UP TRUCK-2 CITY-LOC-3 PACKAGE-1) [1.0000]
48.0014:   (DRIVE TRUCK-2 CITY-LOC-3 CITY-LOC-2) [50.0000]
98.0016:   (DROP TRUCK-2 CITY-LOC-2 PACKAGE-1) [1.0000]


