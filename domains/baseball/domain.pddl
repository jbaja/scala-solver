(define (domain baseball)
     (:requirements :durative-actions :typing :fluents) 
     (:types base runner pitcher)
     (:constants homebase base1 base2 base3 - base)
     (:predicates
          (at ?r - runner ?b - base)
          (free ?b - base)
          (connected ?b1 ?b2 - base)
          (completed ?r - runner)
          (still_to_run ?r - runner)
          (next_pitcher ?p - pitcher)
          (pitcher_order ?p1 ?p2 - pitcher)
          (free_to_pitch)
          (ball_in_air))
     (:functions
          (pitch_speed ?p - pitcher)
          (hit_speed ?r - runner)
          (run_speed ?r - runner))

     (:durative-action RUN
          :parameters
               (?r - runner
               ?from ?to - base)
          :duration (= ?duration (run_speed ?r))
          :condition (and 
               (at start (connected ?from ?to))
               (at start (at ?r ?from))
               (over all (free ?to))
               (over all (ball_in_air)))
          :effect (and 
               (at start (not (at ?r ?from)))
               (at start (free ?from))
               (at end (not (free ?to)))
               (at end (at ?r ?to))))

     (:durative-action COMPLETE
          :parameters
               (?r - runner)
          :duration (= ?duration (run_speed ?r))
          :condition (and 
               (at start (at ?r base3))
               (over all (ball_in_air)))
          :effect (and 
               (at start (not (at ?r base3)))
               (at start (free base3))
               (at end (completed ?r))))

     (:durative-action STEP_UP
          :parameters
               (?r - runner
               ?p1 ?p2 - pitcher)
          :duration (= ?duration 1)
          :condition (and 
               (at start (free homebase))
               (at start (free_to_pitch))
               (at start (still_to_run ?r))
               (at start (pitcher_order ?p1 ?p2))
               (at start (next_pitcher ?p1)))
          :effect (and 
               (at end (at ?r homebase))
               (at end (not (still_to_run ?r)))
               (at start (not (free homebase)))
               (at end (not (next_pitcher ?p1)))
               (at end (next_pitcher ?p2))))

     (:durative-action HIT
          :parameters
               (?r - runner
               ?p - pitcher)
          :duration (= ?duration (* 4 (* (hit_speed ?r) (pitch_speed ?p))))
          :condition (and 
               (at start (at ?r homebase))
               (at start (next_pitcher ?p))
               (at start (free_to_pitch)))
          :effect (and 
               (at start (ball_in_air))
               (at end (not (ball_in_air)))
               (at start (not (free_to_pitch)))
               (at end (free_to_pitch)))))

