(define (domain Mars)
(:requirements :typing :durative-actions :fluents)
(:types rover waypoint sensor drone objective)

(:predicates (at ?x - rover ?y - waypoint) 
		  (grounded ?d - drone ?w - waypoint)
			(launched ?d - drone)
		  (inAir ?d - drone ?w - waypoint)
		  (can_traverse ?r - rover ?x - waypoint ?y - waypoint)
		    (located ?o - objective ?w - waypoint)
		(accessible ?d - drone ?w - waypoint)
		(light ?w - waypoint)
	
	        (equipped_for_soil_analysis ?r - rover)
             (equipped_for_rock_analysis ?r - rover)
             (equipped_for_lighting ?r - rover)
		 (bulldozer ?r - rover)
		 (equipped_for_identification ?d - drone)

             (have_rock_analysis ?r - rover ?o - objective)
             (have_soil_analysis ?r - rover ?o - objective)

             (lineOfSight ?w - waypoint ?p - waypoint)
           (soil_sample ?o - objective)
	      (rock_sample ?o - objective)

		(identified ?o - objective)
		(localised ?o - objective ?w - waypoint)
		(hidden ?w - waypoint)
             
)

(:functions 
	(timeToTraverse ?x - rover ?y - waypoint ?z - waypoint)
	(flightLife ?d - drone)
	(timeToFly ?d - drone ?x - waypoint ?y - waypoint)
	(identificationTime ?d - drone)
)
	
(:durative-action navigate
:parameters (?x - rover ?y - waypoint ?z - waypoint) 
:duration (= ?duration (timeToTraverse ?x ?y ?z))
:condition (and (over all (can_traverse ?x ?y ?z))
			(at start (at ?x ?y)) 
    	    )
:effect (and (at start (not (at ?x ?y))) (at end (at ?x ?z))
		)
)

(:durative-action launch
 :parameters (?d - drone ?w - waypoint)
 :duration (= ?duration (flightLife ?d))
 :condition (and (at start (grounded ?d ?w)))
 :effect (and (at start (not (grounded ?d ?w)))
			(at start (launched ?d))
			(at start (inAir ?d ?w))
			(at end (not (launched ?d)))
	)
)

(:durative-action fly
:parameters (?x - drone ?y - waypoint ?z - waypoint) 
:duration (= ?duration (timeToFly ?x ?y ?z))
:condition (and (at start (inAir ?x ?y))
			(at start (accessible ?x ?z))
			(over all (launched ?x))
    	    )
:effect (and (at start (not (inAir ?x ?y))) (at end (inAir ?x ?z))
		)
)

(:durative-action sample_soil
:parameters (?x - rover ?o - objective ?p - waypoint)
:duration (= ?duration 10)
:condition (and (over all (at ?x ?p)) (at start (at ?x ?p)) (at start (soil_sample ?o)) (at start (equipped_for_soil_analysis ?x)) (at start (located ?o ?p))
		)
:effect (and  (at end (have_soil_analysis ?x ?o)) 
		)
)

(:durative-action sample_rock
:parameters (?x - rover ?o - objective ?p - waypoint)
:duration (= ?duration 8)
:condition (and (over all (at ?x ?p)) (at start (at ?x ?p)) (at start (rock_sample ?o)) (at start (equipped_for_rock_analysis ?x)) (at start (located ?o ?p))
		)
:effect (and (at end (have_rock_analysis ?x ?o))  
		)
)

(:durative-action shineLight
 :parameters (?x - rover  ?w - waypoint)
 :duration (= ?duration 10)
 :condition (and (over all (at ?x ?w)) (at start (equipped_for_lighting ?x)) )
 :effect (and (at start (light ?w)) (at end (not (light ?w)))
		)
)

(:durative-action push
:parameters (?x - rover ?o - objective ?w - waypoint ?t - waypoint)
:duration (= ?duration (* 2 (timeToTraverse ?x ?w ?t)))
:condition (and (at start (identified ?o)) (at start (at ?x ?w)) (at start (bulldozer ?x)) (at start (located ?o ?w)) (at start (rock_sample ?o))
(at start (can_traverse ?x ?w ?t))
		)
:effect (and (at start (not (at ?x ?w))) (at start (not (located ?o ?w))) (at end (at ?x ?t)) (at end (located ?o ?t))
		(at end (not (localised ?o ?w)))
	)
)


(:durative-action reveal
:parameters (?x - rover ?o - objective ?w - waypoint ?t - waypoint)
:duration (= ?duration (* 2 (timeToTraverse ?x ?w ?t)))
:condition (and (at start (hidden ?w)) (at start (at ?x ?w)) (at start (bulldozer ?x)) (at start (located ?o ?w)) (at start (rock_sample ?o))
(at start (can_traverse ?x ?w ?t))
		)
:effect (and (at start (not (at ?x ?w))) (at start (not (located ?o ?w))) (at end (at ?x ?t)) (at end (located ?o ?t))
		(at end (not (localised ?o ?w)))
	)
)

(:durative-action confirmLocation
 :parameters (?x - rover ?o - objective ?w - waypoint)
 :duration (= ?duration 0.001)
 :condition (and (at start (at ?x ?w)) (at start (located ?o ?w)))
 :effect (at end (localised ?o ?w)))



(:durative-action identify
 :parameters (?d - drone ?o - objective ?w - waypoint)
 :duration (= ?duration (identificationTime ?d))
 :condition (and (over all (inAir ?d ?w)) (over all (launched ?d))
		(at start (equipped_for_identification ?d))
		(over all (light ?w))
		(over all (located ?o ?w))
		(over all (localised ?o ?w))
		)
 :effect (and (at end (identified ?o)))

)



)