(define (domain aggregator)
    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :conditional-effects
    )

    (:types
        activity - object
        activity-profile - object

        battery    - object
        charge-profile - object
        discharge-profile - object

        inflexibleload-tif - object
        unitprice-tif - object
    )

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;the facts about our domain (boolean stuff)
    (:predicates

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;valid profiles for an activity
        (valid-activity-profile ?a - activity ?ac - activity-profile)

        ;constraint on when we can perform an activity
        (can-perform ?a - activity)

        ;goal to complete flexible activities
        (completed ?a - activity)

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;valid configurations for battery charging and discharging
        (valid-charge-profile ?b - battery ?bc - charge-profile)
        (valid-discharge-profile ?b - battery ?bc - discharge-profile)

        ;constraint on when we can use the battery
        (available ?b - battery)

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;predicate indicating that metering is taking place
        (metering)
        (not-metering)

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;predicates for each TIF instance
        (can-switch-inflexible-load ?tifid - inflexibleload-tif)
        (can-switch-unit-price ?tifid - unitprice-tif)
        (completed-inflexible-load-tif ?tifid - inflexibleload-tif)
        (completed-unit-price-tif ?tifid - unitprice-tif)

        (plan-complete)
    )

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;numeric stuff
    (:functions

       ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
       ; Aggregator load functions
       (inflexible-load)     ;total inflexible load = consumed - generated (by all households)
       (flexible-load)       ;flexible + charging - discharging
       (unit-price)          ;marginal cost of 1Kwh in pence (assuming linear function for now)
       (energy-cost)         ;total cost for this plan

		;todo: if there are different costs for renewable energy, and use of battery charging/discharging we would need to split energy-cost up


       ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
       ; Activity functions
       (power-needed ?a - activity ?ap - activity-profile)  ;instant power needed
       (duration-needed ?a - activity ?ap - activity-profile) ;duration needed

       ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
       ; Battery functions
       (energy ?b - battery)  ;the quantity of energy available in a battery
       (max-energy ?b - battery) ;the maximum capacity of a battery
       (charge-power ?b - battery ?cp - charge-profile) ;power needed to charge for a specific profile
       (charge-energy ?b - battery ?cp - charge-profile) ;energy in Ah per minute

       (discharge-power ?b - battery ?cp - discharge-profile) ;power extracted if using a specific discharge profile
       (discharge-energy ?b - battery ?dp - discharge-profile) ;energy in Ah per minute

       (inflexible-load-tif-val ?tifid - inflexibleload-tif)
       (unit-price-tif-val ?tifid - unitprice-tif)
    )

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ; Active inflexible load TIF
   (:durative-action activate-inflexible-load-tif
      :parameters (?tifid - inflexibleload-tif)
      :duration (= ?duration 1)
      :condition (and(at start(can-switch-inflexible-load ?tifid)))
      :effect (and (at start (assign (inflexible-load) (inflexible-load-tif-val ?tifid)))
                   (at end (completed-inflexible-load-tif ?tifid)))
      )

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ; Active unit price TIF
   (:durative-action activate-unit-price-tif
      :parameters (?tifid - unitprice-tif)
      :duration (= ?duration 1)
      :condition (and(at start (can-switch-unit-price ?tifid)))
      :effect (and (at start (assign (unit-price) (unit-price-tif-val ?tifid)))
                   (at end (completed-unit-price-tif ?tifid)))
      )


   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ; Perform activity with a certain profile
   (:durative-action perform
      :parameters (?a - activity ?ap - activity-profile)
      :duration (= ?duration (duration-needed ?a ?ap))
      :condition (and
                   ;   (at start (metering))
               ;       (at start (not(completed ?a))) ; this crashes LPG
                      (at start (valid-activity-profile ?a ?ap))
                      (over all (can-perform ?a)) ; this hangs LPG when finding Mutexes
                 )
      :effect (and
                    (at start (increase (flexible-load) (power-needed ?a ?ap)))
                    (at end (decrease (flexible-load) (power-needed ?a ?ap)))
                    (at end (completed ?a))
              )
   )

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ; Charge battery activity
   (:durative-action charge
      :parameters (?b - battery ?cp - charge-profile)
      :duration (= ?duration 40)
      :condition (and ; (at start (metering)) ;this crashes LPG
                      (at start (valid-charge-profile ?b ?cp))
                      ;(over all (< (energy ?b) (max-energy ?b))) ; this hangs LPG when finding Mutexes
                      ;(over all (available ?b)) ; this hangs LPG when finding Mutexes
                 )
      :effect (and
                  (at start (increase (flexible-load) (charge-power ?b ?cp)))
                  (at end (decrease (flexible-load) (charge-power ?b ?cp)))
                  (increase (energy ?b) (* #t (charge-energy ?b ?cp)))
               )
   )

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ; Discharge battery activity
   (:durative-action discharge
      :parameters (?b - battery ?dp - discharge-profile)
      :duration (= ?duration 60)
      :condition (and ;(at start (metering))  ; this crashes LPG
                      (at start (valid-discharge-profile ?b ?dp))
                     ; (over all (> (energy ?b) 0)) ; this hangs LPG when finding Mutexes
                     ; (over all (available ?b)) ; this hangs LPG when finding Mutexes
                 )
      :effect (and
                  (at start (decrease (flexible-load) (discharge-power ?b ?dp)))
                  (at end (decrease (flexible-load) (discharge-power ?b ?dp)))
                  (decrease (energy ?b) (* #t (discharge-energy ?b ?dp)))
               )
   )

   (:durative-action complete-plan
      :parameters ()
      :duration (= ?duration 1)
      :condition (and (at start (> (energy-cost) 1)))
      :effect (and
                  (at end (plan-complete))
               )
   )

   (:durative-action start-metering
      :parameters()
      :duration (= ?duration 1440)
;      :condition (and (at start (not (metering))))
      :condition (and (at start (not-metering)))
     :effect (and
                (at start (metering))
                ;the following won't work in POPF (due to non-linearity)
                (increase (energy-cost) (* #t (* (unit-price) (+ (inflexible-load) (flexible-load)))))
              )
    )
)