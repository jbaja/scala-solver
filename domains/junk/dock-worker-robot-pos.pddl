;; Specification in PDDL1 of the DWR domain

(define (domain dock-worker-robot-pos)
 (:requirements :strips :typing )
 (:types

  location - object
  pile - object

  robot - object
  crane
  container )

  (:constants
  pallet - container)

 (:predicates
   (adjacent ?l1  ?l2 - location)
   (attached ?p - pile ?l - location)
   (belong ?k - crane ?l - location)

   (at ?r - robot ?l - location)
   (free ?l - location)
   (loaded ?r - robot ?c - container )
   (unloaded ?r - robot)

   (holding ?k - crane ?c - container)
   (empty ?k - crane)

   (in ?c - container ?p - pile)
   (top ?c - container ?p - pile)
   (on ?k1 - container ?k2 - container)
   )

   (:functions
        (electricity) )

;; there are 5 operators in this domain
;; moves a robot between two adjacent locations

 (:action move
     :parameters (?r - robot ?from ?to - location)
     :precondition (and (adjacent ?from ?to)
                      (at ?r ?from) (free ?to))
     :effect (and (at ?r ?to) (free ?from)
		                (not (free ?to)) (not (at ?r ?from)) ))

;; loads an empty robot with a container held by a nearby crane
 (:action load                                
     :parameters (?k - crane ?l - location ?c - container ?r - robot)
     :precondition (and (at ?r ?l) (belong ?k ?l)
                      (holding ?k ?c) (unloaded ?r))
     :effect (and  (loaded ?r ?c) (not (unloaded ?r))
                  (empty ?k) (not (holding ?k ?c))
                  ;(increase (electricity) 5)
                  ))


;; unloads a robot holding a container with a nearby crane
 (:action unload                                 
     :parameters (?k - crane ?l - location ?c - container ?r - robot)
     :precondition (and (belong ?k ?l) (at ?r ?l)
                      (loaded ?r ?c) (empty ?k))
     :effect (and (unloaded ?r) (holding ?k ?c)
                (not (loaded ?r ?c))(not (empty ?k))))

;; takes a container from a pile with a crane
 (:action take
     :parameters (?k - crane ?l - location ?c ?else - container ?p - pile)
     :precondition (and (belong ?k ?l)(attached ?p ?l)
                       (empty ?k) (in ?c ?p) 
                       (top ?c ?p) (on ?c ?else))
     :effect (and (holding ?k ?c) (top ?else ?p)
                (not (in ?c ?p)) (not (top ?c ?p))
                (not (on ?c ?else)) (not (empty ?k))))

;; puts a container held by a crane on a nearby pile
 (:action put                                 
     :parameters (?k - crane ?l - location ?c ?else - container ?p - pile)
     :precondition (and (belong ?k ?l) (attached ?p ?l)
                      (holding ?k ?c) (top ?else ?p))
     :effect (and (in ?c ?p) (top ?c ?p) (on ?c ?else)
                 (not (top ?else ?p)) (not (holding ?k ?c))
                 (empty ?k)))
)