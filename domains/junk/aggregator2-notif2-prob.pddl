(define (problem aggregator-prob)
     (:domain aggregator)

     (:objects
        wash-dishes-h1 wash-dishes-h2 wash-dishes-h3 - activity
        wash-dishes-normal wash-dishes-fast - activity-profile

        battery-s1 battery-s2 - battery
        charge-fast charge-normal - charge-profile
        discharge-fast discharge-normal - discharge-profile

        il-0 il-15 il-30 il-45 il-60 il-300 il-360 il-420 il-540 il-720 il-1020 il-1080 il-1140 il-1200 il-1320 - inflexibleload-tif

        up-0 up-480 up-720 up-1080 up-1200 - unitprice-tif
     )

     (:init
        ;Not metering initially, the start-metering has to start it
        (at 0 (not-metering))
        (at 0.1 (not(not-metering)))

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;Appliance configurations
        ;Household 2 has a normal dishwasher with 2 modes
        (can-perform wash-dishes-h1)

        (valid-activity-profile wash-dishes-h1 wash-dishes-normal)
        (= (power-needed wash-dishes-h1 wash-dishes-normal) 1000)
        (= (duration-needed wash-dishes-h1 wash-dishes-normal) 240)

        (valid-activity-profile wash-dishes-h1 wash-dishes-fast)
        (= (power-needed wash-dishes-h1 wash-dishes-fast) 3000)
        (= (duration-needed wash-dishes-h1 wash-dishes-fast) 90)

        ;todo: deadline using can-perform

        ;Household 2 has a new dishwasher, efficient, fast and with 2 modes
        (can-perform wash-dishes-h2)

        (valid-activity-profile wash-dishes-h2 wash-dishes-normal)
        (= (power-needed wash-dishes-h2 wash-dishes-normal) 900)
        (= (duration-needed wash-dishes-h2 wash-dishes-normal) 200)

        (valid-activity-profile wash-dishes-h2 wash-dishes-fast)
        (= (power-needed wash-dishes-h2 wash-dishes-fast) 2500)
        (= (duration-needed wash-dishes-h2 wash-dishes-fast) 80)

        ;Household 3 has an old dishwasher, inefficient and slow, with one config
        (can-perform wash-dishes-h3)

        (valid-activity-profile wash-dishes-h3 wash-dishes-normal)
        (= (power-needed wash-dishes-h3 wash-dishes-normal) 1200)
        (= (duration-needed wash-dishes-h3 wash-dishes-normal) 110)

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;battery capacity
        (= (max-energy battery-s1)  100000) ;in 100,000mAh
        (= (max-energy battery-s2)   80000) ;in  80,000mAh

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;battery charging and discharging profiles (following Peukert properties)

        ;battery-s1
        (valid-charge-profile battery-s1 charge-normal)
        (= (charge-power battery-s1 charge-normal) 1000) ;1kW instant power
        (= (charge-energy battery-s1 charge-normal) 600)   ;per minute = 10mAh per second

        (valid-charge-profile battery-s1 charge-fast)
        (= (charge-power battery-s1 charge-fast) 2500) ;2.5kW instant power
        (= (charge-energy battery-s1 charge-fast) 1200)   ;20mAh per second

        (valid-discharge-profile battery-s1 discharge-normal)
        (= (discharge-power battery-s1 discharge-normal) 800) ;0.8kW instant power
        (= (discharge-energy battery-s1 discharge-normal) 600)   ;10mAh per second

        (valid-discharge-profile battery-s1 discharge-fast)
        (= (discharge-power battery-s1 discharge-fast) 2000) ;2KW instant power
        (= (discharge-energy battery-s1 discharge-fast) 1200)   ;20mAh per second

        ;battery-s2
        (valid-charge-profile battery-s2 charge-normal)
        (= (charge-power battery-s2 charge-normal) 1400) ;1.4kW instant power
        (= (charge-energy battery-s2 charge-normal) 1200)   ;20mAh per second

        (valid-charge-profile battery-s2 charge-fast)
        (= (charge-power battery-s2 charge-fast) 2700) ;2.7kW instant power
        (= (charge-energy battery-s2 charge-fast) 1800)   ;30mAh per second

        (valid-discharge-profile battery-s2 discharge-normal)
        (= (discharge-power battery-s2 discharge-normal) 1200) ;1.2kW instant power
        (= (discharge-energy battery-s2 discharge-normal) 1200)   ;20mAh per second

        (valid-discharge-profile battery-s2 discharge-fast)
        (= (discharge-power battery-s2 discharge-fast) 1800) ;1.8KW instant power
        (= (discharge-energy battery-s2 discharge-fast) 1800)   ;30mAh per second

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;battery availability (always connected)
        (available battery-s1)
        (available battery-s2)

        ;battery initialisation
        (= (energy battery-s1) 0)
        (= (energy battery-s2) 0)

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        (= (energy-cost) 0)
        (= (flexible-load) 0)

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;fluctuation of inflexible load

        (= (inflexible-load-tif-val il-0) 400000)
        (= (inflexible-load-tif-val il-15) 320000)
        (= (inflexible-load-tif-val il-30) 30000)
        (= (inflexible-load-tif-val il-45) 250000)
        (= (inflexible-load-tif-val il-60) 200000)
        (= (inflexible-load-tif-val il-300) 350000)
        (= (inflexible-load-tif-val il-360) 450000)
        (= (inflexible-load-tif-val il-420) 700000)  ;people waking up, massive load
        (= (inflexible-load-tif-val il-540) 650000)
        (= (inflexible-load-tif-val il-720) 600000)
        (= (inflexible-load-tif-val il-1020) 650000)
        (= (inflexible-load-tif-val il-1080) 720000)
        (= (inflexible-load-tif-val il-1140) 800000)
        (= (inflexible-load-tif-val il-1200) 700000)
        (= (inflexible-load-tif-val il-1320) 500000)

        (at 0 (can-switch-inflexible-load il-0))
        (at 0.1 (not(can-switch-inflexible-load il-0)))
        (at 15 (can-switch-inflexible-load il-15))
        (at 15.1 (not(can-switch-inflexible-load il-15)))
        (at 30 (can-switch-inflexible-load il-30))
        (at 30.1 (not(can-switch-inflexible-load il-30)))
        (at 45 (can-switch-inflexible-load il-45))
        (at 45.1 (not(can-switch-inflexible-load il-45)))
        (at 60 (can-switch-inflexible-load il-60))
        (at 60.1 (not(can-switch-inflexible-load il-60)))
        (at 300 (can-switch-inflexible-load il-300))
        (at 300.1 (not(can-switch-inflexible-load il-300)))
        (at 360 (can-switch-inflexible-load il-360))
        (at 360.1 (not(can-switch-inflexible-load il-360)))
        (at 420 (can-switch-inflexible-load il-420))
        (at 420.1 (not(can-switch-inflexible-load il-420)))
        (at 540 (can-switch-inflexible-load il-540))
        (at 540.1 (not(can-switch-inflexible-load il-540)))
        (at 720 (can-switch-inflexible-load il-720))
        (at 720.1 (not(can-switch-inflexible-load il-720)))
        (at 1020 (can-switch-inflexible-load il-1020))
        (at 1020.1 (not(can-switch-inflexible-load il-1020)))
        (at 1080 (can-switch-inflexible-load il-1080))
        (at 1080.1 (not(can-switch-inflexible-load il-1080)))
        (at 1140 (can-switch-inflexible-load il-1140))
        (at 1140.1 (not(can-switch-inflexible-load il-1140)))
        (at 1200 (can-switch-inflexible-load il-1200))
        (at 1200.1 (not(can-switch-inflexible-load il-1200)))
        (at 1320 (can-switch-inflexible-load il-1320))
        (at 1320.1 (not(can-switch-inflexible-load il-1320)))

        (= (unit-price-tif-val up-0) 20)
        (= (unit-price-tif-val up-480) 14)
        (= (unit-price-tif-val up-720) 12)
        (= (unit-price-tif-val up-1080) 16)
        (= (unit-price-tif-val up-1200) 14)

        (at 0 (can-switch-unit-price up-0))
        (at 0.1 (not(can-switch-unit-price up-0)))
        (at 480 (can-switch-unit-price up-480))
        (at 480.1 (not(can-switch-unit-price up-480)))
        (at 720 (can-switch-unit-price up-720))
        (at 720.1 (not(can-switch-unit-price up-720)))
        (at 1080 (can-switch-unit-price up-1080))
        (at 1080.1 (not(can-switch-unit-price up-1080)))
        (at 1200 (can-switch-unit-price up-1200))
        (at 1200.1 (not(can-switch-unit-price up-1200)))

    )

    (:goal (and
              (metering)
              (completed-inflexible-load-tif il-0)
              (completed-inflexible-load-tif il-15)
              (completed-inflexible-load-tif il-30)
              (completed-inflexible-load-tif il-45)
              (completed-inflexible-load-tif il-60)
              (completed-inflexible-load-tif il-300)
              (completed-inflexible-load-tif il-360)
              (completed-inflexible-load-tif il-420)
              (completed-inflexible-load-tif il-540)
              (completed-inflexible-load-tif il-720)
              (completed-inflexible-load-tif il-1020)
              (completed-inflexible-load-tif il-1080)
              (completed-inflexible-load-tif il-1080)
              (completed-inflexible-load-tif il-1140)
              (completed-inflexible-load-tif il-1140)
              (completed-inflexible-load-tif il-1200)
              (completed-inflexible-load-tif il-1320)

              (completed-unit-price-tif up-0)
              (completed-unit-price-tif up-480)
              (completed-unit-price-tif up-720)
              (completed-unit-price-tif up-1080)
              (completed-unit-price-tif up-1200)

             (completed wash-dishes-h1)
             (completed wash-dishes-h2)
             (completed wash-dishes-h3)
         ;    (plan-complete)
        )
    )
    ;removing the following line crashes LPG with a segmentation fault
    (:metric minimize energy-cost)
)
