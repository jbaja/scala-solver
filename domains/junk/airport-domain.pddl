(define (domain airport)
	(:predicates (plane ?p) (gate ?g) (location ?l) (connected ?s ?e) (parkat ?p ?g) (planeat ?p ?l) (gateat ?g ?l) (runway-free) (gatefree ?g))
	(:action land
			:parameters (?p ?l ?e)
			:precondition (and (plane ?p) (location ?l) (location ?e)(planeat ?p ?l) (connected ?l ?e) (runway-free))
			:effect (and (not (planeat ?p ?l)) (planeat ?p ?e) (not (runway-free)) ))
	(:action exit-runway
			:parameters (?p ?l ?e)
			:precondition (and (plane ?p) (location ?l) (location ?e) (planeat ?p ?l) (not (runway-free)) (connected ?l ?e))
			:effect (and (runway-free) (not(planeat ?p ?l))(planeat ?p ?e)))
	(:action taxi
			:parameters (?p ?l ?e)
			:precondition (and (plane ?p) (location ?l) (location ?e) (planeat ?p ?l) (connected ?l ?e))
			:effect (and (not (planeat ?p ?l)) (planeat ?p ?e)))
	(:action park
			:parameters (?p ?l ?e ?g)
			:precondition (and (plane ?p) (location ?l) (location ?e) (gate ?g) (planeat ?p ?l) (gateat ?g ?e) (connected ?l ?e) (gatefree ?g))
			:effect (and (not (planeat ?p ?l)) (planeat ?p ?e) (parkat ?p ?g) (not( gatefree ?g)))))
			
			
			
			