(define (problem torricelli-p01)

    (:domain torricelli)

    (:requirements :typing :durative-actions :fluents :duration-inequalities)

    (:objects
        ;use the imported types that the methods know about
        bucket1 - Bucket
        tank1 tank2 - Torricelli.Tank
    )

    (:init

        (= (Torricelli.height tank1) 20)
        (= (Torricelli.radius tank1) 10)
        (= (Torricelli.hole-radius tank1) 0.1)

        (= (Torricelli.height tank2) 20)
        (= (Torricelli.radius tank2) 10)
        (= (Torricelli.hole-radius tank2) 0.1)

        (= (volume bucket1) 0)
        (= (capacity bucket1) 6350)
    )

    (:goal
        (and (> (volume bucket1) (- (capacity bucket1) 100))
             (<= (volume bucket1) (capacity bucket1))
             (filled-from tank1 bucket1)
             (filled-from tank2 bucket1)
        )
    )
 )

