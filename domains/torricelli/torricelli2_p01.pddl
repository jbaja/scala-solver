(define (problem torricelli-p01)

    (:domain torricelli2)

    (:requirements :typing :durative-actions :fluents :duration-inequalities)

    (:objects
        ;use the imported types that the methods know about
        bucket1 bucket2 - Bucket
        tank1 - Torricelli.Tank
        hole1 hole2 - Torricelli.Hole
    )

    (:init

        (= (Torricelli.height tank1) 20)
        (= (Torricelli.radius tank1) 10)
        (= (Torricelli.hole-radius tank1 hole1) 0.141421356)
        (= (Torricelli.hole-radius tank1 hole2) 0.141421356)
        (= (Torricelli.hole-height tank1 hole1) 0.1)
        (= (Torricelli.hole-height tank1 hole2) 0.1)

        (= (volume bucket1) 0)
        (= (volume bucket2) 0)
        (= (capacity bucket1) 2100)
        (= (capacity bucket2) 1050)
    )

    (:goal (and
              (>= (volume bucket1) (- (capacity bucket1) 100))
              (<= (volume bucket1) (capacity bucket1))
                (>= (volume bucket2) (- (capacity bucket2) 100))
                (<= (volume bucket2) (capacity bucket2))

       ;      (<= (Torricelli.height tank1) 0.001)
           )
    )
 )

