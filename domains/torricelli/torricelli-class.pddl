(define (module Kcl.Planning.Torricelli)
  (:requirements :typing :fluents)

  ;types introduced by this module
  (:types Tank)

  ;numeric fluents
  (:functions
     (init (height ?t - Tank)) ;the height of the water in the tank
     (init (Torricelli.radius ?t - Torricelli.Tank)) ;the surface area of the water in the tank
     (init (Torricelli.hole-radius ?t - Torricelli.Tank)) ;the surface area of the hole in the tank
  )

  ;numeric fluents that change with respect to time
  (:continuous-functions
     (drain-rate ?t - Tank)
     (height-change ?t - Tank)
  )
)