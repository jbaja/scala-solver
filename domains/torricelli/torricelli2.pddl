(define (domain torricelli2)

    (:requirements :typing :durative-actions :fluents :duration-inequalities)

;    (:classes
;        Torricelli - Kcl.Planning.Torricelli
;    )

    (:types
        Torricelli.Tank
        Torricelli.Hole
        Bucket
    )

    (:predicates
        (filling ?b - Bucket)
        (filled-from ?t - Torricelli.Tank ?b - Bucket)
        (filling-from ?t - Torricelli.Tank ?h - Torricelli.Hole)
    )


    (:functions
                (Torricelli.height ?t - Torricelli.Tank) ;the height of the water in the tank
                (Torricelli.radius ?t - Torricelli.Tank) ;the surface area of the water in the tank
                (Torricelli.hole-radius ?t - Torricelli.Tank ?h - Torricelli.Hole) ;the surface area of the hole in the tank
                (Torricelli.hole-height ?t - Torricelli.Tank ?h - Torricelli.Hole) ;the surface area of the hole in the tank

                (Torricelli.drain-rate ?t - Torricelli.Tank) ;continuous function
                (Torricelli.height-change ?t - Torricelli.Tank) ;continuous function

                (volume ?b - Bucket)  ;how much water do we have in the bucket
                (capacity ?b - Bucket) ;how much water can the bucket hold
    )


   (:durative-action fill
      :parameters (?t - Torricelli.Tank ?h - Torricelli.Hole ?b - Bucket)
      :duration (and (>= ?duration 0))
      :condition (and
                      (over all (>= (Torricelli.height ?t) 0))
                      (over all (<= (volume ?b) (capacity ?b)))
                      (at start (not(filling ?b)))
                      (at start (not(filled-from ?t ?b)))
                      (at start (not(filling-from ?t ?h)))
                 )
      :effect (and
                    (at start (filling ?b))
                    (at start (filling-from ?t ?h))
                    (increase (volume ?b) (* #t (Torricelli.drain-rate ?t ?h)))
                    (decrease (Torricelli.height ?t) (* #t (Torricelli.height-change ?t ?h)))
                    (at end (not(filling ?b)))
                    (at end (not(filling-from ?t ?h)))
                    (at end (filled-from ?t ?b))
              )
    )
)