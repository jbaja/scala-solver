(define (problem torricelli-p01)

    (:domain torricelli)

    (:requirements :typing :durative-actions :fluents :duration-inequalities)

    (:objects
        ;use the imported types that the methods know about
        bucket1 - Bucket
        tank1 tank2 tank3 - Torricelli.Tank
    )

    (:init

        (= (Torricelli.height tank1) 10)
        (= (Torricelli.radius tank1) 5)
        (= (Torricelli.hole-radius tank1) 0.05)

        (= (Torricelli.height tank2) 10)
        (= (Torricelli.radius tank2) 5)
        (= (Torricelli.hole-radius tank2) 0.05)

        (= (Torricelli.height tank3) 10)
        (= (Torricelli.radius tank3) 5)
        (= (Torricelli.hole-radius tank3) 0.05)


        (= (volume bucket1) 0)
        (= (capacity bucket1) 2250)
    )

    (:goal
        (and (> (volume bucket1) (- (capacity bucket1) 100))
             (<= (volume bucket1) (capacity bucket1))
             (filled-from tank1 bucket1)
             (filled-from tank2 bucket1)
             (filled-from tank3 bucket1)


      ;       (> (Torricelli.height tank3) 0)
      ;       (<= (Torricelli.height tank3) 0.001)
        )
    )
 )

