(define (problem pumpcontrol-p1)
     (:domain pumpcontrol)

    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :timed-initial-fluents
    )

     (:objects
        p1 - pump
        f1 f2 - fill-process
        u1 - use-process
     )

     (:init
         (running-f plant-f)
         (complete-f plant-f)
         (running-u plant-u)
         (complete-u plant-u)

         (= (current-flow-rate) 10)

         (= (current-pump-rate p1) 0)
         (= (min-pump-rate p1) 300)
         (= (max-pump-rate p1) 1200)
         (= (pump-flow-step p1) 100)

         (= (flow-rate-f f1) 100)
         (= (max-excess-flow-rate-f f1) 1000)
         (= (min-fill-volume f1) 30000)
         (= (max-fill-volume f1) 35000)
         (= (current-volume f1) 0)
         (dependency-ff plant-f f1)
         (conc-dependency-ff plant-f f1)

         (= (flow-rate-f f2) 150)
         (= (max-excess-flow-rate-f f2) 1500)
         (= (min-fill-volume f2) 20000)
         (= (max-fill-volume f2) 25000)
         (= (current-volume f2) 0)
         (dependency-ff plant-f f2)
         (conc-dependency-ff plant-f f2)

         (= (flow-rate-u u1) 400)
         (= (max-excess-flow-rate-u u1) 1000)
         (= (duration-needed u1) 100)
         (dependency-uu plant-u u1)
         (conc-dependency-fu f1 u1)
     )

     (:goal
        (and (complete-f f1)
             (complete-f f2)
             (complete-u u1)
             (= (current-pump-rate p1) 0)
        )
     )
)