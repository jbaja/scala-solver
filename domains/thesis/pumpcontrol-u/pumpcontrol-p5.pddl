(define (problem pumpcontrol-p5)
     (:domain pumpcontrol)

    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :timed-initial-fluents
    )

     (:objects
        p1 - pump
        f1 f2 f3 - fill-process
        u1 u2 u3 - use-process
     )

     (:init
           (running plant)
           (complete plant)

           (= (current-flow-rate) 10)

           (= (current-pump-rate p1) 0)
           (= (min-pump-rate p1) 300)
           (= (max-pump-rate p1) 1200)
           (= (pump-flow-step p1) 100)

           (= (flow-rate f1) 100)
           (= (max-excess-flow-rate f1) 1000)
           (= (min-fill-volume f1) 30000)
           (= (max-fill-volume f1) 35000)
           (= (current-volume f1) 0)
           (dependency plant f1)
           (conc-dependency plant f1)

           (= (flow-rate f2) 150)
           (= (max-excess-flow-rate f2) 1500)
           (= (min-fill-volume f2) 20000)
           (= (max-fill-volume f2) 25000)
           (= (current-volume f2) 0)
           (dependency plant f2)
           (conc-dependency plant f2)

           (= (flow-rate f3) 150)
           (= (max-excess-flow-rate f3) 1500)
           (= (min-fill-volume f3) 30000)
           (= (max-fill-volume f3) 35000)
           (= (current-volume f3) 0)
           (dependency u1 f3)
           (conc-dependency plant f3)

           (= (flow-rate u1) 400)
           (= (max-excess-flow-rate u1) 1000)
           (= (duration-needed u1) 100)
           (dependency plant u1)
           (conc-dependency f1 u1)

           (= (flow-rate u2) 300)
           (= (max-excess-flow-rate u2) 1500)
           (= (duration-needed u2) 60)
           (dependency f2 u2)
           (conc-dependency plant u2)

         (= (flow-rate u3) 300)
         (= (max-excess-flow-rate u3) 2000)
         (= (duration-needed u3) 80)
         (dependency plant u3)
         (conc-dependency plant u3)
     )

     (:goal
        (and (complete f1)
             (complete f2)
             (complete f3)
             (complete u1)
             (complete u2)
             (complete u3)
             (= (current-pump-rate p1) 0)
        )
     )
)