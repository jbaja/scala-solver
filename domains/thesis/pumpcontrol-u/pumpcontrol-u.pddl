(define (domain pump-control)
    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities)

;more complex version due to type issue on UPMurphi
  (:types pump - object
          fill-process - object
          use-process - object
          )

  (:constants plant-f - fill-process
              plant-u - use-process)

  (:predicates
      (dependency-ff ?dep ?pr - fill-process)
      (dependency-fu ?dep - fill-process ?pr - use-process)
      (dependency-uf ?dep - use-process ?pr - fill-process)
      (dependency-uu ?dep ?pr - use-process)

      (conc-dependency-ff ?dep ?pr - fill-process)
      (conc-dependency-fu ?dep - fill-process ?pr - use-process)
      (conc-dependency-uf ?dep - use-process ?pr - fill-process)
      (conc-dependency-uu ?dep ?pr - use-process)

      (complete-f ?pr - fill-process)
      (running-f ?pr - fill-process)

      (complete-u ?pr - use-process)
      (running-u ?pr - use-process)
  )

  (:functions
    (current-flow-rate)

    (flow-rate-u ?pr - use-process)
    (flow-rate-f ?pr - fill-process)

    (max-excess-flow-rate-u ?pr - use-process)
    (max-excess-flow-rate-f ?pr - fill-process)

    (duration-needed ?u - use-process)
    (min-fill-volume ?f - fill-process)
    (max-fill-volume ?f - fill-process)
    (current-volume ?f - fill-process)

    (current-pump-rate ?pu - pump)
    (min-pump-rate ?pu - pump)
    (max-pump-rate ?pu - pump)
    (pump-flow-step ?pu - pump)
   )

  (:durative-action uu-fill
    :parameters (?dep ?conc-dep - use-process ?f - fill-process)
    :duration (>= ?duration 0)
    :condition (and
      (at start (not(complete-f ?f)))
      (at start (not(running-f ?f)))
      (at start (dependency-uf ?dep ?f))
      (at start (complete-u ?dep))
      (at start (conc-dependency-uf ?conc-dep ?f))
      (over all (running-u ?conc-dep))
      (at start (>= (current-flow-rate) (flow-rate-f ?f)))
      (over all (<= (current-flow-rate) (max-excess-flow-rate-f ?f)))
      (over all (<= (current-volume ?f) (max-fill-volume ?f)))
      (over all (>= (current-flow-rate) 0))
      (at end (>= (current-flow-rate) 0))
      (at end (>= (current-volume ?f) (min-fill-volume ?f)))
    )
    :effect (and
      (at start (running-f ?f))
      (at start (decrease (current-flow-rate) (flow-rate-f ?f)))
      (at end (increase (current-flow-rate) (flow-rate-f ?f)))
      (at end (not (running-f ?f)))
      (at end (complete-f ?f))
      (increase (current-volume ?f) (* #t (+ (current-flow-rate) (flow-rate-f ?f))))
    )
  )

  (:durative-action uf-fill
    :parameters (?dep - use-process ?conc-dep - fill-process ?f - fill-process)
    :duration (>= ?duration 0)
    :condition (and
      (at start (not(complete-f ?f)))
      (at start (not(running-f ?f)))
      (at start (dependency-uf ?dep ?f))
      (at start (complete-u ?dep))
      (at start (conc-dependency-ff ?conc-dep ?f))
      (over all (running-f ?conc-dep))
      (at start (>= (current-flow-rate) (flow-rate-f ?f)))
      (over all (<= (current-flow-rate) (max-excess-flow-rate-f ?f)))
      (over all (<= (current-volume ?f) (max-fill-volume ?f)))
      (over all (>= (current-flow-rate) 0))
      (at end (>= (current-flow-rate) 0))
      (at end (>= (current-volume ?f) (min-fill-volume ?f)))
    )
    :effect (and
      (at start (running-f ?f))
      (at start (decrease (current-flow-rate) (flow-rate-f ?f)))
      (at end (increase (current-flow-rate) (flow-rate-f ?f)))
      (at end (not (running-f ?f)))
      (at end (complete-f ?f))
      (increase (current-volume ?f) (* #t (+ (current-flow-rate) (flow-rate-f ?f))))
    )
  )

  (:durative-action fu-fill
    :parameters (?dep - fill-process ?conc-dep - use-process ?f - fill-process)
    :duration (>= ?duration 0)
    :condition (and
      (at start (not(complete-f ?f)))
      (at start (not(running-f ?f)))
      (at start (dependency-ff ?dep ?f))
      (at start (complete-f ?dep))
      (at start (conc-dependency-uf ?conc-dep ?f))
      (over all (running-u ?conc-dep))
      (at start (>= (current-flow-rate) (flow-rate-f ?f)))
      (over all (<= (current-flow-rate) (max-excess-flow-rate-f ?f)))
      (over all (<= (current-volume ?f) (max-fill-volume ?f)))
      (over all (>= (current-flow-rate) 0))
      (at end (>= (current-flow-rate) 0))
      (at end (>= (current-volume ?f) (min-fill-volume ?f)))
    )
    :effect (and
      (at start (running-f ?f))
      (at start (decrease (current-flow-rate) (flow-rate-f ?f)))
      (at end (increase (current-flow-rate) (flow-rate-f ?f)))
      (at end (not (running-f ?f)))
      (at end (complete-f ?f))
      (increase (current-volume ?f) (* #t (+ (current-flow-rate) (flow-rate-f ?f))))
    )
  )

  (:durative-action ff-fill
    :parameters (?dep ?conc-dep ?f - fill-process)
    :duration (>= ?duration 0)
    :condition (and
      (at start (not(complete-f ?f)))
      (at start (not(running-f ?f)))
      (at start (dependency-ff ?dep ?f))
      (at start (complete-f ?dep))
      (at start (conc-dependency-ff ?conc-dep ?f))
      (over all (running-f ?conc-dep))
      (at start (>= (current-flow-rate) (flow-rate-f ?f)))
      (over all (<= (current-flow-rate) (max-excess-flow-rate-f ?f)))
      (over all (<= (current-volume ?f) (max-fill-volume ?f)))
      (over all (>= (current-flow-rate) 0))
      (at end (>= (current-flow-rate) 0))
      (at end (>= (current-volume ?f) (min-fill-volume ?f)))
    )
    :effect (and
      (at start (running-f ?f))
      (at start (decrease (current-flow-rate) (flow-rate-f ?f)))
      (at end (increase (current-flow-rate) (flow-rate-f ?f)))
      (at end (not (running-f ?f)))
      (at end (complete-f ?f))
      (increase (current-volume ?f) (* #t (+ (current-flow-rate) (flow-rate-f ?f))))
    )
  )

  (:durative-action uu-use
    :parameters (?dep ?conc-dep - use-process ?u - use-process )
    :duration (= ?duration (duration-needed ?u))
    :condition (and
      (at start (not(complete-u ?u)))
      (at start (not(running-u ?u)))
      (at start (dependency-uu ?dep ?u))
      (at start (complete-u ?dep))
      (at start (conc-dependency-uu ?conc-dep ?u))
      (over all (running-u ?conc-dep))
      (at start (>= (current-flow-rate) (flow-rate-u ?u)))
      (over all (<= (current-flow-rate) (max-excess-flow-rate-u ?u)))
      (over all (>= (current-flow-rate) 0))
      (at end (>= (current-flow-rate) 0))
    )
    :effect (and
      (at start (running-u ?u))
      (at start (decrease (current-flow-rate) (flow-rate-u ?u)))
      (at end (increase (current-flow-rate) (flow-rate-u ?u)))
      (at end (not (running-u ?u)))
      (at end (complete-u ?u))
    )
  )

  (:durative-action uf-use
    :parameters (?dep - use-process ?conc-dep - fill-process ?u - use-process )
    :duration (= ?duration (duration-needed ?u))
    :condition (and
      (at start (not(complete-u ?u)))
      (at start (not(running-u ?u)))
      (at start (dependency-uu ?dep ?u))
      (at start (complete-u ?dep))
      (at start (conc-dependency-fu ?conc-dep ?u))
      (over all (running-f ?conc-dep))
      (at start (>= (current-flow-rate) (flow-rate-u ?u)))
      (over all (<= (current-flow-rate) (max-excess-flow-rate-u ?u)))
      (over all (>= (current-flow-rate) 0))
      (at end (>= (current-flow-rate) 0))
    )
    :effect (and
      (at start (running-u ?u))
      (at start (decrease (current-flow-rate) (flow-rate-u ?u)))
      (at end (increase (current-flow-rate) (flow-rate-u ?u)))
      (at end (not (running-u ?u)))
      (at end (complete-u ?u))
    )
  )

  (:durative-action fu-use
    :parameters (?dep - fill-process ?conc-dep - use-process ?u - use-process )
    :duration (= ?duration (duration-needed ?u))
    :condition (and
      (at start (not(complete-u ?u)))
      (at start (not(running-u ?u)))
      (at start (dependency-fu ?dep ?u))
      (at start (complete-f ?dep))
      (at start (conc-dependency-uu ?conc-dep ?u))
      (over all (running-u ?conc-dep))
      (at start (>= (current-flow-rate) (flow-rate-u ?u)))
      (over all (<= (current-flow-rate) (max-excess-flow-rate-u ?u)))
      (over all (>= (current-flow-rate) 0))
      (at end (>= (current-flow-rate) 0))
    )
    :effect (and
      (at start (running-u ?u))
      (at start (decrease (current-flow-rate) (flow-rate-u ?u)))
      (at end (increase (current-flow-rate) (flow-rate-u ?u)))
      (at end (not (running-u ?u)))
      (at end (complete-u ?u))
    )
  )

  (:durative-action ff-use
    :parameters (?dep ?conc-dep - fill-process ?u - use-process )
    :duration (= ?duration (duration-needed ?u))
    :condition (and
      (at start (not(complete-u ?u)))
      (at start (not(running-u ?u)))
      (at start (dependency-fu ?dep ?u))
      (at start (complete-f ?dep))
      (at start (conc-dependency-fu ?conc-dep ?u))
      (over all (running-f ?conc-dep))
      (at start (>= (current-flow-rate) (flow-rate-u ?u)))
      (over all (<= (current-flow-rate) (max-excess-flow-rate-u ?u)))
      (over all (>= (current-flow-rate) 0))
      (at end (>= (current-flow-rate) 0))
    )
    :effect (and
      (at start (running-u ?u))
      (at start (decrease (current-flow-rate) (flow-rate-u ?u)))
      (at end (increase (current-flow-rate) (flow-rate-u ?u)))
      (at end (not (running-u ?u)))
      (at end (complete-u ?u))
    )
  )

  (:action start-pump
    :parameters (?p - pump)
    :precondition (and (= (current-pump-rate ?p) 0))
    :effect (and (assign (current-pump-rate ?p) (min-pump-rate ?p))
                 (increase (current-flow-rate) (min-pump-rate ?p)))
  )

  (:action stop-pump
    :parameters (?p - pump)
    :precondition (and (> (current-pump-rate ?p) 0)
                       (< (current-pump-rate ?p) (+ (min-pump-rate ?p) (pump-flow-step ?p)))
                       )
    :effect (and
          (decrease (current-flow-rate) (current-pump-rate ?p))
          (assign (current-pump-rate ?p) 0))
  )


  (:action increase-pump-flow
    :parameters (?p - pump)
    :precondition (and (>= (current-pump-rate ?p) (min-pump-rate ?p))
                       (<= (current-pump-rate ?p) (+ (max-pump-rate ?p) (pump-flow-step ?p))))
    :effect (and (increase (current-pump-rate ?p) (pump-flow-step ?p))
                 (increase (current-flow-rate) (pump-flow-step ?p)))
  )

  (:action decrease-pump-flow
    :parameters (?p - pump)
    :precondition (and (>= (current-pump-rate ?p) (+ (min-pump-rate ?p) (pump-flow-step ?p))))
    :effect (and (decrease (current-pump-rate ?p) (pump-flow-step ?p))
                 (decrease (current-flow-rate) (pump-flow-step ?p)))
  )
)
