(define (problem nonlinear-aggregator-p3)
     (:domain nonlinear-aggregator)

    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
    )

     (:objects dishwasher1-h1 - activity
               dishwasher1-h2 - activity
               dishwasher1-h3 - activity
               normal - profile
               b1 - Storage.Battery
     )

     (:init
            (= (Aggregator.inflexible-load) 400)
            (= (Aggregator.inflexible-load-dt) 1)
            (= (max-load) 100000)
            (= (min-load) 100)

            (= (Aggregator.flexible-load) 0)
            (= (Aggregator.total-cost) 0)
            (= (Aggregator.unit-price) 1)

            (= (Storage.state-of-charge b1) 30)
            (available b1)

            (charge-profile b1 normal)
            (discharge-profile b1 normal)
            (= (Storage.charge-power b1 normal) 2)
            (= (Storage.full-charge-time b1 normal) 240)
            (= (Storage.discharge-power b1 normal) 1.9)
            (= (Storage.discharge-rate b1 normal) 0.9)

            ;clip for timer envelope action
            (at 0 (can-start-metering))
            (at 0.001 (not (can-start-metering)))

            (at 15 (= (Aggregator.inflexible-load-dt) 2))
            (at 30 (= (Aggregator.inflexible-load-dt) 1))
            (at 100 (= (Aggregator.inflexible-load-dt) 0))

            (activity-profile dishwasher1-h1 normal)
            (= (power-needed dishwasher1-h1 normal) 1.2)
            (= (duration-needed dishwasher1-h1 normal) 420)

            (at 120 (can-perform dishwasher1-h1))
            (at 600 (not(can-perform dishwasher1-h1)))

            (activity-profile dishwasher1-h2 normal)
            (= (power-needed dishwasher1-h2 normal) 1.2)
            (= (duration-needed dishwasher1-h2 normal) 220)

            (at 200 (can-perform dishwasher1-h2))
            (at 600 (not(can-perform dishwasher1-h2)))

            (activity-profile dishwasher1-h3 normal)
            (= (power-needed dishwasher1-h3 normal) 1.3)
            (= (duration-needed dishwasher1-h3 normal) 210)

            (at 100 (can-perform dishwasher1-h3))
            (at 700 (not(can-perform dishwasher1-h3)))

            (at 400 (= (Aggregator.unit-price) 1.2))
     )

 (:goal (and
             (performed dishwasher1-h1)
             (performed dishwasher1-h2)
             (performed dishwasher1-h3)
             (>= (Storage.state-of-charge b1) 90)
         )
 )

 (:metric minimize (Aggregator.total-cost))
)
