(define (problem nonlinear-aggregator-p10)
     (:domain nonlinear-aggregator)

    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
    )

     (:objects dishwasher1-h1 - activity
               dishwasher1-h2 - activity
               dishwasher1-h3 - activity
               dishwasher1-h4 - activity
               dishwasher1-h5 - activity
               dishwasher1-h6 - activity
               dishwasher1-h7 - activity
               dishwasher1-h8 - activity
               dishwasher1-h9 - activity

               normal - profile
               b1 - Storage.Battery
               b2 - Storage.Battery
     )

     (:init
            (= (Aggregator.inflexible-load) 400)
            (= (Aggregator.inflexible-load-dt) 1)
            (= (max-load) 100000)
            (= (min-load) 100)

            (= (Aggregator.flexible-load) 0)
            (= (Aggregator.total-cost) 0)
            (= (Aggregator.unit-price) 1)

            (= (Storage.state-of-charge b1) 30)
            (available b1)

            (charge-profile b1 normal)
            (discharge-profile b1 normal)
            (= (Storage.charge-power b1 normal) 2)
            (= (Storage.full-charge-time b1 normal) 240)
            (= (Storage.discharge-power b1 normal) 1.9)
            (= (Storage.discharge-rate b1 normal) 0.9)

            (= (Storage.state-of-charge b2) 50)
            (available b2)

            (charge-profile b2 normal)
            (discharge-profile b2 normal)
            (= (Storage.charge-power b2 normal) 2)
            (= (Storage.full-charge-time b2 normal) 200)
            (= (Storage.discharge-power b2 normal) 1.9)
            (= (Storage.discharge-rate b2 normal) 0.9)

            ;clip for timer envelope action
            (at 0 (can-start-metering))
            (at 0.001 (not (can-start-metering)))

            (at 100 (= (Aggregator.inflexible-load-dt) -1))
            (at 200 (= (Aggregator.inflexible-load-dt) 0))

            (activity-profile dishwasher1-h1 normal)
            (= (power-needed dishwasher1-h1 normal) 1.2)
            (= (duration-needed dishwasher1-h1 normal) 120)

            (at 120 (can-perform dishwasher1-h1))
            (at 600 (not(can-perform dishwasher1-h1)))

            (activity-profile dishwasher1-h2 normal)
            (= (power-needed dishwasher1-h2 normal) 1.2)
            (= (duration-needed dishwasher1-h2 normal) 220)

            (at 200 (can-perform dishwasher1-h2))
            (at 600 (not(can-perform dishwasher1-h2)))

            (activity-profile dishwasher1-h3 normal)
            (= (power-needed dishwasher1-h3 normal) 1.3)
            (= (duration-needed dishwasher1-h3 normal) 210)

            (can-perform dishwasher1-h3)
            (at 700 (not(can-perform dishwasher1-h3)))

            (activity-profile dishwasher1-h4 normal)
            (= (power-needed dishwasher1-h4 normal) 1.2)
            (= (duration-needed dishwasher1-h4 normal) 180)

            (can-perform dishwasher1-h4)
            (at 800 (not(can-perform dishwasher1-h4)))

            (activity-profile dishwasher1-h5 normal)
            (= (power-needed dishwasher1-h5 normal) 0.8)
            (= (duration-needed dishwasher1-h5 normal) 280)

            (can-perform dishwasher1-h5)
            (at 800 (not(can-perform dishwasher1-h5)))

            (activity-profile dishwasher1-h6 normal)
            (= (power-needed dishwasher1-h6 normal) 0.8)
            (= (duration-needed dishwasher1-h6 normal) 280)

            (can-perform dishwasher1-h6)
            (at 800 (not(can-perform dishwasher1-h6)))

            (activity-profile dishwasher1-h7 normal)
            (= (power-needed dishwasher1-h7 normal) 0.8)
            (= (duration-needed dishwasher1-h7 normal) 200)

            (at 200 (can-perform dishwasher1-h7))
            (at 800 (not(can-perform dishwasher1-h7)))

            (activity-profile dishwasher1-h8 normal)
            (= (power-needed dishwasher1-h8 normal) 0.8)
            (= (duration-needed dishwasher1-h8 normal) 100)

            (can-perform dishwasher1-h8)
            (at 900 (not(can-perform dishwasher1-h8)))

            (activity-profile dishwasher1-h9 normal)
            (= (power-needed dishwasher1-h9 normal) 0.8)
            (= (duration-needed dishwasher1-h9 normal) 100)

            (at 100 (can-perform dishwasher1-h9))
            (at 1000 (not(can-perform dishwasher1-h9)))

        ;    (at 400 (= (Aggregator.unit-price) 1.2))
     )

 (:goal (and
             (performed dishwasher1-h1)
             (performed dishwasher1-h2)
             (performed dishwasher1-h3)
             (performed dishwasher1-h4)
             (performed dishwasher1-h5)
             (performed dishwasher1-h6)
             (performed dishwasher1-h7)
             (performed dishwasher1-h8)
             (performed dishwasher1-h9)
             (>= (Storage.state-of-charge b1) 90)
             (>= (Storage.state-of-charge b2) 80)
         )
 )

 (:metric minimize (Aggregator.total-cost))
)
