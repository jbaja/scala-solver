(define (problem linear-aggregator-p5)
     (:domain linear-aggregator)

    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :conditional-effects
    )

     (:objects dishwasher1-h1 - activity
               dishwasher1-h2 - activity
               dishwasher1-h3 - activity
               dishwasher1-h4 - activity
               dishwasher1-h5 - activity

               normal - profile
               b1 - battery
     )

     (:init
                (= (inflexible-load) 400)
                    (= (inflexible-load-dt) 1)
                    (= (max-load) 10000)
                    (= (min-load) 100)

                    (= (flexible-load) 0)
                    (= (flexible-cost) 0)
                    (= (unit-price) 1.0)

                    (= (state-of-charge b1) 30)
                    (available b1)

                    (charge-profile b1 normal)
                    (discharge-profile b1 normal)
                    (= (charge-power b1 normal) 2)
                    (= (discharge-power b1 normal) 1.9)
                    (= (charge-rate b1 normal) 0.5)
                    (= (discharge-rate b1 normal) 0.8)

                    ;clip for timer envelope action
                    (at 0 (can-start-metering))
                    (at 0.001 (not (can-start-metering)))

                    (at 15 (= (inflexible-load-dt) 1.3))
                    (at 30 (= (inflexible-load-dt) 0))

                    (activity-profile dishwasher1-h1 normal)
                    (= (power-needed dishwasher1-h1 normal) 3.3)
                    (= (duration-needed dishwasher1-h1 normal) 420)

                    (at 120 (can-perform dishwasher1-h1))
                    (at 600 (not(can-perform dishwasher1-h1)))


                  (activity-profile dishwasher1-h2 normal)
                  (= (power-needed dishwasher1-h2 normal) 3.3)
                  (= (duration-needed dishwasher1-h2 normal) 420)

                  (at 300 (can-perform dishwasher1-h2))
                  (at 900 (not(can-perform dishwasher1-h2)))

                 (activity-profile dishwasher1-h3 normal)
                 (= (power-needed dishwasher1-h3 normal) 3.3)
                 (= (duration-needed dishwasher1-h3 normal) 220)

                 (at 600 (can-perform dishwasher1-h3))
                 (at 900 (not(can-perform dishwasher1-h3)))

               (activity-profile dishwasher1-h4 normal)
               (= (power-needed dishwasher1-h4 normal) 2.1)
               (= (duration-needed dishwasher1-h4 normal) 230)

               (can-perform dishwasher1-h4)
               (at 900 (not(can-perform dishwasher1-h4)))

            (activity-profile dishwasher1-h5 normal)
            (= (power-needed dishwasher1-h5 normal) 2.1)
            (= (duration-needed dishwasher1-h5 normal) 330)

            (can-perform dishwasher1-h5)
            (at 2000 (not(can-perform dishwasher1-h5)))

            (at 300 (= (unit-price) 1.4))
     )

 (:goal (and
             (performed dishwasher1-h1)
             (performed dishwasher1-h2)
             (performed dishwasher1-h3)
             (performed dishwasher1-h4)
             (performed dishwasher1-h5)

             (>= (state-of-charge b1) 90)
)
)

 (:metric minimize (flexible-cost))
)
