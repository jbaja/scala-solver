(define (problem linear-aggregator-p1)
     (:domain linear-aggregator)

    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :timed-initial-fluents
    )

     (:objects dishwasher1-h1 - activity
               dishwasher1-h2 - activity
               normal - profile
               b1 - battery
     )

     (:init
            (= (inflexible-load) 400)
            (= (inflexible-load-dt) 1)
            (= (max-load) 10000)
            (= (min-load) 100)

            (= (flexible-load) 0)
            (= (flexible-cost) 0)
            (= (unit-price) 1.0)

            (= (state-of-charge b1) 30)
            (available b1)

            (charge-profile b1 normal)
            (discharge-profile b1 normal)
            (= (charge-power b1 normal) 2)
            (= (discharge-power b1 normal) 1.9)
            (= (charge-rate b1 normal) 0.5)
            (= (discharge-rate b1 normal) 0.8)

            ;clip for timer envelope action
            (at 0 (can-start-metering))
            (at 0.001 (not (can-start-metering)))

            (at 15 (= (inflexible-load-dt) 1.3))
            (at 30 (= (inflexible-load-dt) 0))

            (activity-profile dishwasher1-h1 normal)
            (= (power-needed dishwasher1-h1 normal) 3.3)
            (= (duration-needed dishwasher1-h1 normal) 420)

            (at 120 (can-perform dishwasher1-h1))
            (at 600 (not(can-perform dishwasher1-h1)))

            (at 400 (= (unit-price) 1.4))
     )

 (:goal (and
             (performed dishwasher1-h1)
             (>= (state-of-charge b1) 90)
         )
         )

 (:metric minimize (flexible-cost))
)
