(define (problem project-planner-p1)
     (:domain project-planner)

    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :conditional-effects
    )

     (:objects task1 task2 - task
               r1 r2 - resource
     )

     (:init
         (can-perform r1 task1)
         (= (time-required r1 task1) 6)
         (can-perform r1 task2)
         (= (time-required r1 task2) 10)
         (can-perform r2 task2)
         (= (time-required r2 task2) 7)

         (no-dependency task1)
         (dependency task1 task2)

         (= (total-cost) 0)
         (= (cost r1) 10)
         (= (cost r2) 12)

         (not (can-work r1))
         (not (can-work r2))

         (at 9 (can-work r1))
         (at 9 (can-work r2))

         (at 18 (= (cost r1) 15))
         (at 18 (= (cost r2) 18))

         (at 20 (not(can-work r1)))
         (at 20 (not(can-work r2)))

         (at 33 (= (cost r1) 10))
         (at 33 (= (cost r2) 12))
         (at 33 (can-work r1))
         (at 33 (can-work r2))

         (at 42 (= (cost r1) 15))
         (at 42 (= (cost r2) 18))

         (at 44 (not(can-work r1)))
         (at 44 (not(can-work r2)))
     )

 (:goal (and
            (complete task1)
            (complete task2)
         ))

 (:metric minimize (total-cost))
)
