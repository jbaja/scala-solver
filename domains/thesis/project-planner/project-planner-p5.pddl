(define (problem project-planner-p5)
     (:domain project-planner)

  (:requirements
       :strips
       :typing
       :fluents
       :durative-actions
       :timed-initial-literals
       :negative-preconditions
       :duration-inequalities
       :conditional-effects
  )

 (:objects task1 task2 task3 task4 task5 task6 task7 - task
           m1 m2 - task
           r1 r2 r3 - resource
 )

 (:init
     (can-perform r1 task1)
     (= (time-required r1 task1) 3)

     (can-perform r1 task2)
     (= (time-required r1 task2) 5)

     (can-perform r2 task2)
     (= (time-required r2 task2) 3)

     (can-perform r1 task3)
     (= (time-required r1 task3) 6)

     (can-perform r2 task3)
     (= (time-required r2 task3) 4)

     (can-perform r2 task4)
     (= (time-required r2 task4) 2)

     (can-perform r1 task5)
     (= (time-required r1 task5) 5)

     (can-perform r3 task6)
     (= (time-required r3 task6) 1)

     (can-perform r1 task6)
     (= (time-required r1 task6) 8)

     (can-perform r3 task7)
     (= (time-required r3 task7) 4)

     (can-perform r1 task7)
     (= (time-required r1 task7) 3)

     (can-perform r2 task7)
     (= (time-required r2 task7) 2)


     (no-dependency task1)
     (no-dependency task2)
     (milestone task1 task2 m1)
     (dependency m1 task3)
     (no-dependency task4)
     (milestone task3 task4 m2)
     (dependency m2 task5)
     (dependency task1 task6)
     (no-dependency task7)

     (= (total-cost) 0)
     (= (cost r1) 10)
     (= (cost r2) 12)
     (= (cost r3) 15)

     (not (can-work r1))
     (not (can-work r2))
     (not (can-work r3))

     (at 9 (can-work r1))
     (at 9 (can-work r2))
     (at 9 (can-work r3))

     (at 17 (= (cost r1) 15))
     (at 17 (= (cost r2) 18))
     (at 17 (= (cost r3) 21))

     (at 19 (not(can-work r1)))
     (at 19 (not(can-work r2)))
     (at 19 (not(can-work r3)))

     (at 33 (= (cost r1) 10))
     (at 33 (= (cost r2) 12))
     (at 33 (= (cost r3) 12))
     (at 33 (can-work r1))
     (at 33 (can-work r2))
     (at 33 (can-work r3))

     (at 41 (= (cost r1) 15))
     (at 41 (= (cost r2) 18))
     (at 41 (= (cost r3) 21))

     (at 43 (not(can-work r1)))
     (at 43 (not(can-work r2)))
     (at 43 (not(can-work r3)))

     (at 57 (= (cost r1) 10))
     (at 57 (= (cost r2) 12))
     (at 57 (= (cost r3) 12))
     (at 57 (can-work r1))
     (at 57 (can-work r2))
     (at 57 (can-work r3))

 )

 (:goal (and
            (complete task1)
            (complete task2)
            (complete task3)
            (complete task4)
            (complete task5)
            (complete task6)
            (complete task7)
         ))

 (:metric minimize (total-cost))
)
