(define (problem project-planner-p2)
     (:domain project-planner)

  (:requirements
       :strips
       :typing
       :fluents
       :durative-actions
       :timed-initial-literals
       :negative-preconditions
       :duration-inequalities
       :conditional-effects
  )

 (:objects task1 task2 task3 m1 - task
           r1 r2 - resource
 )

 (:init

     (can-perform r1 task1)
     (= (time-required r1 task1) 3)

     (can-perform r1 task2)
     (= (time-required r1 task2) 5)

     (can-perform r2 task2)
     (= (time-required r2 task2) 3)

     (can-perform r1 task3)
     (= (time-required r1 task3) 6)

     (can-perform r2 task3)
     (= (time-required r2 task3) 4)

     (no-dependency task1)
     (no-dependency task2)
     (milestone task1 task2 m1)
     (dependency m1 task3)

     (= (total-cost) 0)
     (= (cost r1) 10)
     (= (cost r2) 12)

     (not (can-work r1))
     (not (can-work r2))

     (at 9 (can-work r1))
     (at 9 (can-work r2))

     (at 18 (= (cost r1) 15))
     (at 18 (= (cost r2) 18))

     (at 20 (not(can-work r1)))
     (at 20 (not(can-work r2)))

     (at 33 (= (cost r1) 10))
     (at 33 (= (cost r2) 12))
     (at 33 (can-work r1))
     (at 33 (can-work r2))

     (at 42 (= (cost r1) 15))
     (at 42 (= (cost r2) 18))

     (at 44 (not(can-work r1)))
     (at 44 (not(can-work r2)))
 )

 (:goal (and
            (complete task1)
            (complete task2)
            (complete task3)
         ))

 (:metric minimize (total-cost))
)
