(define (problem project-planner-p3)
     (:domain project-planner)

  (:requirements
       :strips
       :typing
       :fluents
       :durative-actions
       :timed-initial-literals
       :negative-preconditions
       :duration-inequalities
       :conditional-effects
  )

 (:objects task1 task2 task3 task4 task5 m1 m2 - task
           r1 r2 - resource
 )

 (:init

     (can-perform r1 task1)
     (= (time-required r1 task1) 3)

     (can-perform r1 task2)
     (= (time-required r1 task2) 5)

     (can-perform r2 task2)
     (= (time-required r2 task2) 3)

     (can-perform r1 task3)
     (= (time-required r1 task3) 6)

     (can-perform r2 task3)
     (= (time-required r2 task3) 4)

     (can-perform r2 task4)
     (= (time-required r2 task4) 2)

     (can-perform r1 task5)
     (= (time-required r1 task5) 5)

     (no-dependency task1)
     (no-dependency task2)
     (milestone task1 task2 m1)
     (dependency m1 task3)
     (no-dependency task4)
     (milestone task3 task4 m2)
     (dependency m2 task5)

     (= (total-cost) 0)
     (= (cost r1) 10)
     (= (cost r2) 12)

     (not (can-work r1))
     (not (can-work r2))

     (at 9 (can-work r1))
     (at 9 (can-work r2))

     (at 17 (= (cost r1) 15))
     (at 17 (= (cost r2) 18))

     (at 19 (not(can-work r1)))
     (at 19 (not(can-work r2)))

     (at 33 (= (cost r1) 10))
     (at 33 (= (cost r2) 12))
     (at 33 (can-work r1))
     (at 33 (can-work r2))

     (at 41 (= (cost r1) 15))
     (at 41 (= (cost r2) 18))

     (at 43 (not(can-work r1)))
     (at 43 (not(can-work r2)))
 )

 (:goal (and
            (complete task1)
            (complete task2)
            (complete task3)
            (complete task4)
            (complete task5)
            (complete m1)
            (complete m2)
         ))

 (:metric minimize (total-cost))
)
