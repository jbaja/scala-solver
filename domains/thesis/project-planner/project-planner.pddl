(define (domain project-planner)
  (:requirements
       :strips
       :typing
       :fluents
       :durative-actions
       :timed-initial-literals
       :negative-preconditions
       :duration-inequalities
       :conditional-effects)

  (:types task resource)

  (:predicates
      (can-perform ?r - resource ?t - task)
      (occupied ?r - resource)
      (can-work ?r - resource)
      (complete ?t - task)

      (milestone ?t1 ?t2 ?m - task)
      (dependency ?pt ?t - task)
      (no-dependency ?t - task))

  (:functions
     (time-required ?r - resource ?t - task)
     (cost ?r - resource)
     (total-cost))

  ;perform a task with no dependencies
  (:durative-action perform-task
    :parameters (?r - resource ?t - task)
    :duration (= ?duration (time-required ?r ?t))
    :condition (and (at start (not (occupied ?r)))
                    (at start (not (complete ?t)))
                    (at start (no-dependency ?t))
                    (at start (can-perform ?r ?t))
                    (at start (can-work ?r))
                    (over all (can-work ?r)))
    :effect (and (at start (occupied ?r))
                 (at end (not (occupied ?r)))
                 (at end (complete ?t))
                 (increase (total-cost) (* #t (cost ?r)))))

  ;perform a task if its dependency has been completed
  (:durative-action perform-dependent-task
    :parameters (?r - resource ?pt ?t - task)
    :duration (= ?duration (time-required ?r ?t))
    :condition (and (at start (not (occupied ?r)))
                    (at start (dependency ?pt ?t))
                    (at start (complete ?pt))
                    (at start (not(complete ?t)))
                    (at start (can-work ?r))
                    (at start (can-perform ?r ?t))
                    (over all (can-work ?r)))
    :effect (and (at start (occupied ?r))
                 (at end (not (occupied ?r)))
                 (at end (complete ?t))
                 (increase (total-cost) (* #t (cost ?r)))))

  ;completes a "dummy" milestone task if two tasks are complete
  (:action join-tasks
    :parameters (?t1 ?t2 ?m - task)
    :precondition (and (milestone ?t1 ?t2 ?m)
                       (complete ?t1)
                       (complete ?t2)
                       (not (complete ?m)))
    :effect (and (complete ?m)))
)
