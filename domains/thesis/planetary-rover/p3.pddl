(define (problem planetary-rover-p3)
  (:domain planetary-rover)

  (:requirements
      :strips
      :typing
      :fluents
      :durative-actions
      :timed-initial-literals
      :negative-preconditions
      :duration-inequalities)

  (:objects
 	  rover1 - rover
   	w0 w1 w2 w3 - waypoint
   	ob1 ob2 ob3 ob4 - objective
 	)

 (:init
    (can-start-measuring-power)
    (at 0.001 (not(can-start-measuring-power)))

    (can-travel w0 w1)
    (= (travel-time w0 w1) 1)

    (can-travel w1 w2)
    (= (travel-time w1 w2) 1)

    (can-travel w2 w0)
    (= (travel-time w2 w0) 1.5)

    (can-travel w1 w3)
    (= (travel-time w1 w3) 1)

    (can-travel w3 w2)
    (= (travel-time w3 w2) 1)

    (at 18 (can-transmit-from w2))
    (at 21 (not (can-transmit-from w2)))

    (at 23 (can-transmit-from w3))
    (at 25 (not (can-transmit-from w3)))

    (rover-at rover1 w0)

    (= (current-power rover1) 0)
    (= (current-solar-power-dt rover1) 0)
    (at 6 (= (current-solar-power-dt rover1) 5))
    (at 10 (= (current-solar-power-dt rover1) 3))
    (at 11 (= (current-solar-power-dt rover1) 0))
    (at 13 (= (current-solar-power-dt rover1) -3))
    (at 14 (= (current-solar-power-dt rover1) -5))
    (at 18 (= (current-solar-power-dt rover1) 0))

    (= (power-needed-to-operate rover1) 1)
    (= (power-needed-for-travel rover1) 0.2)

    (objective-at ob1 w1)

    (= (experiment-duration rover1 ob1) 2)
    (= (power-needed-for-experiment rover1 ob1) 2)
    (= (payload-transmission-duration ob1) 0.1)

    (objective-at ob2 w2)

    (= (experiment-duration rover1 ob2) 2)
    (= (power-needed-for-experiment rover1 ob2) 2)
    (= (payload-transmission-duration ob2) 0.1)

    (objective-at ob3 w2)
    (= (experiment-duration rover1 ob3) 1)
    (= (power-needed-for-experiment rover1 ob3) 3)
    (= (payload-transmission-duration ob3) 0.1)

    (objective-at ob4 w3)
    (= (experiment-duration rover1 ob4) 0.5)
    (= (power-needed-for-experiment rover1 ob4) 1)
    (= (payload-transmission-duration ob4) 0.1)

    (= (power-needed-to-charge rover1) 3)
    (= (battery-charge-rate rover1) 2.2)
    (= (battery-capacity rover1) 300)
    (= (battery-max-power rover1) 25)
    (= (battery-energy rover1) 0)

    (= (power-needed-for-transmission rover1) 1)
    (= (establish-uplink-energy rover1) 2)
    (= (teardown-uplink-energy rover1) 1)
 )

 (:goal (and
     (transmitted-data ob1)
     (transmitted-data ob2)
     (transmitted-data ob3)
     (transmitted-data ob4)
 ))
)