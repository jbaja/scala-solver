(define (domain pump-control)
    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities)

  (:types pump - object
          ind-process - object
          fill-process use-process - ind-process
          )

  (:constants plant - ind-process)

  (:predicates
      (dependency ?dep ?pr - ind-process)
      (conc-dependency ?dep ?pr - ind-process)
      (complete ?pr - ind-process)
      (running ?pr - ind-process)
  )

  (:functions
    (current-flow-rate)

    (flow-rate ?pr - ind-process)
    (max-excess-flow-rate ?pr - ind-process)

    (duration-needed ?u - use-process)
    (min-fill-volume ?f - fill-process)
    (max-fill-volume ?f - fill-process)
    (current-volume ?f - fill-process)

    (current-pump-rate ?pu - pump)
    (min-pump-rate ?pu - pump)
    (max-pump-rate ?pu - pump)
    (pump-flow-step ?pu - pump)
   )

  (:durative-action fill
    :parameters (?dep ?conc-dep - ind-process ?f - fill-process)
    :duration (>= ?duration 0)
    :condition (and
      (at start (not(complete ?f)))
      (at start (not(running ?f)))
      (at start (dependency ?dep ?f))
      (at start (conc-dependency ?conc-dep ?f))
      (at start (complete ?dep))
      (at start (>= (current-flow-rate) (flow-rate ?f)))
      (over all (running ?conc-dep))
      (over all (<= (current-flow-rate) (max-excess-flow-rate ?f)))
      (over all (<= (current-volume ?f) (max-fill-volume ?f)))
      (over all (>= (current-flow-rate) 0))
      (at end (>= (current-flow-rate) 0))
      (at end (>= (current-volume ?f) (min-fill-volume ?f)))
    )
    :effect (and
      (at start (running ?f))
      (at start (decrease (current-flow-rate) (flow-rate ?f)))
      (at end (increase (current-flow-rate) (flow-rate ?f)))
      (at end (not (running ?f)))
      (at end (complete ?f))
      (increase (current-volume ?f) (* #t (+ (current-flow-rate) (flow-rate ?f))))
    )
  )

  (:durative-action use
    :parameters (?dep ?conc-dep - ind-process ?u - use-process )
    :duration (= ?duration (duration-needed ?u))
    :condition (and
      (at start (not(complete ?u)))
      (at start (not(running ?u)))
      (at start (dependency ?dep ?u))
      (at start (conc-dependency ?conc-dep ?u))
      (at start (complete ?dep))
      (at start (>= (current-flow-rate) (flow-rate ?u)))
      (over all (running ?conc-dep))
      (over all (<= (current-flow-rate) (max-excess-flow-rate ?u)))
      (over all (>= (current-flow-rate) 0))
      (at end (>= (current-flow-rate) 0))
    )
    :effect (and
      (at start (running ?u))
      (at start (decrease (current-flow-rate) (flow-rate ?u)))
      (at end (increase (current-flow-rate) (flow-rate ?u)))
      (at end (not (running ?u)))
      (at end (complete ?u))
    )
  )

  (:action start-pump
    :parameters (?p - pump)
    :precondition (and (= (current-pump-rate ?p) 0))
    :effect (and (assign (current-pump-rate ?p) (min-pump-rate ?p))
                 (increase (current-flow-rate) (min-pump-rate ?p)))
  )

  (:action stop-pump
    :parameters (?p - pump)
    :precondition (and (> (current-pump-rate ?p) 0)
                       (< (current-pump-rate ?p) (+ (min-pump-rate ?p) (pump-flow-step ?p)))
                       )
    :effect (and
          (decrease (current-flow-rate) (current-pump-rate ?p))
          (assign (current-pump-rate ?p) 0))
  )


  (:action increase-pump-flow
    :parameters (?p - pump)
    :precondition (and (>= (current-pump-rate ?p) (min-pump-rate ?p))
                       (<= (current-pump-rate ?p) (+ (max-pump-rate ?p) (pump-flow-step ?p))))
    :effect (and (increase (current-pump-rate ?p) (pump-flow-step ?p))
                 (increase (current-flow-rate) (pump-flow-step ?p)))
  )

  (:action decrease-pump-flow
    :parameters (?p - pump)
    :precondition (and (>= (current-pump-rate ?p) (+ (min-pump-rate ?p) (pump-flow-step ?p))))
    :effect (and (decrease (current-pump-rate ?p) (pump-flow-step ?p))
                 (decrease (current-flow-rate) (pump-flow-step ?p)))
  )
)
