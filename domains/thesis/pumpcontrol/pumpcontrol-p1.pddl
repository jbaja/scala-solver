(define (problem pumpcontrol-p1)
     (:domain pumpcontrol)

    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :timed-initial-fluents
    )

     (:objects
        p1  - pump
        f1  - fill-process
        u1  - use-process
     )

     (:init
         (running plant)
         (complete plant)

         (= (current-flow-rate) 10)

         (= (current-pump-rate p1) 0)
         (= (min-pump-rate p1) 300)
         (= (max-pump-rate p1) 1200)
         (= (pump-flow-step p1) 100)

         (= (flow-rate f1) 100)
         (= (max-excess-flow-rate f1) 1000)
         (= (min-fill-volume f1) 30000)
         (= (max-fill-volume f1) 35000)
         (= (current-volume f1) 0)
         (dependency plant f1)
         (conc-dependency plant f1)

         (= (flow-rate u1) 400)
         (= (max-excess-flow-rate u1) 1000)
         (= (duration-needed u1) 100)
         (dependency plant u1)
         (conc-dependency f1 u1)
     )

     (:goal
        (and (complete f1)
             (complete u1)
             (= (current-pump-rate p1) 0)
        )
     )
)