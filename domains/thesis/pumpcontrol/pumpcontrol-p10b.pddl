(define (problem pumpcontrol-p10)
     (:domain pumpcontrol)

    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :timed-initial-fluents
    )

     (:objects
        p1 p2 p3 - pump
        f1 f2 f3 f4 f5 - fill-process
        u1 u2 u3 u4 u5 - use-process
     )

     (:init
           (running plant)
           (complete plant)

           (= (current-flow-rate) 10)

           (= (current-pump-rate p1) 0)
           (= (min-pump-rate p1) 300)
           (= (max-pump-rate p1) 1200)
           (= (pump-flow-step p1) 100)

           (= (current-pump-rate p2) 0)
           (= (min-pump-rate p2) 300)
           (= (max-pump-rate p2) 1000)
           (= (pump-flow-step p2) 100)

           (= (current-pump-rate p3) 0)
           (= (min-pump-rate p3) 250)
           (= (max-pump-rate p3) 1500)
           (= (pump-flow-step p3) 150)

           (= (flow-rate f1) 100)
           (= (max-excess-flow-rate f1) 1000)
           (= (min-fill-volume f1) 30000)
           (= (max-fill-volume f1) 35000)
           (= (current-volume f1) 0)
           (dependency plant f1)
           (conc-dependency plant f1)

           (= (flow-rate f2) 150)
           (= (max-excess-flow-rate f2) 1500)
           (= (min-fill-volume f2) 20000)
           (= (max-fill-volume f2) 25000)
           (= (current-volume f2) 0)
           (dependency plant f2)
           (conc-dependency plant f2)

           (= (flow-rate f3) 150)
           (= (max-excess-flow-rate f3) 1500)
           (= (min-fill-volume f3) 30000)
           (= (max-fill-volume f3) 35000)
           (= (current-volume f3) 0)
           (dependency u1 f3)
           (conc-dependency plant f3)

           (= (flow-rate f4) 200)
           (= (max-excess-flow-rate f4) 1500)
           (= (min-fill-volume f4) 40000)
           (= (max-fill-volume f4) 45000)
           (= (current-volume f4) 0)
           (dependency u2 f4)
           (conc-dependency plant f4)

           (= (flow-rate f5) 100)
           (= (max-excess-flow-rate f5) 1500)
           (= (min-fill-volume f5) 30000)
           (= (max-fill-volume f5) 35000)
           (= (current-volume f5) 0)
           (dependency u5 f5)
           (conc-dependency plant f5)


           (= (flow-rate u1) 400)
           (= (max-excess-flow-rate u1) 1000)
           (= (duration-needed u1) 100)
           (dependency f1 u1)
           (conc-dependency plant u1)

           (= (flow-rate u2) 300)
           (= (max-excess-flow-rate u2) 1500)
           (= (duration-needed u2) 60)
           (dependency f2 u2)
           (conc-dependency plant u2)

         (= (flow-rate u3) 300)
         (= (max-excess-flow-rate u3) 2000)
         (= (duration-needed u3) 80)
         (dependency f1 u3)
         (conc-dependency plant u3)

          (= (flow-rate u4) 200)
          (= (max-excess-flow-rate u4) 2000)
          (= (duration-needed u4) 120)
          (dependency u1 u4)
          (conc-dependency plant u4)

          (= (flow-rate u5) 300)
          (= (max-excess-flow-rate u5) 2000)
          (= (duration-needed u5) 40)
          (dependency u3 u5)
          (conc-dependency f1 u5)
     )

     (:goal
        (and (complete f1)
             (complete f2)
             (complete f3)
             (complete f4)
             (complete f5)
             (complete u1)
             (complete u2)
             (complete u3)
             (complete u4)
             (complete u5)
             (= (current-pump-rate p1) 0)
             (= (current-pump-rate p2) 0)
             (= (current-pump-rate p3) 0)
        )
     )
)