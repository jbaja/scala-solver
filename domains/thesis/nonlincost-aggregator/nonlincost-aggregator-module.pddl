(define (module Kcl.Planning.APS.Aggregator)
  (:requirements :typing :fluents)

  (:functions
    (init (flexible-load))
    (init (inflexible-load))
    (mutable (inflexible-load-dt))
    (mutable (unit-price))
    (mutable (total-cost))

  (:continuous-functions
      (total-cost-dt))
)