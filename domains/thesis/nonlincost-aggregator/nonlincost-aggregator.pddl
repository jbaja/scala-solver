(define (domain nonlinear-aggregator)
    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities)

;    (:classes Aggregator - Kcl.Planning.APS.Aggregator
;              Storage - Kcl.Planning.APS.Storage)

    (:types activity profile battery)

    (:predicates
        (can-start-metering)
        (metered)
        (metering)

        (activity-profile ?a - activity ?p - profile)
        (can-perform ?a - activity)
        (performed ?a - activity)

        (charge-profile ?b - Storage.Battery ?p - profile)
        (discharge-profile ?b - Storage.Battery ?p - profile)
        (available ?b - Storage.Battery)
        (in-use ?b - Storage.Battery)
    )

    (:functions
       (Aggregator.inflexible-load) ;absolute value at last change of gradient
       (Aggregator.inflexible-load-dt) ;gradient of inflexible load
       (Aggregator.flexible-load) ;flexible load
       (Aggregator.unit-price)
       (Aggregator.total-cost)

       (max-load) ;maximum load
       (min-load) ;minimum load

       (power-needed ?a - activity ?p - profile)
       (duration-needed ?a - activity ?p - profile)

       (state-of-charge ?b - battery)
       (charge-power ?b - battery ?p - profile)
       (discharge-power ?b - battery ?p - profile)
       (charge-rate ?b - battery ?p - profile)
       (discharge-rate ?b - battery ?p - profile)
    )

    ;envelope action to perform continuous metering
    (:durative-action meter
      :parameters()
      :duration (>= ?duration 0)
      :condition (and (at start (can-start-metering))
                      (at start (not (metering)))
                      (at start (not (metered)))
                      (over all (>= (+ (Aggregator.inflexible-load) (Aggregator.flexible-load)) (min-load)))
                      (over all (<= (+ (Aggregator.inflexible-load) (Aggregator.flexible-load)) (max-load)))
                 )
      :effect (and (at start (metering))
                   (at start (metered))
                   (at end (not (metering)))
                   (increase (Aggregator.inflexible-load) (* #t (Aggregator.inflexible-load-dt)))
                   (increase (Aggregator.total-cost) (* #t Aggregator.total-cost-dt))
               )
    )


    (:durative-action perform
     :parameters (?a - activity ?p - profile)
     :duration (= ?duration (duration-needed ?a ?p))
     :condition (and
       (at start (not(performed ?a)))
       (at start (activity-profile ?a ?p))
       (over all (can-perform ?a))
       (over all (metering)))
     :effect (and
       (at start (increase (Aggregator.flexible-load) (power-needed ?a ?p)))
       (at end (decrease (Aggregator.flexible-load) (power-needed ?a ?p)))
       (at end (performed ?a))))

   (:durative-action charge
       :parameters (?b - battery ?p - profile)
     :duration (>= ?duration 0)
     :condition (and
       (at start (not (in-use ?b)))
       (at start (charge-profile ?b ?p))
       (over all (<= (state-of-charge ?b) 100))
       (over all (available ?b))
       (over all (metering))
      )
     :effect (and
       (at start (in-use ?b))
       (at start (increase (flexible-load) (charge-power ?b ?p)))
       (at end (decrease (flexible-load) (charge-power ?b ?p)))
       (at end (not (in-use ?b)))
       (increase (state-of-charge ?b) (* #t (charge-rate ?b ?p)))))

     (:durative-action discharge
       :parameters (?b - battery ?p - profile)
       :duration (>= ?duration 0)
       :condition (and
         (at start (not (in-use ?b)))
         (at start (discharge-profile ?b ?p))
         (over all (>= (state-of-charge ?b) 0))
         (over all (available ?b))
         (over all (metering))
 )
       :effect (and
         (at start (in-use ?b))
         (at start (decrease (flexible-load) (discharge-power ?b ?p)))
         (at end (increase (flexible-load) (discharge-power ?b ?p)))
         (at end (not (in-use ?b)))
         (decrease (state-of-charge ?b) (* #t (discharge-rate ?b ?p)))))
)
