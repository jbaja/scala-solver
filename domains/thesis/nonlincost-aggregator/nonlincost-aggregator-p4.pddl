(define (problem nonlincost-aggregator-p4)
     (:domain nonlincost-aggregator)

    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :negative-preconditions
         :duration-inequalities
         :timed-initial-literals
         :timed-initial-fluents
         :class-modules
    )

     (:objects dishwasher1-h1 - activity
               dishwasher1-h2 - activity
               dishwasher1-h3 - activity
               dishwasher1-h4 - activity

               normal - profile
               b1 - battery
     )

     (:init
            (= (Aggregator.inflexible-load) 400)
            (= (Aggregator.inflexible-load-dt) 1)
            (= (max-load) 10000)
            (= (min-load) 100)

            (= (Aggregator.flexible-load) 0)
            (= (Aggregator.total-cost) 0)
            (= (Aggregator.unit-price) 1)

            (= (state-of-charge b1) 30)
            (available b1)

            (charge-profile b1 normal)
            (discharge-profile b1 normal)
            (= (charge-power b1 normal) 2)
            (= (discharge-power b1 normal) 1.8)
            (= (charge-rate b1 normal) 0.5)
            (= (discharge-rate b1 normal) 0.7)

            ;clip for timer envelope action
            (at 0 (can-start-metering))
            (at 0.001 (not (can-start-metering)))

            (at 15 (= (Aggregator.inflexible-load-dt) -1))
            (at 30 (= (Aggregator.inflexible-load-dt) 1.3))
            (at 100 (= (Aggregator.inflexible-load-dt) 0))

            (activity-profile dishwasher1-h1 normal)
            (= (power-needed dishwasher1-h1 normal) 3.3)
            (= (duration-needed dishwasher1-h1 normal) 420)

            (at 120 (can-perform dishwasher1-h1))
            (at 600 (not(can-perform dishwasher1-h1)))

            (activity-profile dishwasher1-h2 normal)
            (= (power-needed dishwasher1-h2 normal) 3.0)
            (= (duration-needed dishwasher1-h2 normal) 320)

            (at 300 (can-perform dishwasher1-h2))
            (at 900 (not(can-perform dishwasher1-h2)))

            (activity-profile dishwasher1-h3 normal)
            (= (power-needed dishwasher1-h3 normal) 4.3)
            (= (duration-needed dishwasher1-h3 normal) 220)

            (at 600 (can-perform dishwasher1-h3))
            (at 900 (not(can-perform dishwasher1-h3)))

            (activity-profile dishwasher1-h4 normal)
            (= (power-needed dishwasher1-h4 normal) 2.1)
            (= (duration-needed dishwasher1-h4 normal) 330)

            (can-perform dishwasher1-h4)
            (at 900 (not(can-perform dishwasher1-h4)))

            (at 400 (= (Aggregator.unit-price) 14))
     )

 (:goal (and
             (performed dishwasher1-h1)
             (performed dishwasher1-h2)
             (performed dishwasher1-h3)
             (performed dishwasher1-h4)

             (>= (state-of-charge b1) 95)
))

 (:metric minimize (Aggregator.total-cost))
)
