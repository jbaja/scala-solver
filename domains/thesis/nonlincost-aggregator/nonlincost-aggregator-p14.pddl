(define (problem nonlincost-aggregator-p14)
     (:domain nonlincost-aggregator)

    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :timed-initial-fluents
         :class-modules
    )

     (:objects dishwasher1-h1 - activity
               dishwasher1-h2 - activity
               dishwasher1-h3 - activity
               dishwasher1-h4 - activity
               dishwasher1-h5 - activity
               dishwasher1-h6 - activity
               dishwasher1-h7 - activity
               dishwasher1-h8 - activity
               dishwasher1-h9 - activity
               dishwasher1-h10 - activity
               dishwasher1-h11 - activity
               dishwasher1-h12 - activity
               dishwasher1-h13 - activity
               dishwasher1-h14 - activity

               normal - profile
               b1 - battery
     )

     (:init
           (= (Aggregator.inflexible-load) 400)
                (= (Aggregator.inflexible-load-dt) 1)
                (= (max-load) 10000)
                (= (min-load) 100)

                (= (Aggregator.flexible-load) 0)
                (= (Aggregator.total-cost) 0)
                (= (Aggregator.unit-price) 1)

                (= (state-of-charge b1) 30)
                (available b1)

                (charge-profile b1 normal)
                (discharge-profile b1 normal)
                (= (charge-power b1 normal) 2)
                (= (discharge-power b1 normal) 1.8)
                (= (charge-rate b1 normal) 0.5)
                (= (discharge-rate b1 normal) 0.7)

                ;clip for timer envelope action
                (at 0 (can-start-metering))
                (at 0.001 (not (can-start-metering)))

                (at 15 (= (Aggregator.inflexible-load-dt) -1))
                (at 30 (= (Aggregator.inflexible-load-dt) 1.3))
                (at 100 (= (Aggregator.inflexible-load-dt) 0))

                (activity-profile dishwasher1-h1 normal)
                (= (power-needed dishwasher1-h1 normal) 3.3)
                (= (duration-needed dishwasher1-h1 normal) 420)

                (at 120 (can-perform dishwasher1-h1))
                (at 600 (not(can-perform dishwasher1-h1)))

                (activity-profile dishwasher1-h2 normal)
                (= (power-needed dishwasher1-h2 normal) 3.0)
                (= (duration-needed dishwasher1-h2 normal) 320)

                (at 300 (can-perform dishwasher1-h2))
                (at 900 (not(can-perform dishwasher1-h2)))

                (activity-profile dishwasher1-h3 normal)
                (= (power-needed dishwasher1-h3 normal) 4.3)
                (= (duration-needed dishwasher1-h3 normal) 220)

                (at 600 (can-perform dishwasher1-h3))
                (at 900 (not(can-perform dishwasher1-h3)))

                (activity-profile dishwasher1-h4 normal)
                (= (power-needed dishwasher1-h4 normal) 2.1)
                (= (duration-needed dishwasher1-h4 normal) 330)

                (can-perform dishwasher1-h4)
                (at 900 (not(can-perform dishwasher1-h4)))

               (activity-profile dishwasher1-h5 normal)
               (= (power-needed dishwasher1-h5 normal) 2.1)
               (= (duration-needed dishwasher1-h5 normal) 330)

               (can-perform dishwasher1-h5)
               (at 2000 (not(can-perform dishwasher1-h5)))

               (activity-profile dishwasher1-h6 normal)
               (= (power-needed dishwasher1-h6 normal) 2.1)
               (= (duration-needed dishwasher1-h6 normal) 350)

               (can-perform dishwasher1-h6)
               (at 2300 (not(can-perform dishwasher1-h6)))

               (activity-profile dishwasher1-h7 normal)
               (= (power-needed dishwasher1-h7 normal) 4.1)
               (= (duration-needed dishwasher1-h7 normal) 130)

               (at 1000 (can-perform dishwasher1-h7))
               (at 2300 (not(can-perform dishwasher1-h7)))

              (activity-profile dishwasher1-h8 normal)
              (= (power-needed dishwasher1-h8 normal) 2.1)
              (= (duration-needed dishwasher1-h8 normal) 430)

              (at 200 (can-perform dishwasher1-h8))
              (at 700 (not(can-perform dishwasher1-h8)))

              (activity-profile dishwasher1-h9 normal)
              (= (power-needed dishwasher1-h9 normal) 3.1)
              (= (duration-needed dishwasher1-h9 normal) 330)

            (can-perform dishwasher1-h9)
            (at 1000 (not(can-perform dishwasher1-h9)))

              (activity-profile dishwasher1-h10 normal)
              (= (power-needed dishwasher1-h10 normal) 2.0)
              (= (duration-needed dishwasher1-h10 normal) 330)

              (at 300 (can-perform dishwasher1-h10))
              (at 1500 (not(can-perform dishwasher1-h10)))

              (activity-profile dishwasher1-h11 normal)
              (= (power-needed dishwasher1-h11 normal) 3.0)
              (= (duration-needed dishwasher1-h11 normal) 130)

              (at 300 (can-perform dishwasher1-h11))
              (at 1500 (not(can-perform dishwasher1-h11)))

             (activity-profile dishwasher1-h12 normal)
             (= (power-needed dishwasher1-h12 normal) 2.5)
             (= (duration-needed dishwasher1-h12 normal) 230)

             (at 400 (can-perform dishwasher1-h12))
             (at 800 (not(can-perform dishwasher1-h12)))

            (activity-profile dishwasher1-h13 normal)
            (= (power-needed dishwasher1-h13 normal) 3.5)
            (= (duration-needed dishwasher1-h13 normal) 130)

            (at 400 (can-perform dishwasher1-h13))
            (at 800 (not(can-perform dishwasher1-h13)))

           (activity-profile dishwasher1-h14 normal)
            (= (power-needed dishwasher1-h14 normal) 2.6)
            (= (duration-needed dishwasher1-h14 normal) 150)

            (at 300 (can-perform dishwasher1-h14))
            (at 700 (not(can-perform dishwasher1-h14)))

            (at 400 (= (Aggregator.unit-price) 1.4))
     )

 (:goal (and
             (performed dishwasher1-h1)
             (performed dishwasher1-h2)
             (performed dishwasher1-h3)
             (performed dishwasher1-h4)
             (performed dishwasher1-h5)
             (performed dishwasher1-h6)
             (performed dishwasher1-h7)
             (performed dishwasher1-h8)
             (performed dishwasher1-h9)
             (performed dishwasher1-h10)
             (performed dishwasher1-h11)
             (performed dishwasher1-h12)
             (performed dishwasher1-h13)
             (performed dishwasher1-h14)

             (>= (state-of-charge b1) 90)))

 (:metric minimize (Aggregator.total-cost))
)
