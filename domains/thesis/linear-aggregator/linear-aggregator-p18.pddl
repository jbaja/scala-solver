(define (problem linear-aggregator-p18)
     (:domain linear-aggregator)

    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :conditional-effects
    )

     (:objects dishwasher1-h1 - activity
               dishwasher1-h2 - activity
               dishwasher1-h3 - activity
               dishwasher1-h4 - activity
               dishwasher1-h5 - activity
               dishwasher1-h6 - activity
               dishwasher1-h7 - activity
               dishwasher1-h8 - activity
               dishwasher1-h9 - activity
               dishwasher1-h10 - activity
               dishwasher1-h11 - activity
               dishwasher1-h12 - activity
               dishwasher1-h13 - activity
               dishwasher1-h14 - activity
               dishwasher1-h15 - activity
               dishwasher1-h16 - activity
               dishwasher1-h17 - activity
               dishwasher1-h18 - activity

               normal - profile
               b1 b2 - battery
     )

     (:init
            (= (inflexible-load) 4000)
            (= (inflexible-load-dt) -6)
            (= (max-load) 1000000)
            (= (min-load) 1000)

            (= (flexible-load) 0)
            (= (flexible-cost) 0)
            (= (unit-price) 0.1)

            (= (state-of-charge b1) 1000)
            (= (capacity b1) 10000)
            (at 1000 (available b1))

            (charge-profile b1 normal)
            (discharge-profile b1 normal)
            (= (charge-power b1 normal) 2000)
            (= (discharge-power b1 normal) 1900)
            (= (charge-rate b1 normal) 200)
            (= (discharge-rate b1 normal) 240)

            (= (state-of-charge b2) 1000)
            (= (capacity b2) 10000)
            (at 400 (available b2))

            (charge-profile b2 normal)
            (discharge-profile b2 normal)
            (= (charge-power b2 normal) 2000)
            (= (discharge-power b2 normal) 1900)
            (= (charge-rate b2 normal) 200)
            (= (discharge-rate b2 normal) 240)


            ;clip for timer envelope action
            (at 0 (can-start-metering))
            (at 0.001 (not (can-start-metering)))

            (at 15 (= (inflexible-load-dt) 13))
            (at 60 (= (inflexible-load-dt) 9))
            (at 120 (= (inflexible-load-dt) -10))
            (at 180 (= (inflexible-load-dt) 20))
            (at 320 (= (inflexible-load-dt) -20))
            (at 440 (= (inflexible-load-dt) 11))
            (at 650 (= (inflexible-load-dt) 0))

            (activity-profile dishwasher1-h1 normal)
            (= (power-needed dishwasher1-h1 normal) 3300)
            (= (duration-needed dishwasher1-h1 normal) 420)

            (at 120 (can-perform dishwasher1-h1))
            (at 600 (not(can-perform dishwasher1-h1)))

            (activity-profile dishwasher1-h2 normal)
            (= (power-needed dishwasher1-h2 normal) 3300)
            (= (duration-needed dishwasher1-h2 normal) 420)

            (at 300 (can-perform dishwasher1-h2))
            (at 900 (not(can-perform dishwasher1-h2)))

            (activity-profile dishwasher1-h3 normal)
            (= (power-needed dishwasher1-h3 normal) 3300)
            (= (duration-needed dishwasher1-h3 normal) 420)

            (at 500 (can-perform dishwasher1-h3))
            (at 1000 (not(can-perform dishwasher1-h3)))

            (activity-profile dishwasher1-h4 normal)
            (= (power-needed dishwasher1-h4 normal) 2100)
            (= (duration-needed dishwasher1-h4 normal) 230)

            (at 100 (can-perform dishwasher1-h4))
            (at 1000 (not(can-perform dishwasher1-h4)))

            (activity-profile dishwasher1-h5 normal)
            (= (power-needed dishwasher1-h5 normal) 2100)
            (= (duration-needed dishwasher1-h5 normal) 330)

            (at 600 (can-perform dishwasher1-h5))
            (at 1200 (not(can-perform dishwasher1-h5)))

            (activity-profile dishwasher1-h6 normal)
            (= (power-needed dishwasher1-h6 normal) 2100)
            (= (duration-needed dishwasher1-h6 normal) 330)

            (at 600 (can-perform dishwasher1-h6))
            (at 1200 (not(can-perform dishwasher1-h6)))

            (activity-profile dishwasher1-h7 normal)
            (= (power-needed dishwasher1-h7 normal) 3100)
            (= (duration-needed dishwasher1-h7 normal) 330)

            (at 200 (can-perform dishwasher1-h7))
            (at 700 (not(can-perform dishwasher1-h7)))

            (activity-profile dishwasher1-h8 normal)
            (= (power-needed dishwasher1-h8 normal) 2100)
            (= (duration-needed dishwasher1-h8 normal) 430)

            (at 200 (can-perform dishwasher1-h8))
            (at 700 (not(can-perform dishwasher1-h8)))

            (activity-profile dishwasher1-h9 normal)
            (= (power-needed dishwasher1-h9 normal) 4000)
            (= (duration-needed dishwasher1-h9 normal) 430)

            (at 600 (can-perform dishwasher1-h9))
            (at 2500 (not(can-perform dishwasher1-h9)))

            (activity-profile dishwasher1-h10 normal)
            (= (power-needed dishwasher1-h10 normal) 2000)
            (= (duration-needed dishwasher1-h10 normal) 330)

            (at 300 (can-perform dishwasher1-h10))
            (at 1500 (not(can-perform dishwasher1-h10)))

            (activity-profile dishwasher1-h11 normal)
            (= (power-needed dishwasher1-h11 normal) 3000)
            (= (duration-needed dishwasher1-h11 normal) 130)

            (at 300 (can-perform dishwasher1-h11))
            (at 1500 (not(can-perform dishwasher1-h11)))

            (activity-profile dishwasher1-h12 normal)
            (= (power-needed dishwasher1-h12 normal) 2500)
            (= (duration-needed dishwasher1-h12 normal) 230)

            (at 400 (can-perform dishwasher1-h12))
            (at 800 (not(can-perform dishwasher1-h12)))

            (activity-profile dishwasher1-h13 normal)
            (= (power-needed dishwasher1-h13 normal) 3500)
            (= (duration-needed dishwasher1-h13 normal) 130)

            (at 400 (can-perform dishwasher1-h13))
            (at 800 (not(can-perform dishwasher1-h13)))

           (activity-profile dishwasher1-h14 normal)
            (= (power-needed dishwasher1-h14 normal) 2600)
            (= (duration-needed dishwasher1-h14 normal) 150)

            (at 300 (can-perform dishwasher1-h14))
            (at 700 (not(can-perform dishwasher1-h14)))

            (activity-profile dishwasher1-h15 normal)
            (= (power-needed dishwasher1-h15 normal) 1600)
            (= (duration-needed dishwasher1-h15 normal) 450)

            (at 300 (can-perform dishwasher1-h15))
            (at 800 (not(can-perform dishwasher1-h15)))

            (activity-profile dishwasher1-h16 normal)
            (= (power-needed dishwasher1-h16 normal) 2600)
            (= (duration-needed dishwasher1-h16 normal) 250)

            (at 300 (can-perform dishwasher1-h16))
            (at 1000 (not(can-perform dishwasher1-h16)))

            (activity-profile dishwasher1-h17 normal)
            (= (power-needed dishwasher1-h17 normal) 1600)
            (= (duration-needed dishwasher1-h17 normal) 250)

            (can-perform dishwasher1-h17)
            (at 1700 (not(can-perform dishwasher1-h17)))

            (activity-profile dishwasher1-h18 normal)
            (= (power-needed dishwasher1-h18 normal) 1800)
            (= (duration-needed dishwasher1-h18 normal) 180)

            (at 300 (can-perform dishwasher1-h18))
            (at 1800 (not(can-perform dishwasher1-h18)))


            (at 400 (= (unit-price) 0.14))
     )

 (:goal (and
             (performed dishwasher1-h1)
             (performed dishwasher1-h2)
             (performed dishwasher1-h3)
             (performed dishwasher1-h4)
             (performed dishwasher1-h5)
             (performed dishwasher1-h6)
             (performed dishwasher1-h7)
             (performed dishwasher1-h8)
             (performed dishwasher1-h9)
             (performed dishwasher1-h10)
             (performed dishwasher1-h11)
             (performed dishwasher1-h12)
             (performed dishwasher1-h13)
             (performed dishwasher1-h14)
             (performed dishwasher1-h15)
             (performed dishwasher1-h16)
             (performed dishwasher1-h17)
             (performed dishwasher1-h18)

             (>= (state-of-charge b1) 5000)
             (>= (state-of-charge b2) 5000)
))

 (:metric minimize (flexible-cost))
)
