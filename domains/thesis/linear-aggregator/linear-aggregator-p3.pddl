(define (problem linear-aggregator-p3)
     (:domain linear-aggregator)

    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :conditional-effects
    )

     (:objects dishwasher1-h1 - activity
               dishwasher1-h2 - activity
               dishwasher1-h3 - activity

               normal - profile
               b1 - battery
     )

     (:init
            (= (inflexible-load) 40000)
            (= (inflexible-load-dt) -666.667)
            (= (max-load) 1000000)
            (= (min-load) 1000)

            (= (flexible-load) 0)
            (= (flexible-cost) 0)
            (= (unit-price) 0.1)

            (= (state-of-charge b1) 1000)
            (= (capacity b1) 10000)
            (available b1)

            (charge-profile b1 normal)
            (discharge-profile b1 normal)
            (= (charge-power b1 normal) 2000)
            (= (discharge-power b1 normal) 1900)
            (= (charge-rate b1 normal) 200)
            (= (discharge-rate b1 normal) 240)

            ;clip for timer envelope action
            (at 0 (can-start-metering))
            (at 0.001 (not (can-start-metering)))

            (at 15 (= (inflexible-load-dt) 133.333))
            (at 30 (= (inflexible-load-dt) 200))

            (activity-profile dishwasher1-h1 normal)
            (= (power-needed dishwasher1-h1 normal) 3300)
            (= (duration-needed dishwasher1-h1 normal) 420)

            (at 120 (can-perform dishwasher1-h1))
            (at 600 (not(can-perform dishwasher1-h1)))

            (activity-profile dishwasher1-h2 normal)
            (= (power-needed dishwasher1-h2 normal) 3300)
            (= (duration-needed dishwasher1-h2 normal) 420)

            (at 120 (can-perform dishwasher1-h2))
            (at 900 (not(can-perform dishwasher1-h2)))

            (activity-profile dishwasher1-h3 normal)
            (= (power-needed dishwasher1-h3 normal) 3300)
            (= (duration-needed dishwasher1-h3 normal) 220)

            (at 600 (can-perform dishwasher1-h3))
            (at 900 (not(can-perform dishwasher1-h3)))

            (at 400 (= (unit-price) 0.04))
     )

 (:goal (and
             (performed dishwasher1-h1)
             (performed dishwasher1-h2)
             (performed dishwasher1-h3)

             (>= (state-of-charge b1) 5000)
;             (<= (flexible-cost) 230000)
))

 (:metric minimize (flexible-cost))
)
