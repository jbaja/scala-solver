(define (problem linear-aggregator-p1)
     (:domain linear-aggregator)

    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :timed-initial-fluents
         :negative-preconditions
         :duration-inequalities
    )

     (:objects dishwasher1-h1 - activity
               dishwasher1-h2 - activity
               normal - profile
               b1 - battery
     )

     (:init
            (= (inflexible-load) 4000)
            (= (inflexible-load-dt) -6)
            (= (max-load) 10000)
            (= (min-load) 100)

            (= (flexible-load) 0)
            (= (flexible-cost) 0)
            (= (unit-price) 10)

            (= (state-of-charge b1) 100)
            (= (capacity b1) 1000)
            (available b1)

            (charge-profile b1 normal)
            (discharge-profile b1 normal)
            (= (charge-power b1 normal) 200)
            (= (discharge-power b1 normal) 190)
            (= (charge-rate b1 normal) 20)
            (= (discharge-rate b1 normal) 24)

            ;clip for timer envelope action
            (at 0 (can-start-metering))
            (at 20 (not (can-start-metering)))

            (at 30 (= (inflexible-load-dt) 13))
            (at 60 (= (inflexible-load-dt) 20))

            (activity-profile dishwasher1-h1 normal)
            (= (power-needed dishwasher1-h1 normal) 330)
            (= (duration-needed dishwasher1-h1 normal) 42)

            (at 120 (can-perform dishwasher1-h1))
            (at 600 (not(can-perform dishwasher1-h1)))

            (at 400 (= (unit-price) 14))
     )

 (:goal (and
             (performed dishwasher1-h1)
             (>= (state-of-charge b1) 500)
         )
         )

; (:metric minimize (flexible-cost))
)
