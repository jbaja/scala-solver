(define (problem linear-aggregator-p1)
     (:domain linear-aggregator)

    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :negative-preconditions
         :duration-inequalities
         :timed-initial-literals
         :timed-initial-fluents)

     (:objects dishwasher1-h1 - activity
               dishwasher1-h2 - activity
               normal - profile
               b1 - battery
     )

     (:init
            (= (inflexible-load) 40000)
            (= (inflexible-load-dt) -6)
            (= (max-load) 1000000)
            (= (min-load) 1000)

            (= (flexible-load) 0)
            (= (flexible-cost) 0)
            (= (unit-price) 10)

            (= (state-of-charge b1) 1000)
            (= (capacity b1) 10000)
            (available b1)

            (charge-profile b1 normal)
            (discharge-profile b1 normal)
            (= (charge-power b1 normal) 2000)
            (= (discharge-power b1 normal) 1900)
            (= (charge-rate b1 normal) 200)
            (= (discharge-rate b1 normal) 240)

            ;clip for timer envelope action
            (at 0 (can-start-metering))
            (at 0.001 (not (can-start-metering)))

            (at 15 (= (inflexible-load-dt) 13))
            (at 30 (= (inflexible-load-dt) 20))

            (activity-profile dishwasher1-h1 normal)
            (= (power-needed dishwasher1-h1 normal) 3300)
            (= (duration-needed dishwasher1-h1 normal) 420)

            (at 120 (can-perform dishwasher1-h1))
            (at 600 (not(can-perform dishwasher1-h1)))

            (at 400 (= (unit-price) 14))
     )

 (:goal (and
             (performed dishwasher1-h1)
             (>= (state-of-charge b1) 5000)
         )
         )

 (:metric minimize (flexible-cost))
)
