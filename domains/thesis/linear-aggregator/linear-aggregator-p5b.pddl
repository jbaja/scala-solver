(define (problem linear-aggregator-p4)
     (:domain linear-aggregator)

    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :conditional-effects
    )

     (:objects dishwasher1-h1 - activity
               dishwasher1-h2 - activity
               dishwasher1-h3 - activity
               dishwasher1-h4 - activity
               dishwasher1-h5 - activity

               normal - profile
               b1 - battery
     )

     (:init
            (= (inflexible-load) 40000)
            (= (inflexible-load-dt) -66.667)
            (= (max-load) 1000000)
            (= (min-load) 1000)

            (= (flexible-load) 0)
            (= (flexible-cost) 0)
            (= (unit-price) 10)

            (= (state-of-charge b1) 1000)
            (= (capacity b1) 10000)
            (available b1)

            (charge-profile b1 normal)
            (discharge-profile b1 normal)
            (= (charge-power b1 normal) 2000)
            (= (discharge-power b1 normal) 1900)
            (= (charge-rate b1 normal) 200)
            (= (discharge-rate b1 normal) 240)

            ;clip for timer envelope action
            (at 0 (can-start-metering))
            (at 0.001 (not (can-start-metering)))

            (at 120 (= (inflexible-load-dt) 0))
    ;        (at 300 (= (inflexible-load-dt) 0))

            (activity-profile dishwasher1-h1 normal)
            (= (power-needed dishwasher1-h1 normal) 3300)
            (= (duration-needed dishwasher1-h1 normal) 420)

            (can-perform dishwasher1-h1)
            (at 900 (not(can-perform dishwasher1-h1)))

            (activity-profile dishwasher1-h2 normal)
            (= (power-needed dishwasher1-h2 normal) 3300)
            (= (duration-needed dishwasher1-h2 normal) 420)

            (can-perform dishwasher1-h2)
            (at 900 (not(can-perform dishwasher1-h2)))

            (activity-profile dishwasher1-h3 normal)
            (= (power-needed dishwasher1-h3 normal) 3300)
            (= (duration-needed dishwasher1-h3 normal) 220)

            (can-perform dishwasher1-h3)
            (at 900 (not(can-perform dishwasher1-h3)))

            (activity-profile dishwasher1-h4 normal)
            (= (power-needed dishwasher1-h4 normal) 2100)
            (= (duration-needed dishwasher1-h4 normal) 230)

            (can-perform dishwasher1-h4)
            (at 900 (not(can-perform dishwasher1-h4)))

            (activity-profile dishwasher1-h5 normal)
            (= (power-needed dishwasher1-h5 normal) 2100)
            (= (duration-needed dishwasher1-h5 normal) 230)

            (can-perform dishwasher1-h5)
            (at 900 (not(can-perform dishwasher1-h5)))


 ;           (at 400 (= (unit-price) 14))
     )

 (:goal (and
             (performed dishwasher1-h1)
             (performed dishwasher1-h2)
             (performed dishwasher1-h3)
             (performed dishwasher1-h4)
             (performed dishwasher1-h5)

;             (>= (state-of-charge b1) 5000)
))

 (:metric minimize (flexible-cost))
)
