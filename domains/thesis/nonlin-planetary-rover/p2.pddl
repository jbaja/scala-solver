(define (problem nonlin-planetary-rover-p2)
  (:domain nonlin-planetary-rover)

  (:requirements
      :strips
      :typing
      :fluents
      :durative-actions
      :timed-initial-literals
      :timed-initial-fluents
      :negative-preconditions
      :duration-inequalities
      :class-modules)

  (:objects
 	  rover1 - rover
 	  battery1 - Storage.Battery
 	  profile - Storage.Profile
   	w0 w1 w2 - waypoint
   	ob1 ob2 ob3 - objective
 	)

 (:init
    (can-start-measuring-power)
    (at 0.001 (not(can-start-measuring-power)))

    (can-travel w0 w1)
    (= (travel-time w0 w1) 1)

    (can-travel w1 w2)
    (= (travel-time w1 w2) 2)

    (can-travel w2 w0)
    (= (travel-time w2 w0) 3)

    (at 18 (can-transmit-from w2))
    (at 21 (not (can-transmit-from w2)))

    (rover-at rover1 w0)

    (= (current-power rover1) 0)
    (= (current-solar-power-dt rover1) 0)
    (at 6 (= (current-solar-power-dt rover1) 5))
    (at 10 (= (current-solar-power-dt rover1) 3))
    (at 11 (= (current-solar-power-dt rover1) 0))
    (at 13 (= (current-solar-power-dt rover1) -3))
    (at 14 (= (current-solar-power-dt rover1) -5))
    (at 18 (= (current-solar-power-dt rover1) 0))

    (= (power-needed-to-operate rover1) 1)
    (= (power-needed-for-travel rover1) 0.2)

    (objective-at ob1 w1)

    (= (experiment-duration rover1 ob1) 2)
    (= (power-needed-for-experiment rover1 ob1) 2)
    (= (payload-transmission-duration ob1) 0.1)

    (objective-at ob2 w2)

    (= (experiment-duration rover1 ob2) 2)
    (= (power-needed-for-experiment rover1 ob2) 2)
    (= (payload-transmission-duration ob2) 0.1)

    (objective-at ob3 w2)
    (= (experiment-duration rover1 ob3) 1)
    (= (power-needed-for-experiment rover1 ob3) 3)
    (= (payload-transmission-duration ob3) 0.1)

    (battery-charge-profile battery1 profile)
    (rover-battery rover1 battery1)
    (= (battery-capacity battery1) 20)
    (= (Storage.charge-power battery1 profile) 2)
    (= (Storage.full-charge-time battery1 profile) 2)
    (= (Storage.state-of-charge battery1) 0)

    (= (power-needed-for-transmission rover1) 1)

 )

 (:goal (and
     (transmitted-data ob1)
     (transmitted-data ob2)
     (transmitted-data ob3)
 ))
)