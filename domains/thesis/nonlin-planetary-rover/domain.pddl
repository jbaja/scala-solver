(define (domain nonlin-planetary-rover)
   (:requirements
        :strips
        :typing
        :fluents
        :durative-actions
        :timed-initial-literals
        :timed-initial-fluents
        :negative-preconditions
        :duration-inequalities
        :class-modules)

;    (:classes
;              Storage - Kcl.Planning.APS.Storage)

   (:types rover waypoint objective Storage.Battery Storage.Profile)

   (:predicates
      (can-start-measuring-power)
      (measuring-power ?r - rover)
      (measured-power ?r - rover)

      (operational ?r - rover)
      (rover-at ?r - rover ?w - waypoint)
      (can-travel ?from ?to - waypoint)
      (running-experiment ?o - objective)

      (objective-at ?o - objective ?w - waypoint)
      (complete ?o - objective)
      (travelling ?r - rover)

      (battery-charge-profile ?b - Storage.Battery ?p - Storage.Profile)
      (rover-battery ?r - rover ?b - Storage.Battery)
      (charging ?b - Storage.Battery)
      (can-transmit-from ?w - waypoint)
      (transmitted-data ?o - objective)
      (uplink-established ?r - rover)
      (transmitting ?r - rover)
   )

   (:functions
     (current-power ?r - rover)
     (power-needed-to-operate ?r - rover)
     (current-battery-power ?r - rover)
     (current-solar-power-dt ?r - rover)
     (power-needed-for-travel ?r - rover)
     (travel-time ?from ?to - waypoint)

     (experiment-duration ?r - rover ?o - objective)
     (power-needed-for-experiment ?r - rover ?o - objective)

     (battery-capacity ?b - Storage.Battery)
     (Storage.state-of-charge ?b - Storage.Battery)
     (Storage.charge-power ?b - Storage.Battery ?p - Storage.Profile)
     (Storage.charge-rate ?b - Storage.Battery ?p - Storage.Profile)
     (Storage.full-charge-time ?b - Storage.Battery ?p - Storage.Profile)

     (payload-transmission-duration ?o - objective)
     (power-needed-for-transmission ?r - rover)
     (establish-uplink-energy ?r - rover)
     (teardown-uplink-energy ?r - rover)
   )

   (:durative-action measure-power
      :parameters (?r - rover)
      :duration (>= ?duration 0)
      :condition (and (at start (not (measuring-power ?r)))
                      (at start (can-start-measuring-power))
                      )
      :effect (and (at start (measuring-power ?r))
                   (at start (measured-power ?r))
                   (increase (current-power ?r) (* #t (current-solar-power-dt ?r)))
                   (at end (not (measuring-power ?r))))
   )


   (:durative-action operate
     :parameters (?r - rover)
     :duration (>= ?duration 0)
     :condition (and
      (at start (not(operational ?r)))
      (at start (>= (current-power ?r) (power-needed-to-operate ?r)))
      (at start (measuring-power ?r))
      (over all (measuring-power ?r))
      (at end (measuring-power ?r))
      (over all (>= (current-power ?r) 0))
     )
     :effect (and
              (at start (decrease (current-power ?r) (power-needed-to-operate ?r)))
              (at start (operational ?r))
              (at end (increase (current-power ?r) (power-needed-to-operate ?r)))
              (at end (not (operational ?r)))
             )
   )

   (:durative-action navigate
     :parameters (?r - rover ?from ?to - waypoint)
     :duration (= ?duration (travel-time ?from ?to))
     :condition (and
                   (at start (rover-at ?r ?from))
                   (at start (can-travel ?from ?to))
                   (at start (>= (current-power ?r) (power-needed-for-travel ?r)))
                   (at start (measuring-power ?r))
                   (at start (not (travelling ?r)))
                   (over all (measuring-power ?r))
                   (at start (operational ?r))
                   (over all (operational ?r))
                   (over all (>= (current-power ?r) 0))
   	            )
   :effect (and
   	            (at start (decrease (current-power ?r) (power-needed-for-travel ?r)))
   	            (at start (travelling ?r))
   	            (at start (not (rover-at ?r ?from)))
   	            (at end (rover-at ?r ?to))
   	            (at end (not (travelling ?r)))
   	            (at end (increase (current-power ?r) (power-needed-for-travel ?r)))
   	       )
   )

   (:durative-action experiment
     :parameters (?r - rover ?w - waypoint ?o - objective)
     :duration (= ?duration (experiment-duration ?r ?o))
     :condition (and
                   (at start (not (complete ?o)))
                   (at start (not (running-experiment ?o)))
                   (at start (objective-at ?o ?w))
                   (at start (rover-at ?r ?w))
                   (at start (operational ?r))
                   (at start (>= (current-power ?r) (power-needed-for-experiment ?r ?o)))
                   (at start (measuring-power ?r))
                   (over all (measuring-power ?r))
                   (at end (measuring-power ?r))
                   (over all (operational ?r))
                   (over all (not (travelling ?r)))
                   (over all (>= (current-power ?r) 0))
   	            )
     :effect (and
                (at start (running-experiment ?o))
   	            (at start (decrease (current-power ?r) (power-needed-for-experiment ?r ?o)))
   	            (at end (increase (current-power ?r) (power-needed-for-experiment ?r ?o)))
   	            (at end (not (running-experiment ?o)))
   	            (at end (complete ?o))
   	            )
   )


   (:durative-action establish-uplink
     :parameters (?r - rover ?b - Storage.Battery ?w - waypoint)
     :duration (>= ?duration 0)
     :condition (and
                   (at start (rover-battery ?r ?b))
                   (at start (rover-at ?r ?w))
                   (at start (can-transmit-from ?w))
                   (at start (not (charging ?b)))
                   (at start (not (uplink-established ?r)))
                   (at start (measuring-power ?r))
                   (over all (measuring-power ?r))
                   (at end (measuring-power ?r))
                   (over all (not (travelling ?r)))
                   (over all (>= (Storage.state-of-charge ?b) 0))
   	            )
     :effect (and
                (at start (uplink-established ?r))
                (decrease (Storage.state-of-charge ?b) (* #t (/ (power-needed-for-transmission ?r) (battery-capacity ?b))))
                (at end (not (uplink-established ?r)))
   	         )
   )

   (:durative-action transmit-experiment-data
      :parameters (?r - rover ?o - objective)
      :duration (= ?duration (payload-transmission-duration ?o))
      :condition (and
               (at start (not (transmitting ?r)))
               (at start (complete ?o))
               (at start (not (transmitted-data ?o)))
               (at start (measuring-power ?r))
               (over all (measuring-power ?r))
               (at end (measuring-power ?r))
               (at start (uplink-established ?r))
               (over all (uplink-established ?r))
              )
      :effect (and
          (at start (transmitting ?r))
          (at end (not (transmitting ?r)))
          (at end (transmitted-data ?o))
      )
   )

   (:durative-action charge
     :parameters (?r - rover ?b - Storage.Battery ?p - Storage.Profile)
     :duration (>= ?duration 0)
     :condition (and
      (at start (rover-battery ?r ?b))
      (at start (battery-charge-profile ?b ?p))
      (at start (>= (current-power ?r) (Storage.charge-power ?b ?p)))
      (at start (<= (Storage.state-of-charge ?b) 99))
      (at start (not (charging ?b)))
      (at start (measuring-power ?r))
      (over all (measuring-power ?r))
      (at end (measuring-power ?r))
      (over all (>= (current-power ?r) 0))
      (over all (<= (Storage.state-of-charge ?b) 100))
     )
     :effect (and
      (at start (charging ?b))
      (at start (increase (current-power ?r) (Storage.charge-power ?b ?p)))
      (increase (Storage.state-of-charge ?b) (* #t (Storage.charge-rate ?b ?p)))
      (at end (decrease (current-power ?r) (Storage.charge-power ?b ?p)))
      (at end (not (charging ?b)))
     )
   )

)
