(define (problem nonlin-planetary-rover-p1)
  (:domain nonlin-planetary-rover)
  (:requirements
       :strips
       :typing
       :fluents
       :durative-actions
       :timed-initial-literals
       :negative-preconditions
       :duration-inequalities
       :class-modules
  )

  (:objects
 	  rover1 - rover
 	  battery1 - Storage.Battery
 	  profile - Storage.Profile
   	w0 w1 w2 - waypoint
   	ob1 - objective
 	)

 (:init
    (can-start-measuring-power)
    (at 0.001 (not(can-start-measuring-power)))

    (can-travel w0 w1)
    (= (travel-time w0 w1) 3)

    (can-travel w1 w2)
    (= (travel-time w1 w2) 2)

    (can-travel w2 w0)
    (= (travel-time w2 w0) 4)

    (at 20 (can-transmit-from w2))
    (at 25 (not (can-transmit-from w2)))

    (rover-at rover1 w0)
    (objective-at ob1 w1)

    (= (current-power rover1) 0)
    (= (current-solar-power-dt rover1) 0)
    (at 6 (= (current-solar-power-dt rover1) 5))
    (at 10 (= (current-solar-power-dt rover1) 2))
    (at 11 (= (current-solar-power-dt rover1) 0))
    (at 13 (= (current-solar-power-dt rover1) -2))
    (at 14 (= (current-solar-power-dt rover1) -5))
    (at 18 (= (current-solar-power-dt rover1) 0))

    (= (power-needed-to-operate rover1) 1)
    (= (current-solar-power-dt rover1) 0)
    (= (power-needed-for-travel rover1) 0.2)

    (= (experiment-duration rover1 ob1) 2)
    (= (power-needed-for-experiment rover1 ob1) 4)
    (= (power-on-battery rover1 ob1) 0)

    (battery-charge-profile battery1 profile)
    (rover-battery rover1 battery1)
    (= (battery-capacity battery1) 20)
    (= (Storage.charge-power battery1 profile) 2)
    (= (Storage.full-charge-time battery1 profile) 2)
    (= (Storage.state-of-charge battery1) 0)

    (= (payload-transmission-duration ob1) 0.1)
    (= (power-needed-for-transmission rover1) 1)
 )

 (:goal
   (and
     (transmitted-data ob1)
    )
    )
 )