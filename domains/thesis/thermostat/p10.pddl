(define (problem thermostat-p10)
  (:domain thermostat)
    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities)

  (:objects
        o1 - Temperature.Object
 	)

  (:init
    (= (planning-horizon) 120)

    (= (Temperature.ambient-temp) -6)
    (= (Temperature.current-temp o1) 15)
    (= (Temperature.cooling-constant o1) 0.054)

    (= (minimum-temp o1) 10)
    (= (maximum-temp o1) 25)
    (= (heating-rate o1) 0.5)
    (= (thermostat-on-temp o1) 12)
    (= (thermostat-minoff-temp o1) 13)

    (at 0 (can-start-monitoring))
    (at 0.001 (not(can-start-monitoring)))

    (at 15 (= (Temperature.ambient-temp) -3))
    (at 30 (= (Temperature.ambient-temp) -1))
    (at 45 (= (Temperature.ambient-temp) 1))
    (at 60 (= (Temperature.ambient-temp) 2))
    (at 75 (= (Temperature.ambient-temp) 3))
    (at 90 (= (Temperature.ambient-temp) 4))
    (at 105 (= (Temperature.ambient-temp) 5))
  )

 (:goal
    (and
        (monitored o1)
        (>= (Temperature.current-temp o1) (minimum-temp o1))
        (<= (Temperature.current-temp o1) (maximum-temp o1))
        )
  )

)