(define (problem thermostat-p1)
  (:domain thermostat)
    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities)

  (:objects
        o1 - Temperature.Object
 	)

  (:init
    (= (planning-horizon) 30)

    (= (Temperature.ambient-temp) -10)
    (= (Temperature.current-temp o1) 17)
    (= (Temperature.cooling-constant o1) 0.054)

    (= (minimum-temp o1) 15)
    (= (maximum-temp o1) 25)
    (= (heating-rate o1) 1)
    (= (thermostat-on-temp o1) 16)
    (= (thermostat-minoff-temp o1) 17)

    (at 0 (can-start-monitoring))
    (at 0.001 (not(can-start-monitoring)))
  )

 (:goal
    (and (monitored o1)
         (>= (Temperature.current-temp o1) (minimum-temp o1))
         (<= (Temperature.current-temp o1) (maximum-temp o1))
         )
  )

)