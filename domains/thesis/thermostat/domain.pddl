(define (domain thermostat)
    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities)

;   (:classes Temperature - Kcl.Planning.Temperature)

   (:types Temperature.Object ;an object which can have its temperature measured
   )

   (:predicates
       (Temperature.heating ?o - Temperature.Object)

       (monitoring ?o - Temperature.Object)
       (monitored ?o - Temperature.Object)
       (can-start-monitoring)
   )

   (:functions
      (Temperature.ambient-temp)
      (Temperature.current-temp ?o - Temperature.Object)
      (Temperature.cooling-constant ?o - Temperature.Object)

      (planning-horizon)
      (minimum-temp ?o - Temperature.Object)
      (maximum-temp ?o - Temperature.Object)
      (thermostat-on-temp ?o - Temporal.Object)
      (thermostat-minoff-temp ?o - Temporal.Object)

      (heating-rate ?o - Temperature.Object)
      (heated ?o - Temperature.Object)
   )

   (:durative-action monitor
        :parameters (?o - Temperature.Object)
        :duration (= ?duration (planning-horizon))
        :condition (and
          (at start (can-start-monitoring))
          (at start (not (monitoring ?o)))
          (over all (>= (Temperature.current-temp ?o) (minimum-temp ?o)))
          (over all (<= (Temperature.current-temp ?o) (maximum-temp ?o)))
          (at end (not (Temperature.heating ?o)))
          )
        :effect (and
          (at start (monitoring ?o))
          (at end (not (monitoring ?o)))
          (at end (monitored ?o))
          (decrease (Temperature.current-temp ?o) (* #t (Temperature.cooling-rate ?o)))
         )
   )

   (:durative-action heat
       :parameters (?o - Temperature.Object)
       :duration (>= ?duration 0)
       :condition (and
          (at start (monitoring ?o))
          (over all (monitoring ?o))
          (at end (monitoring ?o))
          (at start (not (Temperature.heating ?o)))
          (at start (<= (Temperature.current-temp ?o) (thermostat-on-temp ?o)))
          (over all (<= (Temperature.current-temp ?o) (maximum-temp ?o)))
          (at end (>= (Temperature.current-temp ?o) (thermostat-minoff-temp ?o)))
          )
       :effect (and
          (at start (Temperature.heating ?o))
          (increase (Temperature.current-temp ?o) (* #t (heating-rate ?o)))
          (at end (not (Temperature.heating ?o)))
          (at end (not (Temperature.heating ?o)))
       )
   )
)