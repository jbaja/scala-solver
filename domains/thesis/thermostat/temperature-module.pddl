(define (module Kcl.Planning.Temperature)
  (:requirements :typing :fluents)
  (:types Thermal)
  (:functions
    (init (ambient-temp))
    (init (current-temp ?o - Thermal))
    (init (cooling-constant ?o - Thermal))
    (mutable (heating ?o - Thermal)))

  (:continuous-functions
    (cooling-rate ?o - Thermal))
)