(define (problem thermostat-p3)
  (:domain thermostat)
    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities)

  (:objects
        o1 - Temperature.Object
 	)

  (:init
    (= (planning-horizon) 50)

    (= (Temperature.ambient-temp) -5)
    (= (Temperature.current-temp o1) 15)
    (= (Temperature.cooling-constant o1) 0.054)


    (= (minimum-temp o1) 10)
    (= (maximum-temp o1) 25)
    (= (heating-rate o1) 0.5)
    (= (thermostat-on-temp o1) 12)
    (= (thermostat-minoff-temp o1) 13)

    (at 0 (can-start-monitoring))
    (at 0.001 (not(can-start-monitoring)))

    (at 15 (= (Temperature.ambient-temp) -6))
  )

 (:goal
    (and (monitored o1)
         (>= (Temperature.current-temp o1) (minimum-temp o1))
         (<= (Temperature.current-temp o1) (maximum-temp o1))
         )
  )

)