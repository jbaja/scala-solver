(define (problem aggregator-p1)
     (:domain aggregator)

     (:objects
        wash-dishes-h1 wash-dishes-h2 wash-dishes-h3 wash-dishes-h4 - activity
        wash-dishes-normal wash-dishes-fast - activity-profile
     )

     (:init
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;Appliance configurations
        ;Household 2 has a normal dishwasher with 2 modes
        (can-perform wash-dishes-h1)
        (not-performing wash-dishes-h1)
        (not-completed wash-dishes-h1)

        (valid-activity-profile wash-dishes-h1 wash-dishes-normal)
        (= (power-needed wash-dishes-h1 wash-dishes-normal) 1000)
        (= (duration-needed wash-dishes-h1 wash-dishes-normal) 240)

        (valid-activity-profile wash-dishes-h1 wash-dishes-fast)
        (= (power-needed wash-dishes-h1 wash-dishes-fast) 3000)
        (= (duration-needed wash-dishes-h1 wash-dishes-fast) 90)

        (at 1440 (not(can-perform wash-dishes-h1)))

        ;todo: deadline using can-perform

        ;Household 2 has a new dishwasher, efficient, fast and with 2 modes
        (can-perform wash-dishes-h2)
        (not-performing wash-dishes-h2)
        (not-completed wash-dishes-h2)

        (valid-activity-profile wash-dishes-h2 wash-dishes-normal)
        (= (power-needed wash-dishes-h2 wash-dishes-normal) 900)
        (= (duration-needed wash-dishes-h2 wash-dishes-normal) 200)

        (valid-activity-profile wash-dishes-h2 wash-dishes-fast)
        (= (power-needed wash-dishes-h2 wash-dishes-fast) 2500)
        (= (duration-needed wash-dishes-h2 wash-dishes-fast) 80)

        (at 400 (not(can-perform wash-dishes-h2)))

        ;Household 3 has an old dishwasher, inefficient and slow, with one config
        (can-perform wash-dishes-h3)
        (not-performing wash-dishes-h3)
        (not-completed wash-dishes-h3)

        (valid-activity-profile wash-dishes-h3 wash-dishes-normal)
        (= (power-needed wash-dishes-h3 wash-dishes-normal) 1200)
        (= (duration-needed wash-dishes-h3 wash-dishes-normal) 110)

        (at 400 (not(can-perform wash-dishes-h3)))

  ;Household 4 has an old dishwasher, inefficient and slow, with one config
        (can-perform wash-dishes-h4)
        (not-performing wash-dishes-h4)
        (not-completed wash-dishes-h4)

        (valid-activity-profile wash-dishes-h4 wash-dishes-normal)
        (= (power-needed wash-dishes-h4 wash-dishes-normal) 1200)
        (= (duration-needed wash-dishes-h4 wash-dishes-normal) 110)

              (valid-activity-profile wash-dishes-h4 wash-dishes-fast)
                (= (power-needed wash-dishes-h4 wash-dishes-fast) 2500)
                (= (duration-needed wash-dishes-h4 wash-dishes-fast) 80)


        (at 400 (not(can-perform wash-dishes-h4)))


        (= (total-cost) 0)

        ;marginal cost at each hour
        (= (unit-price) 0.30)
        (at 250 (= (unit-price) 0.05))  ;cheapest price during the night
        (at 420 (= (unit-price) 0.20))  ;price goes up at 8am
        (at 720 (= (unit-price) 0.50))  ;price goes a bit lower at noon
        (at 1080 (= (unit-price) 0.70)) ;price peaks at 6pm till 8pm
        (at 1200 (= (unit-price) 1.00)) ;price goes down a bit at 8pm till midnight
    )

    (:goal (and
              (completed wash-dishes-h1)
              (completed wash-dishes-h2)
              (completed wash-dishes-h3)
              (completed wash-dishes-h4)
;              (<= (total-cost) 220000)
        )
    )
)
