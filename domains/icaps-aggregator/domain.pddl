(define (domain aggregator)
    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :conditional-effects
    )

    (:types
        activity - object
        activity-profile - object
    )

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;the facts about our domain (boolean stuff)
    (:predicates

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;valid profiles for an activity
        (valid-activity-profile ?a - activity ?ac - activity-profile)

        ;constraint on when we can perform an activity
        (can-perform ?a - activity)
        (performing ?a - activity) ;activity is being performed
        (not-performing ?a - activity) ;activity is not being performed (negative mirror)

        ;goal to complete flexible activities
        (completed ?a - activity)
        (not-completed ?a - activity) ;negative mirror
     )

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;numeric stuff
    (:functions

       ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
       ; Aggregator load functions
       (unit-price)          ;marginal cost of 1Kwh in pence (assuming linear function for now)
       (total-cost)         ;total cost for this plan


       ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
       ; Activity functions
       (power-needed ?a - activity ?ap - activity-profile)  ;instant power needed
       (duration-needed ?a - activity ?ap - activity-profile) ;duration needed
    )

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ; Perform activity with a certain profile
   (:durative-action perform
      :parameters (?a - activity ?ap - activity-profile)
      :duration (= ?duration (duration-needed ?a ?ap))
      :condition (and
                      (at start (not-performing ?a))
                      (at start (not-completed ?a))
                      (at start (valid-activity-profile ?a ?ap))
                      (over all (can-perform ?a))
                 )
      :effect (and
                    (at start (not(not-performing ?a)))
                    (increase (total-cost) (* #t (* (unit-price) (power-needed ?a ?ap))))
                    (at end (not(not-completed ?a))       )
                    (at end (completed ?a))
                    (at end (not-performing ?a))
              )
   )
)
