(define (domain aggregator)
    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :conditional-effects
    )

    (:types
        activity - object
        activity-profile - object

        battery    - object
        charge-profile - object
        discharge-profile - object
    )

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;the facts about our domain (boolean stuff)
    (:predicates

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;valid profiles for an activity
        (valid-activity-profile ?a - activity ?ac - activity-profile)

        ;constraint on when we can perform an activity
        (can-perform ?a - activity)
        (performing ?a - activity) ;activity is being performed
        (not-performing ?a - activity) ;activity is not being performed (negative mirror)

        ;goal to complete flexible activities
        (completed ?a - activity)
        (not-completed ?a - activity) ;negative mirror

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;valid configurations for battery charging and discharging
        (valid-charge-profile ?b - battery ?bc - charge-profile)
        (valid-discharge-profile ?b - battery ?bc - discharge-profile)

        ;constraint on when we can use the battery
        (available ?b - battery)
        (in-use ?b - battery) ;battery is in use
        (not-in-use ?b - battery) ;negative mirror
     )

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;numeric stuff
    (:functions

       ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
       ; Aggregator load functions
       (unit-price)          ;marginal cost of 1Kwh in pence (assuming linear function for now)
       (total-cost)         ;total cost for this plan


       ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
       ; Activity functions
       (power-needed ?a - activity ?ap - activity-profile)  ;instant power needed
       (duration-needed ?a - activity ?ap - activity-profile) ;duration needed

       ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
       ; Battery functions
       (energy ?b - battery)  ;the quantity of energy available in a battery
       (max-energy ?b - battery) ;the maximum capacity of a battery
       (charge-power ?b - battery ?cp - charge-profile) ;power needed to charge for a specific profile
       (charge-energy ?b - battery ?cp - charge-profile) ;energy in Ah per minute

       (discharge-power ?b - battery ?cp - discharge-profile) ;power extracted if using a specific discharge profile
       (discharge-energy ?b - battery ?dp - discharge-profile) ;energy in Ah per minute
    )

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ; Perform activity with a certain profile
   (:durative-action perform
      :parameters (?a - activity ?ap - activity-profile)
      :duration (= ?duration (duration-needed ?a ?ap))
      :condition (and
                      (at start (not-performing ?a))
                      (at start (not-completed ?a))
                      (at start (valid-activity-profile ?a ?ap))
                      (over all (can-perform ?a))
                 )
      :effect (and
                    (at start (not(not-performing ?a)))
                    (increase (total-cost) (* #t (* (unit-price) (power-needed ?a ?ap))))
                    (at end (completed ?a))
                    (at end (not-performing ?a))
              )
   )

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ; Charge battery activity
   (:durative-action charge
      :parameters (?b - battery ?cp - charge-profile)
      :duration (<= ?duration (/ (- (max-energy ?b) (energy ?b)) (charge-energy ?b ?cp)))
      :condition (and
                      (at start (not-in-use ?b))
                      (at start (valid-charge-profile ?b ?cp))
                      (at start (<= (energy ?b) (max-energy ?b)))
                      (over all (available ?b))
                 )
      :effect (and
                  (at start (in-use ?b))
                  (at start (not(not-in-use ?b)))
                  (increase (total-cost) (* #t (* (unit-price) (charge-energy ?b ?cp))))
                  (increase (energy ?b) (* #t (charge-energy ?b ?cp)))
                  (at end (not(in-use ?b)))
                  (at end (not-in-use ?b))
               )
   )

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ; Discharge battery activity
   (:durative-action discharge
      :parameters (?b - battery ?dp - discharge-profile)
      :duration (<= ?duration (/ (energy ?b) (discharge-energy ?b ?dp)))
      :condition (and
                      (at start (not-in-use ?b))
                      (at start (valid-discharge-profile ?b ?dp))
                      (at start (> (energy ?b) 0))
                      (over all (available ?b))
                 )
      :effect (and
                  (at start (in-use ?b))
                  (at start (not(not-in-use ?b)))
                  (decrease (total-cost) (* #t (* (unit-price) (discharge-energy ?b ?dp))))
                  (decrease (energy ?b) (* #t (discharge-energy ?b ?dp)))
                  (at end (not(in-use ?b)))
                  (at end (not-in-use ?b))
               )
   )
)
