(define (problem aggregator-p1)
     (:domain aggregator)

     (:objects
        wash-dishes-h1 wash-dishes-h2 wash-dishes-h3 - activity
        wash-dishes-normal wash-dishes-fast - activity-profile

        battery-s1 battery-s2 - battery
        charge-fast charge-normal - charge-profile
        discharge-fast discharge-normal - discharge-profile
     )

     (:init
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;Appliance configurations
        ;Household 2 has a normal dishwasher with 2 modes
        (can-perform wash-dishes-h1)
        (not-performing wash-dishes-h1)
        (not-completed wash-dishes-h1)

        (valid-activity-profile wash-dishes-h1 wash-dishes-normal)
        (= (power-needed wash-dishes-h1 wash-dishes-normal) 1000)
        (= (duration-needed wash-dishes-h1 wash-dishes-normal) 240)

        (valid-activity-profile wash-dishes-h1 wash-dishes-fast)
        (= (power-needed wash-dishes-h1 wash-dishes-fast) 3000)
        (= (duration-needed wash-dishes-h1 wash-dishes-fast) 90)

        (at 1440 (not(can-perform wash-dishes-h1)))

        ;todo: deadline using can-perform

        ;Household 2 has a new dishwasher, efficient, fast and with 2 modes
        (can-perform wash-dishes-h2)
        (not-performing wash-dishes-h2)
        (not-completed wash-dishes-h2)

        (valid-activity-profile wash-dishes-h2 wash-dishes-normal)
        (= (power-needed wash-dishes-h2 wash-dishes-normal) 900)
        (= (duration-needed wash-dishes-h2 wash-dishes-normal) 200)

        (valid-activity-profile wash-dishes-h2 wash-dishes-fast)
        (= (power-needed wash-dishes-h2 wash-dishes-fast) 2500)
        (= (duration-needed wash-dishes-h2 wash-dishes-fast) 80)

        (at 400 (not(can-perform wash-dishes-h2)))

        ;Household 3 has an old dishwasher, inefficient and slow, with one config
        (can-perform wash-dishes-h3)
        (not-performing wash-dishes-h3)
        (not-completed wash-dishes-h3)

        (valid-activity-profile wash-dishes-h3 wash-dishes-normal)
        (= (power-needed wash-dishes-h3 wash-dishes-normal) 1200)
        (= (duration-needed wash-dishes-h3 wash-dishes-normal) 110)

        (at 400 (not(can-perform wash-dishes-h3)))

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;battery capacity

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ;battery charging and discharging profiles (following Peukert properties)

        ;battery-s1
        (valid-charge-profile battery-s1 charge-normal)
        (= (charge-power battery-s1 charge-normal) 1000) ;1kW instant power
        (= (charge-energy battery-s1 charge-normal) 600)   ;per minute = 10mAh per second

        (valid-charge-profile battery-s1 charge-fast)
        (= (charge-power battery-s1 charge-fast) 2500) ;2.5kW instant power
        (= (charge-energy battery-s1 charge-fast) 1200)   ;20mAh per second

        (valid-discharge-profile battery-s1 discharge-normal)
        (= (discharge-power battery-s1 discharge-normal) 800) ;0.8kW instant power
        (= (discharge-energy battery-s1 discharge-normal) 600)   ;10mAh per second

        (valid-discharge-profile battery-s1 discharge-fast)
        (= (discharge-power battery-s1 discharge-fast) 2000) ;2KW instant power
        (= (discharge-energy battery-s1 discharge-fast) 1200)   ;20mAh per second

        ;battery-s2
        (valid-charge-profile battery-s2 charge-normal)
        (= (charge-power battery-s2 charge-normal) 1400) ;1.4kW instant power
        (= (charge-energy battery-s2 charge-normal) 1200)   ;20mAh per second

        (valid-charge-profile battery-s2 charge-fast)
        (= (charge-power battery-s2 charge-fast) 2700) ;2.7kW instant power
        (= (charge-energy battery-s2 charge-fast) 1800)   ;30mAh per second

        (valid-discharge-profile battery-s2 discharge-normal)
        (= (discharge-power battery-s2 discharge-normal) 1200) ;1.2kW instant power
        (= (discharge-energy battery-s2 discharge-normal) 1200)   ;20mAh per second

        (valid-discharge-profile battery-s2 discharge-fast)
        (= (discharge-power battery-s2 discharge-fast) 1800) ;1.8KW instant power
        (= (discharge-energy battery-s2 discharge-fast) 1800)   ;30mAh per second

        ;battery initialisation
        (not-in-use battery-s1)
        (not-in-use battery-s2)
        (available battery-s1)
        (available battery-s2)

        (= (energy battery-s1) 0)
        (= (energy battery-s2) 0)
        (= (max-energy battery-s1)  100000) ;in 100,000mAh
        (= (max-energy battery-s2)   80000) ;in  80,000mAh


        (= (total-cost) 0)

        ;marginal cost at each hour
        (= (unit-price) 0.30)
        (at 250 (= (unit-price) 0.05))  ;cheapest price during the night
        (at 420 (= (unit-price) 0.20))  ;price goes up at 8am
        (at 720 (= (unit-price) 0.50))  ;price goes a bit lower at noon
        (at 1080 (= (unit-price) 0.70)) ;price peaks at 6pm till 8pm
        (at 1200 (= (unit-price) 1.00)) ;price goes down a bit at 8pm till midnight
    )

    (:goal (and
              (completed wash-dishes-h1)
              (completed wash-dishes-h2)
              (completed wash-dishes-h3)
              (<= (total-cost) 165000)
        )
    )
)
