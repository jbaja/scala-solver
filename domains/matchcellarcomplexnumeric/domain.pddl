(define (domain matchCellarComplexNumeric)
     (:requirements :durative-actions :typing :fluents) 
     (:types fuse match lift electrician floor room - object)
     (:predicates
          (light ?room - room)
          (handfree ?elec - electrician)
          (mended ?fuse - fuse)
          (onfloor ?elec - electrician ?floor - floor)
          (inlift ?elec - electrician ?lift - lift)
          (roomonfloor ?room - room ?floor - floor)
          (liftonfloor ?lift - lift ?floor - floor)
          (inroom ?elec - electrician ?room - room)
          (fuseinroom ?fuse - fuse ?room - room)
          (connectedfloors ?floor1 ?floor2 - floor))
     (:functions
          (matchesleft))


     (:durative-action LIGHT-MATCH
          :parameters
               (?elec - electrician 
               ?room - room)
          :duration (= ?duration 8)
          :condition (and 
               (at start (> (matchesleft) 0))
               (over all (inroom ?elec ?room))
               (over all (light ?room)))
          :effect (and 
               (at start (decrease (matchesleft) 1))
               (at start (light ?room))
               (at end (not (light ?room)))))

