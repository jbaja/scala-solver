(define (domain test-simul)
(:requirements :durative-actions :negative-preconditions )
(:predicates 
	(p)
	(q)
	(r)
	(s)
	(executed_op1)
	(executed_op2)
)

(:durative-action op1
  :parameters ()
  :duration (= ?duration 1)
  :condition (and
 ;                 (at start (not(executed_op1)))
                  (at start (not (r)))
                  (at start (not (s)))
                  (over all (not (p)))
            ;      (over all (not (q)))
               )

  :effect (and
;               (at start (executed_op1))
               (at start (s))
               (at end (p)) )
          )

(:durative-action op2
  :parameters ()
  :duration (= ?duration 1)
  :condition (and
;                  (at start (not(executed_op2)))
                  (over all (not (p)))
                  (over all (not (q)))
             )

  :effect (and
  ;             (at start (executed_op2))
               (at start (not (r)))
               (at end (q)) )
          )

)
