(define (domain water)
    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :conditional-effects
    )

    (:types
        heater
    )

    (:predicates
         (in_use ?h - heater)
         (ambience_cooling)
    )

    (:functions
         (water-temperature)
         (heat-rate ?h - heater)
    )

    (:action heater-on
      :parameters (?h - heater)
      :precondition (and  (ambience_cooling)
                          (= (heat-rate ?h) 0))
      :effect (and (assign (heat-rate ?h) 10))
    )

    (:durative-action heat
      :parameters (?h - heater)
      :duration (and (<= ?duration 10) (>= ?duration 0) )
      :condition (and
                      (at start (> (heat-rate ?h) 0))
                      (at start (ambience_cooling))
                      (at start (not(in_use ?h)))
                      (over all (ambience_cooling))
                      (over all (< (water-temperature) 100)) ; we shouldn't reach boiling point
                 )
      :effect (and
                    (at start (in_use ?h))
                    (at end (not(in_use ?h)))
                    (increase (water-temperature) (* #t (heat-rate ?h)))
              )
    )

    (:durative-action cool
      :parameters ()
      :duration (and (<= ?duration 20) (>= ?duration 2) )
      :condition (and
                      (at start (not(ambience_cooling)))
                 )
      :effect    (and
                      (at start (ambience_cooling))
                      (at end (not(ambience_cooling)))
                      (decrease (water-temperature) (* #t 1))
                  )
    )
)



