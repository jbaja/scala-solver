(define (problem water-prob1)
  (:domain water)
  (:objects
    h1 - heater
  )

  (:init
       (= (heat-rate h1) 0)
       (= (water-temperature) 10)
       (not (ambience-cooling))
  )

  (:goal
      (and
            (> (water-temperature) 30)
           (<= (water-temperature) 50))
  )

)
