(define (problem CafeProblem1)
	(:domain CafeDomain)
	(:objects 
		table1 table2 table3 table4 table5 table6 table7 table8 table9 table10 table11 table12 table13 table14  - table
		tea1 tea2 tea3 tea4 tea5 tea6 tea7 tea8 tea9 tea10 tea11 tea12 tea13 tea14  - tea
		toast1 toast2 toast3 toast4 toast5 toast6 toast7 toast8 toast9 toast10 toast11 toast12 toast13 toast14  - toast
		socket1 - socket
		chef1 - chef
	)
	(:init
		(d_w_available table1)
		(d_w_available table2)
		(d_w_available table3)
		(d_w_available table4)
		(d_w_available table5)
		(d_w_available table6)
		(d_w_available table7)
		(d_w_available table8)
		(d_w_available table9)
		(d_w_available table10)
		(d_w_available table11)
		(d_w_available table12)
		(d_w_available table13)
		(d_w_available table14)
		(chef_free chef1)
		(socket_free socket1)
	)
	(:goal
		(and
		(delivered tea1 table1)
		(delivered tea2 table2)
		(delivered tea3 table3)
		(delivered tea4 table4)
		(delivered tea5 table5)
		(delivered tea6 table6)
		(delivered tea7 table7)
		(delivered tea8 table8)
		(delivered tea9 table9)
		(delivered tea10 table10)
		(delivered tea11 table11)
		(delivered tea12 table12)
		(delivered tea13 table13)
		(delivered tea14 table14)
		(delivered toast1 table1)
		(delivered toast2 table2)
		(delivered toast3 table3)
		(delivered toast4 table4)
		(delivered toast5 table5)
		(delivered toast6 table6)
		(delivered toast7 table7)
		(delivered toast8 table8)
		(delivered toast9 table9)
		(delivered toast10 table10)
		(delivered toast11 table11)
		(delivered toast12 table12)
		(delivered toast13 table13)
		(delivered toast14 table14)
		)
	)
(:metric minimize (total_delivery_window))
)
