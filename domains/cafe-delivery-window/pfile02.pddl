(define (problem CafeProblem1)
	(:domain CafeDomain)
	(:objects 
		table1 table2  - table
		tea1 tea2  - tea
		toast1 toast2  - toast
		socket1 - socket
		chef1 - chef
	)
	(:init
		(d_w_available table1)
		(d_w_available table2)
		(chef_free chef1)
		(socket_free socket1)
	)
	(:goal
		(and
		(delivered tea1 table1)
		(delivered tea2 table2)
		(delivered toast1 table1)
		(delivered toast2 table2)
		)
	)
(:metric minimize (total_delivery_window))
)
