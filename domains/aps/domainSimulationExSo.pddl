(define (domain APS)
    (:requirements :strips :typing :fluents :universal-preconditions :disjunctive-preconditions :conditional-effects :duration-inequalities :durative-actions :timed-initial-literals :preferences :constraints)
    (:types 
   	shedding-load - load
	load - device 
	switch - line
	line - controller 
	p-gen - gen
	q-gen - gen
	slack-gen - p-gen
	slack-gen - q-gen 
	extern-gen - gen 
	intern-gen - gen
	curtail-gen - intern-gen	
	pv-gen - q-gen
	gen - device 
	bus - controller
	level - object		
	device - object
	tap - device
	capacitor - device
        sop - device
	ess - device	
	thermal-violation - object
	controller - object	
	)
    (:predicates (is-available ?t - tap) (is-before-end) (is-end) (S) (F) (Gd) (is-active ?d - device) (is-not-active ?c - capacitor) (is-not-increasing ?t - tap) (is-not-decreasing ?t - tap)
	(is-increasing ?t - tap) (is-decreasing ?t - tap) (can-increase ?t - tap) (can-decrease ?t)
	(is-not-curtail ?g - curtail-gen) (is-connected ?d - device ?l - level)	
	(precondition-violation ?l - line ?tv - thermal-violation)
	(precondition-release-violation ?l - line ?tv - thermal-violation)
	(has-violation ?l - line ?tv - thermal-violation)
	(is-not-charging ?e - ess)
	(can-shed ?l - load)
	)

    (:functions
	(system-voltage)
	(action-voltage)
	(p-level ?l - load)
	(q-level ?l - load)
	(p-shedding ?l - shedding-load)
	(q-shedding ?l - shedding-load)
	(p-shedding-level ?l - shedding-load)
	(q-shedding-level ?l - shedding-load)	
	(p-minimum-shedding ?l - shedding-load)
	(tap-level ?t - tap)
	(tap-step ?t - tap)	
	(switch-level ?s - switch)
 	(ipsa-voltage ?b - bus)
 	(ipsa-line-power ?l - line)
 	(rating-line ?l - line)
	(maximum-violation ?l - line ?tv - thermal-violation)
	(release-violation ?l - line ?tv - thermal-violation)
	(diff-rating-violation ?l - line ?tv - thermal-violation)
	(gen-p-level ?g - gen) 
	(gen-q-level ?g - gen)
	(ipsa-slack-p ?g - p-gen)
        (ipsa-slack-q ?g - q-gen)
	(p-maximum-gen ?g - gen)
	(p-minimum-gen ?g - gen)
	(q-maximum-gen ?g - gen)
	(q-minimum-gen ?g - gen)
        (can-import ?g - extern-gen)
	(cost-gen)
	(cost-slack)
	(cost-shedding)
	(total-cost-gen)
	(total-cost-slack)
	(total-cost-shedding)
	(step-min ?t - device ?b - controller)
	(step-max ?t - device ?b - controller)
	(heuristic-c ?t - device ?l - level ?b - controller) ; when chargin ess
	(heuristic-d ?t - device ?l - level ?b - controller) ; when dischargin ess
	(heuristic-fc ?t - device ?l - level ?b - controller); when ess is fully charged
	(heuristic-fd ?t - device ?l - level ?b - controller); when ess is fully discharged
	(heuristic-approximation-line ?l ?l2 - line ?tv - thermal-violation)	
	(capacitor-level ?c - capacitor)
	(ipsa-total-voltage)
	(maximum-voltage ?b - bus)
	(minimum-voltage ?b - bus)	
	(slack-period ?t - device)
	(maximum-outage ?t - shedding-load)
	(max-tap-level ?t - tap)
	(min-tap-level ?t - tap)
	(minimum-curtail ?g - curtail-gen)
	(p-curtailed ?g - curtail-gen)
	(q-curtailed ?g - curtail-gen)
	(p-step ?g - curtail-gen)
	(q-step ?g - curtail-gen)
	(is-curtail-active ?g - curtail-gen)	
	(p-level-sop ?l - level)	
	(q-level-sop ?l - level)
	(p-sop ?s - sop)
	(q-sop ?s - sop)	
	(minimum-sop ?s - sop)
	(charging-rate ?e - ess ?l - level)
	(p-level-ess ?l - level)
	(q-level-ess ?l - level)
	(p-ess ?e - ess)
	(q-ess ?e - ess)
	(charge ?e - ess)
	(soc-max ?e - ess)
	(soc-min ?e - ess)
     (can-meter)
	   (metered)
          (metering)
	)

    (:durative-action connect-discharging
      :parameters (?e - ess ?l - level)
      :duration (and (>= ?duration 0) (<= ?duration 100));;(/ (- (charge ?e) (soc-min ?e)) (charging-rate ?e ?l)))
      :condition (and
	    (over all (>= (charge ?e) (soc-min ?e )))
	     (at start (is-connected ?e ?l))
	    (at start (is-not-charging ?e))
	    (over all (metering))
	    )
	:effect (and 
	    (decrease (charge ?e) (* (charging-rate ?e ?l) #t))
	    (at start (not (is-not-charging ?e)))
	    (at end (is-not-charging ?e))
	    (at start (increase (action-voltage) 0.3))
	    (at end (decrease (action-voltage) 0.3))
	    )
	)

    
    (:durative-action connect-discharged
	:parameters (?e - ess ?l - level)
	:duration (and (>= ?duration 0) (<= ?duration 48))
	:condition (and 
	    (over all (<= (charge ?e) (soc-min ?e)))
	    (at start (is-connected ?e ?l))
	    (at start (is-not-charging ?e))
	    )
	:effect (and 
	    (at start (not (is-not-charging ?e)))
	    (at end (is-not-charging ?e))
	    (at start (decrease (q-ess ?e) (q-level-ess ?l)))
	    (at start (increase (action-voltage) 0.3))	    
	    (at end (decrease (action-voltage) 0.3))	    
	    (at end (increase (q-ess ?e) (q-level-ess ?l)))	    
	    )
	)	        
    
    (:durative-action constraint-check
        :parameters ()
	:duration (<= ?duration 1000)
	:condition (and
	    (at start (can-meter))
      (at start (not (metering)))
      (at start (not (metered)))
;	    (at start (not(F)))
;;	    (at start (S))
;	    (at end (F))
	    ;; voltage constraint
	    (over all (<= (+ (system-voltage) (action-voltage)) 2))

	    )
	:effect (and
	      (at start (metering))
        (at start (metered))
        (at end (not (metering)))

	    ;(forall (?l - line) (and
	    ;    (at start (decrease (ipsa-line-power ?l) 0))))
	    (at end (is-end))
	    ))


    )    

