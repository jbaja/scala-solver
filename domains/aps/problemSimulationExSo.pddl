;; simulation of 
(define (problem APS-problem)
    (:domain APS)
    (:objects
	ess1 ess2 ess3  - ess
	levelESS1000 levelESS200 levelESS2000 levelESS250 levelESS400 levelESS500  - level
	)

    (:init
	(S)
	(at 0.5 (not (S)))
	(at 24 (F))

	(= (system-voltage) 0.8)
	(= (action-voltage) 0)	


	(= (p-ess ess1) 0)
	(= (q-ess ess1) 0)
	(= (charge ess1) 4.000)
	(= (soc-max ess1) 7.600)
	(= (soc-min ess1) 0.800)
	(= (p-ess ess2) 0)
	(= (q-ess ess2) 0)
	(at 2 (is-not-charging ess2))
	(= (charge ess2) 50.000)
	(= (soc-max ess2) 95.000)
	(= (soc-min ess2) 10.000)
	(= (p-ess ess3) 0)
	(= (q-ess ess3) 0)
	(at 2 (is-not-charging ess3))
	(= (charge ess3) 10.000)
	(= (soc-max ess3) 19.000)
	(= (soc-min ess3) 2.000)
	(= (p-level-ess levelESS1000 ) 10.000)
	(= (q-level-ess levelESS1000 ) 8.000)
	(= (charging-rate ess2 levelESS1000 ) 9.000)
	(is-connected ess2 levelESS1000)
	(= (p-level-ess levelESS200 ) 2.000)
	(= (q-level-ess levelESS200 ) 1.600)
	(= (charging-rate ess1 levelESS200 ) 1.600)
	(is-connected ess1 levelESS200)
	(= (p-level-ess levelESS2000 ) 20.000)
	(= (q-level-ess levelESS2000 ) 16.000)
	(= (charging-rate ess2 levelESS2000 ) 18.000)
	(is-connected ess2 levelESS2000)
	(= (p-level-ess levelESS250 ) 2.500)
	(= (q-level-ess levelESS250 ) 2.000)
	(= (charging-rate ess3 levelESS250 ) 2.250)
	(is-connected ess3 levelESS250)
	(= (p-level-ess levelESS400 ) 4.000)
	(= (q-level-ess levelESS400 ) 3.200)
	(= (charging-rate ess1 levelESS400 ) 3.200)
	(is-connected ess1 levelESS400)
	(= (p-level-ess levelESS500 ) 5.000)
	(= (q-level-ess levelESS500 ) 4.000)
	(= (charging-rate ess3 levelESS500 ) 4.500)
	(is-connected ess3 levelESS500)
	(at 0.5 (= (system-voltage) 0.5))
	(at 2.5 (= (system-voltage) 1.8))
	(at 3 (= (system-voltage) 0.5))
	)

    (:goal (and (is-end) (< (charge ess2) 30)))
    ;(:goal (and (< (charge ess2) 30)))
    )
