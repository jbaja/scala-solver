
(define (problem function)
	(:domain Discrete)
	(:init
	(= (x) 0)
	(= (y) 0)	
	)
	(:goal (and (>= (y) 1)))
)
