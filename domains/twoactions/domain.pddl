(define (domain Discrete)
   (:requirements :strips :typing :fluents :duration-inequalities :durative-actions :timed-initial-literals)
   (:predicates (is-end) (S) (F) (Gd) (is-active))

   (:functions
	(x) ; set by timed initial literal
	(y) ; value choosen by actions
	(upper-bound)
	(lower-bound)
	(step)
   )
 
    (:action increasex
	:parameters()
	:precondition (and (< (x) 5))
	:effect (and (assign (x) 1))
	)
;    (:action increasey
;	:parameters()
;	:precondition (and (> (x) 0))
;	:effect (and (increase (y) (x)))
;	)

	    (:durative-action increasey-durative
	  :parameters()
	  :duration (< ?duration 1440)
  	:condition (and (at start (= (x) 0)))
  	:effect (and (increase (y) (* #t (x))))
  	)
)