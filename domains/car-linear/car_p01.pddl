(define (problem car_p01)
  (:domain car)
  (:init
            (stopped)
            (can-drive)
            (= (distance) 0.0)
            (= (velocity) 0.0)
            (= (driving-time) 0.0)
  )

  (:goal (and
              (> (distance) 70)
                (< (driving-time) 20)
         )
  )
)
