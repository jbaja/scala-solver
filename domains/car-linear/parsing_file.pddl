(define (domain car)
    (:requirements :strips :fluents :durative-actions :duration-inequalities :typing :negative-preconditions)

    (:predicates
        (stopped)
        (running)
    )

    (:functions
            (acceleration)
            (velocity)
            (distance)
            (driving-time)
    )

    (:durative-action drive
        :parameters ()
        :duration (>= ?duration 0)
        :condition (and
                            (at start (stopped))
                            (over all (running))
                            (over all (>= (velocity) 0))
                            (at end (running))
                            (at end (= (velocity) 0))
                       )
        :effect (and (at start (not (stopped)))
                     (at start (running))
                     (at end (not(running)))
                     (at end (stopped))
                     (at end (assign (acceleration)  0))
                     (increase (velocity) (* #t (acceleration)))
                     (increase (distance) (* #t (velocity)))
                     (increase (driving-time) (* #t 1))
                )
    )

    (:action accelerate
        :parameters ()
        :precondition (running)
        :effect (and (increase (acceleration) 1))
    )

    (:action decelerate
        :parameters ()
        :precondition (running)
        :effect (and (decrease (acceleration) 1))
    )
)
(define (problem car_p01)
  (:domain car)
  (:init
            (stopped)
            (= (distance) 0.0)
            (= (acceleration) 0.0)
            (= (velocity) 0.0)
            (= (driving-time) 0.0)
  )

  (:goal (and
              (> (distance) 30)       
         )
  )
)
