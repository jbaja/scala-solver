(define (domain car)
    (:requirements :strips :fluents :durative-actions :duration-inequalities :typing :negative-preconditions)

    (:predicates
        (stopped)
        (running)
        (can-drive)
    )

    (:functions
            (velocity)
            (distance)
            (driving-time)
    )

    (:durative-action drive
        :parameters ()
        :duration (>= ?duration 0)
        :condition (and
                            (at start (can-drive))
                            (at start (stopped))
                            (over all (running))
                            (over all (>= (velocity) 0))
                            (at end (running))
                            (at end (= (velocity) 0))
                       )
        :effect (and (at start (not (stopped)))
                     (at start (running))
                     (at end (not(running)))
                     (at end (stopped))
                     (at end (not(can-drive)))
                     (increase (distance) (* #t (velocity)))
                     (increase (driving-time) (* #t 1))
                )
    )

    (:action accelerate
        :parameters ()
        :precondition (running)
        :effect (and (increase (velocity) 1))
    )

    (:action decelerate
        :parameters ()
        :precondition (running)
        :effect (and (decrease (velocity) 1))
    )
)
