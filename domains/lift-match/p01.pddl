(define (problem matchliftproblem01)
  (:domain matchlift)
  (:objects match1 match2 - match
            fuse1 fuse2 - fuse
            lift1 - lift
            elec1 elec2 - electrician
            floor1 floor2 - floor
            room1a room1b room2a room2b - room)
  (:init
            (unused match1)
            (unused match2)
            (handfree elec1)
            (handfree elec2)
            (onfloor elec1 floor1)
            (onfloor elec2 floor1)
            (roomonfloor room1a floor1)
            (roomonfloor room1b floor1)
            (roomonfloor room2a floor2)
            (roomonfloor room2b floor2)
            (liftonfloor lift1 floor1)
            (fuseinroom fuse1 room1a)
            (fuseinroom fuse2 room2b)
            (connectedfloors floor1 floor2)
            (connectedfloors floor2 floor1))

  (:goal (and (mended fuse1) (mended fuse2)))
  (:metric minimize (total-time)))