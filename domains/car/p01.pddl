(define (problem car_p01)
  (:domain car)
  (:init
            (stopped)
            (= (distance) 0.0)
            (= (acceleration) 0.0)
            (= (velocity) 0.0)
            (= (driving-time) 0.0)
  )

  (:goal (and
              (> (distance) 10)
              (< (driving-time) 1000)
         )
  )
)