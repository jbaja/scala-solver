(define (domain car)
    (:requirements :durative-actions :typing)

    (:predicates
        (stopped)
        (running)
    )

    (:functions
            (acceleration)
            (velocity)
            (distance)
            (driving-time)
    )

    (:durative-action drive
        :duration (>= ?duration 0)
        :condition (and
                            (at start (stopped))
                            (over all (running))
                            (over all (>= (velocity) 0))
                            (at end (running))
                            (at end (= (velocity) 0))
                       )
        :effect (and (at start (not (stopped)))
                     (at start (running))
                     (at end (not(running)))
                     (at end (stopped))
                     (at end (assign (acceleration)  0))
                     (increase (velocity) (* #t (acceleration)))
                     (increase (distance) (* #t (velocity)))
                     (increase (driving-time) (* #t 1))
                )
    )


    (:action accelerate
        :precondition (running)
        :effect (increase (acceleration) 1)
    )

    (:action decelerate
        :precondition (running)
        :effect (decrease (acceleration) 1)
    )
)