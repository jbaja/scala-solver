(define (domain piecewise)

  (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :conditional-effects
    )
        (:predicates
            (increasing)
            (not-increasing)
        )
     ;numeric stuff
        (:functions

           (rate) ;rate of change
           (level)       ;flexible + charging - discharging
        )

      (:durative-action increaseLevel
          :parameters()
          :duration (< ?duration 1440)
          :condition (and (at start (not-increasing)))
          :effect (and
                    (at start (not(not-increasing)))

                    (increase (level) (* #t (rate)))
                    (at end (not(not-notincreasing)))
                  )
        )

)