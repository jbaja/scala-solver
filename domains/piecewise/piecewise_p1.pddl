(define (problem aggregator3-prob1)
(:domain aggregator)

(:init
     (not-increasing)
     (= (level) 0)
     (= (rate) 10)
     (at 15 (= (rate) 25))
)

(:goal (and (>= (level) 200)))
)
