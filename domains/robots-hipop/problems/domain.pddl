
(define (domain survivor)
  (:requirements :strips :typing :durative-actions)
  (:types robot survivor loc zone team)

  (:predicates
    (explored ?z - loc)
    (at-r ?r - robot ?z - loc)
    (at-s ?s - survivor ?z - loc)
    (stabilized ?s - survivor)
    (hospitalized ?s - survivor)
    (belong ?l - loc ?z - zone)
    (adjacent ?l1 ?l2 - loc)
    (part-of ?r - robot ?t - team)
    (different ?r1 ?r2 - robot)
    (at-team ?t - team ?z - zone)
    (hospital ?l - loc)
  )

  (:functions
    (distance ?from ?to - loc)
    (distance-zone ?from ?to - zone)
    (total-distance)
  )

  (:durative-action move
    :parameters (?r - robot ?from ?to - loc)
    :duration (= ?duration (distance ?from ?to))
    :condition (and (over all (at-r ?r ?from)) )
    :effect (and (at end (at-r ?r ?to))
                 (at end (not (at-r ?r ?from)))
                 (at end (increase total-distance (distance ?from ?to))))
                 )

  (:durative-action explore
    :parameters (?r - robot ?l - loc)
    :duration (= ?duration 1)
    :condition (and (over all (at-r ?r ?l)) )
    :effect (and (at end (explored ?l)) ))

  (:durative-action move-team
    :parameters (?team - team ?from ?to - zone)
    :duration (= ?duration (distance-zone ?from ?to))
    :condition (and (over all (at-team ?team ?from)) )
    :effect (and (at end (not (at-team ?team ?from))) (at end (at-team ?team ?to)) )
  )
  
  (:durative-action stabilize
    :parameters (?r - robot ?s - survivor ?l - loc)
    :duration (= ?duration 1)
    :condition (and (over all (at-r ?r ?l)) (over all (at-s ?s ?l)) )
    :effect (and (at end (stabilized ?s)) )
  )
  
  (:durative-action hospitalize
    :parameters (?s - survivor ?l - loc)
    :duration (= ?duration 1)
    :condition (and (over all (stabilized ?s)) (over all (hospital ?l)) (over all (at-s ?s ?l)) )
    :effect (and (at end (hospitalized ?s)) )
  )
  
  (:durative-action move-survivor
    :parameters (?r1 ?r2 - robot ?s - survivor ?from ?to - loc)
    :duration (= ?duration (distance ?from ?to))
    :condition (and (over all (stabilized ?s)) (over all (at-r ?r1 ?from)) (over all (at-r ?r2 ?from)) (over all (at-s ?s ?from)) (over all (different ?r1 ?r2)))
    :effect (and (at end (at-r ?r1 ?to)) (at end (at-r ?r2 ?to)) (at end (at-s ?s ?to))
                  (at end (not (at-r ?r1 ?from))) (at end (not (at-r ?r2 ?from))) (at end (not (at-s ?s ?from)))
                  (at end (increase total-distance (distance ?from ?to))))
  )
)