(define (domain dsm-aggregator)
    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :conditional-effects
    )

    (:types gradient activity profile battery)

    (:predicates
        (can-start-metering)
        (metered)
        (metering)

        (activity-profile ?a - activity ?p - profile)
        (can-perform ?a - activity)
        (performed ?a - activity)

        (charge-profile ?b - battery ?p - profile)
        (discharge-profile ?b - battery ?p - profile)
        (available ?b - battery)
        (in-use ?b - battery)
    )

    (:functions
       (inflexible-load) ;absolute value at last change of gradient
       (inflexible-load-dt) ;gradient of inflexible load

       (flexible-load) ;flexible load
       (power-needed ?a - activity ?p - profile)
       (duration-needed ?a - activity ?p - profile)

       (capacity ?b - battery)
       (state-of-charge ?b - battery)
       (charge-power ?b - battery ?p - profile)
       (discharge-power ?b - battery ?p - profile)
       (charge-rate ?b - battery ?p - profile)
       (discharge-rate ?b - battery ?p - profile)

       (unit-price)
       (total-cost)
    )

    ;envelope action to perform continuous metering
    (:durative-action meter
      :parameters()
      :duration (>= ?duration 0)
      :condition (and (at start (can-start-metering))
                      (at start (not (metering)))
                      (at start (not (metered))))
      :effect (and (at start (metering))
                   (at start (metered))
                   (at end (not (metering)))
                   (increase (inflexible-load) (* #t (inflexible-load-dt)))
;                   (increase (total-cost) (* #t (* (unit-price)
;                                                   (+ (flexible-load) (inflexible-load)))))
                    )
                    )


;
     (:durative-action perform
       :parameters (?a - activity ?p - profile)
       :duration (= ?duration (duration-needed ?a ?p))
       :condition (and
         (at start (not(performed ?a)))
         (at start (activity-profile ?a ?p))
         (over all (can-perform ?a))
         (over all (metering)))
       :effect (and
         (at start (increase (flexible-load) (power-needed ?a ?p)))
         (at end (decrease (flexible-load) (power-needed ?a ?p)))
         (at end (performed ?a))))


    (:durative-action charge
      :parameters (?b - battery ?p - profile)
      :duration (>= ?duration 0)
      :condition (and
        (at start (not (in-use ?b)))
        (at start (charge-profile ?b ?p))
        (over all (<= (state-of-charge ?b) (capacity ?b)))
        (over all (available ?b))
        (over all (metering))
       )
      :effect (and
        (at start (in-use ?b))
        (at start (increase (flexible-load) (charge-power ?b ?p)))
        (at end (decrease (flexible-load) (charge-power ?b ?p)))
        (at end (not (in-use ?b)))
        (increase (state-of-charge ?b) (* #t (charge-rate ?b ?p)))))

    (:durative-action discharge
      :parameters (?b - battery ?p - profile)
      :duration (>= ?duration 0)
      :condition (and
        (at start (not (in-use ?b)))
        (at start (discharge-profile ?b ?p))
        (over all (>= (state-of-charge ?b) 0))
        (over all (available ?b))
        (over all (metering))
       )
      :effect (and
        (at start (in-use ?b))
        (at start (decrease (flexible-load) (discharge-power ?b ?p)))
        (at end (increase (flexible-load) (discharge-power ?b ?p)))
        (at end (not (in-use ?b)))
        (decrease (state-of-charge ?b) (* #t (discharge-rate ?b ?p)))))
)
