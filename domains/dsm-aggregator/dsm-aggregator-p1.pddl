(define (problem aggregator-p1)
     (:domain aggregator)

    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :conditional-effects
    )

     (:objects g1 g2 - gradient
               charge-ev1-h1 charge-ev1-h2 charge-ev1-h3 - activity
               normal - profile
               b1 - battery
     )

     (:init
            (= (current-time) 0)
            (= (inflexible-load) 4000)
            (= (last-gradient-change-time) 0)
            (= (inflexible-load-dt) -6666.667)
            (= (flexible-load) 0)
            (= (total-cost) 0)
            (= (unit-price) 10)

            (= (rate-of-change g1) 1333.333)
            (= (rate-of-change g2) 0)

            (= (state-of-charge b1) 8000)
            (= (capacity b1) 10000)
            (available b1)

            (charge-profile b1 normal)
            (discharge-profile b1 normal)
            (= (charge-power b1 normal) 2000)
            (= (discharge-power b1 normal) 1900)
            (= (charge-rate b1 normal) 200)
            (= (discharge-rate b1 normal) 240)

            ;clip for timer envelope action
            (at 0 (can-start-timer))
            (at 0.001 (not (can-start-timer)))

            ;clip for the first change in gradient
            (at 14.999 (can-change-rate g1))
            (at 15.001 (not (can-change-rate g1)))

            ;clip for the second change in gradient
            (at 29.999 (can-change-rate g2))
            (at 30.001 (not (can-change-rate g2)))

            (activity-profile charge-ev1-h1 normal)
            (= (power-needed charge-ev1-h1 normal) 3300)
            (= (duration-needed charge-ev1-h1 normal) 420)

            (at 120 (can-perform charge-ev1-h1))
            (at 600 (not(can-perform charge-ev1-h1)))

            (activity-profile charge-ev1-h2 normal)
            (= (power-needed charge-ev1-h2 normal) 3300)
            (= (duration-needed charge-ev1-h2 normal) 420)

            (at 300 (can-perform charge-ev1-h2))
            (at 900 (not(can-perform charge-ev1-h2)))

      ;      (at 60 (= (unit-price) 12))
      ;      (at 120 (= (unit-price) 8))
      ;      (at 180 (= (unit-price) 5))
     )

 (:goal (and
             (changed-rate g1) (changed-rate g2)
             (performed charge-ev1-h1)
             (performed charge-ev1-h2)
             (<= (state-of-charge b1) 200)
             ))
)
