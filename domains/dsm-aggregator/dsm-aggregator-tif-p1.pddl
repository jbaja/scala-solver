(define (problem aggregator-p1)
     (:domain aggregator)

    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :conditional-effects
    )

     (:objects charge-ev1-h1 charge-ev1-h2 charge-ev1-h3 - activity
               normal - profile
               b1 - battery
     )

     (:init
            (= (inflexible-load) 40000)
            (= (inflexible-load-dt) -666.667)

            (= (flexible-load) 0)
            (= (total-cost) 0)
            (= (flexible-cost) 0)
            (= (unit-price) 10)

            (= (state-of-charge b1) 8000)
            (= (capacity b1) 10000)
            (available b1)

            (charge-profile b1 normal)
            (discharge-profile b1 normal)
            (= (charge-power b1 normal) 2000)
            (= (discharge-power b1 normal) 1900)
            (= (charge-rate b1 normal) 200)
            (= (discharge-rate b1 normal) 240)

            ;clip for timer envelope action
            (at 0 (can-start-metering))
            (at 0.001 (not (can-start-metering)))

            (at 15 (= (inflexible-load-dt) 133.333))
            (at 30 (= (inflexible-load-dt) 200))

            (activity-profile charge-ev1-h1 normal)
            (= (power-needed charge-ev1-h1 normal) 3300)
            (= (duration-needed charge-ev1-h1 normal) 420)

            (at 120 (can-perform charge-ev1-h1))
            (at 600 (not(can-perform charge-ev1-h1)))

            (activity-profile charge-ev1-h2 normal)
            (= (power-needed charge-ev1-h2 normal) 3300)
            (= (duration-needed charge-ev1-h2 normal) 420)

            (at 300 (can-perform charge-ev1-h2))
            (at 900 (not(can-perform charge-ev1-h2)))

      ;      (at 60 (= (unit-price) 12))
      ;      (at 120 (= (unit-price) 8))
      ;      (at 180 (= (unit-price) 5))
     )

 (:goal (and
             (performed charge-ev1-h1)
             (performed charge-ev1-h2)
             (<= (state-of-charge b1) 200)
             ))
)
