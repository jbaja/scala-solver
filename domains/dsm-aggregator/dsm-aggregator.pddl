(define (domain dsm-aggregator)
    (:requirements
         :strips
         :typing
         :fluents
         :durative-actions
         :timed-initial-literals
         :negative-preconditions
         :duration-inequalities
         :conditional-effects
    )

    (:types gradient activity profile battery)

    (:predicates
        (can-start-timer)
        (timer-used)
        (timing)
        (can-change-rate ?g - gradient)
        (changed-rate ?g - gradient)

        (activity-profile ?a - activity ?p - profile)
        (can-perform ?a - activity)
        (performed ?a - activity)

        (charge-profile ?b - battery ?p - profile)
        (discharge-profile ?b - battery ?p - profile)
        (available ?b - battery)
        (in-use ?b - battery)
    )

    (:functions
       (inflexible-load) ;absolute value at last change of gradient
       (inflexible-load-dt) ;gradient of inflexible load
       (last-gradient-change-time) ;time of last change
       (current-time) ;current time since starting the plan
       (rate-of-change ?g - gradient) ;rate of change definition

       (flexible-load) ;flexible load
       (power-needed ?a - activity ?p - profile)
       (duration-needed ?a - activity ?p - profile)

       (capacity ?b - battery)
       (state-of-charge ?b - battery)
       (charge-power ?b - battery ?p - profile)
       (discharge-power ?b - battery ?p - profile)
       (charge-rate ?b - battery ?p - profile)
       (discharge-rate ?b - battery ?p - profile)

       (unit-price)
       (total-cost)
    )

    ;update inflexible load
    (:action updateInflexibleLoad
      :parameters (?g - gradient)
      :precondition (and (timing)
                         (can-change-rate ?g)
                         (not(changed-rate ?g))
                    )

      :effect (and (increase (inflexible-load)
                             (* (- (current-time) (last-gradient-change-time))
                                (inflexible-load-dt)))
                   (assign (last-gradient-change-time) (current-time))
                   (assign (inflexible-load-dt) (rate-of-change ?g))
                   (changed-rate ?g))
    )

    ;envelope action to keep track of the time
    (:durative-action timer
        :parameters()
        :duration (and (>= ?duration 0) (<= ?duration 1440))
        :condition (and (at start (can-start-timer))
                        (at start (not (timing)))
                        (at start (not (timer-used))))
        :effect (and (at start (timing))
                     (at start (timer-used))
                     (at end (not (timing)))
                     (increase (current-time) #t)
                     (increase (total-cost)
                       (* #t (* (unit-price)
                                (+ (flexible-load) (inflexible-load))))
;                                (* (- (current-time) (last-gradient-change-time))
;                                   (inflexible-load-dt)))
                     )
                )

     )

;
     (:durative-action perform
       :parameters (?a - activity ?p - profile)
       :duration (= ?duration (duration-needed ?a ?p))
       :condition (and
         (at start (not(performed ?a)))
         (at start (activity-profile ?a ?p))
         (over all (can-perform ?a))
         (over all (timing)))
       :effect (and
         (at start (increase (flexible-load) (power-needed ?a ?p)))
         (at end (decrease (flexible-load) (power-needed ?a ?p)))
         (at end (performed ?a))))

    (:durative-action charge
      :parameters (?b - battery ?p - profile)
      :duration (and (>= ?duration 0) (<= ?duration 1440))
      :condition (and
        (at start (not (in-use ?b)))
        (at start (charge-profile ?b ?p))
        (over all (<= (state-of-charge ?b) (capacity ?b)))
        (over all (available ?b))
        (over all (timing))
       )
      :effect (and
        (at start (in-use ?b))
        (at start (increase (flexible-load) (charge-power ?b ?p)))
        (at end (decrease (flexible-load) (charge-power ?b ?p)))
        (at end (not (in-use ?b)))
        (increase (state-of-charge ?b) (* #t (charge-rate ?b ?p)))))

    (:durative-action discharge
      :parameters (?b - battery ?p - profile)
      :duration (and (>= ?duration 0) (<= ?duration 1440))
      :condition (and
        (at start (not (in-use ?b)))
        (at start (discharge-profile ?b ?p))
        (over all (>= (state-of-charge ?b) 0))
        (over all (available ?b))
        (over all (timing))
       )
      :effect (and
        (at start (in-use ?b))
        (at start (decrease (flexible-load) (discharge-power ?b ?p)))
        (at end (increase (flexible-load) (discharge-power ?b ?p)))
        (at end (not (in-use ?b)))
        (decrease (state-of-charge ?b) (* #t (discharge-rate ?b ?p)))))
)