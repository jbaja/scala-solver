(define (domain vehicle_linear)

    (:requirements :typing :durative-actions :fluents :duration-inequalities)

    (:types
        Car
    )

    (:predicates
        (running ?c - Car)
        (driven ?c - Car)
    )

    (:functions
            (velocity ?c - Car)
            (distance-traveled ?c - Car)
            (driving-time ?c - Car)
    )

  (:durative-action drive
        :parameters (?c - Car)
        :duration (> ?duration 0)
        :condition (and
                     ;       (at start (not(driven ?c)))
                            (at start (not(running ?c)))
                            (over all (running ?c))
                            (over all (>= (velocity ?c) 0))
                     ;       (over all (<= (Vehicle.velocity ?c) 100))
                            (over all (>= (distance-traveled ?c) 0))
                            (at end (running ?c))
                            (at end (<= (velocity ?c) 0))
                       )
        :effect (and
                     (at start (driven ?c))
                     (at start (running ?c))
                     (at start (increase (velocity ?c) 1))
                     (at end (not(running ?c)))
                     (at end (assign (velocity ?c) 0))
                     (increase (distance-traveled ?c) (* #t (velocity ?c)))
                     (increase (driving-time ?c) (* #t 1))
                )
    )


    (:action accelerate
        :parameters (?c - Car)
        :precondition (and (running ?c))
        :effect (increase (velocity ?c) 1)
    )

    (:action decelerate
        :parameters (?c - Car)
        :precondition (and (running ?c))
        :effect (decrease (velocity ?c) 1)
    )
)