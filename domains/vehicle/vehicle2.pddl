(define (domain vehicle)

    (:requirements :typing :durative-actions :fluents :duration-inequalities)

    (:types
        Vehicle.Car
    )

    (:predicates
        (Vehicle.running ?c - Vehicle.Car)
        (driven ?c - Vehicle.Car)
    )

    (:functions
            (Vehicle.acceleration ?c - Vehicle.Car)
            (Vehicle.velocity ?c - Vehicle.Car)
            (Vehicle.distance-traveled ?c - Vehicle.Car)
            (Vehicle.driving-time ?c - Vehicle.Car)
    )

  (:durative-action drive
        :parameters (?c - Vehicle.Car)
        :duration (and (> ?duration 0) (< ?duration 2000))
        :condition (and
                            (at start (not(driven ?c)))
                            (at start (not(Vehicle.running ?c)))
                            (over all (Vehicle.running ?c))
                            (over all (>= (Vehicle.velocity ?c) 0))
                     ;       (over all (<= (Vehicle.velocity ?c) 100))
                            (over all (>= (Vehicle.distance-traveled ?c) 0))
                            (at end (Vehicle.running ?c))
                            (at end (<= (Vehicle.velocity ?c) 0))
                       )
        :effect (and
                     (at start (driven ?c))
                     (at start (Vehicle.running ?c))
                     (at end (not(Vehicle.running ?c)))
                     (at end (assign (Vehicle.acceleration ?c)  0))
                     (at end (assign (Vehicle.velocity ?c) 0))
                     (increase (Vehicle.velocity ?c) (* #t (Vehicle.accelerate ?c)))
                     (increase (Vehicle.distance-traveled ?c) (* #t (Vehicle.travel-rate ?c)))
                     (increase (Vehicle.driving-time ?c) (* #t 1))
                )
    )

    (:action accelerate
        :parameters (?c - Vehicle.Car)
        :precondition (Vehicle.running ?c)
        :effect (increase (Vehicle.acceleration ?c) 1)
    )

    (:action decelerate
        :parameters (?c - Vehicle.Car)
        :precondition (and (Vehicle.running ?c))
        :effect (decrease (Vehicle.acceleration ?c) 1)
    )
)