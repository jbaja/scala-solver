(define (problem vehicle_linear_p01)
  (:domain vehicle_linear)
  (:objects car1 - Car
            )

  (:init
          (= (distance-traveled car1) 0)
          (= (velocity car1) 0)
          (= (driving-time car1) 0)

  )

  (:goal (and
              (>= (distance-traveled car1) 12)
          ;    (<= (driving-time car1) 10)
         )
  )
)