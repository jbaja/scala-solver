(define (problem vehicle_p01)
  (:domain vehicle)
  (:objects car1 - Vehicle.Car
            )

  (:init
          (= (Vehicle.distance-traveled car1) 0)
          (= (Vehicle.acceleration car1) 0)
          (= (Vehicle.velocity car1) 0)
          (= (Vehicle.driving-time car1) 0)

  )

  (:goal (and (driven car1)
              (> (Vehicle.distance-traveled car1) 30)
       ;       (<= (Vehicle.driving-time) 3000)
         )
  )
)